<?php

return [
    'policy.list.error' => 'Your policy not found.',
    'policy.list.success' => 'ระบบจะแสดงความคุ้มครองของคุณในกรณีหมายเลขโทรศัพท์ที่ลงทะเบียนตรงกับฐานข้อมูลของระบบ (EN)',
    'success' => 'Your information has been recorded.'
];
