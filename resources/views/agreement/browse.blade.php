<!-- First you need to extend the CB layout -->
@extends('crudbooster::themes.adminlte.layout.layout')
@section('content')
<!-- Your custom  HTML goes here -->
<style>
    table#table-module th {
        text-align: center;
    }

    table#table-module td:first-child {
        width: 60px;
        text-align: center;
    }

    table#table-module td:nth-child(2) {
        text-align: left;
    }

    table#table-module td:nth-child(3), table#table-module td:nth-child(4) {
        width: 160px;
        text-align: center;
    }

    table#table-module td:last-child {
        width: 60px;
        text-align: center;
    }

</style>

<div class="box">
    <div class="box-header">

        <h1 class="box-title" style="font-size: 20px;margin-top:5px">ข้อตกลงและข้อกำหนด</h1>



    </div>

    <div class="box-body table-responsive">

        <table id="table-module" class="table table-hover table-striped table-bordered">
            <thead>
                <tr>
                    <th>ลำดับ</th>
                    <th>ชื่อเนื้อหา</th>
                    <th>ยอมรับข้อตกลง</th>
                    <th>แก้ไขล่าสุด</th>
                    <th>จัดการ</th>
                </tr>
            </thead>
            <tbody>
                @if(count($record) > 0)

                @foreach($record as $row)
                <tr>
                    <td>{{$row->no}}</td>
                    <td>{{$row->title}}</td>
                    <td>{{$row->updated_text}}</td>
                    <td>{{$row->updated_at}}</td>
                    <td>
                        <!-- To make sure we have read access, wee need to validate the privilege -->
                        @if(module()->canUpdate())
                        <a class='btn btn-warning btn-xs' href='{{url("admin/agreement/edit/$row->id")}}'><i
                                class="fa fa-pencil-square-o" aria-hidden="true"></i></a>
                        @endif
                    </td>
                </tr>
                @endforeach
                @else
                <tr class="warning">
                    <td colspan="4" style="text-align: center">
                        <i class="fa fa-table"></i> There is no data yet
                    </td>
                </tr>
                @endif
            </tbody>
        </table>

    </div>

</div>

@endsection
