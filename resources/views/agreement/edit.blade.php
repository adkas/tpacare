<!-- First you need to extend the CB layout -->
@extends('crudbooster::themes.adminlte.layout.layout')
@section('content')
<!-- Your custom  HTML goes here -->
<style>
   
    span.error {
        color: red;
        font-style: italic
    }

    div.error {
        display: none
    }

    input {
        border: 1px solid #000
    }

    input.checkbox {
        border: none
    }

    input:focus {
        border: 1px solid #000
    }

    input.error {
        border: 1px solid red
    }

    .error .select2-choice.select2-default,
    .error .select2-choices {
        color: #a94442;
        border-color: #a94442;
        -webkit-box-shadow: inset 0 1px 1px rgba(0, 0, 0, .075);
        box-shadow: inset 0 1px 1px rgba(0, 0, 0, .075);
    }

    .error:focus,
    .error .select2-choice.select2-defaultLfocus,
    .error .select2-choicesLfocus {
        border-color: #843534;
        -webkit-box-shadow: inset 0 1px 1px rgba(0, 0, 0, .075), 0 0 6px #ce8483;
        box-shadow: inset 0 1px 1px rgba(0, 0, 0, .075), 0 0 6px #ce8483;
    }

    .select2-container .select2-choices .select2-search-field input,
    .select2-container .select2-choice,
    .select2-container .select2-choices,
    .error {
        border-radius: 1px;
    }

</style>

<div class="box box-warning">
    <div class="box-header with-border">
    <h1 class="box-title" style="font-size: 20px;margin-top:5px">แก้ไข{{$row->title}}</h1>

    </div>
    <form method="POST" enctype="multipart/form-data" id="WebForm" accept-charset="utf-8">
        <input type="hidden" name="agreement_id" value="{{$row->agreement_id}}">
        <div class="box-body">

            <div class="form-group ">
                <label>เนื้อหาภาษาไทย</label>
                <div class="row">
                    <div class="col-sm-12">
                        <textarea name="agreement_th" id="agreement_th" class="summernote">{{$row->agreement_th}}</textarea>
                    </div>
                </div>
            </div>

            <div class="form-group ">
                <label>เนื้อหาภาษาอังกฤษ</label>
                <div class="row">
                    <div class="col-sm-12">
                        <textarea name="agreement_en" id="agreement_en" class="summernote">{{$row->agreement_en}}</textarea>
                    </div>
                </div>
            </div>


            <div class="row">
                <div class="col-md-2">
                    <div class="form-group">
                        <label>การยอมรับข้อตกลง</label>
                        <select class="form-control force" name="agreement_force">
                            <option value="1">บังคับ</option>
                            <option value="0">ไม่บังคับ</option>
                            <option value="2" selected>อัพเดทข้อมูล</option>
                        </select>
                    </div>
                </div>
            </div>



        </div>

        <div class="box-footer">
            <div style="text-align: right">
                <a href="{{url('admin/agreement')}}" class="btn btn-default"> ยกเลิก </a>
                <input type="submit" name="submit" value="บันทึก" class="btn btn-success">

            </div>
        </div>

    </form>

</div>


@endsection
