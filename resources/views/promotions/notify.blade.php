<!-- First you need to extend the CB layout -->
@extends('crudbooster::themes.adminlte.layout.layout')
@section('content')
<!-- Your custom  HTML goes here -->
<style>

    textarea.form-control.upostTextarea {
        border: 0;
        box-shadow: none;
        height: 85px;
        resize: none
    }

    .user-post-text-wrap {
        border-bottom: 1px solid #ccc;
        margin: 0 0 15px
    }

    .user-post-btn-wrap {
        margin: 25px 0 0
    }

    ul.btn-nav {
        list-style: none;
        padding: 0;
        margin: 0
    }

    ul.btn-nav li {
        position: relative;
        float: left;
        margin: 0 10px 0 0
    }

    ul.btn-nav li span input {
        position: absolute;
        left: 0;
        top: 0;
        height: 100%;
        width: 100%;
        z-index: 9;
        opacity: 0;
        filter: alpha(opacity=100)
    }

    ul#media-list {
        list-style: none;
        padding: 0;
        margin: 0
    }

    span.error {
        color: red;
        font-style: italic
    }

    div.error {
        display: none
    }

    input {
        border: 1px solid #000
    }

    input.checkbox {
        border: none
    }

    input:focus {
        border: 1px solid #000
    }

    input.error {
        border: 1px solid red
    }

</style>

<div class="box box-success">
    <div class="box-header with-border">
        <h1 class="box-title" style="font-size: 20px;margin-top:5px">แจ้งเตือนโปรโมชั่น</h1>

    </div>
    <form method="POST" enctype="multipart/form-data" id="WebForm" accept-charset="utf-8">
        <input type="hidden" name="notification_type" value="promotion">
        <input type="hidden" name="id" value="{{$promotion->id}}">
        <div class="box-body">
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group ">
                        <label>หัวข้อภาษาไทย</label>
                        <input type="text" placeholder="หัวข้อภาษาไทย" class="form-control" name="title_th"
                            value="{{$promotion->title_th}}">
                    </div>
                </div>

                <div class="col-md-6">
                    <div class="form-group ">
                        <label>หัวข้อภาษาอังกฤษ</label>
                        <input type="text" placeholder="หัวข้อภาษาอังกฤษ" class="form-control" name="title_en"
                            value="{{$promotion->title_en}}">
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-6">
                    <div class="form-group ">
                        <label>รายละเอียดภาษาไทย</label>
                        <textarea name="body_th" id="body_th"
                            class="form-control" style="height: 150px;" placeholder="รายละเอียดภาษาไทย">{{$promotion->detail_th}}</textarea>
                    </div>
                </div>

                <div class="col-md-6">
                    <div class="form-group ">
                        <label>รายละเอียดภาษาอังกฤษ</label>
                        <textarea name="body_en" id="body_en"
                            class="form-control" style="height: 150px;" placeholder="รายละเอียดภาษาอังกฤษ">{{$promotion->detail_en}}</textarea>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-3">
                    <div class="form-group">
                        <label>ช่วงอายุ</label>
                        <select class="form-control status" name="age_range">
                            @foreach ($age_range as $row)
                            <option value="{{$row->id}}" {{$row->selected}}>{{$row->name}}</option>
                            @endforeach
                        </select>
                    </div>
                </div>

                <div class="col-md-3">
                    <div class="form-group">
                        <label>เพศ</label>
                        <select class="form-control status" name="gender">
                            @foreach ($gender as $row)
                            <option value="{{$row->id}}" {{$row->selected}}>{{$row->name}}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
            </div>



        </div>

        <div class="box-footer">
            <div style="text-align: right">
                <a href="{{url('admin/promotions')}}" class="btn btn-default"> ยกเลิก </a>
                <input type="submit" name="submit" value="ส่ง" class="btn btn-success">

            </div>
        </div>

    </form>

</div>

@endsection
