<!-- First you need to extend the CB layout -->
@extends('crudbooster::themes.adminlte.layout.layout')
@section('content')
<!-- Your custom  HTML goes here -->
<style>
    #media-list li img {
        width: 180px;
        /* height: 100px */
        height: auto;
    }

    #media-list li {
        min-height: 182px;
        height: auto;
    }


    div#hint_brand .modal-dialog {
        top: 110px;
        width: 567px;
        max-width: 100%
    }

    li.myupload span {
        position: relative;
        width: 180px;
        min-height: 180px;
        height: auto;
        display: block;
        background: #fff
    }

    li.myupload span input {
        opacity: 0;
        position: absolute;
        top: 0;
        bottom: 0;
        left: 0;
        right: 0
    }

    li.myupload span i {
        position: absolute;
        top: 50%;
        left: 50%;
        transform: translate(-50%, -50%);
        color: #ccc;
        font-size: 54px
    }

    #media-list li {
        float: left;
        border: 1px solid #ccc;
        background: #ccc;
        position: relative;
        margin: 0 5px 5px 0;
        width: 182px
    }

    #media-list li:last-child {
        margin-right: 0
    }

    .post-thumb {
        position: absolute;
        background: rgba(0, 0, 0, 0.4);
        left: 0;
        top: 0;
        right: 0;
        bottom: 0;
        display: none;
    }

    #media-list li:hover .post-thumb {
        display: block
    }

    a.remove-pic {
        position: absolute;
        top: 5px;
        right: 5px;
        font-size: 12px;
        color: #fff;
        border: 1px solid #fff;
        border-radius: 50%;
        display: block;
        height: 25px;
        width: 25px;
        text-align: center;
        padding: 3px 0;
        z-index: 9999;
    }

    a.view-pic {
        position: absolute;
        top: 5px;
        right: 35px;

        font-size: 12px;
        color: #fff;
        border: 1px solid #fff;
        border-radius: 50%;
        display: block;
        height: 25px;
        width: 25px;
        text-align: center;
        padding: 3px 0;
    }

    a.close {
        top: 0px;
        right: 35px;

        font-size: 12px;
        font-weight: bold;
        color: #333;
        border: 2px solid #333;
        border-radius: 50%;
        display: block;
        height: 25px;
        width: 25px;
        text-align: center;
        padding: 5px 0;
        margin-bottom: 10px;
    }

    .inner-post-thumb {
        position: relative
    }

    .user-post-text-wrap {
        position: relative
    }

    .user-pic-post {
        position: absolute;
        width: 50px;
        height: 50px;
        top: 0;
        left: 0
    }

    .user-pic-post img {
        width: 100%
    }

    .user-txt-post {
        padding: 0 0 0 65px
    }

    textarea.form-control.upostTextarea {
        border: 0;
        box-shadow: none;
        height: 85px;
        resize: none
    }

    .user-post-text-wrap {
        border-bottom: 1px solid #ccc;
        margin: 0 0 15px
    }

    .user-post-btn-wrap {
        margin: 25px 0 0
    }

    ul.btn-nav {
        list-style: none;
        padding: 0;
        margin: 0
    }

    ul.btn-nav li {
        position: relative;
        float: left;
        margin: 0 10px 0 0
    }

    ul.btn-nav li span input {
        position: absolute;
        left: 0;
        top: 0;
        height: 100%;
        width: 100%;
        z-index: 9;
        opacity: 0;
        filter: alpha(opacity=100)
    }

    ul#media-list {
        list-style: none;
        padding: 0;
        margin: 0
    }

    span.error {
        color: red;
        font-style: italic
    }

    div.error {
        display: none
    }

    input {
        border: 1px solid #000
    }

    input.checkbox {
        border: none
    }

    input:focus {
        border: 1px solid #000
    }

    input.error {
        border: 1px solid red
    }

    .error .select2-choice.select2-default,
    .error .select2-choices {
        color: #a94442;
        border-color: #a94442;
        -webkit-box-shadow: inset 0 1px 1px rgba(0, 0, 0, .075);
        box-shadow: inset 0 1px 1px rgba(0, 0, 0, .075);
    }

    .error:focus,
    .error .select2-choice.select2-defaultLfocus,
    .error .select2-choicesLfocus {
        border-color: #843534;
        -webkit-box-shadow: inset 0 1px 1px rgba(0, 0, 0, .075), 0 0 6px #ce8483;
        box-shadow: inset 0 1px 1px rgba(0, 0, 0, .075), 0 0 6px #ce8483;
    }

    .select2-container .select2-choices .select2-search-field input,
    .select2-container .select2-choice,
    .select2-container .select2-choices,
    .error {
        border-radius: 1px;
    }

</style>

<div class="box box-warning">
    <div class="box-header with-border">
        <h1 class="box-title" style="font-size: 20px;margin-top:5px">แก้ไขโปรโมชั่น</h1>

    </div>
    <form method="POST" enctype="multipart/form-data" id="WebForm" accept-charset="utf-8">
        <input type="hidden" name="file_index" value="{{$file_index}}">
        <input type="hidden" name="id" value="{{$promotion->id}}">
        <input type="hidden" name="delete_pictures">
        <div class="box-body">
            <div class="form-group ">
                <label>ชื่อโปรโมชั่นภาษาไทย</label>
                <div class="row">
                    <div class="col-sm-12">
                        <input type="text" placeholder="ชื่อโปรโมชั่นภาษาไทย" class="form-control" name="title_th"
                            value="{{$promotion->title_th}}">
                    </div>
                </div>
            </div>

            <div class="form-group ">
                <label>รายละเอียดโปรโมชั่นภาษาไทย</label>
                <div class="row">
                    <div class="col-sm-12">
                        <textarea name="detail_th" id="detail_th" class="summernote">{{$promotion->detail_th}}</textarea>
                    </div>
                </div>
            </div>

            <div class="form-group ">
                <label>ชื่อโปรโมชั่นภาษาอังกฤษ</label>
                <div class="row">
                    <div class="col-sm-12">
                        <input type="text" placeholder="ชื่อโปรโมชั่นภาษาอังกฤษ" class="form-control" name="title_en"
                            value="{{$promotion->title_en}}">
                    </div>
                </div>
            </div>

            <div class="form-group ">
                <label>รายละเอียดโปรโมชั่นภาษาอังกฤษ</label>
                <div class="row">
                    <div class="col-sm-12">
                        <textarea name="detail_en" id="detail_en" class="summernote">{{$promotion->detail_en}}</textarea>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-12">
                    <div class="form-group">
                        <label>รูปภาพ</label>
                        <div class="row">
                            <div class="col-md-12">
                                <ul id="media-list" class="clearfix">
                                    @foreach ($promotion->pictures as $p)
                                    <li>
                                        <img src="{{$p->url}}">
                                        <div class="post-thumb">
                                            <div class="inner-post-thumb">
                                                <a href="javascript:void(0);" data-id="{{$p->id}}" class="remove-pic db"
                                                    data-index="{{$p->index}}"><i
                                                        class="fa fa-times" aria-hidden="true"></i></a>
                                                <a href="javascript:void(0);" data-id="{{$p->id}}" class="view-pic"
                                                    data-index="{{$p->index}}"><i class="fa fa-search"
                                                        aria-hidden="true"></i></a>
                                            </div>
                                        </div>
                                    </li>
                                    @endforeach

                                    <li class="myupload">
                                        <span id="row-{{$file_index}}"><i class="fa fa-plus" aria-hidden="true"></i><input
                                                name="files[]" type="file" click-type="type2" class="picupload"
                                                accept="image/*"></span>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-8">
                    <div class="form-group">
                        <label>โรงพยาบาล</label>
                        <select class="form-control" name="hospital_code" id="hospital_code" data-live-search="true">
                            <option value="">เลือก</option>
                            @foreach ($hospital as $row)
                            <option value="{{$row->code}}" {{$row->selected}}>{{$row->name}}</option>
                            @endforeach
                        </select>
                    </div>
                </div>

                <div class="col-md-4">
                    <div class="form-group">
                        <label>หมวดหมู่</label>
                        <select class="form-control category" name="category_id">
                            <option value="">เลือก</option>
                            @foreach ($categories as $row)
                            <option value="{{$row->id}}" {{$row->selected}}>{{$row->name}}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-2">
                    <div class="form-group">
                        <label>ช่วงอายุ</label>
                        <select class="form-control status" name="age_range">
                            @foreach ($age_range as $row)
                            <option value="{{$row->id}}" {{$row->selected}}>{{$row->name}}</option>
                            @endforeach
                        </select>
                    </div>
                </div>

                <div class="col-md-2">
                    <div class="form-group">
                        <label>เพศ</label>
                        <select class="form-control status" name="gender">
                             @foreach ($gender as $row)
                            <option value="{{$row->id}}" {{$row->selected}}>{{$row->name}}</option>
                            @endforeach
                        </select>
                    </div>
                </div>

                <div class="col-md-3">
                    <div class="form-group">
                        <label>เริ่มต้น</label>
                        <input type="text" placeholder="วันที่เริ่มต้น" class="form-control datepicker"
                            name="start_date" value="{{$promotion->start_date}}">
                    </div>
                </div>

                <div class="col-md-3">
                    <div class="form-group">
                        <label>สิ้นสุด</label>
                        <input type="text" placeholder="วันที่สิ้นสุด" class="form-control datepicker" name="end_date" value="{{$promotion->end_date}}">
                    </div>
                </div>

                <div class="col-md-2">
                    <div class="form-group">
                        <label>สถานะ</label>
                        <select class="form-control status" name="status">
                            <option value="1" <?php if($promotion->status ==1){?> selected <?php } ?>>แสดง</option>
                            <option value="0" <?php if($promotion->status ==0){?> selected <?php } ?>>ซ่อน</option>
                        </select>
                    </div>
                </div>
            </div>



        </div>

        <div class="box-footer">
            <div style="text-align: right">
                <a href="{{url('admin/promotions')}}" class="btn btn-default"> ยกเลิก </a>
                <input type="submit" name="submit" value="บันทึก" class="btn btn-success">

            </div>
        </div>

    </form>

</div>
<div class="modal fade" id="imagemodal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-body">
                <a href="javascript:void(0);" class="close" data-dismiss="modal"><i class="fa fa-times"
                        aria-hidden="true"></i></a>
                <img src="" class="imagepreview" style="width: 100%;">
            </div>
        </div>
    </div>
</div>

@endsection
