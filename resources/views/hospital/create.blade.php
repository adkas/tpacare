<!-- First you need to extend the CB layout -->
@extends('crudbooster::themes.adminlte.layout.layout')
@section('content')
<!-- Your custom  HTML goes here -->

<div class="box box-default">
    <div class="box-header with-border">
        <h1 class="box-title">ข้อมูลสถานพยาบาล</h1>
    </div>

    <form method="post" id="form" enctype="multipart/form-data"
action="{{ url("admin/hospital/add-save") }}">
        {{-- <input type="hidden" name="_token" value="D0bEX7AC3UYUgX0yhRxq0qUDRoR5b6bi61KHYDmn">
        <input type="hidden" name="ref" value="3YdkSvQ"> --}}
        <input type="hidden" name="file_index" value="1">
        <div class="box-body">
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        <label>
                            Provider Code
                            <span class="text-danger" title="crudbooster.this_field_is_required">*</span>
                        </label>

                        <input type="text" placeholder="Provider Code" class="form-control" name="provider_code">
                    </div>

                    <div class="form-group">
                        <label>
                            ชื่อภาษาไทย
                            <span class="text-danger" title="crudbooster.this_field_is_required">*</span>
                        </label>

                        <input type="text" placeholder="ชื่อภาษาไทย" class="form-control" name="name">
                    </div>

                    <div class="form-group ">
                        <label>
                            ชื่อภาษาอังกฤษ
                            <span class="text-danger" title="crudbooster.this_field_is_required">*</span>
                        </label>

                        <input type="text" placeholder="ชื่อภาษาอังกฤษ" required="" maxlength="250" class="form-control"
                            name="name_en" value="">
                    </div>



                    <div class="form-group ">
                        <label>
                            เบอร์โทร
                            <span class="text-danger" title="crudbooster.this_field_is_required">*</span>
                        </label>
                        <input type="text" placeholder="เบอร์โทร" class="form-control telephone" name="telephone">
                    </div>

                    <div class="form-group ">
                        <label>เว็บไซต์</label>
                        <input type="text" placeholder="เว็บไซต์" class="form-control" name="url">
                    </div>

                    <div class="form-group " id="form-group-type">
                        <label>
                            ประเภทของ ร.พ.
                            <span class="text-danger" title="crudbooster.this_field_is_required">*</span>
                        </label>

                        <div class="row">
                            <div class="col-sm-12"> <select style="width: 100%" id="select-type"
                                    class="form-control select2 select2-hidden-accessible" required="" name="type"
                                    tabindex="-1" aria-hidden="true">
                                    <option value="">เลือก</option>
                                    <option value="1">ครอบคลุมทั้งหมด</option>
                                    <option value="2">ครอบคลุมบางรายการ</option>
                                    <option value="3">อื่นๆ</option>
                                </select>
                            </div>
                        </div>

                        <div class="text-danger"></div>
                        <p class="help-block"></p>
                    </div>


                </div>
                <div class="col-md-6">

                    <div class="form-group">
                        <label>เลขที่ / หมู่ / ถนน / ซอย / หมู่บ้าน / อาคาร</label>
                        <input type="text" placeholder="เลขที่ / หมู่ / ถนน / ซอย / หมู่บ้าน / อาคาร"
                            class="form-control" name="address_no">
                    </div>

                    <div class="form-group">
                        <label>ภูมิภาค</label>
                        <select class="form-control" name="geo_id">
                            <option value="">เลือก</option>
                        </select>
                    </div>

                    <div class="form-group">
                        <label>จังหวัด</label>
                        <select class="form-control" name="province_id">
                            <option value="">เลือก</option>
                        </select>
                    </div>

                    <div class="form-group">
                        <label>อำเภอ/เขต</label>
                        <select class="form-control" name="amphur_id">
                            <option value="">เลือก</option>
                        </select>
                    </div>

                    <div class="form-group">
                        <label>รหัสไปรษณีย์</label>
                        <input type="text" placeholder="รหัสไปรษณีย์" class="form-control" name="postcode">
                    </div>

                    <div class="form-group ">
                        <label>พิกัด</label>
                        <input type="text" class="form-control" name="coordinates" placeholder="ตัวอย่าง 13.72917,100.52389">
                    </div>
                </div>
            </div>


        </div>
        <!-- /.box-body -->
        <div class="box-header with-border">
            <h1 class="box-title">ค่าบริการและเวลาในการบริการ</h1>
        </div>

        <div class="box-body">
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        <label>ค่าบริการ</label>
                        <input type="text" placeholder="ตัวอย่าง 1,000-10,000 บาท" class="form-control" name="address_no">
                    </div>
                </div>

                <div class="col-md-3">
                    <div class="form-group">
                        <label>ตั้งค่าการแสดงค่าบริการ</label>
                        <select class="form-control" name="amphur_id">
                            <option value="">ซ่อน</option>
                            <option value="" selected>แสดง</option>
                        </select>
                    </div>
                </div>

                <div class="col-md-3">
                    <div class="form-group">
                        <label>ตั้งค่าการแสดงเข้าร่วมกับ</label>
                        <select class="form-control" name="amphur_id">
                            <option value="">ซ่อน</option>
                            <option value="" selected>แสดง</option>
                        </select>
                    </div>
                </div>

            </div>

            <div class="row">
                <div class="col-md-12">
                    <div class="form-group">
                        <label>เข้าร่วมกับ</label>
                        <textarea class="form-control" placeholder="เข้าร่วมกับ" class="form-control"
                            style="height: 150px;"></textarea>
                    </div>
                </div>
            </div>

            <div class="row services-time" data-index="1">
                <div class="col-md-4">
                     <div class="form-group">
                         <label>วันในการให้บริการ</label>
                         <input type="text" placeholder="วันในการให้บริการ" class="form-control" name="services_day[]">
                     </div>
                </div>
                <div class="col-md-7">
                    <div class="form-group">
                        <label>เวลาทำการ</label>
                        <input type="text" class="form-control service-time" name="services_time[]" value="08.00-12.00">
                    </div>
                </div>

                <div class="col-md-1">
                    <a href="javascript:void(0)" class="btn btn btn-default disabled" data-row="1"
                        style="margin-top: 24px;"><i
                            class="fa fa-times-circle"></i></a>
                </div>
            </div>

            {{-- <div class="row services-time">
                <div class="col-md-4">
                    <div class="form-group">
                        <label>วันในการให้บริการ</label>
                        <input type="text" placeholder="วันในการให้บริการ" class="form-control" name="services_day[]">
                    </div>
                </div>
                <div class="col-md-7">
                    <div class="form-group">
                        <label>เวลาทำการ</label>
                        <input type="text" class="form-control service-time" name="services_time[]" value="08.00-12.00">
                    </div>
                </div>
            
                <div class="col-md-1">
                    <a href="javascript:void(0)" class="btn btn btn-danger" style="margin-top: 24px;"><i
                            class="fa fa-times-circle"></i></a>
                </div>
            </div> --}}

            <div class="row">
                <div class="col-md-11"></div>
                <div class="col-md-1">
                    <a href="javascript:void(0)" class="btn btn btn-success btn-clone-service" data-index="2"><i
                            class="fa fa-plus-circle"></i></a>
                </div>
            </div>

            <div class="row">
                <div class="col-md-12">
                    <div class="form-group">
                        <label>รูปภาพ</label>
                         <div class="row">
                             <div class="col-md-12">
                                 <ul id="media-list" class="clearfix">
                                     <li class="myupload">
                                         <span id="row-1"><i class="fa fa-plus" aria-hidden="true"></i><input
                                                 name="files[]" type="file" click-type="type2" class="picupload"
                                                 accept="image/*"></span>
                                     </li>
                                 </ul>
                             </div>
                         </div>
                    </div>
                </div>
            </div>

        </div>

        <!-- /.box-body -->
        {{-- <div class="box-header with-border">
            <h1 class="box-title">รูปภาพ</h1>
        </div>

        <div class="box-body">
            <div class="row">
                <div class="col-md-12">
                    <ul id="media-list" class="clearfix">
                        <li class="myupload">
                            <span id="row-1"><i class="fa fa-plus" aria-hidden="true"></i><input name="files[]"
                                    type="file" click-type="type2" class="picupload" accept="image/*"></span>
                        </li>
                    </ul>
                </div>
            </div>
        </div> --}}

        <div class="box-footer">

            <div style="text-align: right">
                <!-- <input type="submit" name="submit" value='Save &amp; Add More' class='btn btn-default'> -->
                <a href="{{ url("admin/hospital") }}" class="btn btn-default"> Cancel
                </a>


                <input type="submit" name="submit" value="Save" class="btn btn-success">

            </div>


        </div><!-- /.box-footer-->

    </form>
    <div class="modal fade" id="imagemodal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
        aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-body">
                    <a href="javascript:void(0);" class="close" data-dismiss="modal"><i class="fa fa-times"
                            aria-hidden="true"></i></a>
                    <img src="" class="imagepreview" style="width: 100%;">
                </div>
            </div>
        </div>
    </div>
    @endsection
