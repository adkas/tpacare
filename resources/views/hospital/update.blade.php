<!-- First you need to extend the CB layout -->
@extends('crudbooster::themes.adminlte.layout.layout')
@section('content')
<!-- Your custom  HTML goes here -->

<div class="box box-default">
    <div class="box-header with-border">
        <h1 class="box-title">ข้อมูลสถานพยาบาล</h1>
    </div>

    <form method="post" id="form" enctype="multipart/form-data" action="{{ url("admin/hospital/add-save") }}">
        {{-- <input type="hidden" name="_token" value="D0bEX7AC3UYUgX0yhRxq0qUDRoR5b6bi61KHYDmn">
        <input type="hidden" name="ref" value="3YdkSvQ"> --}}
        <input type="hidden" name="file_index" value="1">
        <input type="hidden" name="remove_images" value="">
        <div class="box-body">
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        <label>
                            Provider Code
                            <span class="text-danger" title="crudbooster.this_field_is_required">*</span>
                        </label>

                        <input type="text" placeholder="Provider Code" class="form-control" name="provider_code"
                            value="{{$hospital->code}}" readonly>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        <label>
                            ชื่อภาษาไทย
                            <span class="text-danger" title="crudbooster.this_field_is_required">*</span>
                        </label>

                        <input type="text" placeholder="ชื่อภาษาไทย" class="form-control" name="name_th"
                            value="{{$hospital->name_th}}">
                    </div>

                    <div class="form-group ">
                        <label>
                            ชื่อภาษาอังกฤษ
                            <span class="text-danger" title="crudbooster.this_field_is_required">*</span>
                        </label>

                        <input type="text" placeholder="ชื่อภาษาอังกฤษ" required="" maxlength="250" class="form-control"
                            name="name_en" value="{{$hospital->name_en}}">
                    </div>
                    <div class="form-group ">
                        <label>
                            เบอร์โทร
                            <span class="text-danger" title="crudbooster.this_field_is_required">*</span>
                        </label>
                        <input type="text" placeholder="เบอร์โทร" class="form-control" name="telephone"
                            value="{{$hospital->telephone}}">
                    </div>

                    <div class="form-group ">
                        <label>เว็บไซต์</label>
                        <input type="text" placeholder="เว็บไซต์" class="form-control" name="website"
                            value="{{$hospital->website}}">
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-6">
                    <div class="form-group " id="form-group-type">
                        <label>
                            ประเภทของ ร.พ.
                            <span class="text-danger" title="crudbooster.this_field_is_required">*</span>
                        </label>

                        <div class="row">
                            <div class="col-sm-12"> <select style="width: 100%" id="select-type"
                                    class="form-control select2 select2-hidden-accessible" required="" name="type"
                                    tabindex="-1" aria-hidden="true">
                                    <option value="">เลือก</option>
                                    @foreach($types as $t)
                                    <option value="{{$t->id}}" {{$t->selected}}>{{$t->name}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>

                        <div class="text-danger"></div>
                        <p class="help-block"></p>
                    </div>
					  

					<div class="row">
						<div class="col-md-6">
							<div class="form-group ">
								<label>Latitude</label>
								<input type="text" class="form-control" name="latitude"
									placeholder="Latitude"
									value="{{$hospital->latitude}}">
							</div>
						</div>

						<div class="col-md-6">
							<div class="form-group ">
								<label>Longitude</label>
								<input type="text" class="form-control" name="longitude"
									placeholder="Latitude"
									value="{{$hospital->longitude}}">
							</div>
						</div>
					</div>
                </div>
            </div>

			<!-- /.box-address -->
			<div class="box-header with-border">
				<h1 class="box-title">ข้อมูลที่อยู่</h1>
			</div>

			<div class="box-body">
			<div class="row">
        <!-- address TH -->
				<div class="col-md-6">
					<div class="row">
						<div class="col-md-6">
							<div class="form-group">
								<label>เลขที่</label>
								<input type="text" placeholder="เลขที่" class="form-control" name="address_no"
									value="{{$hospital->address_no}}">
							</div>
						</div>
						<div class="col-md-6">
							<div class="form-group">
								<label>ถนน</label>
								<input type="text" placeholder="ถนน" class="form-control" name="address_road"
									value="{{$hospital->address_road}}">
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-md-12">
							<div class="form-group">
								<label>อำเภอ/เขต</label>
								<input type="text" placeholder="อำเภอ/เขต" class="form-control" name="address_amphur"
									value="{{$hospital->address_amphur}}">
							</div>
							<div class="form-group">
                        <label>จังหวัด</label>
							<input type="text" placeholder="จังหวัด" class="form-control" name="address_province"
								value="{{$hospital->address_province}}">
						</div>

						<div class="form-group">
							<label>ภูมิภาค</label>
							<input type="text" placeholder="ภูมิภาค" class="form-control" name="location"
								value="{{$hospital->location}}">
						</div>

						<div class="form-group">
							<label>รหัสไปรษณีย์</label>
							<input type="text" placeholder="รหัสไปรษณีย์" class="form-control" name="address_postcode"
								value="{{$hospital->address_postcode}}">
						</div>
						</div>
					</div>

				</div>
        <!-- address EN -->
				<div class="col-md-6">
					<div class="row">
						<div class="col-md-6">
							<div class="form-group">
								<label>Address</label>
								<input type="text" placeholder="Address" class="form-control" name="address_no_en"
									value="{{$hospital->address_no_en}}">
							</div>
						</div>
						<div class="col-md-6">
							<div class="form-group">
								<label>Street Address</label>
								<input type="text" placeholder="Street Address" class="form-control" name="address_road_en"
									value="{{$hospital->address_road_en}}">
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-md-12">
							<div class="form-group">
								<label>District</label>
								<input type="text" placeholder="District" class="form-control" name="address_amphur_en"
									value="{{$hospital->address_amphur_en}}">
							</div>
							<div class="form-group">
                        <label>City</label>
							<input type="text" placeholder="City" class="form-control" name="address_province_en"
								value="{{$hospital->address_province_en}}">
						</div>

						<div class="form-group">
							<label>Reqoin</label>
							<input type="text" placeholder="Reqoin" class="form-control" name="location_en"
								value="{{$hospital->location_en}}">
						</div>

						<div class="form-group">
							<label>Postal Code</label>
							<input type="text" placeholder="Postal Code" class="form-control" name="address_postcode_en"
								value="{{$hospital->address_postcode_en}}">
						</div>
						</div>
					</div>

				</div>
			</div>
			</div>
        </div>



        <!-- /.box-body -->
        <div class="box-header with-border">
            <h1 class="box-title">ค่าบริการและเวลาในการบริการ</h1>
        </div>

        <div class="box-body">
            <div class="row">
                <div class="col-md-3">
                    <div class="form-group">
                        <label>ค่าบริการภาษาไทย</label>
                        <input type="text" placeholder="ตัวอย่าง 1,000-10,000 บาท" class="form-control"
                            name="service_fee_th" value="{{$hospital->service_fee_th}}">
                    </div>
                </div>

                <div class="col-md-3">
                    <div class="form-group">
                        <label>ค่าบริการภาษาอังกฤษ</label>
                        <input type="text" placeholder="ตัวอย่าง 1,000-10,000 บาท" class="form-control"
                            name="service_fee_en" value="{{$hospital->service_fee_en}}">
                    </div>
                </div>

                <div class="col-md-3">
                    <div class="form-group">
                        <label>ตั้งค่าการแสดงค่าบริการ</label>
                        <select class="form-control" name="show_service_fee">
                            <option value="0" <?php if($hospital->show_service_fee == 0){?>  selected<?php } ?>>ซ่อน</option>
                            <option value="1" <?php if($hospital->show_service_fee == 1){?>  selected<?php } ?>>แสดง</option>
                        </select>
                    </div>
                </div>

                <div class="col-md-3">
                    <div class="form-group">
                        <label>ตั้งค่าการแสดงรายละเอียด</label>
                        <select class="form-control" name="show_description">
                            <option value="0" <?php if($hospital->show_description == 0){?>  selected<?php } ?>>ซ่อน</option>
                            <option value="1" <?php if($hospital->show_description == 1){?>  selected<?php } ?>>แสดง</option>
                        </select>
                    </div>
                </div>

            </div>

            <div class="row">
                <div class="col-md-12">
                    <div class="form-group">
                        <label>รายละเอียดภาษาไทย</label>
                        <textarea class="form-control" placeholder="รายละเอียดภาษาไทย" class="form-control"
                            style="height: 150px;" name="description_th">{{$hospital->description_th}}</textarea>
                    </div>
                </div>

                <div class="col-md-12">
                    <div class="form-group">
                        <label>รายละเอียดภาษาอังกฤษ</label>
                        <textarea class="form-control" placeholder="รายละเอียดภาษาอังกฤษ" class="form-control"
                            style="height: 150px;" name="description_en">{{$hospital->description_en}}</textarea>
                    </div>
                </div>
            </div>

            <div class="row services-time" data-index="1">
                <div class="col-md-6">
                    <div class="form-group">
                        <label>วันเวลาทำการภาษาไทย</label>
                        <input type="text" placeholder="วันเวลาทำการภาษาไทย" class="form-control service-time"
                            name="service_times_th[]" value="{{$hospital->service_times_th}}">
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label>วันเวลาทำการภาษาอังกฤษ</label>
                        <input type="text" class="form-control service-time" name="service_times_en[]"
                            placeholder="วันเวลาทำการภาษาอังกฤษ" value="{{$hospital->service_times_th}}">
                    </div>
                </div>

                {{-- <div class="col-md-1">
                    <a href="javascript:void(0)" class="btn btn btn-default disabled" data-row="1"
                        style="margin-top: 24px;"><i class="fa fa-times-circle"></i></a>
                </div> --}}
            </div>

            {{-- <div class="row services-time">
                <div class="col-md-4">
                    <div class="form-group">
                        <label>วันในการให้บริการ</label>
                        <input type="text" placeholder="วันในการให้บริการ" class="form-control" name="services_day[]">
                    </div>
                </div>
                <div class="col-md-7">
                    <div class="form-group">
                        <label>เวลาทำการ</label>
                        <input type="text" class="form-control service-time" name="services_time[]" value="08.00-12.00">
                    </div>
                </div>

                <div class="col-md-1">
                    <a href="javascript:void(0)" class="btn btn btn-danger" style="margin-top: 24px;"><i
                            class="fa fa-times-circle"></i></a>
                </div>
            </div> --}}

            {{-- <div class="row">
                <div class="col-md-10"></div>
                <div class="col-md-1">
                    <a href="javascript:void(0)" class="btn btn btn-success btn-clone-service" data-index="2"><i
                            class="fa fa-plus-circle"></i></a>
                </div>
            </div> --}}

            <div class="row">
                <div class="col-md-12">
                    <div class="form-group">
                        <label>รูปภาพ</label>
                        <div class="row">
                            <div class="col-md-12">
                                <ul id="media-list" class="clearfix">
                                     @foreach ($hospital->pictures as $p)
                                    <li>
                                        <img src="{{$p->url}}">
                                        <div class="post-thumb">
                                            <div class="inner-post-thumb">
                                                <a href="javascript:void(0);" data-id="{{$p->id}}" class="remove-pic db"
                                                    data-index="{{$p->index}}"><i
                                                        class="fa fa-times" aria-hidden="true"></i></a>
                                                <a href="javascript:void(0);" data-id="{{$p->id}}" class="view-pic"
                                                    data-index="{{$p->index}}"><i class="fa fa-search"
                                                        aria-hidden="true"></i></a>
                                            </div>
                                        </div>
                                    </li>
                                    @endforeach

                                    <li class="myupload">
                                        <span id="row-{{$file_index}}"><i class="fa fa-plus" aria-hidden="true"></i><input
                                                name="files[]" type="file" click-type="type2" class="picupload"
                                                accept="image/*"></span>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>

        <!-- /.box-body -->
        {{-- <div class="box-header with-border">
            <h1 class="box-title">รูปภาพ</h1>
        </div>

        <div class="box-body">
            <div class="row">
                <div class="col-md-12">
                    <ul id="media-list" class="clearfix">
                        <li class="myupload">
                            <span id="row-1"><i class="fa fa-plus" aria-hidden="true"></i><input name="files[]"
                                    type="file" click-type="type2" class="picupload" accept="image/*"></span>
                        </li>
                    </ul>
                </div>
            </div>
        </div> --}}

        <div class="box-footer">

            <div style="text-align: right">
                <!-- <input type="submit" name="submit" value='Save &amp; Add More' class='btn btn-default'> -->
                <a href="{{ url("admin/hospital") }}" class="btn btn-default"> Cancel
                </a>


                <input type="submit" name="submit" value="Save" class="btn btn-success">

            </div>


        </div><!-- /.box-footer-->

    </form>
    <div class="modal fade" id="imagemodal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
        aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-body">
                    <a href="javascript:void(0);" class="close" data-dismiss="modal"><i class="fa fa-times"
                            aria-hidden="true"></i></a>
                    <img src="" class="imagepreview" style="width: 100%;">
                </div>
            </div>
        </div>
    </div>
    @endsection
