<div class="row services-time" data-index="{{ $index }}">
    <div class="col-md-6">
        <div class="form-group">
            <label>วันเวลาทำการภาษาไทย</label>
            <input type="text" placeholder="วันเวลาทำการภาษาไทย" class="form-control service-time" name="service_times_th[]">
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group">
            <label>วันเวลาทำการภาษาอังกฤษ</label>
            <input type="text" class="form-control service-time" name="service_times_en[]" placeholder="วันเวลาทำการภาษาอังกฤษ">
        </div>
    </div>

    {{-- <div class="col-md-1">
        <a href="javascript:void(0)" class="btn btn btn-danger btn-delete-service" data-row="{{ $index }}" style="margin-top: 24px;"><i
                class="fa fa-times-circle"></i></a>
    </div> --}}
</div>