<!-- First you need to extend the CB layout -->
@extends('crudbooster::themes.adminlte.layout.layout')
@section('content')
<!-- Your custom  HTML goes here -->

<div class="box box-default" data-select2-id="16">
    <div class="box-header with-border">
        <h3 class="box-title">ค้นหา</h3>

        <div class="box-tools pull-right">
            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
        </div>
    </div>
    <!-- /.box-header -->
    <form method="GET" action="?">
        <div class="box-body">

            <div class="row">
                <div class="col-md-3">
                    <div class="form-group">
                        <label>Name</label>
                        <input type="text" placeholder="Name" class="form-control" name="name"
                            value="{{$name}}">
                    </div>
                    <!-- /.form-group -->
                </div>
                <!-- /.col -->

                <div class="col-md-3">
                    <div class="form-group">
                        <label>Surname</label>
                        <input type="text" placeholder="Surname" class="form-control" name="surname"
                            value="{{$surname}}">
                    </div>
                    <!-- /.form-group -->
                </div>
                <!-- /.col -->

                <div class="col-md-2">
                    <div class="form-group">
                        <label>Username</label>
                        <input type="text" placeholder="Username" class="form-control" name="username"
                            value="{{$username}}">
                    </div>
                    <!-- /.form-group -->
                </div>
                <!-- /.col -->



                <div class="col-md-2">
                    <div class="form-group">
                        <label>Mobile</label>
                        <input type="text" placeholder="Mobile" class="form-control" name="mobile"
                            value="{{$mobile}}">
                    </div>
                    <!-- /.form-group -->
                </div>
                <!-- /.col -->

                <div class="col-md-2">
                    <div class="form-group">
                        <label>Citizen ID</label>
                        <input type="text" placeholder="Citizen ID" class="form-control" name="citizen_id"
                            value="{{$citizen_id}}">
                    </div>
                    <!-- /.form-group -->
                </div>
                <!-- /.col -->

            </div>
            <!-- /.row -->

        </div>

        <div class="box-footer">
            <div style="text-align: right">
                <input type="submit" id="submit" value="ค้นหา" class="btn btn-info" style="width:120px;">
            </div>

        </div><!-- /.box-footer-->
    </form>
</div>


<div class="box">
    <div class="box-header">

        <h1 class="box-title" style="font-size: 20px;margin-top:5px">ข้อมูลผู้ใช้งาน App</h1>

        <!--<div class="box-tools pull-right">
            <a href="{{ module()->addURL()."?ref=".makeReferalUrl()  }}{{ isset($subModuleKey)?"&sub_module=".$subModuleKey:null }}"
                id='btn_add_new_data' class="btn btn-sm btn-success " title="{{ cbLang("add").' '.cbLang('data') }}">
                <i class="fa fa-plus-circle"></i> {{ cbLang("add").' '.cbLang('data') }}
            </a>
        </div> -->



    </div>

    <div class="box-body table-responsive">

        <table id="table-module" class="table table-hover table-striped table-bordered">
            <thead>
                <tr>
                    <th>No</th>
                    <th>Name</th>
                    <th>Username</th>
                    <th>Mobile</th>
                    <th>Citizen ID</th>
                    <th>Status</th>
                    <th>Created Date</th>
                    <th>Action</th>
                </tr>
            </thead>
            <tbody>
                @foreach($record as $row)
                <tr>
                    <td>{{$row->no}}</td>
                    <td>{{$row->name}}</td>
                    <td>{{$row->username}}</td>
                    <td>{{$row->mobile}}</td>
                    <td>{{$row->citizen_id}}</td>
                    <td>{{$row->status_text}}</td>
                    <td>{{$row->created_at}}</td>
                    <td>
                        <!-- To make sure we have read access, wee need to validate the privilege -->
                        @if(module()->canRead())
                        <a class='btn btn-info btn-xs' href='{{url("admin/members/detail/$row->id")}}'><i
                                class="fa fa-eye" aria-hidden="true"></i></a>
                        @endif

                        @if(module()->canUpdate())
                        <a class='btn btn-warning btn-xs' href='{{url("admin/members/edit/$row->id")}}'><i
                                class="fa fa-pencil-square-o" aria-hidden="true"></i></a>
                        @endif
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>

    </div>

</div>


<!-- ADD A PAGINATION -->
<p>{!! urldecode(str_replace("/?","?",$result->appends(Request::all())->render())) !!}</p>
@endsection
