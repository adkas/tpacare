<!-- First you need to extend the CB layout -->
@extends('crudbooster::themes.adminlte.layout.layout')
@section('content')
<!-- Your custom  HTML goes here -->
<div class="box box-info">
    <div class="box-header with-border">
        <h1 class="box-title"><i class="fa fa-eye"></i> รายละเอียดข้อมูลผู้ใช้งาน App</h1>
    </div>
    <div class="box-body">
        <div class="table-responsive">
            <table id="table-detail" class="table table-striped">
                <tbody>
                    <tr>
                        <th width="25%">Name</th>
                        <td>{{$name}}</td>
                    </tr>

                    <tr>
                        <th width="25%">Surname</th>
                        <td>{{$surname}}</td>
                    </tr>

                    <tr>
                        <th width="25%">Username</th>
                        <td>{{$mobile}}</td>
                    </tr>

                    <tr>
                        <th width="25%">Mobile</th>
                        <td>{{$mobile}}</td>
                    </tr>

                    <tr>
                        <th width="25%">Citizen ID</th>
                        <td>{{$citizen_id}}</td>
                    </tr>

                    <tr>
                        <th width="25%">Status</th>
                        <td>{{$status}}</td>
                    </tr>

                    <tr>
                        <th width="25%">Is New Password</th>
                        <td>{{$is_new_pass}}</td>
                    </tr>

                    <tr>
                        <th width="25%">Reference ID</th>
                        <td>{{$ref_id}}</td>
                    </tr>

                    <tr>
                        <th width="25%">Created At</th>
                        <td>{{$created_at}}</td>
                    </tr>

                    <tr>
                        <th width="25%">Updated At</th>
                        <td>{{$updated_at}}</td>
                    </tr>

                    <tr>
                        <th width="25%">Login At</th>
                        <td>{{$login_at}}</td>
                    </tr>

                </tbody>
            </table>
        </div>
    </div>
    <div class="box-footer">
        <div style="text-align: left">
            <a href="{{url('admin/members')}}" class="btn btn-info"> ย้อนกลับ </a>

        </div>
    </div>
</div>
@endsection
