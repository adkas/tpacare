<!-- First you need to extend the CB layout -->
@extends('crudbooster::themes.adminlte.layout.layout')
@section('content')
<!-- Your custom  HTML goes here -->

<div class="box box-default" data-select2-id="16">
    <div class="box-header with-border">
        <h3 class="box-title">ค้นหา</h3>

        <div class="box-tools pull-right">
            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
        </div>
    </div>
    <!-- /.box-header -->
    <form method="GET" action="?">
        <div class="box-body">

            <div class="row">
                <div class="col-md-3">
                    <div class="form-group">
                        <label>รหัสบริษัทประกัน</label>
                        <input type="text" placeholder="รหัสบริษัทประกัน" class="form-control" name="ins_code"
                            value="{{$ins_code}}">
                    </div>
                </div>
				
				<div class="col-md-3">
                    <div class="form-group">
                        <label>ชื่อบริษัทประกันภาษาไทย</label>
                        <input type="text" placeholder="ชื่อบริษัทประกัน" class="form-control" name="name_th"
                            value="{{$name_th}}">
                    </div>
                </div>
				<div class="col-md-3">
                    <div class="form-group">
                        <label>ชื่อบริษัทประกันภาษาอังกฤษ</label>
                        <input type="text" placeholder="ชื่อบริษัทประกัน" class="form-control" name="name_en"
                            value="{{$name_en}}">
                    </div>
                </div>
				<div class="col-md-2">
                    <div class="form-group">
                        <input type="submit" name="submit" value="ค้นหา" class="btn btn-info"
                            style="margin-top: 25px; width:100%;">
                    </div>
                </div>
            </div>

        </div>
    </form>
</div>


<div class="box">
    <div class="box-header">
        <h1 class="box-title" style="font-size: 20px;margin-top:5px">รายการบริษัทประกัน</h1>
		<div class="box-tools pull-right">
            <a href="{{ url('admin/tpa_partner/add')."?ref=".makeReferalUrl()  }}{{ isset($subModuleKey)?"&sub_module=".$subModuleKey:null }}"
                id='btn_add_new_data' class="btn btn-sm btn-success " title="เพิ่มข้อมูล">
                <i class="fa fa-plus-circle"></i> เพิ่มข้อมูล
            </a>
        </div>
    </div>

    <div class="box-body table-responsive">

        <table id="table-module" class="table table-hover table-striped table-bordered">
            <thead>
                <tr>
                    <th>ลำดับ</th>
                    <th>รหัสบริษัทประกัน</th>
                    <th>ชื่อบริษัทประกันภาษาไทย</th>
                    <th>ชื่อบริษัทประกันภาษาอังกฤษ</th>
                    <th>Action</th>
                </tr>
            </thead>
            <tbody>
                @foreach($result as $key => $row)
                <tr>
                    <td>{{$key+1}}</td>
                    <td>{{$row->ins_code}}</td>
                    <td>{{$row->name_th}}</td>
                    <td>{{$row->name_en}}</td>
                    <td>
                        <!-- To make sure we have read access, wee need to validate the privilege -->
                        @if(module()->canRead())
                        <a class='btn btn-info btn-xs' href='{{url("admin/tpa_partner/detail/$row->ins_code")}}'><i
                                class="fa fa-eye" aria-hidden="true"></i></a>
                        @endif

                        @if(module()->canUpdate())
                        <a class='btn btn-warning btn-xs' href='{{url("admin/tpa_partner/edit/$row->ins_code")}}'><i
                                class="fa fa-pencil-square-o" aria-hidden="true"></i></a>
                        @endif
						
						@if(module()->canDelete())
						<a class='btn btn-danger btn-xs delete' data-id='{{$row->ins_code}}'><i class="fa fa-times-circle" aria-hidden="true"></i></a>
						@endif
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>

    </div>

</div>


<!-- ADD A PAGINATION -->
<p>{!! urldecode(str_replace("/?","?",$result->appends(Request::all())->render())) !!}</p>
@endsection
