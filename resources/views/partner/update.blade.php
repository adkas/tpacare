<!-- First you need to extend the CB layout -->
@extends('crudbooster::themes.adminlte.layout.layout')
@section('content')
<!-- Your custom  HTML goes here -->
<style>
	.head-title{
		border-top: 1px solid #f4f4f4;
		margin: 22px 0px 0px 0px;
		padding: 12px 0px 16px 0px;
	}
	
	select:required:invalid {
	  color: gray;
	}
	option[value=""][disabled] {
	  display: none;
	}
	option {
	  color: black;
	}
	
	body {font-family: Arial, Helvetica, sans-serif;}

	/* The Modal (background) */
	.modal {
	  display: none; /* Hidden by default */
	  position: fixed; /* Stay in place */
	  /* z-index: 1;  Sit on top */
	  padding-top: 40px; /* Location of the box */
	  left: 0;
	  top: 0;
	  width: 100%; /* Full width */
	  height: 100%; /* Full height */
	  overflow: auto; /* Enable scroll if needed */
	  background-color: rgb(0,0,0); /* Fallback color */
	  background-color: rgba(0,0,0,0.4); /* Black w/ opacity */
	}

	/* Modal Content */
	.modal-content {
	  background-color: #fefefe;
	  margin: auto;
	  padding: 20px;
	  border: 1px solid #888;
	  width: 70%;
	}

	/* The Close Button */
	.close {
	  color: #aaaaaa;
	  float: right;
	  font-size: 28px;
	  font-weight: bold;
	  margin: 10px;
	}

	.close:hover,
	.close:focus {
	  color: #000;
	  text-decoration: none;
	  cursor: pointer;
	}
	
	.border-card{
		border: 1px solid #000000;
	}
	
	.error {
        color: red;
        font-style: italic
    }

    div.error {
        display: none
    }

    input {
        border: 1px solid #000
    }

    input.checkbox {
        border: none
    }

    input:focus {
        border: 1px solid #000
    }

    input.error {
        border: 1px solid red
    }

    .error .select2-choice.select2-default,
    .error .select2-choices {
        color: #a94442;
        border-color: #a94442;
        -webkit-box-shadow: inset 0 1px 1px rgba(0, 0, 0, .075);
        box-shadow: inset 0 1px 1px rgba(0, 0, 0, .075);
    }

    .error:focus,
    .error .select2-choice.select2-defaultLfocus,
    .error .select2-choicesLfocus {
        border-color: #843534;
        -webkit-box-shadow: inset 0 1px 1px rgba(0, 0, 0, .075), 0 0 6px #ce8483;
        box-shadow: inset 0 1px 1px rgba(0, 0, 0, .075), 0 0 6px #ce8483;
    }

    .select2-container .select2-choices .select2-search-field input,
    .select2-container .select2-choice,
    .select2-container .select2-choices,
    .error {
        border-radius: 1px;
    }
	
	.tooltip_copy .tooltiptext {
	  visibility: hidden;
	  width: 60px;
	  background-color: #555;
	  color: #fff;
	  text-align: center;
	  border-radius: 6px;
	  padding: 5px;
	  position: absolute;
	  z-index: 1;
	  bottom: 150%;
	  margin-left: -38px;
	  opacity: 0;
	  transition: opacity 0.3s;
	}

	.tooltip_copy .tooltiptext::after {
	  content: "";
	  position: absolute;
	  top: 100%;
	  left: 50%;
	  margin-left: -5px;
	  border-width: 5px;
	  border-style: solid;
	  border-color: #555 transparent transparent transparent;
	  transition: opacity 0.3s;
	}

	.tooltip_copy:hover .tooltiptext {
	  visibility: visible;
	  opacity: 1;
	}
	
	.input-icons i { 
		position: absolute;
		right: 0;
	}
	
	.icon { 
		padding: 10px;
	} 
	  
	.input-field { 
		width: 100%; 
		padding: 10px;  
	} 
	  
	h2 { 
		color: green; 
	} 
</style>
<link href="{{ asset('css/preview.css') }}" rel="stylesheet">
<div class="box box-info">
    <div class="box-header with-border">
        <h1 class="box-title"><i class="fa fa-eye"></i> รายละเอียด TPA Partner</h1>
    </div>
	
	<form method="post" id="form" enctype="multipart/form-data" >
	
    <div class="box-body">
		<div class="row">
			<div class="form-row">
				<div class="form-group col-md-4">
					<label for="inputEmail4">รหัสบริษัทประกัน</label>
					<input class="form-control" name="ins_code" id="ins_code" value="{{$partner->ins_code}}">
				</div>
			</div>
			<div class="form-row">
				<div class="form-group col-md-4">
					<label for="inputEmail4">ชื่อบริษัทประกันภาษาไทย</label>
					<input class="form-control" name="name_th" value="{{$partner->name_th}}">
				</div>
			</div>
			<div class="form-row">
				<div class="form-group col-md-4">
					<label for="inputEmail4">ชื่อบริษัทประกันภาษาอังกฤษ</label>
					<input class="form-control" name="name_en" value="{{$partner->name_en}}">
				</div>
			</div>
		</div>
		<p class="head-title">สถานะเคลม</p>
		<div class="row">
			
			<div class="col-md-3">
				<p>Color Font 1</p>
				<div class="col-md-5" style=" padding-right: 0px;padding-left: 0px; ">
					<input class="form-control" name="status_cf1" value="{{$partner->status_cf1}}">
				</div>
				<div class="col-md-4" style="padding-left: 1px;padding-right: 0px;">
					<select class="form-control type" name="status_cf1_c" required>
						<option value="1" {{ $partner->status_cf1_c == "1" ? "selected" : ""}}>100%</option>
						<option value="0.75" {{ $partner->status_cf1_c == "0.75" ? "selected" : ""}}>75%</option>
						<option value="0.5" {{ $partner->status_cf1_c == "0.5" ? "selected" : ""}}>50%</option>
						<option value="0.25" {{ $partner->status_cf1_c == "0.25" ? "selected" : ""}}>25%</option>
					</select>
				</div>
			</div>
			<div class="col-md-3">
				<p>Color Font 2</p>
				<div class="col-md-5" style=" padding-right: 0px;padding-left: 0px; ">
					<input class="form-control" name="status_cf2" value="{{$partner->status_cf2}}">
				</div>
				<div class="col-md-4" style="padding-left: 1px;padding-right: 0px;">
					<select class="form-control type" name="status_cf2_c" required>
						<option value="1" {{ $partner->status_cf2_c == "1" ? "selected" : ""}}>100%</option>
						<option value="0.75" {{ $partner->status_cf2_c == "0.75" ? "selected" : ""}}>75%</option>
						<option value="0.5" {{ $partner->status_cf2_c == "0.5" ? "selected" : ""}}>50%</option>
						<option value="0.25" {{ $partner->status_cf2_c == "0.25" ? "selected" : ""}}>25%</option>
					</select>
				</div>
			</div>
			<div class="col-md-3">
				<p>Color Card background </p>
				<div class="col-md-5" style=" padding-right: 0px;padding-left: 0px; ">
					<input class="form-control" name="status_ccb1" value="{{$partner->status_ccb1}}">
				</div>
				<div class="col-md-4" style="padding-left: 1px;padding-right: 0px;">
					<select class="form-control type" name="status_ccb1_c" required>
						<option value="1" {{ $partner->status_ccb1_c == "1" ? "selected" : ""}}>100%</option>
						<option value="0.75" {{ $partner->status_ccb1_c == "0.75" ? "selected" : ""}}>75%</option>
						<option value="0.5" {{ $partner->status_ccb1_c == "0.5" ? "selected" : ""}}>50%</option>
						<option value="0.25" {{ $partner->status_ccb1_c == "0.25" ? "selected" : ""}}>25%</option>
					</select>
				</div>
			</div>
			<div class="col-md-3">
				<p>Color background </p>
				<div class="col-md-5" style=" padding-right: 0px;padding-left: 0px; ">
					<input class="form-control" name="status_cb1" value="{{$partner->status_cb1}}">
				</div>
				<div class="col-md-4" style="padding-left: 1px;padding-right: 0px;">
					<select class="form-control type" name="status_cb1_c" required>
						<option value="1" {{ $partner->status_cb1_c == "1" ? "selected" : ""}}>100%</option>
						<option value="0.75" {{ $partner->status_cb1_c == "0.75" ? "selected" : ""}}>75%</option>
						<option value="0.5" {{ $partner->status_cb1_c == "0.5" ? "selected" : ""}}>50%</option>
						<option value="0.25" {{ $partner->status_cb1_c == "0.25" ? "selected" : ""}}>25%</option>
					</select>
				</div>
			</div>
		</div>
		
		<p class="head-title">รายละเอียดสถานะการเคลม</p>
		<div class="row">
			
			<div class="col-md-3">
				<p>Color Font 1</p>
				<div class="col-md-5" style=" padding-right: 0px;padding-left: 0px; ">
					<input class="form-control" name="detail_cf1" value="{{$partner->detail_cf1}}">
				</div>
				<div class="col-md-4" style="padding-left: 1px;padding-right: 0px;">
					<select class="form-control type" name="detail_cf1_c" required>
						<option value="1" {{ $partner->detail_cf1_c == "1" ? "selected" : ""}}>100%</option>
						<option value="0.75" {{ $partner->detail_cf1_c == "0.75" ? "selected" : ""}}>75%</option>
						<option value="0.5" {{ $partner->detail_cf1_c == "0.5" ? "selected" : ""}}>50%</option>
						<option value="0.25" {{ $partner->detail_cf1_c == "0.25" ? "selected" : ""}}>25%</option>
					</select>
				</div>
			</div>
			<div class="col-md-3">
				<p>Color Font 2</p>
				<div class="col-md-5" style=" padding-right: 0px;padding-left: 0px; ">
					<input class="form-control" name="detail_cf2" value="{{$partner->detail_cf2}}">
				</div>
				<div class="col-md-4" style="padding-left: 1px;padding-right: 0px;">
					<select class="form-control type" name="detail_cf2_c" required>
						<option value="1" {{ $partner->detail_cf2_c == "1" ? "selected" : ""}}>100%</option>
						<option value="0.75" {{ $partner->detail_cf2_c == "0.75" ? "selected" : ""}}>75%</option>
						<option value="0.5" {{ $partner->detail_cf2_c == "0.5" ? "selected" : ""}}>50%</option>
						<option value="0.25" {{ $partner->detail_cf2_c == "0.25" ? "selected" : ""}}>25%</option>
					</select>
				</div>
			</div>
			<div class="col-md-3">
				<p>Color Font 3</p>
				<div class="col-md-5" style=" padding-right: 0px;padding-left: 0px; ">
					<input class="form-control" name="detail_cf3" value="{{$partner->detail_cf3}}">
				</div>
				<div class="col-md-4" style="padding-left: 1px;padding-right: 0px;">
					<select class="form-control type" name="detail_cf3_c" required>
						<option value="1" {{ $partner->detail_cf3_c == "1" ? "selected" : ""}}>100%</option>
						<option value="0.75" {{ $partner->detail_cf3_c == "0.75" ? "selected" : ""}}>75%</option>
						<option value="0.5" {{ $partner->detail_cf3_c == "0.5" ? "selected" : ""}}>50%</option>
						<option value="0.25" {{ $partner->detail_cf3_c == "0.25" ? "selected" : ""}}>25%</option>
					</select>
				</div>
			</div>
			<div class="col-md-3">
				<p>Color background </p>
				<div class="col-md-5" style=" padding-right: 0px;padding-left: 0px; ">
					<input class="form-control" name="detail_cb" value="{{$partner->detail_cb}}">
				</div>
				<div class="col-md-4" style="padding-left: 1px;padding-right: 0px;">
					<select class="form-control type" name="detail_cb_c" required>
						<option value="1" {{ $partner->detail_cb_c == "1" ? "selected" : ""}}>100%</option>
						<option value="0.75" {{ $partner->detail_cb_c == "0.75" ? "selected" : ""}}>75%</option>
						<option value="0.5" {{ $partner->detail_cb_c == "0.5" ? "selected" : ""}}>50%</option>
						<option value="0.25" {{ $partner->detail_cb_c == "0.25" ? "selected" : ""}}>25%</option>
					</select>
				</div>
			</div>
		</div>
		
		<p class="head-title">ประวัติการเคลม</p>
		<div class="row">
			
			<div class="col-md-3">
				<p>Color Font 1</p>
				<div class="col-md-5" style=" padding-right: 0px;padding-left: 0px; ">
					<input class="form-control" name="history_cf1" value="{{$partner->history_cf1}}">
				</div>
				<div class="col-md-4" style="padding-left: 1px;padding-right: 0px;">
					<select class="form-control type" name="history_cf1_c" required>
						<option value="1" {{ $partner->history_cf1_c == "1" ? "selected" : ""}}>100%</option>
						<option value="0.75" {{ $partner->history_cf1_c == "0.75" ? "selected" : ""}}>75%</option>
						<option value="0.5" {{ $partner->history_cf1_c == "0.5" ? "selected" : ""}}>50%</option>
						<option value="0.25" {{ $partner->history_cf1_c == "0.25" ? "selected" : ""}}>25%</option>
					</select>
				</div>
			</div>
			<div class="col-md-3">
				<p>Color Font 2</p>
				<div class="col-md-5" style=" padding-right: 0px;padding-left: 0px; ">
					<input class="form-control" name="history_cf2" value="{{$partner->history_cf2}}">
				</div>
				<div class="col-md-4" style="padding-left: 1px;padding-right: 0px;">
					<select class="form-control type" name="history_cf2_c" required>
						<option value="1" {{ $partner->history_cf2_c == "1" ? "selected" : ""}}>100%</option>
						<option value="0.75" {{ $partner->history_cf2_c == "0.75" ? "selected" : ""}}>75%</option>
						<option value="0.5" {{ $partner->history_cf2_c == "0.5" ? "selected" : ""}}>50%</option>
						<option value="0.25" {{ $partner->history_cf2_c == "0.25" ? "selected" : ""}}>25%</option>
					</select>
				</div>
			</div>
			<div class="col-md-3">
				<p>Color background </p>
				<div class="col-md-5" style=" padding-right: 0px;padding-left: 0px; ">
					<input class="form-control" name="history_cb" value="{{$partner->history_cb}}">
				</div>
				<div class="col-md-4" style="padding-left: 1px;padding-right: 0px;">
					<select class="form-control type" name="history_cb_c" required>
						<option value="1" {{ $partner->history_cb_c == "1" ? "selected" : ""}}>100%</option>
						<option value="0.75" {{ $partner->history_cb_c == "0.75" ? "selected" : ""}}>75%</option>
						<option value="0.5" {{ $partner->history_cb_c == "0.5" ? "selected" : ""}}>50%</option>
						<option value="0.25" {{ $partner->history_cb_c == "0.25" ? "selected" : ""}}>25%</option>
					</select>
				</div>
			</div>
		</div>
		
		<p class="head-title"></p>
		<div class="row">
			<div class="col-md-6">
				<p>secret key</p>
				<div class="col-md-8" style="padding: 0; ">
					<div class="input-icons"> 
						<i class="fa fa-copy icon tooltip_copy" onclick="FTCopy('secret_key','Tooltip_secret')" onmouseout="outFunc('Tooltip_secret')">
							<span class="tooltiptext" id="Tooltip_secret">Copy</span>
						</i>
						<input class="form-control" value="{{$partner->secret_key}}" id="secret_key" readonly>
					</div>
				</div>
				
					<div class="col-md-2" style="padding-left: 1px;padding-right: 0px;">
						<input type="button" class="form-control btn btn-info reset-secret" value="Reset" />
					</div>
				
			</div>
			<div class="col-md-6">
				<p>Partner ID</p>
				<div class="col-md-5" style="padding: 0;">
					<div class="input-icons"> 
						<i class="fa fa-copy icon tooltip_copy" onclick="FTCopy('partner_id','Tooltip_partner')" onmouseout="outFunc('Tooltip_partner')">
							<span class="tooltiptext" id="Tooltip_partner">Copy</span>
						</i>
						<input class="form-control"  value="{{$partner->partner_id}}" id="partner_id" readonly>
					</div>
				</div>
			</div>
		</div>
		
    </div>

    <div class="box-footer">
		<span style="float: right;">
			<a href="{{url('admin/tpa_partner')}}" class="btn btn-default"> ยกเลิก </a>
			<input type="submit" name="submit" value="บันทึก" class="btn btn-success">
		</span>
		<span>
			<a class="btn btn-info Preview" id="myBtn">Preview</a>
		</span>
    </div>
	
	</form>
</div>
<!-- The Modal -->
<div id="myModal" class="modal">

  <!-- Modal content -->
  <div class="modal-content">
	<div class="head-popup">
		<span class="close">&times;</span>
	</div>
	<div id="Preview">
        <div class="row preview-container">
			
            <div class="col preview-col-first">
                <div id="ClaimStatus">
                    <div class="claimstatus-detail">
                        <div class="claim-card">
                            <div class="row claim-row">
                                <div class="col claim-header">
                                    <label class="lbClaim">วันที่เข้ารับการรักษา</label>
                                </div>
                                <div class="col claim-data">
                                    <label class="lbClaim">01/02/2020</label>
                                </div>
                            </div>
                            <div class="row claim-row">
                                <div class="col claim-header">
                                    <label class="lbClaim">ประเภทบัตร</label>
                                </div>
                                <div class="col claim-data">
                                    <label class="lbClaim">Product XXX</label>
                                </div>
                            </div>
                            <div class="row claim-row">
                                <div class="col claim-header">
                                    <label class="lbClaim">ชื่อบริษัท</label>
                                </div>
                                <div class="col claim-data">
                                    <label class="lbClaim">บริษัท XXX จำกัด</label>
                                </div>
                            </div>
                            <div class="row claim-row">
                                <div class="col claim-header">
                                    <label class="lbClaim">เวลาอัพเดตล่าสุด</label>
                                </div>
                                <div class="col claim-data">
                                    <label class="lbClaim">20/02/2020 01:05:26</label>
                                </div>
                            </div>
                            <div class="row claim-row status-row">
                                <div class="col claim-header">
                                    <label class="lbClaimBlue">อยู่ระหว่างการพิจารณา</label>
                                </div>
                                <div class="col claim-data">
                                    <div class="claim-type">
                                        <img
                                          src="{{URL::to('/images/eclaim-icon.svg')}}"
                                          class="icon"
                                        />
                                        <div class="btnDetail">
                                            <img
                                              class="claim-next"
                                              center
                                              src="{{URL::to('/images/claim-next.svg')}}"
                                              alt="center image"
                                            />
                                          </div>
                                      </div>
                                </div>
                            </div>
                        </div>
                        <div class="claim-card">
                            <div class="row claim-row">
                                <div class="col claim-header">
                                    <label class="lbClaim">วันที่เข้ารับการรักษา</label>
                                </div>
                                <div class="col claim-data">
                                    <label class="lbClaim">01/02/2020</label>
                                </div>
                            </div>
                            <div class="row claim-row">
                                <div class="col claim-header">
                                    <label class="lbClaim">ประเภทบัตร</label>
                                </div>
                                <div class="col claim-data">
                                    <label class="lbClaim">Product XXX</label>
                                </div>
                            </div>
                            <div class="row claim-row">
                                <div class="col claim-header">
                                    <label class="lbClaim">ชื่อบริษัท</label>
                                </div>
                                <div class="col claim-data">
                                    <label class="lbClaim">บริษัท XXX จำกัด</label>
                                </div>
                            </div>
                            <div class="row claim-row">
                                <div class="col claim-header">
                                    <label class="lbClaim">เวลาอัพเดตล่าสุด</label>
                                </div>
                                <div class="col claim-data">
                                    <label class="lbClaim">20/02/2020 01:05:26</label>
                                </div>
                            </div>
                            <div class="row claim-row status-row">
                                <div class="col claim-header">
                                    <label class="lbClaimBlue">อยู่ระหว่างการพิจารณา</label>
                                </div>
                                <div class="col claim-data">
                                    <div class="claim-type">
                                        <img
                                          src="{{URL::to('/images/eclaim-icon.svg')}}"
                                          class="icon"
                                        />
                                        <div class="btnDetail">
                                            <img
                                              class="claim-next"
                                              center
                                              src="{{URL::to('/images/claim-next.svg')}}"
                                              alt="center image"
                                            />
                                          </div>
                                      </div>
                                </div>
                            </div>
                        </div>
                        <div class="claim-card">
                            <div class="row claim-row">
                                <div class="col claim-header">
                                    <label class="lbClaim">วันที่เข้ารับการรักษา</label>
                                </div>
                                <div class="col claim-data">
                                    <label class="lbClaim">01/02/2020</label>
                                </div>
                            </div>
                            <div class="row claim-row">
                                <div class="col claim-header">
                                    <label class="lbClaim">ประเภทบัตร</label>
                                </div>
                                <div class="col claim-data">
                                    <label class="lbClaim">Product XXX</label>
                                </div>
                            </div>
                            <div class="row claim-row">
                                <div class="col claim-header">
                                    <label class="lbClaim">ชื่อบริษัท</label>
                                </div>
                                <div class="col claim-data">
                                    <label class="lbClaim">บริษัท XXX จำกัด</label>
                                </div>
                            </div>
                            <div class="row claim-row">
                                <div class="col claim-header">
                                    <label class="lbClaim">เวลาอัพเดตล่าสุด</label>
                                </div>
                                <div class="col claim-data">
                                    <label class="lbClaim">20/02/2020 01:05:26</label>
                                </div>
                            </div>
                            <div class="row claim-row status-row">
                                <div class="col claim-header">
                                    <label class="lbClaimBlue">อยู่ระหว่างการพิจารณา</label>
                                </div>
                                <div class="col claim-data">
                                    <div class="claim-type">
                                        <img
                                          src="{{URL::to('/images/eclaim-icon.svg')}}"
                                          class="icon"
                                        />
                                        <div class="btnDetail">
                                            <img
                                              class="claim-next"
                                              center
                                              src="{{URL::to('/images/claim-next.svg')}}"
                                              alt="center image"
                                            />
                                          </div>
                                      </div>
                                </div>
                            </div>
                        </div>
                        <div class="claim-card">
                            <div class="row claim-row">
                                <div class="col claim-header">
                                    <label class="lbClaim">วันที่เข้ารับการรักษา</label>
                                </div>
                                <div class="col claim-data">
                                    <label class="lbClaim">01/02/2020</label>
                                </div>
                            </div>
                            <div class="row claim-row">
                                <div class="col claim-header">
                                    <label class="lbClaim">ประเภทบัตร</label>
                                </div>
                                <div class="col claim-data">
                                    <label class="lbClaim">Product XXX</label>
                                </div>
                            </div>
                            <div class="row claim-row">
                                <div class="col claim-header">
                                    <label class="lbClaim">ชื่อบริษัท</label>
                                </div>
                                <div class="col claim-data">
                                    <label class="lbClaim">บริษัท XXX จำกัด</label>
                                </div>
                            </div>
                            <div class="row claim-row">
                                <div class="col claim-header">
                                    <label class="lbClaim">เวลาอัพเดตล่าสุด</label>
                                </div>
                                <div class="col claim-data">
                                    <label class="lbClaim">20/02/2020 01:05:26</label>
                                </div>
                            </div>
                            <div class="row claim-row status-row">
                                <div class="col claim-header">
                                    <label class="lbClaimBlue">อยู่ระหว่างการพิจารณา</label>
                                </div>
                                <div class="col claim-data">
                                    <div class="claim-type">
                                        <img
										  src="{{URL::to('/images/eclaim-icon.svg')}}"
                                          class="icon"
                                        />
                                        <div class="btnDetail">
                                            <img
                                              class="claim-next"
                                              center
                                              src="{{URL::to('/images/claim-next.svg')}}"
                                              alt="center image"
                                            />
                                          </div>
                                      </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col preview-col">
                <div id="ClaimStatus">
                    <div class="claimstatus-detail">
                        <div class="claim-card">
                            <div class="row claim-row">
                                <div class="col claim-header">
                                    <label class="lbClaim detail">วันที่เข้ารับการรักษา</label>
                                </div>
                                <div class="col claim-data">
                                    <label class="lbClaim detail">01/02/2020</label>
                                </div>
                            </div>
                            <div class="row claim-row">
                                <div class="col claim-header">
                                    <label class="lbClaim detail">ประเภทบัตร</label>
                                </div>
                                <div class="col claim-data">
                                    <label class="lbClaim detail">Product XXX</label>
                                </div>
                            </div>
                            <div class="row claim-row">
                                <div class="col claim-header">
                                    <label class="lbClaim detail">ชื่อบริษัท</label>
                                </div>
                                <div class="col claim-data">
                                    <label class="lbClaim detail">บริษัท XXX จำกัด</label>
                                </div>
                            </div>
                            <div class="row claim-row">
                                <div class="col claim-header">
                                    <label class="lbClaim detail">เลขที่กรมธรรม์/เลขหน้าบัตร</label>
                                </div>
                                <div class="col claim-data">
                                    <label class="lbClaim detail">44553</label>
                                </div>
                            </div>
                        </div>
                        <div class="claim-status">
                            <div class="row status-list">
                                <div class="col status-badge">
                                    <img
                                        class="badge-icon"
                                        center
										src="{{URL::to('/images/claim-complete.svg')}}"
                                        alt="center image"
                                    />
                                    <div class="status-line"></div>
                                </div>
                                <div class="col status-detail">
                                    <label class="lbStatus">รับแจ้งการขอใช้สิทธิ์</label>
                                    <label class="lbDate">20/02/2020 | 01:05</label>
                                </div>
                            </div>
                            <div class="row status-list">
                                <div class="col status-badge">
                                    <img
                                        class="badge-icon"
                                        center
										src="{{URL::to('/images/claim-success.svg')}}"
                                        alt="center image"
                                    />
                                    <div class="status-line"></div>
                                </div>
                                <div class="col status-detail">
                                    <label class="lbStatus">ได้รับเอกสารแจ้งกลับบ้านแล้ว</label>
                                    <label class="lbDate">25/02/2020 | 01:05</label>
                                </div>
                            </div>
                            <div class="row status-list">
                                <div class="col status-badge">
                                    <div class="badge-none"></div>
                                    <div class="status-line"></div>
                                </div>
                                <div class="col status-detail">
                                    <label class="lbStatus">อยู่ระหว่างการพิจารณา</label>
                                </div>
                            </div>
                            <div class="row status-list">
                                <div class="col status-badge">
                                    <div class="badge-none"></div>
                                    <div class="status-line"></div>
                                </div>
                                <div class="col status-detail">
                                    <label class="lbStatus">อยู่ระหว่างรอเอกสารเพิ่มเติม</label>
                                </div>
                            </div>
                            <div class="row status-list">
                                <div class="col status-badge">
                                    <div class="badge-none"></div>
                                </div>
                                <div class="col status-detail">
                                    <label class="lbStatus">พิจารณาเรียบร้อยแล้ว</label>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col preview-col-last">
                <div id="ClaimHistory">
                    <div class="claim-history">
                        <div class="claim-list">
                            <div class="row table-claim">
                                <div class="col detail-col">
                                    <label class="lbClaim">โรงพยาบาลพญาไท 1</label>
                                    <label class="lbClaim">วันที่เข้ารับการรักษา : 20/10/2018</label>
                                    <label class="lbClaim">มูลค่าที่คุ้มครอง : 595 บาท</label>
                                </div>
                                <div class="col status-col">
                                    <div class="claimStatus">
                                        <img class="status" 
											src="{{URL::to('/images/claim-success.svg')}}"
										/>
                                        <label class="lbStatus">อนุมัติ</label>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="claim-list">
                            <div class="row table-claim">
                                <div class="col detail-col">
                                    <label class="lbClaim">โรงพยาบาลพญาไท 1</label>
                                    <label class="lbClaim">วันที่เข้ารับการรักษา : 20/10/2018</label>
                                    <label class="lbClaim">มูลค่าที่คุ้มครอง : -</label>
                                </div>
                                <div class="col status-col">
                                    <div class="claimStatus">
                                        <img class="status" 
											src="{{URL::to('/images/claim-reject.svg')}}"
										/>
                                        <label class="lbStatus">ไม่อนุมัติ</label>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="claim-list">
                            <div class="row table-claim">
                                <div class="col detail-col">
                                    <label class="lbClaim">โรงพยาบาลพญาไท 1</label>
                                    <label class="lbClaim">วันที่เข้ารับการรักษา : 20/10/2018</label>
                                    <label class="lbClaim">มูลค่าที่คุ้มครอง : 595 บาท</label>
                                </div>
                                <div class="col status-col">
                                    <div class="claimStatus">
                                        <img class="status" 
											src="{{URL::to('/images/claim-pending.svg')}}"
										/>
                                        <label class="lbStatus">กำลังดำเนินการ</label>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="claim-list">
                            <div class="row table-claim">
                                <div class="col detail-col">
                                    <label class="lbClaim">โรงพยาบาลพญาไท 1</label>
                                    <label class="lbClaim">วันที่เข้ารับการรักษา : 20/10/2018</label>
                                    <label class="lbClaim">มูลค่าที่คุ้มครอง : 595 บาท</label>
                                </div>
                                <div class="col status-col">
                                    <div class="claimStatus">
                                        <img class="status" 
											src="{{URL::to('/images/claim-success.svg')}}"
										/>
                                        <label class="lbStatus">อนุมัติ</label>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="claim-list">
                            <div class="row table-claim">
                                <div class="col detail-col">
                                    <label class="lbClaim">โรงพยาบาลพญาไท 1</label>
                                    <label class="lbClaim">วันที่เข้ารับการรักษา : 20/10/2018</label>
                                    <label class="lbClaim">มูลค่าที่คุ้มครอง : -</label>
                                </div>
                                <div class="col status-col">
                                    <div class="claimStatus">
                                        <img class="status" 
											src="{{URL::to('/images/claim-reject.svg')}}"
										/>
                                        <label class="lbStatus">ไม่อนุมัติ</label>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="claim-list">
                            <div class="row table-claim">
                                <div class="col detail-col">
                                    <label class="lbClaim">โรงพยาบาลพญาไท 1</label>
                                    <label class="lbClaim">วันที่เข้ารับการรักษา : 20/10/2018</label>
                                    <label class="lbClaim">มูลค่าที่คุ้มครอง : 595 บาท</label>
                                </div>
                                <div class="col status-col">
                                    <div class="claimStatus">
                                        <img class="status" 
											src="{{URL::to('/images/claim-pending.svg')}}"
										/>
                                        <label class="lbStatus">กำลังดำเนินการ</label>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="claim-list">
                            <div class="row table-claim">
                                <div class="col detail-col">
                                    <label class="lbClaim">โรงพยาบาลพญาไท 1</label>
                                    <label class="lbClaim">วันที่เข้ารับการรักษา : 20/10/2018</label>
                                    <label class="lbClaim">มูลค่าที่คุ้มครอง : 595 บาท</label>
                                </div>
                                <div class="col status-col">
                                    <div class="claimStatus">
                                        <img class="status"  
											src="{{URL::to('/images/claim-success.svg')}}"
										/>
                                        <label class="lbStatus">อนุมัติ</label>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="claim-list">
                            <div class="row table-claim">
                                <div class="col detail-col">
                                    <label class="lbClaim">โรงพยาบาลพญาไท 1</label>
                                    <label class="lbClaim">วันที่เข้ารับการรักษา : 20/10/2018</label>
                                    <label class="lbClaim">มูลค่าที่คุ้มครอง : -</label>
                                </div>
                                <div class="col status-col">
                                    <div class="claimStatus">
                                        <img class="status" 
											src="{{URL::to('/images/claim-reject.svg')}}"
										/>
                                        <label class="lbStatus">ไม่อนุมัติ</label>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.5.4/dist/umd/popper.min.js" integrity="sha384-q2kxQ16AaE6UbzuKqyBE9/u/KzioAlnx2maXQHiDX9d4/zp8Ok3f+M7DPm+Ib6IU" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/js/bootstrap.min.js" integrity="sha384-pQQkAEnwaBkjpqZ8RU1fF1AKtTcHJwFl3pblpTlHXybJjHpMYo79HY3hIi4NKxyj" crossorigin="anonymous"></script>
	
  </div>

</div>
<script>
	
	function FTCopy(id, tooltipname) {
	  var copyText = document.getElementById(id);
	  copyText.select();
	  copyText.setSelectionRange(0, 99999);
	  document.execCommand("copy");
	  copyText.blur();
	  var tooltip = document.getElementById(tooltipname);
	  //tooltip.innerHTML = "Copied: " + copyText.value;
	  tooltip.innerHTML = "Copied";
	}

	function outFunc(tooltipname) {
	  var tooltip = document.getElementById(tooltipname);
	  //alert(0);
	  tooltip.innerHTML = "Copy";
	}
	
	function rgbaCol(color, cpt){
		var rgbaCol = 'rgba(' + parseInt(color.slice(-6,-4),16)
			+ ',' + parseInt(color.slice(-4,-2),16)
			+ ',' + parseInt(color.slice(-2),16)
			+','+cpt+')';
			
		return rgbaCol;
	}
	// Get the modal
	var modal = document.getElementById("myModal");

	// Get the button that opens the modal
	var btn = document.getElementById("myBtn");

	// Get the <span> element that closes the modal
	var span = document.getElementsByClassName("close")[0];

	// When the user clicks the button, open the modal 
	btn.onclick = function() {
	
		//สถานะเคลม
		var status_cf1 = $('input[name="status_cf1"]').val();
		var status_cf1_c = $('select[name="status_cf1_c"]').val();
		var status_cf2 = $('input[name="status_cf2"]').val();
		var status_cf2_c = $('select[name="status_cf2_c"]').val();
		var status_ccb1 = $('input[name="status_ccb1"]').val();
		var status_ccb1_c = $('select[name="status_ccb1_c"]').val();
		var status_cb1 = $('input[name="status_cb1"]').val();
		var status_cb1_c = $('select[name="status_cb1_c"]').val();
		//รายละเอียดสถานะการเคลม
		var detail_cf1 = $('input[name="detail_cf1"]').val();
		var detail_cf1_c = $('select[name="detail_cf1_c"]').val();
		var detail_cf2 = $('input[name="detail_cf2"]').val();
		var detail_cf2_c = $('select[name="detail_cf2_c"]').val();
		var detail_cf3 = $('input[name="detail_cf3"]').val();
		var detail_cf3_c = $('select[name="detail_cf3_c"]').val();
		var detail_cb = $('input[name="detail_cb"]').val();
		var detail_cb_c = $('select[name="detail_cb_c"]').val();
		
		//ประวัติการเคลม
		var history_cf1 = $('input[name="history_cf1"]').val();
		var history_cf1_c = $('select[name="history_cf1_c"]').val();
		var history_cf2 = $('input[name="history_cf2"]').val();
		var history_cf2_c = $('select[name="history_cf2_c"]').val();
		var history_cb = $('input[name="history_cb"]').val();
		var history_cb_c = $('select[name="history_cb_c"]').val();
		
		$( ".preview-col-first" ).children().children().children().children().children().children( ".lbClaim" ).css( "color", rgbaCol(status_cf1, status_cf1_c) );
		$( ".preview-col-first" ).children().children().children().children().children().children( ".lbClaimBlue" ).css( "color", rgbaCol(status_cf2, status_cf2_c) );
		$( ".preview-col-first" ).children().children().children( ".claim-card" ).css( "background-color", rgbaCol(status_ccb1, status_ccb1_c) );
		$( ".preview-col-first" ).css( "background-color", rgbaCol(status_cb1, status_cb1_c) );
		
		$( ".preview-col" ).children().children().children().children().children().children( ".lbClaim" ).css( "color", rgbaCol(detail_cf1, detail_cf1_c) );
		$( ".preview-col" ).children().children().children().children().children().children( ".lbStatus" ).css( "color", rgbaCol(detail_cf2, detail_cf2_c) );
		$( ".preview-col" ).children().children().children().children().children().children( ".lbDate" ).css( "color", rgbaCol(detail_cf3, detail_cf3_c) );
		$( ".preview-col" ).css( "background-color", rgbaCol(detail_cb, detail_cb_c) );
		
		$( ".preview-col-last" ).children().children().children().children().children().children( ".lbClaim" ).css( "color", rgbaCol(history_cf1, history_cf1_c) );
		$( ".preview-col-last" ).children().children().children().children().children().children().children( ".lbStatus" ).css( "color", rgbaCol(history_cf2, history_cf2_c) );
		$( ".preview-col-last" ).css( "background-color", rgbaCol(history_cb, history_cb_c) );
		
		modal.style.display = "block";

	}
	

	// When the user clicks on <span> (x), close the modal
	span.onclick = function() {
	  modal.style.display = "none";
	}

	// When the user clicks anywhere outside of the modal, close it
	window.onclick = function(event) {
	  if (event.target == modal) {
		modal.style.display = "none";
	  }
	}
</script>
@endsection
