<!-- First you need to extend the CB layout -->
@extends('crudbooster::themes.adminlte.layout.layout')
@section('content')
<!-- Your custom  HTML goes here -->
<style>
    img# {
        width: 180px;
        /* height: 100px */
        height: auto;
    }

    span.error {
        color: red;
        font-style: italic
    }

    div.error {
        display: none
    }

    input {
        border: 1px solid #000
    }

    input.checkbox {
        border: none
    }

    input:focus {
        border: 1px solid #000
    }

    input.error {
        border: 1px solid red
    }

    .error .select2-choice.select2-default,
    .error .select2-choices {
        color: #a94442;
        border-color: #a94442;
        -webkit-box-shadow: inset 0 1px 1px rgba(0, 0, 0, .075);
        box-shadow: inset 0 1px 1px rgba(0, 0, 0, .075);
    }

    .error:focus,
    .error .select2-choice.select2-defaultLfocus,
    .error .select2-choicesLfocus {
        border-color: #843534;
        -webkit-box-shadow: inset 0 1px 1px rgba(0, 0, 0, .075), 0 0 6px #ce8483;
        box-shadow: inset 0 1px 1px rgba(0, 0, 0, .075), 0 0 6px #ce8483;
    }

    .select2-container .select2-choices .select2-search-field input,
    .select2-container .select2-choice,
    .select2-container .select2-choices,
    .error {
        border-radius: 1px;
    }

</style>

<div class="box box-info">
    <div class="box-header with-border">
        <h1 class="box-title" style="font-size: 20px;margin-top:5px">แจ้งเตือน</h1>

    </div>
    <form method="POST" enctype="multipart/form-data" id="WebForm" accept-charset="utf-8">
        <div class="box-body">
            <div class="row">
                <div class="col-md-2">
                    <div class="form-group ">
                        <label>ประเภทการแจ้งเตือน</label>
                        <select class="form-control" name="notification_type">
                            <option value="all">ทั้งหมด</option>
                            <option value="target">ระบุ</option>
                        </select>
                    </div>
                </div>

                <div class="col-md-2 target">
                    <div class="form-group ">
                        <label>ตัวเลือกการส่ง</label>
                        <select class="form-control" name="target_criteria">
                            <option value="gender">ตามเพศ</option>
                            <option value="age_range">ช่วงอายุ</option>
                        </select>
                    </div>
                </div>

                <div class="col-md-2 target age_range">
                    <div class="form-group ">
                        <label>ช่วงอายุ</label>
                        <select class="form-control status" name="age_range">
                            @foreach ($age_range as $row)
                            <option value="{{$row->id}}">{{$row->name}}</option>
                            @endforeach
                        </select>
                    </div>
                </div>

                <div class="col-md-2 target gender">
                    <div class="form-group">
                        <label>เพศ</label>
                        <select class="form-control status" name="gender">
                            @foreach ($gender as $id => $name)
                            <option value="{{$id}}">{{$name}}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                
            </div>
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group ">
                        <label>หัวข้อภาษาไทย</label>
                        <input type="text" placeholder="หัวข้อภาษาไทย" class="form-control" name="title_th"
                            value="">
                    </div>

                    <div class="form-group">
                        <label>รายละเอียดภาษาไทย</label>
                        <textarea class="form-control" placeholder="รายละเอียดภาษาไทย" class="form-control"
                            style="height: 150px;" name="body_th"></textarea>
                    </div>
                </div>

                <div class="col-md-6">
                    <div class="form-group ">
                        <label>หัวข้อภาษาอังกฤษ</label>
                        <input type="text" placeholder="หัวข้อภาษาอังกฤษ" class="form-control" name="title_en">
                    </div>

                    <div class="form-group">
                        <label>รายละเอียดภาษาอังกฤษ</label>
                        <textarea class="form-control" placeholder="รายละเอียดภาษาอังกฤษ" class="form-control"
                            style="height: 150px;" name="body_en"></textarea>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-6">
                    <div class="form-group ">
                        <label>URL</label>
                        <input type="text" placeholder="URL" class="form-control" name="url">
                    </div>
                </div>
            </div>

            


        <div class="box-footer">
            <div style="text-align: right">
                <a href="{{url('admin/notifications')}}" class="btn btn-default"> ยกเลิก </a>
                <input type="submit" name="submit" value="ส่ง" class="btn btn-info">

            </div>
        </div>

    </form>

</div>
<div class="modal fade" id="imagemodal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-body">
                <a href="javascript:void(0);" class="close" data-dismiss="modal"><i class="fa fa-times"
                        aria-hidden="true"></i></a>
                <img src="" class="imagepreview" style="width: 100%;">
            </div>
        </div>
    </div>
</div>

@endsection

