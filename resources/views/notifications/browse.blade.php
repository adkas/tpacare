<!-- First you need to extend the CB layout -->
@extends('crudbooster::themes.adminlte.layout.layout')
@section('content')

<style>
    table#table-module td:first-child {
        width: 80px;
        text-align: center;
    }

    table#table-module td:nth-child(3) {
        width: 150px;
        text-align: left;
    }

    table#table-module td:nth-child(4) {
        width: 120px;
        text-align: center;
    }

    table#table-module td:nth-child(5) {
        width: 140px;
        text-align: center;
    }

    table#table-module th {
        text-align: center;
        font-weight: normal;
    }

    table#table-module td:last-child {
        width: 100px;
        text-align: center;
    }

</style>
<!-- Your custom  HTML goes here -->
<div class="box box-default" data-select2-id="16" style="display: none">
    <div class="box-header with-border">
        <h3 class="box-title">ค้นหา</h3>

        <div class="box-tools pull-right">
            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
        </div>
    </div>
    <!-- /.box-header -->
    <div class="box-body">
        <form method="GET" action="?">
            <div class="row">
                <div class="col-md-3">
                    <div class="form-group">
                        <label>วันที่เข้ารับการรักษาตั้งแต่วันที่</label>
                        <input type="text" placeholder="ตั้งแต่วันที่" class="form-control datepicker" name="start_date"
                            value="{{$start_date}}">
                    </div>
                    <!-- /.form-group -->
                </div>
                <!-- /.col -->

                <div class="col-md-3">
                    <div class="form-group">
                        <label>ถึงวันที่</label>
                        <input type="text" placeholder="ถึงวันที่" class="form-control datepicker" name="end_date"
                            value="{{$end_date}}">
                    </div>
                    <!-- /.form-group -->
                </div>
                <!-- /.col -->

                <div class="col-md-4">
                    <div class="form-group">
                        <label>ชื่อผู้เอาประกัน</label>
                        <input type="text" placeholder="ชื่อผู้เอาประกัน" class="form-control" name="insurance_name"
                            value="{{$insurance_name}}">
                    </div>
                    <!-- /.form-group -->
                </div>
                <!-- /.col -->

                <div class="col-md-2">
                    <div class="form-group">
                        <input type="submit" name="submit" value="ค้นหา" class="btn btn-info"
                            style="margin-top: 25px; width:100%;">
                    </div>
                    <!-- /.form-group -->
                </div>
                <!-- /.col -->

            </div>
            <!-- /.row -->
        </form>
    </div>
</div>

<div class="box">
    <div class="box-header">

        <h1 class="box-title" style="font-size: 20px;margin-top:5px">การแจ้งเตือน</h1>

        <div class="box-tools pull-right">
            <a href="{{ url('admin/notifications/send') }}" id='btn_add_new_data' class="btn btn-sm btn-success "
                title="{{ cbLang("การแจ้งเตือน") }}">
                <i class="fa fa-bell" aria-hidden="true"></i> {{ __("การแจ้งเตือน") }}
            </a>
        </div>



    </div>

    <div class="box-body table-responsive">

        <table id="table-module" class="table table-hover table-striped table-bordered">
            <thead>
                <tr>
                    <th>ลำดับ</th>
                    <th>หัวข้อ</th>
                    <th>ตัวเลือกการส่ง</th>
                    <th>หมวดหมู่</th>
                    <th>วันที่ส่ง</th>
                    <th>กระบวนการ</th>
                </tr>
            </thead>
            <tbody>
                @if(count($result) > 0)
                @foreach($record as $row)
                <tr>
                    <td>{{$row->no}}</td>
                    <td>{{$row->title}}</td>
                    <td>{{$row->target_criteria}} ({{$row->target_value}})</td>
                    <td>{{$row->category}}</td>
                    <td>{{$row->created_at}}</td>
                    <td>
                        <!-- To make sure we have read access, wee need to validate the privilege -->
                        @if(module()->canRead())
                        <a class='btn btn-info btn-xs' href='{{url("admin/notifications/detail/$row->id")}}'><i
                                class="fa fa-eye" aria-hidden="true"></i></a>
                        @endif

                        @if(module()->canUpdate())
                        {{-- <a class='btn btn-warning btn-xs' href='{{url("admin/hospital/edit/$row->code")}}'><i
                            class="fa fa-pencil-square-o" aria-hidden="true"></i></a> --}}
                        @endif

                        @if(module()->canDelete())
                        {{-- <a class='btn btn-danger btn-xs' href='{{url("admin/hospital/delete/$row->code")}}'><i
                            class="fa fa-times-circle" aria-hidden="true"></i></a> --}}
                        @endif
                    </td>
                </tr>
                @endforeach
                @else
                <tr class="warning">
                    <td colspan="4" style="text-align: center">
                        <i class="fa fa-table"></i> There is no data yet
                    </td>
                </tr>
                @endif
            </tbody>
        </table>

    </div>

</div>


<!-- ADD A PAGINATION -->
<p>{!! urldecode(str_replace("/?","?",$result->appends(Request::all())->render())) !!}</p>
@endsection
