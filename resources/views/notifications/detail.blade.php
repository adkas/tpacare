<!-- First you need to extend the CB layout -->
@extends('crudbooster::themes.adminlte.layout.layout')
@section('content')
<!-- Your custom  HTML goes here -->
<div class="box box-info">
    <div class="box-header with-border">
        <h1 class="box-title"><i class="fa fa-eye"></i> รายละเอียดการแจ้งเตือน</h1>
    </div>
    <div class="box-body">
        <div class="table-responsive">
            <table id="table-detail" class="table table-striped">
                <tbody>
                    <tr>
                        <th width="25%">ประเภทการแจ้งเตือน</th>
                        <td>{{$notification_type}}</td>
                    </tr>

                    <tr>
                        <th width="25%">ตัวเลือกการส่ง</th>
                        <td>{{$target_criteria}} ({{$target_value}})</td>
                    </tr>

                    <tr>
                        <th width="25%">หัวข้อภาษาไทย</th>
                        <td>{{$title_th}}</td>
                    </tr>

                    <tr>
                        <th width="25%">รายละเอียดภาษาไทย</th>
                        <td>{{$body_th}}</td>
                    </tr>

                    <tr>
                        <th width="25%">หัวข้อภาษาอังกฤษ</th>
                        <td>{{$title_en}}</td>
                    </tr>

                    <tr>
                        <th width="25%">รายละเอียดภาษาอังกฤษ</th>
                        <td>{{$body_en}}</td>
                    </tr>

                    <tr>
                        <th width="25%">URL</th>
                        <td>{{$url}}</td>
                    </tr>

                    <tr>
                        <th width="25%">Created At</th>
                        <td>{{$created_at}} </td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
    <div class="box-footer">
        <div style="text-align: left">
            <a href="{{url('admin/notifications')}}" class="btn btn-info"> ย้อนกลับ </a>

        </div>
    </div>
</div>
@endsection
