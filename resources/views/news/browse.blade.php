<!-- First you need to extend the CB layout -->
@extends('crudbooster::themes.adminlte.layout.layout')
@section('content')
<!-- Your custom  HTML goes here -->


<div class="box">
  <div class="box-header">

    <h1 class="box-title" style="font-size: 20px;margin-top:5px">Browse Data</h1>

    <div class="box-tools pull-right">
      <a href="{{ module()->addURL()."?ref=".makeReferalUrl()  }}{{ isset($subModuleKey)?"&sub_module=".$subModuleKey:null }}" id='btn_add_new_data' class="btn btn-sm btn-success " title="{{ cbLang("add").' '.cbLang('data') }}">
        <i class="fa fa-plus-circle"></i> {{ cbLang("add").' '.cbLang('data') }}
      </a>
    </div>



  </div>

  <div class="box-body table-responsive">

    <table id="table-module" class="table table-hover table-striped table-bordered">
      <thead>
        <tr>
          <th>Name</th>
          <th>Description</th>
          <th>Price</th>
        </tr>
      </thead>
      <tbody>
        @foreach($result as $row)
        <tr>
          <td>{{$row->no}}</td>
          <td>{{$row->title}}</td>
          <td>{{$row->description}}</td>
          <td>
            <!-- To make sure we have read access, wee need to validate the privilege -->
            @if(module()->canUpdate())
            <a class='btn btn-warning btn-sm' href='{{url("admin/news/edit/$row->id")}}'><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Edit</a>
            @endif

            @if(module()->canDelete())
            <a class='btn btn-danger btn-sm' href='{{url("admin/news/delete/$row->id")}}'><i class="fa fa-times-circle" aria-hidden="true"></i> Delete</a>
            @endif
          </td>
        </tr>
        @endforeach
      </tbody>
    </table>

  </div>

</div>


<!-- ADD A PAGINATION -->
<p>{!! urldecode(str_replace("/?","?",$result->appends(Request::all())->render())) !!}</p>
@endsection