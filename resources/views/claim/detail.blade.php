<!-- First you need to extend the CB layout -->
@extends('crudbooster::themes.adminlte.layout.layout')
@section('content')
<!-- Your custom  HTML goes here -->
<div class="box box-info">
    <div class="box-header with-border">
        <h1 class="box-title"><i class="fa fa-eye"></i> รายละเอียดการเคลม</h1>
    </div>
    <div class="box-body">
        <div class="table-responsive">
            <table id="table-detail" class="table table-striped">
                <tbody>
                    <tr>
                        <th width="25%">รหัสพนักงาน</th>
                        <td>{{$staff_no}}</td>
                    </tr>

                    <tr>
                        <th width="25%">ชื่อผู้เข้ารับการรักษา</th>
                        <td>{{$name}} {{$surname}}</td>
                    </tr>

                    <tr>
                        <th width="25%">หมายเลขโทรศัพท์มือถือ</th>
                        <td>{{$mobile}}</td>
                    </tr>

                    <tr>
                        <th width="25%">อีเมล์</th>
                        <td>{{$email}}</td>
                    </tr>

                    <tr>
                        <th width="25%">หมายเลขกรมธรรม์</th>
                        <td>{{$policy_no}}</td>
                    </tr>

                    <tr>
                        <th width="25%">บริษัทประกัน</th>
                        <td>{{$insurer_name}}</td>
                    </tr>

                    <tr>
                        <th width="25%">ประเภทของการเข้ารับรักษา</th>
                        <td>{{$claim_type}}</td>
                    </tr>

                    <tr>
                        <th width="25%">ประเภทผลประโยชน์</th>
                        <td>{{$admited_type}}</td>
                    </tr>

                    <tr>
                        <th width="25%">ประเภทความคุ้มครอง</th>
                        <td>{{$card_type}}</td>
                    </tr>

                    <tr>
                        <th width="25%">วันที่เข้ารับรักษา</th>
                        <td>{{$admited_date}}</td>
                    </tr>

                    <tr>
                        <th width="25%">โรค/อาการ/สาเหตุ</th>
                        <td>{{$symptom}} </td>
                    </tr>
                    <tr>
                        <th with="25%">สถานที่เข้ารับการรักษา</th>
                        <td>{{$provider_name}} </td>
                    </tr>
                    <tr>
                        <th width="25%">จำนวนเงินตามใบเสร็จรับเงิน</th>
                        <td>{{$medical_expense}} </td>
                    </tr>

                    <tr>
                        <th width="25%">Created At</th>
                        <td>{{$created_at}} </td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
    <div class="box-footer">
        <div style="text-align: left">
            <a href="{{url('admin/claim')}}" class="btn btn-info"> ย้อนกลับ </a>

        </div>
    </div>
</div>
@endsection
