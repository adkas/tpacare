<!-- First you need to extend the CB layout -->
@extends('crudbooster::themes.adminlte.layout.layout')
@section('content')
<!-- Your custom  HTML goes here -->
<div class="box box-default" data-select2-id="16">
    <div class="box-header with-border">
        <h3 class="box-title">ค้นหา</h3>

        <div class="box-tools pull-right">
            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
        </div>
    </div>
    <!-- /.box-header -->
    <form method="GET" action="?" id="SearchForm">
        <div class="box-body">

            <div class="row">

                <div class="col-md-3">
                    <div class="form-group">
                        <label>รหัสพนักงาน</label>
                        <input type="text" placeholder="รหัสพนักงาน" class="form-control" name="staff_no"
                            value="{{$staff_no}}">
                    </div>
                    <!-- /.form-group -->
                </div>
                <!-- /.col -->

                <div class="col-md-3">
                    <div class="form-group">
                        <label>ชื่อผู้เอาประกัน</label>
                        <input type="text" placeholder="ชื่อผู้เอาประกัน" class="form-control" name="insurance_name"
                            value="{{$insurance_name}}">
                    </div>
                    <!-- /.form-group -->
                </div>
                <!-- /.col -->

                <div class="col-md-3">
                    <div class="form-group">
                        <label>ชื่อผู้บริษัทประกัน</label>
                        <input type="text" placeholder="ชื่อผู้บริษัทประกัน" class="form-control" name="insurer_name"
                            value="{{$insurer_name}}">
                    </div>
                    <!-- /.form-group -->
                </div>
                <!-- /.col -->

                <div class="col-md-3">
                    <div class="form-group">
                        <label>ประเภทผลประโยชน์</label>
                        <input type="text" placeholder="ประเภทผลประโยชน์
" class="form-control" name="admited_type" value="{{$admited_type}}">
                    </div>
                    <!-- /.form-group -->
                </div>
                <!-- /.col -->

            </div>
            <!-- /.row -->

            <div class="row">
                <div class="col-md-3">
                    <div class="form-group">
                        <label>วันที่เข้ารับการรักษา</label>
                        <input type="text" placeholder="ตั้งแต่วันที่" class="form-control datepicker" name="start_date"
                            value="{{$start_date}}">
                    </div>
                    <!-- /.form-group -->
                </div>
                <!-- /.col -->

                <div class="col-md-3">
                    <div class="form-group">
                        <label>ถึงวันที่</label>
                        <input type="text" placeholder="ถึงวันที่" class="form-control datepicker" name="end_date"
                            value="{{$end_date}}">
                    </div>
                    <!-- /.form-group -->
                </div>
                <!-- /.col -->

                <div class="col-md-3">
                    <div class="form-group">
                        <label>วันที่เคลม</label>
                        <input type="text" placeholder="ตั้งแต่วันที่" class="form-control datepicker"
                            name="claim_start_date" value="{{$claim_start_date}}">
                    </div>
                    <!-- /.form-group -->
                </div>
                <!-- /.col -->

                <div class="col-md-3">
                    <div class="form-group">
                        <label>ถึงวันที่</label>
                        <input type="text" placeholder="ถึงวันที่" class="form-control datepicker" name="claim_end_date"
                            value="{{$claim_end_date}}">
                    </div>
                    <!-- /.form-group -->
                </div>
                <!-- /.col -->

            </div>
            <!-- /.row -->


        </div>

        <div class="box-footer">

            <div style="text-align: right">
                <input type="submit" id="submit" value="ค้นหา" class="btn btn-info" style="width:120px;">

            </div>


        </div><!-- /.box-footer-->

    </form>
</div>

<div class="box">
    <div class="box-header">

        <h1 class="box-title" style="font-size: 20px;margin-top:5px">ข้อมูลการเคลม</h1>

        <div class="box-tools pull-right">
            <button type="button"
                id='btn-export' class="btn btn-sm btn-success " title="{{ cbLang("export") }}">
                <i class="fa fa-download" aria-hidden="true"></i> {{ __("Export") }}
            </button>
        </div>



    </div>

    <div class="box-body table-responsive">

        <table id="table-module" class="table table-hover table-striped table-bordered">
            <thead>
                <tr>
                    <th>ลำดับ</th>
                    <th>ชื่อผู้เอาประกัน</th>
                    <th>บริษัทประกัน</th>
                    <th>วันที่รักษา</th>
                    <th>สถานที่รักษา</th>
                    <th>จำนวนเงิน</th>
                    <th>วันที่เคลม</th>
                    <th>กระบวนการ</th>
                </tr>
            </thead>
            <tbody>
                @if(count($result) > 0)
                @foreach($record as $row)
                <tr>
                    <td>{{$row->no}}</td>
                    <td>{{$row->name}}</td>
                    <td>{{$row->insurer_name}}</td>
                    <td>{{$row->admited_date}}</td>
                    <td>{{$row->provider_name}}</td>
                    <td>{{$row->medical_expense}}</td>
                    <td>{{$row->created_at}}</td>
                    <td>
                        <!-- To make sure we have read access, wee need to validate the privilege -->
                        @if(module()->canRead())
                        <a class='btn btn-info btn-xs' href='{{url("admin/claim/detail/$row->id")}}'><i
                                class="fa fa-eye" aria-hidden="true"></i></a>
                        @endif

                        @if(module()->canUpdate())
                        {{-- <a class='btn btn-warning btn-xs' href='{{url("admin/hospital/edit/$row->code")}}'><i
                            class="fa fa-pencil-square-o" aria-hidden="true"></i></a> --}}
                        @endif

                        @if(module()->canDelete())
                        {{-- <a class='btn btn-danger btn-xs' href='{{url("admin/hospital/delete/$row->code")}}'><i
                            class="fa fa-times-circle" aria-hidden="true"></i></a> --}}
                        @endif
                    </td>
                </tr>
                @endforeach
                @else
                <tr class="warning">
                    <td colspan="6" style="text-align: center">
                        <i class="fa fa-table"></i> There is no data yet
                    </td>
                </tr>
                @endif
            </tbody>
        </table>

    </div>

</div>


<!-- ADD A PAGINATION -->
<p>{!! urldecode(str_replace("/?","?",$result->appends(Request::all())->render())) !!}</p>
@endsection
