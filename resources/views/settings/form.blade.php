<!-- First you need to extend the CB layout -->
@extends('crudbooster::themes.adminlte.layout.layout')
@section('content')
<!-- Your custom  HTML goes here -->
<style>
    img# {
        width: 180px;
        /* height: 100px */
        height: auto;
    }

    span.error {
        color: red;
        font-style: italic
    }

    div.error {
        display: none
    }

    input {
        border: 1px solid #000
    }

    input.checkbox {
        border: none
    }

    input:focus {
        border: 1px solid #000
    }

    input.error {
        border: 1px solid red
    }

    .error .select2-choice.select2-default,
    .error .select2-choices {
        color: #a94442;
        border-color: #a94442;
        -webkit-box-shadow: inset 0 1px 1px rgba(0, 0, 0, .075);
        box-shadow: inset 0 1px 1px rgba(0, 0, 0, .075);
    }

    .error:focus,
    .error .select2-choice.select2-defaultLfocus,
    .error .select2-choicesLfocus {
        border-color: #843534;
        -webkit-box-shadow: inset 0 1px 1px rgba(0, 0, 0, .075), 0 0 6px #ce8483;
        box-shadow: inset 0 1px 1px rgba(0, 0, 0, .075), 0 0 6px #ce8483;
    }

    .select2-container .select2-choices .select2-search-field input,
    .select2-container .select2-choice,
    .select2-container .select2-choices,
    .error {
        border-radius: 1px;
    }

</style>

<div class="box box-info">
    <form method="POST" enctype="multipart/form-data" id="WebForm" accept-charset="utf-8">
        <div class="box-header with-border">
            <h1 class="box-title" style="font-size: 20px;margin-top:5px">กำหนดค่าที่สามารถค้นหาโรงพยาบาลได้</h1>

        </div>

        <div class="box-body">
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group ">
                        <label>โรงพยาบาลในกรุงเทพและปริมณฑล (กม.)</label>
                        <input type="text" placeholder="โรงพยาบาลในกรุงเทพและปริมณฑล" class="form-control"
                            name="distance_search_bangkok" value="{{$config->distance_search_bangkok}}">
                    </div>
                </div>

                <div class="col-md-6">
                    <div class="form-group ">
                        <label>โรงพยาบาลในต่างจังหวัด (กม.)</label>
                        <input type="text" placeholder="โรงพยาบาลในต่างจังหวัด" class="form-control" name="distance_search_upcountry" value="{{$config->distance_search_upcountry}}">
                    </div>
                </div>
            </div>
        </div>

        <div class="box-header with-border">
            <h1 class="box-title" style="font-size: 20px;margin-top:5px">กำหนดระยะเวลา OTP</h1>
        </div>

        <div class="box-body">
            <div class="row">
                <div class="col-md-4">
                    <div class="form-group ">
                        <label>ลงทะเบียน (นาที)</label>
                        <input type="text" placeholder="ลงทะเบียน" class="form-control"
                    name="ttl_otp_register" value="{{$config->ttl_otp_register}}">
                    </div>
                </div>

                <div class="col-md-4">
                    <div class="form-group ">
                        <label>เปลี่ยนแปลงเบอร์โทรศัพท์ (นาที)</label>
                        <input type="text" placeholder="เปลี่ยนแปลงเบอร์โทรศัพท์" class="form-control" name="ttl_otp_change_mobile" value="{{$config->ttl_otp_change_mobile}}">
                    </div>
                </div>

                <div class="col-md-4">
                    <div class="form-group ">
                        <label>ลืม Username (นาที)</label>
                        <input type="text" placeholder="ลืม Username" class="form-control" name="ttl_otp_forgot_username" value="{{$config->ttl_otp_forgot_username}}">
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-4">
                    <div class="form-group ">
                        <label>ลืมรหัสผ่าน (นาที)</label>
                        <input type="text" placeholder="ลืมรหัสผ่าน" class="form-control" name="ttl_otp_forgot_password" value="{{$config->ttl_otp_forgot_password}}">
                    </div>
                </div>

                <div class="col-md-4">
                    <div class="form-group ">
                        <label>Activate บัตร (นาที)</label>
                        <input type="text" placeholder="Activate บัตร" class="form-control"
                            name="ttl_otp_activate_card" value="{{$config->ttl_otp_activate_card}}">
                    </div>
                </div>
            </div>
        </div>

        <div class="box-footer">
            <div style="text-align: right">
                <a href="{{url('admin/notifications')}}" class="btn btn-default"> ยกเลิก </a>
                <input type="submit" name="submit" value="บันทึก" class="btn btn-success">
            </div>
        </div>
    </form>
    <div class="modal fade" id="imagemodal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
        aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-body">
                    <a href="javascript:void(0);" class="close" data-dismiss="modal"><i class="fa fa-times"
                            aria-hidden="true"></i></a>
                    <img src="" class="imagepreview" style="width: 100%;">
                </div>
            </div>
        </div>
    </div>

    @endsection
