<!-- First you need to extend the CB layout -->
@extends('crudbooster::themes.adminlte.layout.layout')
@section('content')
<!-- Your custom  HTML goes here -->
<style>
    img# {
        width: 180px;
        /* height: 100px */
        height: auto;
    }

    span.error {
        color: red;
        font-style: italic
    }

    div.error {
        display: none
    }

    input {
        border: 1px solid #000
    }

    input.checkbox {
        border: none
    }

    input:focus {
        border: 1px solid #000
    }

    input.error {
        border: 1px solid red
    }

    .error .select2-choice.select2-default,
    .error .select2-choices {
        color: #a94442;
        border-color: #a94442;
        -webkit-box-shadow: inset 0 1px 1px rgba(0, 0, 0, .075);
        box-shadow: inset 0 1px 1px rgba(0, 0, 0, .075);
    }

    .error:focus,
    .error .select2-choice.select2-defaultLfocus,
    .error .select2-choicesLfocus {
        border-color: #843534;
        -webkit-box-shadow: inset 0 1px 1px rgba(0, 0, 0, .075), 0 0 6px #ce8483;
        box-shadow: inset 0 1px 1px rgba(0, 0, 0, .075), 0 0 6px #ce8483;
    }

    .select2-container .select2-choices .select2-search-field input,
    .select2-container .select2-choice,
    .select2-container .select2-choices,
    .error {
        border-radius: 1px;
    }

</style>

<div class="box box-success">
    <div class="box-header with-border">
        <h1 class="box-title" style="font-size: 20px;margin-top:5px">เพิ่มข้อมูล</h1>

    </div>
    <form method="POST" enctype="multipart/form-data" id="WebForm" accept-charset="utf-8">
        <div class="box-body">
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group ">
                        <label>รหัสผลิตภัณฑ์</label>
                        <input type="text" placeholder="รหัสผลิตภัณฑ์" class="form-control" name="code"
                            >
                    </div>
                </div>

                
                
            </div>
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group ">
                        <label>ชื่อภาษาไทย</label>
                        <input type="text" placeholder="ชื่อภาษาไทย" class="form-control banner-name" name="name_th"
                            >
                    </div>
                </div>

                <div class="col-md-6">
                    <div class="form-group ">
                        <label>ชื่อภาษาอังกฤษ</label>
                        <input type="text" placeholder="ชื่อภาษาอังกฤษ" class="form-control banner-name" name="name_en"
                            >
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        <label>รูปภาพ</label>
                        <input type="file" class="form-control" name="picture" id="picture">
                    </div>
                </div>

                <div class="col-md-3">
                    <div class="form-group ">
                        <label>สี Font</label>
                        <input type="text" placeholder="สี Font" class="form-control" name="color">
                    </div>
                </div>

                <div class="col-md-3">
                    <div class="form-group">
                        <label>สถานะ</label>
                        <select class="form-control status" name="banner_status">
                            <option value="1">แสดง</option>
                            <option value="0">ซ่อน</option>
                        </select>
                    </div>
                </div>
        </div>

         <div class="row">
                <div class="col-md-6">
                    <img id="preview-picture" style="max-width: 100%;" src="">
                </div>
            </div>

        <div class="box-footer">
            <div style="text-align: right">
                <a href="{{url('admin/company')}}" class="btn btn-default"> ยกเลิก </a>
                <input type="submit" name="submit" value="บันทึก" class="btn btn-success">

            </div>
        </div>

    </form>

</div>
<div class="modal fade" id="imagemodal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-body">
                <a href="javascript:void(0);" class="close" data-dismiss="modal"><i class="fa fa-times"
                        aria-hidden="true"></i></a>
                <img src="" class="imagepreview" style="width: 100%;">
            </div>
        </div>
    </div>
</div>

@endsection


