<!-- First you need to extend the CB layout -->
@extends('crudbooster::themes.adminlte.layout.layout')
@section('content')
<style>

    table#table-module td:last-child {
        width: 100px;
        text-align: center;
    }

</style>
<!-- Your custom  HTML goes here -->
<div class="box box-default" data-select2-id="16">
    <div class="box-header with-border">
        <h3 class="box-title">ค้นหา</h3>

        <div class="box-tools pull-right">
            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
        </div>
    </div>
    <!-- /.box-header -->
    <div class="box-body">
        <form method="GET" action="?">
            <div class="row">
                <div class="col-md-2">
                    <div class="form-group">
                        <label>รหัสบริษัทประกันภัย</label>
                        <input type="text" placeholder="รหัสบริษัทประกันภัย" class="form-control" name="search_code"
                            value="{{$search_code}}">
                    </div>
                    <!-- /.form-group -->
                </div>
                <!-- /.col -->

                <div class="col-md-4">
                    <div class="form-group">
                        <label>ชื่อภาษาไทย</label>
                        <input type="text" placeholder="ชื่อภาษาไทย" class="form-control" name="search_name_th"
                            value="{{$search_name_th}}">
                    </div>
                    <!-- /.form-group -->
                </div>
                <!-- /.col -->

                <div class="col-md-4">
                    <div class="form-group">
                        <label>ชื่อภาษาอังกฤษ</label>
                        <input type="text" placeholder="ชื่อภาษาอังกฤษ" class="form-control" name="search_name_en"
                            value="{{$search_name_en}}">
                    </div>
                    <!-- /.form-group -->
                </div>
                <!-- /.col -->

                <div class="col-md-2">
                    <div class="form-group">
                        <input type="submit" name="submit" value="ค้นหา" class="btn btn-info"
                            style="margin-top: 25px; width:100%;">
                    </div>
                    <!-- /.form-group -->
                </div>
                <!-- /.col -->

            </div>
            <!-- /.row -->
        </form>
    </div>
</div>

<div class="box">


    <div class="box-header">

        <h1 class="box-title" style="font-size: 20px;margin-top:5px">ข้อมูลบริษัทประกันภัย</h1>

        <div class="box-tools pull-right">
            <a href="{{ url('admin/insurer/add')."?ref=".makeReferalUrl()  }}{{ isset($subModuleKey)?"&sub_module=".$subModuleKey:null }}"
                id='btn_add_new_data' class="btn btn-sm btn-success " title="เพิ่มข้อมูล">
                <i class="fa fa-plus-circle"></i> เพิ่มข้อมูล
            </a>
        </div>



    </div>



    <div class="box-body table-responsive">

        <table id="table-module" class="table table-hover table-striped table-bordered">
            <thead>
                <tr>
                    <th>รหัสบริษัทประกันภัย</th>
                    <th>ชื่อภาษาไทย</th>
                    <th>ชื่อภาษาอังกฤษ</th>
                    <th>กระบวนการ</th>
                </tr>
            </thead>
            <tbody>
                @if(count($result) > 0)
                @foreach($record as $row)
                <tr>
                    <td>{{$row->code}}</td>
                    <td>{{$row->name_th}}</td>
                    <td>{{$row->name_en}}</td>
                    <td>

                        @if(module()->canUpdate())
                        <a class='btn btn-warning btn-xs' href='{{url("admin/insurer/edit/$row->code")}}'><i
                                class="fa fa-pencil-square-o" aria-hidden="true"></i></a>
                        @endif

                        @if(module()->canDelete())
                        <a class='btn btn-danger btn-xs delete' data-id='{{$row->code}}' href="javascript:void(0)"><i
                                class="fa fa-times-circle" aria-hidden="true"></i></a>
                        @endif
                    </td>
                </tr>
                @endforeach
                @else
                <tr class="warning">
                    <td colspan="4" style="text-align: center">
                        <i class="fa fa-table"></i> There is no data yet
                    </td>
                </tr>
                @endif
            </tbody>
        </table>

    </div>

</div>


<!-- ADD A PAGINATION -->
<p>{!! urldecode(str_replace("/?","?",$result->appends(Request::all())->render())) !!}</p>
@endsection
