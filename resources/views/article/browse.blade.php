<!-- First you need to extend the CB layout -->
@extends('crudbooster::themes.adminlte.layout.layout')
@section('content')

<!-- Your custom  HTML goes here -->
<div class="box box-default" data-select2-id="16">
    <div class="box-header with-border">
        <h3 class="box-title">ค้นหา</h3>

        <div class="box-tools pull-right">
            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
        </div>
    </div>
    <!-- /.box-header -->
    <form method="GET" action="?" id="SearchForm">
        <div class="box-body">

            <div class="row">

                <div class="col-md-6">
                    <div class="form-group">
                        <label>ชื่อภาษาไทย</label>
                        <input type="text" placeholder="ชื่อภาษาไทย" class="form-control" name="title_th"
                            value="{{$title_th}}">
                    </div>
                    <!-- /.form-group -->
                </div>
                <!-- /.col -->

                <div class="col-md-6">
                    <div class="form-group">
                        <label>ชื่อภาษาอังกฤษ</label>
                        <input type="text" placeholder="ชื่อภาษาอังกฤษ" class="form-control" name="title_en"
                            value="{{$title_en}}">
                    </div>
                    <!-- /.form-group -->
                </div>
                <!-- /.col -->

            </div>
            <!-- /.row -->

            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        <label>โรงพยาบาล</label>
                        <input type="text" placeholder="โรงพยาบาล" class="form-control" name="hospital_name"
                            value="{{$hospital_name}}">
                    </div>
                    <!-- /.form-group -->
                </div>
                <!-- /.col -->

                <div class="col-md-4">
                    <div class="form-group">
                        <label>หมวดหมู่</label>
                        <select class="form-control" name="category_id">
                            <option value="">ทั้งหมด</option>
                            @foreach ($categories as $row)
                            <option value="{{$row->id}}" {{$row->selected}}>{{$row->name}}</option>
                            @endforeach
                        </select>
                    </div>
                    <!-- /.form-group -->
                </div>
                <!-- /.col -->

                <div class="col-md-2">
                    <div class="form-group">
                        <label>สถานะ</label>
                        <select class="form-control" name="status">
                            <option value="all">ทั้งหมด</option>
                            <option value="1" <?php if($status ==1){?> selected <?php } ?>>แสดง</option>
                            <option value="2" <?php if($status ==2){?> selected <?php } ?>>ซ่อน</option>
                        </select>
                    </div>
                    <!-- /.form-group -->
                </div>
                <!-- /.col -->

            </div>
            <!-- /.row -->


        </div>

        <div class="box-footer">

            <div style="text-align: right">
                <input type="submit" id="submit" value="ค้นหา" class="btn btn-info" style="width:120px;">

            </div>


        </div><!-- /.box-footer-->

    </form>
</div>


<div class="box">
    <div class="box-header">

        <h1 class="box-title" style="font-size: 20px;margin-top:5px">บทความด้านสุขภาพ</h1>

        <div class="box-tools pull-right">
            <a href="{{ module()->addURL()."?ref=".makeReferalUrl()  }}{{ isset($subModuleKey)?"&sub_module=".$subModuleKey:null }}"
                id='btn_add_new_data' class="btn btn-sm btn-success " title="{{ cbLang("add").' '.cbLang('data') }}">
                <i class="fa fa-plus-circle"></i> {{ cbLang("add").' '.cbLang('data') }}
            </a>
        </div>



    </div>

    <div class="box-body table-responsive">

        <table id="table-module" class="table table-hover table-striped table-bordered">
            <thead>
                <tr>
                    <th>ลำดับ</th>
                    <th>ชื่อภาษาไทย</th>
                    <th>ชื่อภาษาอังกฤษ</th>
                    <th>โรงพยาบาล</th>
                    <th>หมวดหมู่</th>
                    <th>วันที่สร้าง</th>
                    <th>สถานะ</th>
                    <th>จัดการ</th>
                </tr>
            </thead>
            <tbody>
                @if(count($record) > 0)

                @foreach($record as $row)
                <tr>
                    <td>{{$row->no}}</td>
                    <td>{{$row->title_th}}</td>
                    <td>{{$row->title_en}}</td>
                    <td>{{$row->hospital_name}}</td>
                    <td>{{$row->category_name}}</td>
                    <td>{{$row->created_at}}</td>
                    <td>{{$row->status_text}}</td>
                    <td>
                        <!-- To make sure we have read access, wee need to validate the privilege -->
                        <a class='btn btn-success btn-xs' href='{{ url("admin/article/send/" . $row->id) }}' data-id="{{$row->id}}"><i
                                class="fa fa-bell" aria-hidden="true"></i></a>

                        @if(module()->canUpdate())
                        <a class='btn btn-warning btn-xs' href='{{url("admin/article/edit/$row->id")}}'><i
                                class="fa fa-pencil-square-o" aria-hidden="true"></i></a>
                        @endif

                        @if(module()->canDelete())
                        <a class='btn btn-danger btn-xs btn-delete' href='javascript:void(0)' data-id="{{$row->id}}"><i
                                class="fa fa-times-circle" aria-hidden="true"></i></a>
                        @endif
                    </td>
                </tr>
                @endforeach
                @else
                <tr class="warning">
                    <td colspan="8" style="text-align: center">
                        <i class="fa fa-table"></i> There is no data yet
                    </td>
                </tr>
                @endif
            </tbody>
        </table>

    </div>

</div>


<!-- ADD A PAGINATION -->
<p>{!! urldecode(str_replace("/?","?",$result->appends(Request::all())->render())) !!}</p>
@endsection
