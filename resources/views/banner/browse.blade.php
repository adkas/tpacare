<!-- First you need to extend the CB layout -->
@extends('crudbooster::themes.adminlte.layout.layout')
@section('content')
<!-- Your custom  HTML goes here -->
<style>
    table#table-module th {
        text-align: center;
    }

    table#table-module td:first-child {
        width: 60px;
        text-align: center;
    }

    table#table-module td:nth-child(2) {
        width: 120px;
        text-align: center;
    }

    /*table#table-module td:nth-child(3), table#table-module td:nth-child(4) {
        width: 160px;
        text-align: center;
    }*/

    table#table-module td:last-child {
        width: 60px;
        text-align: center;
    }

</style>

<div class="box">
    <div class="box-header">

        <h1 class="box-title" style="font-size: 20px;margin-top:5px">แบนเนอร์หน้า Home</h1>

    </div>

    <div class="box-body table-responsive">

        <table id="table-module" class="table table-hover table-striped table-bordered">
            <thead>
                <tr>
                    <th>ลำดับ</th>
                    <th>รูปภาพ</th>
                    <th>ชื่อแบนเนอร์</th>
                    <th>ประเภท</th>
                    <th>สถานะ</th>
                    <th>จัดการ</th>
                </tr>
            </thead>
            <tbody>
                @if(count($record) > 0)

                @foreach($record as $row)
                <tr>
                    <td>{{$row->id}}</td>
                    <td><img src="{{url($row->picture)}}" width="100"></td>
                    <td>{{$row->title}}</td>
                    <td>{{ucfirst($row->type)}}</td>
                    <td>{{$row->status_text}}</td>
                    <td>
                        @if(module()->canUpdate())
                        <a class='btn btn-warning btn-xs' href='{{ url("admin/banner/edit/" . $row->id) }}'><i
                                class="fa fa-pencil-square-o" aria-hidden="true"></i></a>
                        @endif
                    </td>
                </tr>
                @endforeach
                @else
                <tr class="warning">
                    <td colspan="5" style="text-align: center">
                        <i class="fa fa-table"></i> There is no data yet
                    </td>
                </tr>
                @endif
            </tbody>
        </table>

    </div>

</div>



@endsection
