<?php
use Illuminate\Http\Request;
use Illuminate\Support\Str;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
 */

Route::get( '/', function () {
    return view( 'welcome' );
} );

Route::get( 'admin/hospital/row/{index?}', function ( $index = '' ) {
    return view( 'hospital/row-services-item', ['index' => $index] );
} );

Route::post( 'admin/company/exists', function ( Request $request ) {
    $code = $request->input( 'code' );
    $count = DB::table( 'tpa_company' )->select( 'company_code' )->where( 'company_code', $code )->count();
    if ( $count > 0 ) {
        return response()->json( false, 200 );
    } else {
        return response()->json( true, 200 );
    }
} );

Route::get( 'admin/notifications', 'AdminNotificationsController@getIndex' );
Route::get( 'admin/notifications/send', 'AdminNotificationsController@getAdd' );
Route::get( 'admin/notifications/detail/{id}', 'AdminNotificationsController@getDetail' );
Route::get( 'admin/insurer', 'AdminInsurerController@getIndex' );
//cb()->routeController( "admin/insurer", "AdminInsurerController" );

Route::post( 'admin/article/save-add', function ( Request $request ) {
    $article = [
        'title_th'      => $request->input( 'title_th' ),
        'detail_th'     => $request->input( 'detail_th' ),
        'title_en'      => $request->input( 'title_en' ),
        'detail_en'     => $request->input( 'detail_en' ),
        'hospital_code' => $request->input( 'hospital_code' ),
        'category_id'   => $request->input( 'category_id' ),
        'age_range'     => $request->input( 'age_range' ),
        'gender'        => $request->input( 'gender' ),
        'status'        => $request->input( 'status' ),
    ];

    $article_id = DB::table( 'tpa_article' )->insertGetId( $article );

    $files = [];
    foreach ( $_FILES['files'] as $key => $items ) {
        foreach ( $items as $k => $v ) {
            $files[$k][$key] = $v;
        }
    }

    $picture = [
        'ref_id'       => $article_id,
        'picture_type' => 2,
    ];

    foreach ( $files as $file ) {
        if ( $file['size'] > 0 ) {
            list( $name, $ext ) = explode( '.', $file['name'] );
            $imageName = Str::random( 10 ) . '.' . $ext;
            $photo_dir = '/storage/article/' . $imageName;
            $path = public_path() . $photo_dir;
            if ( move_uploaded_file( $file['tmp_name'], $path ) ) {
                $picture['path'] = $photo_dir;
                DB::table( 'tpa_pictures' )->insert( $picture );
            }
        }

    }

    $result = [
        'status' => 'ok',
    ];

    return response()->json( $result, 200 );

} );

Route::post( 'admin/article/save-edit', function ( Request $request ) {
    $article = [
        'title_th'      => $request->input( 'title_th' ),
        'detail_th'     => $request->input( 'detail_th' ),
        'title_en'      => $request->input( 'title_en' ),
        'detail_en'     => $request->input( 'detail_en' ),
        'hospital_code' => $request->input( 'hospital_code' ),
        'category_id'   => $request->input( 'category_id' ),
        'age_range'     => $request->input( 'age_range' ),
        'gender'        => $request->input( 'gender' ),
        'status'        => $request->input( 'status' ),
        'updated_at'    => $request->input( 'updated_at' ),
    ];
    $article_id = $request->input( 'id' );
    DB::table( 'tpa_article' )->where( 'id', $article_id )->update( $article );
    $delete_pictures = explode( ',', $request->input( 'delete_pictures' ) );
    foreach ( $delete_pictures as $ref_id ) {
        $img = DB::table( 'tpa_pictures' )->select( 'path' )->where( 'id', $ref_id )->first();
        @unlink( $img->path );
        DB::table( 'tpa_pictures' )->where( 'id', $ref_id )->delete();
    }

    $files = [];
    foreach ( $_FILES['files'] as $key => $items ) {
        foreach ( $items as $k => $v ) {
            $files[$k][$key] = $v;
        }
    }

    $picture = [
        'ref_id'       => $article_id,
        'picture_type' => 2,
    ];

    foreach ( $files as $file ) {
        if ( $file['size'] > 0 ) {
            list( $name, $ext ) = explode( '.', $file['name'] );
            $imageName = Str::random( 10 ) . '.' . $ext;
            $photo_dir = '/storage/article/' . $imageName;
            $path = public_path() . $photo_dir;
            if ( move_uploaded_file( $file['tmp_name'], $path ) ) {
                $picture['path'] = $photo_dir;
                DB::table( 'tpa_pictures' )->insert( $picture );
            }
        }

    }

    $result = [
        'status' => 'ok',
        'id'     => $article_id,
    ];

    return response()->json( $result, 200 );

} );

Route::delete( 'admin/article/delete', function ( Request $request ) {
    try {

        $data = [
            'status'     => -1,
            'updated_at' => date( 'Y-m-d H:i:s' ),
        ];
        DB::table( 'tpa_article' )->where( 'id', $request->input( 'id' ) )->update( $data );

        $result = [
            'status' => 'ok',
        ];

        return response()->json( $result, 200 );

    } catch ( Exception $e ) {
        return response()->json( [
            'status'    => 'error',
            'message'   => 'Internal server error.',
            'exception' => $e->getMessage(),
        ], 500 );

    }

} );


Route::post( 'admin/promotions/save-add', function ( Request $request ) {
    $promotion = [
        'title_th'      => $request->input( 'title_th' ),
        'detail_th'     => $request->input( 'detail_th' ),
        'title_en'      => $request->input( 'title_en' ),
        'detail_en'     => $request->input( 'detail_en' ),
        'hospital_code' => $request->input( 'hospital_code' ),
        'category_id'   => $request->input( 'category_id' ),
        'age_range'     => $request->input( 'age_range' ),
        'gender'        => $request->input( 'gender' ),
        'status'        => $request->input( 'status' ),
        'start_date'    => $request->input( 'start_date' ),
        'end_date'      => $request->input( 'end_date' ),
    ];

    $promotion_id = DB::table( 'tpa_promotions' )->insertGetId( $promotion );

    $files = [];
    foreach ( $_FILES['files'] as $key => $items ) {
        foreach ( $items as $k => $v ) {
            $files[$k][$key] = $v;
        }
    }

    $picture = [
        'ref_id'       => $promotion_id,
        'picture_type' => 1,
    ];

    foreach ( $files as $file ) {
        if ( $file['size'] > 0 ) {
            list( $name, $ext ) = explode( '.', $file['name'] );
            $imageName = Str::random( 10 ) . '.' . $ext;
            $photo_dir = '/storage/promotions/' . $imageName;
            $path = public_path() . $photo_dir;
            if ( move_uploaded_file( $file['tmp_name'], $path ) ) {
                $picture['path'] = $photo_dir;
                DB::table( 'tpa_pictures' )->insert( $picture );
            }
        }

    }

    $result = [
        'status' => 'ok',
    ];

    return response()->json( $result, 200 );

} );

Route::post( 'admin/promotions/save-edit', function ( Request $request ) {
    $promotion = [
        'title_th'      => $request->input( 'title_th' ),
        'detail_th'     => $request->input( 'detail_th' ),
        'title_en'      => $request->input( 'title_en' ),
        'detail_en'     => $request->input( 'detail_en' ),
        'hospital_code' => $request->input( 'hospital_code' ),
        'category_id'   => $request->input( 'category_id' ),
        'age_range'     => $request->input( 'age_range' ),
        'gender'        => $request->input( 'gender' ),
        'status'        => $request->input( 'status' ),
        'start_date'    => $request->input( 'start_date' ),
        'end_date'      => $request->input( 'end_date' ),
        'updated_at'    => $request->input( 'updated_at' ),
    ];
    $promotion_id = $request->input( 'id' );
    DB::table( 'tpa_promotions' )->where( 'id', $promotion_id )->update( $promotion );
    $delete_pictures = explode( ',', $request->input( 'delete_pictures' ) );
    foreach ( $delete_pictures as $ref_id ) {
        $img = DB::table( 'tpa_pictures' )->select( 'path' )->where( 'id', $ref_id )->first();
        @unlink( $img->path );
        DB::table( 'tpa_pictures' )->where( 'id', $ref_id )->delete();
    }

    $files = [];
    foreach ( $_FILES['files'] as $key => $items ) {
        foreach ( $items as $k => $v ) {
            $files[$k][$key] = $v;
        }
    }

    $picture = [
        'ref_id'       => $promotion_id,
        'picture_type' => 1,
    ];

    foreach ( $files as $file ) {
        if ( $file['size'] > 0 ) {
            list( $name, $ext ) = explode( '.', $file['name'] );
            $imageName = Str::random( 10 ) . '.' . $ext;
            $photo_dir = '/storage/promotions/' . $imageName;
            $path = public_path() . $photo_dir;
            if ( move_uploaded_file( $file['tmp_name'], $path ) ) {
                $picture['path'] = $photo_dir;
                DB::table( 'tpa_pictures' )->insert( $picture );
            }
        }

    }

    $result = [
        'status' => 'ok',
    ];

    return response()->json( $result, 200 );

} );

Route::delete( 'admin/promotions/delete', function ( Request $request ) {
    try {

        $data = [
            'status'     => -1,
            'deleted_at' => date( 'Y-m-d H:i:s' ),
        ];
        DB::table( 'tpa_promotions' )->where( 'id', $request->input( 'id' ) )->update( $data );

        $result = [
            'status' => 'ok',
        ];

        return response()->json( $result, 200 );

    } catch ( Exception $e ) {
        return response()->json( [
            'status'    => 'error',
            'message'   => 'Internal server error.',
            'exception' => $e->getMessage(),
        ], 500 );

    }

} );


Route::post( 'admin/agreement/save-edit', function ( Request $request ) {
    $row = DB::table( 'tpa_agreement' )->where( 'agreement_id', $request->input( 'agreement_id' ) )->first();
    if ( $request->input( 'agreement_force' ) == "2" ) {
        DB::table( 'tpa_agreement' )->where( 'agreement_id', $request->input( 'agreement_id' ) )->update( [
            'agreement_th' => $request->input( 'agreement_th' ),
            'agreement_en' => $request->input( 'agreement_en' ),
            'updated_at'   => date( 'Y-m-d H:i:s' ),
        ] );

    } else {
        DB::table( 'tpa_agreement' )->where( 'agreement_id', $request->input( 'agreement_id' ) )->update( [
            'agreement_status' => "0",
            'updated_at'       => date( 'Y-m-d H:i:s' ),
        ] );

        DB::table( 'tpa_agreement' )->insert( [
            'agreement_th'     => $request->input( 'agreement_th' ),
            'agreement_en'     => $request->input( 'agreement_en' ),
            'agreement_type'   => $row->agreement_type,
            'agreement_force'  => $request->input( 'agreement_force' ),
            'agreement_status' => 1,
            'updated_at'       => date( 'Y-m-d H:i:s' ),
        ] );

    }
    $result = [
        'status' => 'ok',
    ];

    return response()->json( $result, 200 );

} );

Route::post( 'admin/banner/save-edit', function ( Request $request ) {
    try {
        $data = [
            'banner_title_th' => $request->input( 'title_th' ),
            'banner_title_en' => $request->input( 'title_en' ),
            'banner_status'   => $request->input( 'banner_status' ),
            'banner_type'     => $request->input( 'banner_type' ),
            'banner_link'     => $request->input( 'link' ),
            'updated_at'      => date( 'Y-m-d H:i:s' ),
        ];
        $file = $_FILES['picture'];
        if ( $file['size'] > 0 ) {
            list( $name, $ext ) = explode( '.', $file['name'] );
            $imageName = Str::random( 10 ) . '.' . $ext;
            $photo_dir = '/storage/banner/' . $imageName;
            $path = public_path() . $photo_dir;
            if ( move_uploaded_file( $file['tmp_name'], $path ) ) {
                $data['banner_picture'] = $photo_dir;
            }
        }

        DB::table( 'tpa_banner' )->where( 'banner_id', $request->input( 'id' ) )->update( $data );

        $result = [
            'status' => 'ok',
        ];

        return response()->json( $result, 200 );

    } catch ( Exception $e ) {
        return response()->json( [
            'status'    => 'error',
            'message'   => 'Internal server error.',
            'exception' => $e->getMessage(),
        ], 500 );

    }

} );

Route::post( 'admin/insurer/exists', function ( Request $request ) {
    $insurer_code = $request->input( 'insurer_code' );
    $count = DB::table( 'tpa_insurer' )->select( 'insurer_code' )->where( 'insurer_code', $insurer_code )->count();
    if ( $count > 0 ) {
        return response()->json( false, 200 );
    } else {
        return response()->json( true, 200 );
    }
} );

Route::post( 'admin/insurer/save-add', function ( Request $request ) {
    try {
        $data = [
            'insurer_code' => $request->input( 'insurer_code' ),
            'name_th'      => $request->input( 'name_th' ),
            'name_en'      => $request->input( 'name_en' ),
            'insurer_type' => $request->input( 'insurer_type' ),
            'short_name'   => $request->input( 'short_name' ),
            'website'      => $request->input( 'website' ),
            'color'        => $request->input( 'color' ),
        ];

        $file = $_FILES['picture'];
        if ( $file['size'] > 0 ) {
            list( $name, $ext ) = explode( '.', $file['name'] );
            $imageName = Str::random( 10 ) . '.' . $ext;
            $photo_dir = '/storage/insurer/' . $imageName;
            $path = public_path() . $photo_dir;
            if ( move_uploaded_file( $file['tmp_name'], $path ) ) {
                $data['background'] = $photo_dir;
            }
        }

        DB::table( 'tpa_insurer' )->insert( $data );

        $result = [
            'status' => 'ok',
        ];

        return response()->json( $result, 200 );

    } catch ( Exception $e ) {
        return response()->json( [
            'status'    => 'error',
            'message'   => 'Internal server error.',
            'exception' => $e->getMessage(),
        ], 500 );

    }

} );

Route::post( 'admin/insurer/save-edit', function ( Request $request ) {
    try {
        $data = [
            'name_th'      => $request->input( 'name_th' ),
            'name_en'      => $request->input( 'name_en' ),
            'insurer_type' => $request->input( 'insurer_type' ),
            'short_name'   => $request->input( 'short_name' ),
            'website'      => $request->input( 'website' ),
            'color'        => $request->input( 'color' ),
            'updated_at'   => date( 'Y-m-d H:i:s' ),
        ];

        $file = $_FILES['picture'];
        if ( $file['size'] > 0 ) {
            list( $name, $ext ) = explode( '.', $file['name'] );
            $imageName = Str::random( 10 ) . '.' . $ext;
            $photo_dir = '/storage/insurer/' . $imageName;
            $path = public_path() . $photo_dir;
            if ( move_uploaded_file( $file['tmp_name'], $path ) ) {
                $data['background'] = $photo_dir;
            }
        }

        DB::table( 'tpa_insurer' )->where( 'insurer_code', $request->input( 'code' ) )->update( $data );

        $result = [
            'status' => 'ok',
        ];

        return response()->json( $result, 200 );

    } catch ( Exception $e ) {
        return response()->json( [
            'status'    => 'error',
            'message'   => 'Internal server error.',
            'exception' => $e->getMessage(),
        ], 500 );

    }

} );

Route::delete( 'admin/insurer/delete', function ( Request $request ) {
    try {

        $data = [
            'background' => '',
            'updated_at' => date( 'Y-m-d H:i:s' ),
        ];
        DB::table( 'tpa_insurer' )->where( 'insurer_code', $request->input( 'code' ) )->update( $data );

        $result = [
            'status' => 'ok',
        ];

        return response()->json( $result, 200 );

    } catch ( Exception $e ) {
        return response()->json( [
            'status'    => 'error',
            'message'   => 'Internal server error.',
            'exception' => $e->getMessage(),
        ], 500 );

    }

} );



Route::post( 'admin/company/save-edit', function ( Request $request ) {
    try {
        $data = [
            'company_name_th' => $request->input( 'name_th' ),
            'company_name_en' => $request->input( 'name_en' ),
            'company_status'  => $request->input( 'status' ),
            'color'           => $request->input( 'color' ),
            'updated_at'      => date( 'Y-m-d H:i:s' ),
        ];

        $file = $_FILES['picture'];
        if ( $file['size'] > 0 ) {
            list( $name, $ext ) = explode( '.', $file['name'] );
            $imageName = Str::random( 10 ) . '.' . $ext;
            $photo_dir = '/storage/company/' . $imageName;
            $path = public_path() . $photo_dir;
            if ( move_uploaded_file( $file['tmp_name'], $path ) ) {
                $data['background'] = $photo_dir;
            }
        }

        DB::table( 'tpa_company' )->where( 'company_code', $request->input( 'code' ) )->update( $data );

        $result = [
            'status' => 'ok',
        ];

        return response()->json( $result, 200 );

    } catch ( Exception $e ) {
        return response()->json( [
            'status'    => 'error',
            'message'   => 'Internal server error.',
            'exception' => $e->getMessage(),
        ], 500 );

    }

} );

Route::post( 'admin/company/save-add', function ( Request $request ) {
    try {
        $data = [
            'company_code'    => $request->input( 'code' ),
            'company_name_th' => $request->input( 'name_th' ),
            'company_name_en' => $request->input( 'name_en' ),
            'company_status'  => $request->input( 'status' ),
            'color'           => $request->input( 'color' ),
        ];

        $file = $_FILES['picture'];
        if ( $file['size'] > 0 ) {
            list( $name, $ext ) = explode( '.', $file['name'] );
            $imageName = Str::random( 10 ) . '.' . $ext;
            $photo_dir = '/storage/company/' . $imageName;
            $path = public_path() . $photo_dir;
            if ( move_uploaded_file( $file['tmp_name'], $path ) ) {
                $data['background'] = $photo_dir;
            }
        }

        DB::table( 'tpa_company' )->insert( $data );

        $result = [
            'status' => 'ok',
        ];

        return response()->json( $result, 200 );

    } catch ( Exception $e ) {
        return response()->json( [
            'status'    => 'error',
            'message'   => 'Internal server error.',
            'exception' => $e->getMessage(),
        ], 500 );

    }

} );

Route::delete( 'admin/company/delete', function ( Request $request ) {
    try {

        $data = [
            'background' => '',
            'updated_at' => date( 'Y-m-d H:i:s' ),
        ];
        DB::table( 'tpa_company' )->where( 'company_code', $request->input( 'code' ) )->update( $data );

        /*
        $company = DB::table( 'tpa_company' )
        ->select( 'background' )
        ->where( 'company_code', $request->input( 'code' ) )
        ->first();

        $file = $_FILES['picture'];
        if ( $file['size'] > 0 ) {
        list( $name, $ext ) = explode( '.', $file['name'] );
        $imageName = Str::random( 10 ) . '.' . $ext;
        $photo_dir = '/storage/company/' . $imageName;
        $path = public_path() . $photo_dir;
        if ( move_uploaded_file( $file['tmp_name'], $path ) ) {
        $data['background'] = $photo_dir;
        }
        }

         */

        $result = [
            'status' => 'ok',
        ];

        return response()->json( $result, 200 );

    } catch ( Exception $e ) {
        return response()->json( [
            'status'    => 'error',
            'message'   => 'Internal server error.',
            'exception' => $e->getMessage(),
        ], 500 );

    }

} );

Route::post( 'admin/hospital/save-edit', function ( Request $request ) {
    try {
        $remove_images = ( $request->input( 'remove_images' ) ) ? explode( ',', $request->input( 'remove_images' ) ) : [];
        if ( count( $remove_images ) > 0 ) {
            $rs_pictures = DB::table( 'tpa_hospital_pictures' )->select( 'id', 'path' )->whereIn( 'id', $remove_images )->get();
            foreach ( $rs_pictures as $r ) {
                @unlink( public_path() . $r->path );
            }

            DB::table( 'tpa_hospital_pictures' )->whereIn( 'id', $remove_images )->delete();

        }

        $data = [
            'name_th'             => $request->input( 'name_th' ),
            'name_en'             => $request->input( 'name_en' ),
            'website'             => $request->input( 'website' ),
            'location'            => $request->input( 'location' ),
            'location_en'         => $request->input( 'location_en' ),
            'latitude'            => $request->input( 'latitude' ),
            'longitude'           => $request->input( 'longitude' ),
            'telephone'           => $request->input( 'telephone' ),
            'address_no'          => $request->input( 'address_no' ),
            'address_no_en'       => $request->input( 'address_no_en' ),
            'address_road'        => $request->input( 'address_road' ),
            'address_road_en'     => $request->input( 'address_road_en' ),
            'address_amphur'      => $request->input( 'address_amphur' ),
            'address_amphur_en'   => $request->input( 'address_amphur_en' ),
            'address_province'    => $request->input( 'address_province' ),
            'address_province_en' => $request->input( 'address_province_en' ),
            'address_postcode'    => $request->input( 'address_postcode' ),
            'address_postcode_en' => $request->input( 'address_postcode_en' ),
            'type'                => $request->input( 'type' ),
            'service_fee_th'      => $request->input( 'service_fee_th' ),
            'service_fee_en'      => $request->input( 'service_fee_en' ),
            'service_times_th'    => implode(',' , $request->input( 'service_times_th' )),
            'service_times_en'    => implode(',' , $request->input( 'service_times_en' )),
            'description_th'      => $request->input( 'description_th' ),
            'description_en'      => $request->input( 'description_en' ),
            'show_service_fee'    => $request->input( 'show_service_fee' ),
            'show_description'    => $request->input( 'show_description' ),
            'updated_at'          => date( 'Y-m-d H:i:s' ),
        ];

        DB::table( 'tpa_hospital' )->where( 'code', $request->input( 'provider_code' ) )->update( $data );

        $files = [];
        foreach ( $_FILES['files'] as $key => $items ) {
            foreach ( $items as $k => $v ) {
                $files[$k][$key] = $v;
            }
        }

        $picture = [
            'hospital_code' => $request->input( 'provider_code' ),
        ];

        foreach ( $files as $file ) {
            if ( $file['size'] > 0 ) {
                list( $name, $ext ) = explode( '.', $file['name'] );
                $imageName = Str::random( 10 ) . '.' . $ext;
                $photo_dir = '/storage/hospital/' . $imageName;
                $path = public_path() . $photo_dir;
                if ( move_uploaded_file( $file['tmp_name'], $path ) ) {
                    $picture['path'] = $photo_dir;
                    DB::table( 'tpa_hospital_pictures' )->insert( $picture );
                }
            }

        }

        $result = [
            'status' => 'ok',
        ];

        return response()->json( $result, 200 );

    } catch ( Exception $e ) {
        return response()->json( [
            'status'    => 'error',
            'message'   => 'Internal server error.',
            'exception' => $e->getMessage(),
        ], 500 );

    }

} );

Route::post( 'admin/push-notifications', function ( Request $request ) {
    try {
        $data = [
            'type'            => $request->input( 'notification_type' ),
            'title_th'        => $request->input( 'title_th' ),
            'title_en'        => $request->input( 'title_en' ),
            'body_th'         => $request->input( 'body_th' ),
            'body_en'         => $request->input( 'body_en' ),
            'target_criteria' => $request->input( 'target_criteria' ),
            'age_range'       => $request->input( 'age_range' ),
            'gender'          => $request->input( 'gender' ),
            'url'             => ( $request->input( 'url' ) ) ? $request->input( 'url' ) : '',
            'created_at'      => date( 'Y-m-d H:i:s' ),
            'created_by'      => Auth::user()->id,
        ];
        DB::table( 'tpa_notifications' )->insert( $data );

        $client = new GuzzleHttp\Client();
        $endpoint = env( 'PUSH_URL' ) . env( 'PUSH_APP_ID' ) . '/push/';
        $params = [
            'app_id'             => env( 'PUSH_APP_ID' ),
            'app_secret'         => env( 'PUSH_APP_SECRET' ),
            //'user'               => ["tpa_dev_aaaa1111"],
            //'topic'              => "__all__",
            'data'               => [
                'type'          => 'normal',
                'push_title_th' => $request->input( 'title_th' ),
                'push_title_en' => $request->input( 'title_en' ),
                'push_body_th'  => $request->input( 'body_th' ),
                'push_body_en'  => $request->input( 'body_en' ),
                'title_th'      => $request->input( 'title_th' ),
                'title_en'      => $request->input( 'title_en' ),
                'detail_th'     => $request->input( 'body_th' ),
                'detail_en'     => $request->input( 'body_en' ),
                'url'           => ( $request->input( 'url' ) ) ? $request->input( 'url' ) : '',
            ],
            'notification'       => [
                'title' => $request->input( 'title_th' ),
                'body'  => $request->input( 'body_th' ),
            ],
            'android'            => [
                'priority' => 'high',
            ],
            'push_gears_options' => [
                'eta'     => date( 'Y-m-d H:i:s' ),
                'history' => true,
            ],
        ];

        if ( $request->input( 'notification_type' ) == 'all' ) {
            $params['topic'] = '__all__';
        } else {
            $users = [];
            if ( $request->input( 'target_criteria' ) == 'age_range' ) {
                $age = DB::table( 'tpa_age_range' )->where( 'id', $request->input( 'age_range' ) )->select( 'start_age', 'end_age' )->first();

                $rs = DB::select( 'SELECT username FROM users WHERE birthdate is not null AND DATEDIFF(yy,CONVERT(DATETIME, birthdate),GETDATE()) BETWEEN ' . $age->start_age . ' AND ' . $age->end_age );

                foreach ( $rs as $row ) {
                    array_push( $users, 'tpa_dev_' . $row->username );
                }
                $params['user'] = $users;

            } else {
                $query = DB::table( 'users' );
                $sex = (int) $request->input( 'gender' );
                if ( $sex != 0 ) {
                    $query->where( 'sex', $sex );
                    $rs = $query->get();
                    foreach ( $rs as $row ) {
                        array_push( $users, 'tpa_dev_' . $row->username );
                    }
                    $params['user'] = $users;

                } else {
                    $params['topic'] = '__all__';
                }

            }

        }

        $res = $client->request( 'POST', $endpoint, [
            'headers' => ['Content-Type' => 'application/json'],
            'body'    => json_encode( $params ),
        ] );

        $result = [
            'status'   => 'ok',
            'params'   => $params,
            'response' => json_decode( $res->getBody()->getContents() ),
        ];

        return response()->json( $result, 200 );

    } catch ( Exception $e ) {
        return response()->json( [
            'status'    => 'error',
            'params'    => $params,
            'message'   => 'Internal server error.',
            'exception' => $e->getMessage(),
        ], 500 );

    }

} );

Route::get( 'admin/settings', 'AdminSettingsController@getIndex' );
Route::post( 'admin/settings/save', function ( Request $request ) {
    try {
        $data = [
            'distance_search_bangkok' => $request->input( 'code' ),
            'company_name_th'         => $request->input( 'name_th' ),
            'company_name_en'         => $request->input( 'name_en' ),
            'company_status'          => $request->input( 'status' ),
        ];

        DB::table( 'tpa_config' )->where( 'config_key', 'distance_search_bangkok' )->update( [
            'config_val' => $request->input( 'distance_search_bangkok' ),
        ] );

        DB::table( 'tpa_config' )->where( 'config_key', 'distance_search_upcountry' )->update( [
            'config_val' => $request->input( 'distance_search_upcountry' ),
        ] );

        DB::table( 'tpa_config' )->where( 'config_key', 'ttl_otp_register' )->update( [
            'config_val' => $request->input( 'ttl_otp_register' ),
        ] );

        DB::table( 'tpa_config' )->where( 'config_key', 'ttl_otp_change_mobile' )->update( [
            'config_val' => $request->input( 'ttl_otp_change_mobile' ),
        ] );

        DB::table( 'tpa_config' )->where( 'config_key', 'ttl_otp_forgot_username' )->update( [
            'config_val' => $request->input( 'ttl_otp_forgot_username' ),
        ] );

        DB::table( 'tpa_config' )->where( 'config_key', 'ttl_otp_forgot_password' )->update( [
            'config_val' => $request->input( 'ttl_otp_forgot_password' ),
        ] );

        DB::table( 'tpa_config' )->where( 'config_key', 'ttl_otp_activate_card' )->update( [
            'config_val' => $request->input( 'ttl_otp_activate_card' ),
        ] );

        $result = [
            'status' => 'ok',
        ];

        return response()->json( $result, 200 );

    } catch ( Exception $e ) {
        return response()->json( [
            'status'    => 'error',
            'message'   => 'Internal server error.',
            'exception' => $e->getMessage(),
        ], 500 );

    }

} );

Route::post( 'admin/insurer/delete', function ( Request $request ) {
    try {

        DB::table( 'tpa_insurer' )->where( 'insurer_code', $request->input( 'insurer_code' ) )->delete();

        $result = [
            'status' => 'ok',
            'code'   => $request->input( 'insurer_code' ),
        ];

        return response()->json( $result, 200 );

    } catch ( Exception $e ) {
        return response()->json( [
            'status'    => 'error',
            'message'   => 'Internal server error.',
            'exception' => $e->getMessage(),
        ], 500 );

    }

} );

Route::post( 'admin/hospital/delete', function ( Request $request ) {
    try {

        DB::table( 'tpa_hospital' )->where( 'code', $request->input( 'code' ) )->delete();

        $result = [
            'status' => 'ok',
        ];

        return response()->json( $result, 200 );

    } catch ( Exception $e ) {
        return response()->json( [
            'status'    => 'error',
            'message'   => 'Internal server error.',
            'exception' => $e->getMessage(),
        ], 500 );

    }

} );

Route::get( 'admin/claim/export', 'AdminClaimController@export' );

Route::post( 'admin/members/save-edit', function ( Request $request ) {
    try {
        $data = [
            'name' => $request->input( 'name' ),
            'surname' => $request->input( 'surname' ),
            'status'  => $request->input( 'status' ),
            'updated_at'      => date( 'Y-m-d H:i:s' ),
        ];

        DB::table( 'users' )->where( 'id', $request->input( 'id' ) )->update( $data );

        $result = [
            'status' => 'ok',
        ];

        return response()->json( $result, 200 );

    } catch ( Exception $e ) {
        return response()->json( [
            'status'    => 'error',
            'message'   => 'Internal server error.',
            'exception' => $e->getMessage(),
        ], 500 );

    }

} );

Route::post( 'admin/tpa_partner/reset-secret', function ( Request $request ) {

	try {
	
		$secret_key = Str::random(32);
		$ins_code = $request->ins_code;
		
		$data = [
            'secret_key'          => $secret_key,
            'updated_at'          => date( 'Y-m-d H:i:s' ),
        ];
		
		DB::table( 'tpa_partner' )->where( 'ins_code', $request->input( 'ins_code' ) )->update( $data );
		
		$result = [
            'status' => 'ok',
			'ins_code' => $ins_code,
			'secret_key' => $secret_key 
        ];

        return response()->json( $result, 200 );
		
	} catch ( Exception $e ) {
        return response()->json( [
            'status'    => 'error',
            'message'   => 'Internal server error.',
            'exception' => $e->getMessage(),
        ], 500 );

    }
} );

Route::post( 'admin/tpa_partner/save-add', function ( Request $request ) {
	
    try {
	
		$count = DB::table( 'tpa_partner' )->select( 'ins_code' )->where( 'ins_code', $request->input( 'ins_code' ) )->count();
		if ( $count > 0 ) {
			$result = [
				'status' => 'error',
				'message' => 'รหัสบริษัทประกันนี้มีอยู่ในระบบแล้ว กรุณาตรวจสอบข้อมูลอีกครั้ง',
			];

			return response()->json( $result, 200 );
		}
	
		$partner_id = 'TPA'.date('iHYmds');
		$data = [
            'ins_code' 			=> $request->input( 'ins_code' ),
			'name_th' 			=> $request->input( 'name_th' ),
			'name_en' 			=> $request->input( 'name_en' ),
			'status_cf1' 		=> $request->input( 'status_cf1' ),
			'status_cf1_c' 		=> $request->input( 'status_cf1_c' ),
			'status_cf2' 		=> $request->input( 'status_cf2' ),
			'status_cf2_c' 		=> $request->input( 'status_cf2_c' ),
			'status_ccb1' 		=> $request->input( 'status_ccb1' ),
			'status_ccb1_c' 	=> $request->input( 'status_ccb1_c' ),
			'status_cb1' 		=> $request->input( 'status_cb1' ),
			'status_cb1_c' 		=> $request->input( 'status_cb1_c' ),
			'detail_cf1' 		=> $request->input( 'detail_cf1' ),
			'detail_cf1_c' 		=> $request->input( 'detail_cf1_c' ),
			'detail_cf2' 		=> $request->input( 'detail_cf2' ),
			'detail_cf2_c' 		=> $request->input( 'detail_cf2_c' ),
			'detail_cf3' 		=> $request->input( 'detail_cf3' ),
			'detail_cf3_c' 		=> $request->input( 'detail_cf3_c' ),
			'detail_cb' 		=> $request->input( 'detail_cb' ),
			'detail_cb_c' 		=> $request->input( 'detail_cb_c' ),
			'history_cf1' 		=> $request->input( 'history_cf1' ),
			'history_cf1_c' 	=> $request->input( 'history_cf1_c' ),
			'history_cf2' 		=> $request->input( 'history_cf2' ),
			'history_cf2_c' 	=> $request->input( 'history_cf2_c' ),
			'history_cb' 		=> $request->input( 'history_cb' ),
			'history_cb_c' 		=> $request->input( 'history_cb_c' ),
			'secret_key' 		=> Str::random(32),
			'partner_id' 		=> $partner_id,
			'created_at'      	=> date( 'Y-m-d H:i:s' ),
            'updated_by'      	=> Auth::user()->id,
        ];

        DB::table( 'tpa_partner' )->insert( $data );

        $result = [
            'status' => 'ok',
        ];

        return response()->json( $result, 200 );

    } catch ( Exception $e ) {
        return response()->json( [
            'status'    => 'error',
            'message'   => 'Internal server error.',
            'exception' => $e->getMessage(),
        ], 500 );

    }

} );

Route::post( 'admin/tpa_partner/save-edit', function ( Request $request ) {
	try {
		$data = [
			'ins_code' 			=> $request->input( 'ins_code' ),
			'name_th' 			=> $request->input( 'name_th' ),
			'name_en' 			=> $request->input( 'name_en' ),
			'status_cf1' 		=> $request->input( 'status_cf1' ),
			'status_cf1_c' 		=> $request->input( 'status_cf1_c' ),
			'status_cf2' 		=> $request->input( 'status_cf2' ),
			'status_cf2_c' 		=> $request->input( 'status_cf2_c' ),
			'status_ccb1' 		=> $request->input( 'status_ccb1' ),
			'status_ccb1_c' 	=> $request->input( 'status_ccb1_c' ),
			'status_cb1' 		=> $request->input( 'status_cb1' ),
			'status_cb1_c' 		=> $request->input( 'status_cb1_c' ),
			'detail_cf1' 		=> $request->input( 'detail_cf1' ),
			'detail_cf1_c' 		=> $request->input( 'detail_cf1_c' ),
			'detail_cf2' 		=> $request->input( 'detail_cf2' ),
			'detail_cf2_c' 		=> $request->input( 'detail_cf2_c' ),
			'detail_cf3' 		=> $request->input( 'detail_cf3' ),
			'detail_cf3_c' 		=> $request->input( 'detail_cf3_c' ),
			'detail_cb' 		=> $request->input( 'detail_cb' ),
			'detail_cb_c' 		=> $request->input( 'detail_cb_c' ),
			'history_cf1' 		=> $request->input( 'history_cf1' ),
			'history_cf1_c' 	=> $request->input( 'history_cf1_c' ),
			'history_cf2' 		=> $request->input( 'history_cf2' ),
			'history_cf2_c' 	=> $request->input( 'history_cf2_c' ),
			'history_cb' 		=> $request->input( 'history_cb' ),
			'history_cb_c' 		=> $request->input( 'history_cb_c' ),
			'updated_at'      	=> date( 'Y-m-d H:i:s' ),
            'updated_by'      	=> Auth::user()->id,
        ];
		
		DB::table( 'tpa_partner' )->where( 'ins_code', $request->input( 'ins_code' ) )->update( $data );
	
		$result = [
            'status' => 'ok',
        ];

        return response()->json( $result, 200 );
		
	} catch ( Exception $e ) {
        return response()->json( [
            'status'    => 'error',
            'message'   => 'Internal server error.',
            'exception' => $e->getMessage(),
        ], 500 );

    }
} );

Route::post( 'admin/tpa_partner/delete', function ( Request $request ) {
    try {

        DB::table( 'tpa_partner' )->where( 'ins_code', $request->input( 'code' ) )->delete();

        $result = [
            'status' => 'ok',
        ];

        return response()->json( $result, 200 );

    } catch ( Exception $e ) {
        return response()->json( [
            'status'    => 'error',
            'message'   => 'Internal server error.',
            'exception' => $e->getMessage(),
        ], 500 );

    }

} );


