<?php

use App\User;
use crocodicstudio\crudbooster\exceptions\CBValidationException;
use GuzzleHttp\Exception\RequestException;
use Illuminate\Contracts\Encryption\DecryptException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use \Firebase\JWT\JWT;
error_reporting( 0 );

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
 */
include 'phpseclib/Net/SFTP.php';

Route::get( 'img/{base64_path}', function ( $base64_path, Request $request ) {

    /**/
    try {
        $path = base64_decode( $base64_path );
        list( $x, $y ) = explode( '.', $path );
        if ( strtolower( $y ) == 'png' ) {
            image_png_filter( $path );
        } else {
            image_jpg_filter( $path );
        }

    } catch ( RequestException $e ) {
        return response()->json( [
            'status'    => 'error',
            'message'   => __( 'Internal server error.' ),
            'exception' => $e->getMessage(),
        ], 500 );
    }
} );

Route::group( ['prefix' => 'v1', 'middleware' => ['verify.apikey', 'set.language']], function () {
    Route::get( 'login', function ( Request $request ) {
        return response()->json( [
            'status'  => 'error',
            'message' => 'Unauthenticate.',
        ], 401 );
    } )->name( 'login' );

    Route::post( 'claim-notify', function ( Request $request ) {

        /**/
        try {
            $client = new GuzzleHttp\Client();
            $endpoint = env( 'PUSH_URL' ) . env( 'PUSH_APP_ID' ) . '/push/';

            foreach ( $request->all() as $row ) {
                $row = (object) $row;
                $params = [
                    'app_id'             => env( 'PUSH_APP_ID' ),
                    'app_secret'         => env( 'PUSH_APP_SECRET' ),
                    'user'               => ["tpa_dev_" . $row->username],
                    'data'               => [
                        'type'                 => 'claim',
                        'push_title_th'        => $row->message_title,
                        'push_title_en'        => $row->message_title,
                        'push_body_th'         => $row->message,
                        'push_body_en'         => $row->message,
                        'title_th'             => $row->message_title,
                        'title_en'             => $row->message_title,
                        'detail_th'            => $row->message,
                        'detail_en'            => $row->message,
                        'enable_add_more_file' => ( $row->message_title == 'สถานะการเคลม-ขอข้อมูลเพิ่มเติม' ) ? 'true' : 'false',
                        'url'                  => '',
                    ],
                    'notification'       => [
                        'title' => $row->message_title,
                        'body'  => $row->message,
                    ],
                    'android'            => [
                        'priority' => 'high',
                    ],
                    'push_gears_options' => [
                        'eta'     => date( 'Y-m-d H:i:s' ),
                        'history' => true,
                    ],
                ];

                $res = $client->request( 'POST', $endpoint, [
                    'headers' => ['Content-Type' => 'application/json'],
                    'body'    => json_encode( $params ),
                ] );

            }

            $result = [
                'status' => 'ok',
                'params' => $params,
                //'response' => json_decode( $res->getBody()->getContents() ),
            ];

            return response()->json( $result, 200 );

        } catch ( RequestException $e ) {
            return response()->json( [
                'status'    => 'error',
                'message'   => __( 'Internal server error.' ),
                'exception' => $e->getMessage(),
            ], 500 );
        }
    } );

    Route::post( 'push-notifications', function ( Request $request ) {
        try {
            $gender = [
                'all'    => 0,
                'male'   => 1,
                'female' => 2,
            ];

            $data = [
                'type'            => 'target',
                'category'        => $request->input( 'notification_type' ),
                'data_id'         => $request->input( 'id' ),
                'title_th'        => $request->input( 'title_th' ),
                'title_en'        => $request->input( 'title_en' ),
                'body_th'         => $request->input( 'body_th' ),
                'body_en'         => $request->input( 'body_en' ),
                'target_criteria' => "all",
                'age_range'       => $request->input( 'age_range' ),
                'gender'          => $gender[$request->input( 'gender' )],
                'url'             => ( $request->input( 'url' ) ) ? $request->input( 'url' ) : "http://",
                'created_at'      => date( 'Y-m-d H:i:s' ),
                'created_by'      => 0,
            ];
            $notification_id = DB::table( 'tpa_notifications' )->insertGetId( $data );

            $client = new GuzzleHttp\Client();

            $endpoint = env( 'PUSH_URL' ) . env( 'PUSH_APP_ID' ) . '/push/';
            $params = [
                'app_id'             => env( 'PUSH_APP_ID' ),
                'app_secret'         => env( 'PUSH_APP_SECRET' ),
                'data'               => [
                    'type'          => $request->input( 'notification_type' ),
                    'push_title_th' => $request->input( 'title_th' ),
                    'push_title_en' => $request->input( 'title_en' ),
                    'push_body_th'  => $request->input( 'body_th' ),
                    'push_body_en'  => $request->input( 'body_en' ),
                    'title_th'      => $request->input( 'title_th' ),
                    'title_en'      => $request->input( 'title_en' ),
                    'detail_th'     => $request->input( 'body_th' ),
                    'detail_en'     => $request->input( 'body_en' ),
                    'url'           => ( $request->input( 'url' ) ) ? $request->input( 'url' ) : '',
                    'id'            => $request->input( 'id' ),
                ],
                'notification'       => [
                    'title' => $request->input( 'title_th' ),
                    'body'  => $request->input( 'body_th' ),
                ],
                'android'            => [
                    'priority' => 'high',
                ],
                'push_gears_options' => [
                    'eta'     => date( 'Y-m-d H:i:s' ),
                    'history' => true,
                ],
            ];

            $users = [];
            $query = DB::table( 'users' )->where( 'channel', 2 );
            $sex = $gender[$request->input( 'gender' )];
            $age_range = $request->input( 'age_range' );

            $result = [
                'status' => 'ok',
            ];
            $httpCode = 200;

            if (  ( $sex == 0 ) && ( $age_range == 1 ) ) {
                $params['topic'] = '__all__';
                $res = $client->request( 'POST', $endpoint, [
                    'headers' => ['Content-Type' => 'application/json'],
                    'body'    => json_encode( $params ),
                ] );
                $response = json_decode( $res->getBody()->getContents() );
                $result['response'] = $response;

            } else {
                if ( !empty( $sex ) ) {
                    $query->where( 'sex', $sex );
                }

                if ( $age_range != 1 ) {
                    $age = DB::table( 'tpa_age_range' )
                        ->where( 'id', $age_range )
                        ->select( 'start_age', 'end_age' )
                        ->first();

                    $query->whereNotNull( 'birthdate' )
                        ->whereBetween( DB::raw( 'DATEDIFF(yy,CONVERT(DATETIME, birthdate),GETDATE())' ), [$age->start_age, $age->end_age] );

                }

                $rs = $query->get();
                foreach ( $rs as $row ) {
                    array_push( $users, 'tpa_dev_' . $row->username );
                }

                if ( count( $users ) > 0 ) {
                    if ( count( $users ) > 300 ) {
                        foreach ( array_chunk( $users, 300 ) as $k => $user ) {
                            DB::table( 'tpa_schedule' )->insertGetId( [
                                'notification_id' => $notification_id,
                                'users'           => implode( ',', $user ),
                            ] );

                        }
                    } else {
                        $params['user'] = $users;
                        $res = $client->request( 'POST', $endpoint, [
                            'headers' => ['Content-Type' => 'application/json'],
                            'body'    => json_encode( $params ),
                        ] );
                        $response = json_decode( $res->getBody()->getContents() );
                        $result['response'] = $response;
                        $result['user'] = $users;
                    }

                } else {
                    $result = [
                        'status'  => 'error',
                        'message' => 'ไม่มีผู้รับ',
                    ];
                    $httpCode = 500;
                }

            }

            return response()->json( $result, $httpCode );

        } catch ( Exception $e ) {
            return response()->json( [
                'status'    => 'error',
                'params'    => $params,
                'message'   => 'Internal server error.',
                'exception' => $e->getMessage(),
            ], 500 );

        }

    } );

    Route::get( 'version/{platform}/{app_version}', function ( $platform, $app_version, Request $request ) {

        $rs = DB::table( 'tpa_app_version' )->select( 'current_version', 'force_version' )
            ->where( 'platform', $platform )
            ->first();

        $force_version = explode( ',', $rs->force_version );
        $force = in_array( $app_version, $force_version ) ? true : false;

        $version = (int) str_replace( '.', '', $app_version );
        $current_version = (int) str_replace( '.', '', $rs->current_version );

        $code = 200;
        if ( $version > $current_version ) {

            $result = [
                'status'  => 'error',
                'message' => __( 'failed' ),
                'force'   => $force,
            ];

        } else if ( $version < $current_version ) {
            $result = [
                'status'  => 'ok',
                'message' => __( 'messages.success' ),
                'force'   => $force,
            ];

        } else {
            $result = [
                'status'  => 'ok',
                'message' => __( 'messages.success' ),
            ];
        }

        return response()->json( $result, $code );

    } );

    Route::get( 'agreement/{agreement_type}', function ( $agreement_type, Request $request ) {
        try {
            $data = DB::table( 'tpa_agreement' )
                ->select( 'agreement_id as id', 'agreement_' . App::getLocale() . ' as content' )
                ->where( 'agreement_status', 1 )
                ->where( 'agreement_type', $agreement_type )
                ->first();

            $result = [
                'status'  => 'ok',
                'message' => __( 'messages.success' ),
                'data'    => $data,
            ];

            return response()->json( $result, 200 );
        } catch ( RequestException $e ) {
            return response()->json( [
                'status'    => 'error',
                'message'   => __( 'Internal server error.' ),
                'exception' => $e->getMessage(),
            ], 500 );
        }

    } );

    Route::get( 'home/{count?}', function ( $count = null ) {
        $rs = DB::table( 'tpa_banner' )
            ->select( 'banner_type AS type', 'banner_title_' . App::getLocale() . ' AS title', 'banner_picture AS picture', 'ref_id', 'banner_link AS link' )
            ->where( 'banner_status', 1 )
            ->get();

        $data = [];
        foreach ( $rs as $row ) {
            if ( $row->ref_id == "0" ) {
                unset( $row->ref_id );
            }

            if ( empty( $row->link ) ) {
                unset( $row->link );
            }
            $row->picture = url( $row->picture );
            array_push( $data, $row );

        }

        return response()->json( [
            'status'  => 'ok',
            'message' => __( 'messages.success' ),
            'data'    => $data,
        ], 200 );
    } );

    Route::get( 'bank', function ( Request $request ) {
        $data = DB::table( 'tpa_insurer' )->select( 'insurer_code AS code', 'name_' . App::getLocale() . ' AS name' )
            ->where( 'insurer_type', 4 )
            ->get();

        return response()->json( [
            'status'  => 'ok',
            'message' => __( 'messages.success' ),
            'data'    => $data,
        ], 200 );
    } );

    Route::get( 'companies', function ( Request $request ) {
        $data = DB::table( 'tpa_insurer' )->select( 'insurer_code AS code', 'name_' . App::getLocale() . ' AS name' )
            ->where( 'insurer_type', '<>', 4 )
            ->get();

        return response()->json( [
            'status'  => 'ok',
            'message' => __( 'messages.success' ),
            'data'    => $data,
        ], 200 );
    } );

    Route::put( 'companies', function ( Request $request ) {
        $client = new GuzzleHttp\Client();
        $res = $client->request( 'POST', env( 'TPA_URL' ) . 'login', [
            'headers' => ['Content-Type' => 'application/json'],
            'body'    => json_encode( [
                'Username' => env( 'TPA_USER' ),
                'Password' => env( 'TPA_PASS' ),
            ] ),
            'verify'  => false,
        ] );

        $token = json_decode( $res->getBody()->getContents() );
        if ( empty( $token ) ) {
            return response()->json( [
                'status'  => 'error',
                'message' => __( 'Internal server error.' ),
            ], 500 );
        } else {
            $client = new GuzzleHttp\Client();
            $res = $client->request( 'GET', env( 'TPA_URL' ) . 'CompanyList', [
                'headers' => [
                    'Content-Type'  => 'application/json',
                    'Authorization' => $token,
                ],
                'verify'  => false,
            ] );

            $httpCode = $res->getStatusCode();
            if ( $httpCode != 200 ) {
                return response()->json( [
                    'status'  => 'error',
                    'message' => __( 'Internal server error.' ),
                ], $httpCode );
            } else {
                $rs = json_decode( $res->getBody()->getContents() );
                foreach ( $rs->ListOfCompany as $row ) {
                    $com = [
                        'company_code'    => $row->CompanyCode,
                        'company_name_th' => $row->CompanyName,
                        'company_name_en' => $row->CompanyNameEN,
                        'insurer_code'    => $row->InsurerCode,
                        'company_status'  => 1,
                        'updated_at'      => date( 'Y-m-d H:i:s' ),
                    ];
                    $count = DB::table( 'tpa_company' )
                        ->select( 'company_code' )
                        ->where( 'company_code', $row->CompanyCode )
                        ->count();

                    if ( $count <= 0 ) {
                        DB::table( 'tpa_company' )->insert( $com );

                    } else {
                        if ( !empty( $row->CompanyCode ) ) {
                            DB::table( 'tpa_company' )->where( 'company_code', $row->CompanyCode )->update( $com );

                        }

                    }

                }
                return response()->json( [
                    'status'  => 'ok',
                    'message' => __( 'messages.success' ),
                    'data'    => $rs->ListOfCompany,
                ], 200 );
            }

        }

    } );

    Route::get( 'hospital-group', function ( Request $request ) {

        $data = DB::table( 'tpa_hospital_group' )
            ->where( 'status', 1 )->select( 'id', 'name_' . App::getLocale() . ' AS name' )
            ->get();

        return response()->json( [
            'status'  => 'ok',
            'message' => __( 'messages.success' ),
            'data'    => $data,
        ], 200 );
    } );

    Route::get( 'hospital-search', function ( Request $request ) {
        $keyword = ( $request->input( 'keyword' ) ) ? $request->input( 'keyword' ) : '';

        try {
            $data = DB::table( 'tpa_hospital' )
            //->where( 'name_' . App::getLocale(), 'like', '%' . $keyword . '%' )
                ->where( 'name_th', 'like', '%' . $keyword . '%' )
                ->orWhere( 'name_en', 'like', '%' . $keyword . '%' )
                ->select( 'code', 'name_' . App::getLocale() . ' as name' )
                ->get();

            return response()->json( [
                'status'  => 'ok',
                'message' => __( 'messages.success' ),
                'data'    => $data,
            ], 200 );

        } catch ( Exception $e ) {
            return response()->json( [
                'status'    => 'error',
                'message'   => __( 'Internal server error.' ),
                'exception' => $e->getMessage(),
            ], 500 );

        }

    } );

    Route::get( 'hospital', function ( Request $request ) {
        $group = ( $request->input( 'group' ) ) ? $request->input( 'group' ) : '';
        $latitude = ( $request->input( 'latitude' ) ) ? $request->input( 'latitude' ) : 13.7874812;
        $longitude = ( $request->input( 'longitude' ) ) ? $request->input( 'longitude' ) : 100.6309176;
        $is_limit = ( $request->input( 'is_limit' ) == 'true' ) ? (string) $request->input( 'is_limit' ) : 'false';
        $hospital_type = [
            '1' => 'ครอบคลุมทั้งหมด',
            '2' => 'ครอบคลุมบางรายการ',
        ];

        $query = DB::table( 'tpa_hospital' )->select( 'code', 'name_' . App::getLocale() . ' AS name', 'latitude', 'longitude', 'group', 'type' )
            ->whereNotNull( 'longitude' )
            ->whereNotNull( 'latitude' );
        if ( !empty( $group ) ) {
            $query->where( 'group', $group );
        }

        $rs = $query->get();
        $data = [];
        foreach ( $rs as $row ) {
            $distance = distance( $latitude, $longitude, floatval( $row->latitude ), floatval( $row->longitude ) );
            $row->distance = cut_num( $distance ) . ' km.';
            if ( $is_limit == 'true' ) {
                if ( $row->distance <= 2 ) {
                    $row->full_distance = $distance;
                    if ( !empty( $row->group ) ) {
                        $g = DB::table( 'tpa_hospital_group' )
                            ->where( 'id', $row->group )->select( 'name_' . App::getLocale() . ' AS name' )
                            ->first();
                        $row->group = $g->name;
                    } else {
                        $row->group = null;
                    }
                    $row->type = $hospital_type[$row->type];
                    array_push( $data, $row );
                }
            } else {
                $row->full_distance = $distance;
                if ( !empty( $row->group ) ) {
                    $g = DB::table( 'tpa_hospital_group' )
                        ->where( 'id', $row->group )->select( 'name_' . App::getLocale() . ' AS name' )
                        ->first();
                    $row->group = $g->name;
                } else {
                    $row->group = null;
                }
                $row->type = $hospital_type[$row->type];
                array_push( $data, $row );

            }

        }
        $full_distance = array_column( $data, 'full_distance' );
        array_multisort( $full_distance, SORT_ASC, $data );

        return response()->json( [
            'status'  => 'ok',
            'message' => __( 'messages.success' ),
            'data'    => $data,
        ], 200 );
    } );

    Route::get( 'hospital/{code}', function ( $code, Request $request ) {
        $latitude = ( $request->input( 'latitude' ) ) ? $request->input( 'latitude' ) : 13.7874812;
        $longitude = ( $request->input( 'longitude' ) ) ? $request->input( 'longitude' ) : 100.6309176;

        $addrs = "CASE WHEN '" . App::getLocale() . "' = 'th' THEN location WHEN '" . App::getLocale() . "' = 'en' AND location_en is not null THEN location_en collate Thai_CI_AS else location END location,
                  CASE WHEN '" . App::getLocale() . "' = 'th' THEN address_no WHEN '" . App::getLocale() . "' = 'en' AND address_no_en is not null THEN address_no_en collate Thai_CI_AS else address_no END address_no,
                  CASE WHEN '" . App::getLocale() . "' = 'th' THEN address_road WHEN '" . App::getLocale() . "' = 'en' AND address_road_en is not null THEN address_road_en collate Thai_CI_AS else address_road END address_road,
                  CASE WHEN '" . App::getLocale() . "' = 'th' THEN address_amphur WHEN '" . App::getLocale() . "' = 'en' AND address_amphur_en is not null THEN address_amphur_en collate Thai_CI_AS else address_amphur END address_amphur,
                  CASE WHEN '" . App::getLocale() . "' = 'th' THEN address_province WHEN '" . App::getLocale() . "' = 'en' AND address_province_en is not null THEN address_province_en collate Thai_CI_AS else address_province END address_province,
                  CASE WHEN '" . App::getLocale() . "' = 'th' THEN address_postcode WHEN '" . App::getLocale() . "' = 'en' AND address_postcode_en is not null THEN address_postcode_en collate Thai_CI_AS else address_postcode END address_postcode";

        $row = DB::table( 'tpa_hospital' )->select(
            'code', 'website', 'latitude', 'longitude', 'telephone',
            //DB::raw("case when '".App::getLocale()."' = 'th' then 'th' else 'en' end as langs"),
            DB::raw( $addrs ),
            //'location','address_no','address_road','address_amphur','address_province','address_postcode',
            'type', 'group', 'show_service_fee', 'show_description', 'updated_at',
            'name_' . App::getLocale() . ' AS name', 'service_fee_' . App::getLocale() . ' AS service_fee', 'service_times_' . App::getLocale() . ' AS service_times', 'description_' . App::getLocale() . ' AS description', 'show_service_fee', 'show_description'

        )
            ->where( 'code', $code )->first();
        $distance = distance( $latitude, $longitude, floatval( $row->latitude ), floatval( $row->longitude ) );
        $row->distance = cut_num( $distance );
        $row->full_distance = $distance;

        $rs = DB::table( 'tpa_hospital_pictures' )->where( 'hospital_code', $code )->get();

        foreach ( $rs as $pic ) {

            $row->pictures[] = url( $pic->path );
        }

        unset( $row->name_th );
        unset( $row->name_en );
        unset( $row->service_fee_th );
        unset( $row->service_fee_en );
        unset( $row->service_times_th );
        unset( $row->service_times_en );
        unset( $row->description_th );
        unset( $row->description_en );

        if ( $row->show_description != 1 ) {
            $row->description = '';
        }

        if ( $row->show_service_fee != 1 ) {
            $row->service_fee = '';
        }

        return response()->json( [
            'status'  => 'ok',
            'message' => __( 'messages.success' ),
            'data'    => $row,
        ], 200 );
    } );

    Route::get( 'promotions', function ( Request $request ) {
        $category_id = ( $request->input( 'category_id' ) ) ? $request->input( 'category_id' ) : '';
        $keyword = ( $request->input( 'keyword' ) ) ? $request->input( 'keyword' ) : '';

        $page_no = ( $request->input( 'page_no' ) ) ? (int) $request->input( 'page_no' ) : 1;
        $page_size = ( $request->input( 'page_size' ) ) ? (int) $request->input( 'page_size' ) : 10;
        if ( $page_no > 1 ) {
            $skip = $page_no * $page_size;
        } else {
            $skip = 0;
        }

        try {
            $query = DB::table( 'tpa_promotions as p' )
                ->leftjoin( 'tpa_hospital as h', 'h.code', '=', 'p.hospital_code' )
                ->where( 'p.status', 1 )
                ->where( 'p.start_date', '<=', date( 'Y-m-d' ) )
                ->where( 'p.end_date', '>=', date( 'Y-m-d' ) )
                ->select( DB::raw( 'p.id,p.title_' . App::getLocale() . ' AS title,h.name_' . App::getLocale() . ' AS hospital,FORMAT(p.created_at, \'dd/MM/yyyy\') AS date' ) );

            if ( !empty( $category_id ) ) {
                $query->where( 'category_id', $category_id );
            }

            if ( !empty( $keyword ) ) {
                $query->where( 'p.title_' . App::getLocale(), 'like', '%' . $keyword . '%' );
            }

            $total_record = $query->count();
            $data = $query->skip( $skip )
                ->limit( $page_size )
                ->get();

            if (  ( $total_record % $page_size ) > 0 ) {
                $total_page = floor( $total_record / $page_size );
            } else {
                $total_page = $total_record / $page_size;
            }

            foreach ( $data as $p => $val ) {
                $picture = DB::table( 'tpa_pictures' )->where( 'ref_id', $val->id )->where( 'picture_type', 1 )->first();
                $data[$p]->picture = ( $picture->path ) ? url( $picture->path ) : '';
            }
            return response()->json( [
                'status'     => 'ok',
                'message'    => __( 'messages.success' ),
                'data'       => $data,
                'pagination' => [
                    'page_no'      => $page_no,
                    'page_size'    => $page_size,
                    'total_page'   => $total_page,
                    'total_record' => $total_record,
                ],
            ], 200 );
        } catch ( Exception $e ) {
            return response()->json( [
                'status'    => 'error',
                'message'   => __( 'Internal server error.' ),
                'exception' => $e->getMessage(),
            ], 500 );

        }
    } );

    Route::get( 'promotions/{id}', function ( $id ) {
        $data = DB::table( 'tpa_promotions as p' )
            ->leftjoin( 'tpa_hospital as h', 'h.code', '=', 'p.hospital_code' )
            ->where( 'p.id', $id )
            ->select( DB::raw( 'p.id,p.title_' . App::getLocale() . ' AS title,p.detail_' . App::getLocale() . ' AS detail,h.name_' . App::getLocale() . ' AS hospital,FORMAT(p.start_date, \'dd/MM/yyyy\') AS start_date,FORMAT(p.end_date, \'dd/MM/yyyy\') AS end_date,p.age_range,p.gender' ) )
            ->first();
        $data->date = $data->start_date . ' - ' . $data->end_date;
        unset( $data->start_date );
        unset( $data->end_date );
        $rs = DB::table( 'tpa_pictures' )->where( 'ref_id', $data->id )->where( 'picture_type', 1 )->get();

        foreach ( $rs as $pic ) {

            $data->pictures[] = url( $pic->path );
        }

        $age = DB::table( 'tpa_age_range' )->where( 'id', $data->age_range )->select( 'title_' . App::getLocale() . ' AS title' )->first();
        $data->age_range = $age->title;
        $gender = ( $data->gender ) ? $data->gender : 'all';
        $gender_th = [
            'all'    => 'ทุกเพศ',
            'male'   => 'ชาย',
            'female' => 'หญิง',
        ];

        $gender_en = [
            'all'    => 'All',
            'male'   => 'Male',
            'female' => 'Female',
        ];

        if ( App::getLocale() == 'en' ) {
            $data->gender = $gender_en[$gender];
        } else {
            $data->gender = $gender_th[$gender];
        }

        return response()->json( [
            'status'  => 'ok',
            'message' => __( 'messages.success' ),
            'data'    => $data,
        ], 200 );
    } );

    Route::get( 'promotions-category', function ( Request $request ) {

        $data = DB::table( 'tpa_promotions_category' )
            ->where( 'status', 1 )->select( 'id', 'name_' . App::getLocale() . ' AS name' )
            ->get();

        return response()->json( [
            'status'  => 'ok',
            'message' => __( 'messages.success' ),
            'data'    => $data,
        ], 200 );
    } );

    Route::get( 'promotions-search', function ( Request $request ) {
        $keyword = ( $request->input( 'keyword' ) ) ? $request->input( 'keyword' ) : '';

        try {
            $data = DB::table( 'tpa_promotions' )
                ->where( 'title_' . App::getLocale(), 'like', '%' . $keyword . '%' )
                ->select( 'id', 'title_' . App::getLocale() . ' AS name' )
                ->get();

            return response()->json( [
                'status'  => 'ok',
                'message' => __( 'messages.success' ),
                'data'    => $data,
            ], 200 );

        } catch ( Exception $e ) {
            return response()->json( [
                'status'    => 'error',
                'message'   => __( 'Internal server error.' ),
                'exception' => $e->getMessage(),
            ], 500 );

        }

    } );

    Route::get( 'articles-category', function ( Request $request ) {

        $data = DB::table( 'tpa_article_category' )
            ->where( 'status', 1 )->select( 'id', 'name_' . App::getLocale() . ' AS name' )
            ->get();

        return response()->json( [
            'status'  => 'ok',
            'message' => __( 'messages.success' ),
            'data'    => $data,
        ], 200 );
    } );

    Route::get( 'articles-search', function ( Request $request ) {
        $keyword = ( $request->input( 'keyword' ) ) ? $request->input( 'keyword' ) : '';

        try {
            $data = DB::table( 'tpa_article' )
                ->where( 'title_' . App::getLocale(), 'like', '%' . $keyword . '%' )
                ->select( 'id', 'title_' . App::getLocale() . ' AS name' )
                ->get();

            return response()->json( [
                'status'  => 'ok',
                'message' => __( 'messages.success' ),
                'data'    => $data,
            ], 200 );

        } catch ( Exception $e ) {
            return response()->json( [
                'status'    => 'error',
                'message'   => __( 'Internal server error.' ),
                'exception' => $e->getMessage(),
            ], 500 );

        }

    } );

    Route::get( 'articles', function ( Request $request ) {
        $category_id = ( $request->input( 'category_id' ) ) ? $request->input( 'category_id' ) : '';
        $keyword = ( $request->input( 'keyword' ) ) ? $request->input( 'keyword' ) : '';
        $page_no = ( $request->input( 'page_no' ) ) ? (int) $request->input( 'page_no' ) : 1;
        $page_size = ( $request->input( 'page_size' ) ) ? (int) $request->input( 'page_size' ) : 10;
        if ( $page_no > 1 ) {
            $skip = $page_no * $page_size;
        } else {
            $skip = 0;
        }

        try {
            $query = DB::table( 'tpa_article as a' )
                ->leftjoin( 'tpa_hospital as h', 'h.code', '=', 'a.hospital_code' )
                ->where( 'a.status', 1 )
                ->select( DB::raw( 'a.id,a.title_' . App::getLocale() . ' AS title,h.name_' . App::getLocale() . ' AS hospital,FORMAT(a.created_at, \'dd/MM/yyyy\') AS date' ) );
            if ( !empty( $category_id ) ) {
                $query->where( 'category_id', $category_id );
            }

            if ( !empty( $keyword ) ) {
                $query->where( 'a.title_' . App::getLocale(), 'like', '%' . $keyword . '%' );
            }

            $total_record = $query->count();
            $data = $query->skip( $skip )
                ->limit( $page_size )
                ->get();

            if (  ( $total_record % $page_size ) > 0 ) {
                $total_page = floor( $total_record / $page_size );
            } else {
                $total_page = $total_record / $page_size;
            }

            foreach ( $data as $p => $val ) {
                $picture = DB::table( 'tpa_pictures' )->where( 'ref_id', $val->id )->where( 'picture_type', 2 )->first();
                $data[$p]->picture = ( $picture->path ) ? url( $picture->path ) : '';

            }
            return response()->json( [
                'status'     => 'ok',
                'message'    => __( 'messages.success' ),
                'data'       => $data,
                'pagination' => [
                    'page_no'      => $page_no,
                    'page_size'    => $page_size,
                    'total_page'   => $total_page,
                    'total_record' => $total_record,
                ],
            ], 200 );
        } catch ( Exception $e ) {
            return response()->json( [
                'status'    => 'error',
                'message'   => __( 'Internal server error.' ),
                'exception' => $e->getMessage(),
            ], 500 );

        }

    } );

    Route::get( 'articles/{id}', function ( $id ) {
        try {

            $data = DB::table( 'tpa_article as a' )
                ->leftjoin( 'tpa_hospital as h', 'h.code', '=', 'a.hospital_code' )
                ->where( 'a.id', $id )
                ->select( DB::raw( 'a.id,a.title_' . App::getLocale() . ' AS title,a.detail_' . App::getLocale() . ' AS detail,h.name_' . App::getLocale() . ' AS hospital,FORMAT(a.created_at, \'dd/MM/yyyy\') AS created_at' ) )
                ->first();
            $rs = DB::table( 'tpa_pictures' )->where( 'ref_id', $data->id )->where( 'picture_type', 2 )->get();

            foreach ( $rs as $pic ) {

                $data->pictures[] = url( $pic->path );
            }
            return response()->json( [
                'status'  => 'ok',
                'message' => __( 'messages.success' ),
                'data'    => $data,
            ], 200 );

        } catch ( Exception $e ) {
            return response()->json( [
                'status'    => 'error',
                'message'   => __( 'Internal server error.' ),
                'exception' => $e->getMessage(),
            ], 500 );

        }

    } );

    Route::post( 'PNCallToken', function ( Request $request ) {

        try {

            cb()->validation( [
                'citizen_id'   => 'required|max:13|min:13',
                'policy_no'    => 'required',
                'insurer_code' => 'required',
                'dates'        => 'required|date_format:Ymd',
                'secret_key'   => 'required',
            ] );

            $subtext = ':';
            $plaintext = $request->citizen_id . $subtext . $request->policy_no . $subtext . $request->insurer_code . $subtext . $request->dates . $subtext . $request->secret_key;
            $key = $request->secret_key;
            $ivlen = openssl_cipher_iv_length( $cipher = "AES-128-CBC" );
            $iv = openssl_random_pseudo_bytes( $ivlen );
            $ciphertext_raw = openssl_encrypt( $plaintext, $cipher, $key, $options = OPENSSL_RAW_DATA, $iv );
            $hmac = hash_hmac( 'sha256', $ciphertext_raw, $key, $as_binary = true );
            $ciphertext = base64_encode( $iv . $hmac . $ciphertext_raw );

            $ciphertext = rtrim( strtr( base64_encode( $ciphertext ), '+/', '-_' ), '=' );

            return response()->json( [
                'status' => '1',
                'token'  => $ciphertext,
            ] );

        } catch ( CBValidationException $e ) {
            return response()->json( [
                'status'  => '0',
                'message' => $e->getMessage(),
            ] );
        }
    } );

    Route::get( 'claimhistory', function ( Request $request ) {

        try {

            cb()->validation( [
                'token'      => 'required',
                'partner_id' => 'required',
            ] );

            $count = DB::table( 'tpa_partner' )->select( 'partner_id' )->where( 'partner_id', $request->partner_id )->count();
            if ( $count == 0 ) {
                return response()->json( [
                    'status'  => '0',
                    'message' => 'No partner information found.',
                ] );
            }

            $partner = DB::table( 'tpa_partner' )
                ->select( 'ins_code'
                    , 'name_th'
                    , 'name_en'
                    , 'history_cf1'
                    , 'history_cf1_c'
                    , 'history_cf2'
                    , 'history_cf2_c'
                    , 'history_cb'
                    , 'history_cb_c'
                    , 'secret_key'
                    , 'partner_id'
                    , 'cn_flg'
                )
                ->where( 'partner_id', $request->partner_id )->first();

            //decrypt later....
            $key = $partner->secret_key;
            $ciphertext = base64_decode( str_pad( strtr( $request->token, '-_', '+/' ), strlen( $request->token ) % 4, '=', STR_PAD_RIGHT ) );

            $c = base64_decode( $ciphertext );
            $ivlen = openssl_cipher_iv_length( $cipher = "AES-128-CBC" );
            $iv = substr( $c, 0, $ivlen );
            $hmac = substr( $c, $ivlen, $sha2len = 32 );
            $ciphertext_raw = substr( $c, $ivlen + $sha2len );
            $original_plaintext = openssl_decrypt( $ciphertext_raw, $cipher, $key, $options = OPENSSL_RAW_DATA, $iv );
            $calcmac = hash_hmac( 'sha256', $ciphertext_raw, $key, $as_binary = true );
            if ( hash_equals( $hmac, $calcmac ) ) //PHP 5.6+ timing attack safe comparison
            {
                $subtext = ':';
                $plaintexts = explode( $subtext, $original_plaintext );
                $citizen_id = $plaintexts[0];
                $policy_no = $plaintexts[1];
                $insurer_code = $plaintexts[2];
                $dates = $plaintexts[3];
                $secret_key = $plaintexts[4];

                if ( date( 'Ymd' ) != $dates ) {
                    return response()->json( [
                        'status'  => '0',
                        'message' => 'Expired date token',
                    ] );
                }

                $client = new GuzzleHttp\Client();
                $res = $client->request( 'POST', env( 'TPA_URL' ) . 'login', [
                    'headers' => ['Content-Type' => 'application/json'],
                    'body'    => json_encode( [
                        'Username' => env( 'TPA_USER' ),
                        'Password' => env( 'TPA_PASS' ),
                    ] ),
                    'verify'  => false,
                ] );

                $token = json_decode( $res->getBody()->getContents() );
                if ( empty( $token ) ) {
                    return response()->json( [
                        'status'  => 'error',
                        'message' => __( 'Internal server error.' ),
                    ], 500 );
                } else {

                    $client = new GuzzleHttp\Client();
                    $res = $client->request( 'POST', env( 'TPA_URL' ) . 'ClaimHistory', [
                        'headers' => [
                            'Content-Type'  => 'application/json',
                            'Authorization' => $token,
                        ],
                        'body'    => json_encode( [
                            'CITIZEN_ID'   => $citizen_id,
                            'POL_NO'       => $policy_no,
                            'INSURER_CODE' => $insurer_code,
                        ] ),
                        'verify'  => false,
                    ] );

                    $httpCode = $res->getStatusCode();
                    if ( $httpCode != 200 ) {
                        return response()->json( [
                            'status'  => '0',
                            'message' => __( 'Internal server error.' ),
                        ], $httpCode );
                    }

                    $body = json_decode( $res->getBody()->getContents() );

                    return response()->json( [
                        'status'      => '1',
                        'claimstatus' => $body,
                        'color'       => $partner,
                    ] );
                }

                return $plaintexts;
            } else {
                return response()->json( [
                    'status'  => 'error',
                    'message' => 'Expired key token',
                ] );
            }

        } catch ( CBValidationException $e ) {
            return response()->json( [
                'status'  => '0',
                'message' => $e->getMessage(),
            ] );
        }

    } );

    Route::get( 'claimstatusext', function ( Request $request ) {

        try {

            cb()->validation( [
                'token'      => 'required',
                'partner_id' => 'required',
            ] );

            $count = DB::table( 'tpa_partner' )->select( 'partner_id' )->where( 'partner_id', $request->partner_id )->count();
            if ( $count == 0 ) {
                return response()->json( [
                    'status'  => '0',
                    'message' => 'No partner information found.',
                ] );
            }

            //search secret key from partner_id
            $partner = DB::table( 'tpa_partner' )
                ->select( 'ins_code'
                    , 'name_th'
                    , 'name_en'
                    , 'status_cf1'
                    , 'status_cf1_c'
                    , 'status_cf2'
                    , 'status_cf2_c'
                    , 'status_ccb1'
                    , 'status_ccb1_c'
                    , 'status_cb1'
                    , 'status_cb1_c'
                    , 'detail_cf1'
                    , 'detail_cf1_c'
                    , 'detail_cf2'
                    , 'detail_cf2_c'
                    , 'detail_cf3'
                    , 'detail_cf3_c'
                    , 'detail_cb'
                    , 'detail_cb_c'
                    , 'secret_key'
                    , 'partner_id'
                    , 'cn_flg'
                )
                ->where( 'partner_id', $request->partner_id )->first();
            //decrypt token
            $key = $partner->secret_key;
            $ciphertext = base64_decode( str_pad( strtr( $request->token, '-_', '+/' ), strlen( $request->token ) % 4, '=', STR_PAD_RIGHT ) );
            $c = base64_decode( $ciphertext );
            $ivlen = openssl_cipher_iv_length( $cipher = "AES-128-CBC" );
            $iv = substr( $c, 0, $ivlen );
            $hmac = substr( $c, $ivlen, $sha2len = 32 );
            $ciphertext_raw = substr( $c, $ivlen + $sha2len );
            $original_plaintext = openssl_decrypt( $ciphertext_raw, $cipher, $key, $options = OPENSSL_RAW_DATA, $iv );
            $calcmac = hash_hmac( 'sha256', $ciphertext_raw, $key, $as_binary = true );
            if ( hash_equals( $hmac, $calcmac ) ) //PHP 5.6+ timing attack safe comparison
            {
                $subtext = ':';
                $plaintexts = explode( $subtext, $original_plaintext );
                $citizen_id = $plaintexts[0];
                $policy_no = $plaintexts[1];
                $insurer_code = $plaintexts[2];
                $dates = $plaintexts[3];
                $secret_key = $plaintexts[4];

                if ( date( 'Ymd' ) != $dates ) {
                    return response()->json( [
                        'status'  => '0',
                        'message' => 'Expired date token',
                    ] );
                }

                $client = new GuzzleHttp\Client();
                $res = $client->request( 'POST', env( 'TPA_URL' ) . 'login', [
                    'headers' => ['Content-Type' => 'application/json'],
                    'body'    => json_encode( [
                        'Username' => env( 'TPA_USER' ),
                        'Password' => env( 'TPA_PASS' ),
                    ] ),
                    'verify'  => false,
                ] );

                $token = json_decode( $res->getBody()->getContents() );
                if ( empty( $token ) ) {
                    return response()->json( [
                        'status'  => 'error',
                        'message' => __( 'Internal server error.' ),
                    ], 500 );
                } else {

                    $client = new GuzzleHttp\Client();
                    $res = $client->request( 'POST', env( 'TPA_URL' ) . 'ClaimStatusExt', [
                        'headers' => [
                            'Content-Type'  => 'application/json',
                            'Authorization' => $token,
                        ],
                        'body'    => json_encode( [
                            'CITIZEN_ID'   => $citizen_id,
                            'POL_NO'       => $policy_no,
                            'INSURER_CODE' => $insurer_code,
                        ] ),
                        'verify'  => false,
                    ] );

                    $httpCode = $res->getStatusCode();
                    if ( $httpCode != 200 ) {
                        return response()->json( [
                            'status'  => '0',
                            'message' => __( 'Internal server error.' ),
                        ], $httpCode );
                    }

                    $body = json_decode( $res->getBody()->getContents() );

                    return response()->json( [
                        'status'      => '1',
                        'claimstatus' => $body,
                        'color'       => $partner,
                    ] );
                }

                return $plaintexts;
            } else {
                return response()->json( [
                    'status'  => 'error',
                    'message' => 'Expired key token',
                ] );
            }

        } catch ( CBValidationException $e ) {
            return response()->json( [
                'status'  => '0',
                'message' => $e->getMessage(),
            ] );
        }

    } );

    Route::post( 'signin', function ( Request $request ) {
        try {
            cb()->validation( [
                'username'     => 'required',
                //'password'     => 'required|max:15|min:8',
                'password'     => 'required|max:15',
                'device_type'  => 'required',
                'device_token' => 'required',
            ] );
            $credential = request()->only( ['username', 'password'] );
            $credential['status'] = 1;
            $credential['channel'] = 2;
            //Log::critical('Showing user profile for user: ', $credential);

            /**/

            $old_query = DB::table( 'users' )->select( 'is_new_pass' )
                ->where( 'username', $request->input( 'username' ) )
                ->whereNotNull( 'ref_id' )
                ->where( 'is_new_pass', 0 );

            if ( $old_query->count() > 0 ) {
                return response()->json( [
                    'status'  => 'ok',
                    'message' => __( 'messages.success' ),
                    'data'    => [
                        'access_token' => null,
                        'is_pin'       => false,
                        'is_old_user'  => true,
                    ],
                ], 200 );

            } else {
                if ( auth()->attempt( $credential ) ) {
                    $user = auth()->user();
                    $uid = $user->id;
                    $query = DB::table( 'tpa_users_device' )->select( 'udid' )
                        ->where( 'uid', $user->id )
                        ->where( 'device_type', $request->input( 'device_type' ) )
                        ->where( 'device_token', $request->input( 'device_token' ) );

                    $count = $query->count();
                    $current_time = date( 'Y-m-d H:i:s' );

                    if ( $count > 0 ) {
                        $ud = $query->first();
                        $udid = $ud->udid;

                        DB::table( 'tpa_users_device' )
                            ->where( 'udid', $udid )
                            ->update( [
                                'ip_address' => $request->ip(),
                                'user_agent' => $request->server( 'HTTP_USER_AGENT' ),
                                'login_at'   => $current_time,
                            ] );
                    } else {
                        $udid = DB::table( 'tpa_users_device' )->insertGetId( [
                            'uid'          => $uid,
                            'device_type'  => $request->input( 'device_type' ),
                            'device_token' => $request->input( 'device_token' ),
                            'ip_address'   => $request->ip(),
                            'user_agent'   => $request->server( 'HTTP_USER_AGENT' ),
                            'created_at'   => $current_time,
                            'login_at'     => $current_time,
                        ] );
                    }

                    $iat = time();
                    $payload = [
                        'uid'  => $uid,
                        'udid' => $udid,
                        'iat'  => $iat,
                        // 'exp'  => $iat + 86400,
                        'exp'  => $iat + 2629746,
                    ];
                    $access_token = JWT::encode( $payload, env( 'COLLECTION' ) );

                    DB::table( 'users' )
                        ->where( 'id', $uid )
                        ->update( [
                            'access_token' => $access_token,
                            'login_at'     => $current_time,
                        ] );

                    return response()->json( [
                        'status'  => 'ok',
                        'message' => __( 'messages.success' ),
                        'data'    => [
                            'access_token' => $access_token,
                            'is_pin'       => ( $user->pin ) ? true : false,
                            'is_old_user'  => false,
                        ],
                    ], 200 );
                } else {
                    return response()->json( [
                        'status'  => 'error',
                        'message' => cbLang( 'password_and_username_is_wrong' ),
                    ], 500 );
                }
            }

        } catch ( CBValidationException $e ) {
            return response()->json( [
                'status'  => 'error',
                'message' => $e->getMessage(),
            ], 500 );
        }
    } );

    Route::group( ['prefix' => 'register'], function () {
        Route::get( '/', function ( Request $request ) {
            $uid = decode_register_token( $request->header( 'register-token' ) );
            $data = [];
            try {
                if ( !empty( $uid ) ) {
                    $data = DB::table( 'users' )->select( 'username', 'name', 'surname', 'mobile' )->where( 'id', $uid )->where( 'status', 0 )->first();
                }
            } catch ( DecryptException $e ) {
            }

            return response()->json( [
                'status' => 'ok',
                'data'   => $data,
            ], 200 );
        } );

        Route::post( '/', function ( Request $request ) {
            try {
                $remember_token = $request->header( 'register-token' );
                $uid = decode_register_token( $remember_token );
                $validator = [
                    'username' => 'required',
                    'password' => 'required|max:15|min:8|required_with:passconf|same:passconf',
                    'passconf' => 'required|max:15|min:8',
                    'name'     => 'required',
                    'surname'  => 'required',
                    'mobile'   => 'required',
                ];

                if ( empty( $uid ) && !empty( $request->input( 'username' ) ) && !empty( $request->input( 'mobile' ) ) ) {
                    //$validator['username'] .= '|unique:users';
                    $count = DB::table( 'users' )->select( 'id' )
                        ->where( 'username', $request->input( 'username' ) )
                        ->where( 'status', 1 )
                        ->where( 'mobile', $request->input( 'mobile' ) )
                        ->count();

                    if ( $count > 0 ) {
                        return response()->json( [
                            'status'  => 'error',
                            'message' => __( 'validation.custom.account.unique' ),
                        ], 500 );
                    } else {
                        $query = DB::table( 'users' )->select( 'id' )
                            ->where( 'username', $request->input( 'username' ) )
                            ->where( 'status', 0 )
                            ->where( 'mobile', $request->input( 'mobile' ) );
                        $count = $query->count();

                        if ( $count <= 0 ) {
                            $validator['username'] .= '|unique:users';
                            $validator['mobile'] .= '|unique:users';
                        } else {
                            $u = $query->first();
                            $uid = $u->id;
                        }

                    }

                } else {
                    /*$count = DB::table( 'users' )->select( 'id' )->where( 'id', '<>', $uid )
                    ->whereNotIn( 'status', [0, 1, 2] )
                    ->where( 'channel', 2 )->count();
                    if ( $count > 0 ) {
                    return response()->json( [
                    'status'  => 'error',
                    'message' => __( 'validation.custom.username.unique' ),
                    ], 500 );
                    }*/

                    $count = DB::table( 'users' )->select( 'id' )->where( 'id', $uid )
                        ->where( 'status', 1 )->count();
                    if ( $count > 0 ) {
                        return response()->json( [
                            'status'  => 'error',
                            'message' => __( 'validation.custom.account.unique' ),
                        ], 500 );
                    }

                }

                cb()->validation( $validator );

                $data = [
                    'username'   => $request->input( 'username' ),
                    'password'   => Hash::make( $request->input( 'password' ) ),
                    'name'       => $request->input( 'name' ),
                    'surname'    => $request->input( 'surname' ),
                    'mobile'     => $request->input( 'mobile' ),
                    'ip_address' => $request->ip(),
                    'user_agent' => $request->server( 'HTTP_USER_AGENT' ),
                    'channel'    => 2,
                    'status'     => 0,
                    'created_at' => date( 'Y-m-d H:i:s' ),
                ];

                if ( !empty( $uid ) ) {
                    unset( $data['created_at'] );
                    $data['updated_at'] = date( 'Y-m-d H:i:s' );
                    DB::table( 'users' )->where( 'id', $uid )->update( $data );
                } else {
                    $uid = DB::table( 'users' )->insertGetId( $data );
                }

                $remember_token = encrypt( $uid );
                DB::table( 'users' )->where( 'id', $uid )->update( [
                    'remember_token' => $remember_token,
                ] );

                DB::table( 'tpa_otp' )
                    ->where( 'uid', $uid )
                    ->where( 'otp_type', '1' )
                    ->where( 'verified', '0' )
                    ->update( ['verified' => -1] );

                $otp = GenerateOTP();
                $ref_no = GenerateRefOTP();
                $minute = tpa_config( 'ttl_otp_register' );
                DB::table( 'tpa_otp' )->insert( [
                    'uid'        => $uid,
                    'mobile'     => $data['mobile'],
                    'otp'        => $otp,
                    'ref_no'     => $ref_no,
                    'created_at' => date( "Y-m-d H:i:s" ),
                    'expired_at' => date( "Y-m-d H:i:s", strtotime( "+" . $minute . " min" ) ),
                ] );

                $msdata = 'รหัส OTP ในการลงทะเบียนของคุณคือ ' . $otp . ' รหัสอ้างอิง ' . $ref_no . ' (มีอายุ ' . $minute . ' นาที)';
                if ( App::getLocale() == 'en' ) {
                    $msdata = $otp . ' is your OTP to register (ref. ' . $ref_no . '). OTP will be expired within ' . $minute . ' minute.';
                }

                send_sms( [
                    'id'      => date( 'YmdHis' ) . rand( 3, 3 ),
                    'msisdn'  => $data['mobile'],
                    'msgtype' => 'T',
                    'msdata'  => build_otp_message( App::getLocale(), $ref_no, $otp ),
                ] );

                return response()->json( [
                    'status'         => 'ok',
                    'register_token' => $remember_token,
                    'otp'            => $otp,
                    'ref_no'         => $ref_no,
                    'ttl_minute'     => $minute,
                ], 200 );
            } catch ( CBValidationException $e ) {
                return response()->json( [
                    'status'   => 'error',
                    'message'  => $e->getMessage(),
                    'messages' => explode( '; ', $e->getMessage() ),
                ], 500 );
            } catch ( Exception $e ) {
                return response()->json( [
                    'status'  => 'error',
                    'message' => $e->getMessage(),
                ], 500 );
            }
        } );

        Route::post( 'verifyotp', function ( Request $request ) {
            try {
                $remember_token = $request->header( 'register-token' );
                $uid = decode_register_token( $remember_token );

                if ( empty( $uid ) ) {
                    return response()->json( [
                        'status'    => 'error',
                        'message'   => 'เกิดข้อผิดพลาดไม่สามารถ ยืนยันรหัส OTP ได้',
                        'exception' => 'register-token is empty.',
                    ], 500 );
                }

                $validator = [
                    'otp'          => 'required',
                    'device_type'  => 'required',
                    'device_token' => 'required',
                ];
                cb()->validation( $validator );

                $otp = DB::table( 'tpa_otp' )->select( 'id' )
                    ->where( 'uid', $uid )
                    ->where( 'otp', $request->input( 'otp' ) )
                    ->where( 'otp_type', 1 )
                    ->where( 'verified', 0 )
                    ->where( 'expired_at', '>=', date( 'Y-m-d H:i:s' ) )
                    ->first();

                if ( !$otp ) {
                    return response()->json( [
                        'status'  => 'error',
                        'message' => 'OTP ไม่ถูกต้อง',
                    ], 500 );
                } else {
                    $current_time = date( 'Y-m-d H:i:s' );
                    $affected_row = DB::table( 'tpa_otp' )
                        ->where( 'id', $otp->id )
                        ->update( [
                            'verified'    => 1,
                            'verified_at' => $current_time,
                        ] );
                    $access_token = '';

                    if ( $affected_row > 0 ) {
                        $udid = DB::table( 'tpa_users_device' )->insertGetId( [
                            'uid'          => $uid,
                            'device_type'  => $request->input( 'device_type' ),
                            'device_token' => $request->input( 'device_token' ),
                            'ip_address'   => $request->ip(),
                            'user_agent'   => $request->server( 'HTTP_USER_AGENT' ),
                            'created_at'   => $current_time,
                            'login_at'     => $current_time,
                        ] );

                        $iat = time();
                        $payload = [
                            'uid'  => $uid,
                            'udid' => $udid,
                            'iat'  => $iat,
                            'exp'  => $iat + 86400,
                        ];
                        $access_token = JWT::encode( $payload, env( 'COLLECTION' ) );

                        DB::table( 'users' )
                            ->where( 'id', $uid )
                            ->update( [
                                'status'         => 1,
                                'access_token'   => $access_token,
                                'remember_token' => '',
                                'updated_at'     => $current_time,
                                'login_at'       => $current_time,
                            ] );

                        $condition_id = DB::table( 'tpa_agreement' )->select( 'agreement_id as id' )
                            ->where( 'agreement_status', 1 )
                            ->where( 'agreement_type', 1 )
                            ->first()->id;
                        $private_policy_id = DB::table( 'tpa_agreement' )->select( 'agreement_id as id' )
                            ->where( 'agreement_status', 1 )
                            ->where( 'agreement_type', 2 )
                            ->first()->id;

                        DB::table( 'tpa_consent' )->insert( [
                            'uid'          => $uid,
                            'agreement_id' => $condition_id,
                            'created_date' => $current_time,
                        ] );

                        DB::table( 'tpa_consent' )->insert( [
                            'uid'          => $uid,
                            'agreement_id' => $private_policy_id,
                            'created_date' => $current_time,
                        ] );

                        return response()->json( [
                            'status' => 'ok',
                            'data'   => [
                                'access_token' => $access_token,
                            ],
                        ], 200 );
                    } else {
                        return response()->json( [
                            'status'  => 'error',
                            'message' => 'ไม่สามารถตรวจสอบรหัส OTP ได้',
                        ], 500 );
                    }
                }
            } catch ( CBValidationException $e ) {
                return response()->json( [
                    'status'  => 'error',
                    'message' => $e->getMessage(),
                ], 500 );
            } catch ( Exception $e ) {
                return response()->json( [
                    'status'  => 'error',
                    'message' => $e->getMessage(),
                ], 500 );
            }
        } );

        Route::get( 'resendotp', function ( Request $request ) {
            $register_token = $request->header( 'register-token' );
            $uid = decode_register_token( $register_token );
            try {
                if ( empty( $uid ) ) {
                    return response()->json( [
                        'status'    => 'error',
                        'message'   => 'เกิดข้อผิดพลาดไม่สามารถส่งรหัส OTP ได้',
                        'exception' => 'register-token is empty.',
                    ], 500 );
                } else {
                    $minute = tpa_config( 'ttl_otp_register' );
                    $row = DB::table( 'tpa_otp' )->select( 'id', 'mobile', DB::raw( 'DATEADD(MINUTE, ' . $minute . ', created_at) AS created_at' ) )
                        ->where( 'uid', $uid )
                        ->where( 'otp_type', 1 )
                        ->where( 'verified', 0 )->first();

                    if ( $row != false ) {
                        if ( strtotime( $row->created_at ) > time() ) {
                            $message = 'เกิดข้อผิดพลาดไม่สามารถส่งรหัส OTP ได้ เนื่องจากยังไม่ครบ ' . $minute . ' นาที';
                            if ( App::getLocale() == 'en' ) {
                                $message = 'An error occured. OTP cannot be sent as you need to wait  ' . $minute . ' minutes before resending.';
                            }

                            return response()->json( [
                                'status'  => 'error',
                                'message' => $message,
                            ], 500 );

                        }
                        DB::table( 'tpa_otp' )
                            ->where( 'id', $row->id )
                            ->update( ['verified' => -1] );
                        $mobile = $row->mobile;
                    } else {
                        $user = DB::table( 'users' )->select( 'mobile' )
                            ->where( 'id', $uid )
                            ->where( 'status', 0 )
                            ->first();

                        if ( $user != false ) {
                            $mobile = $user->mobile;
                        } else {
                            return response()->json( [
                                'status'    => 'error',
                                'message'   => 'เกิดข้อผิดพลาดไม่สามารถส่งรหัส OTP ได้',
                                'exception' => 'register-token not found.',
                            ], 500 );
                        }
                    }

                    $otp = GenerateOTP();
                    $ref_no = GenerateRefOTP();

                    DB::table( 'tpa_otp' )->insert( [
                        'uid'        => $uid,
                        'mobile'     => $mobile,
                        'otp'        => $otp,
                        'ref_no'     => $ref_no,
                        'created_at' => date( "Y-m-d H:i:s" ),
                        'expired_at' => date( "Y-m-d H:i:s", strtotime( "+" . $minute . " min" ) ),
                    ] );

                    $msdata = 'รหัส OTP ในการลงทะเบียนของคุณคือ ' . $otp . ' รหัสอ้างอิง ' . $ref_no . ' (มีอายุ ' . $minute . ' นาที)';
                    if ( App::getLocale() == 'en' ) {
                        $msdata = $otp . ' is your OTP to register (ref. ' . $ref_no . '). OTP will be expired within ' . $minute . ' minute.';
                    }

                    send_sms( [
                        'id'      => date( 'YmdHis' ) . rand( 3, 3 ),
                        'msisdn'  => $mobile,
                        'msgtype' => 'T',
                        'msdata'  => build_otp_message( App::getLocale(), $ref_no, $otp ),
                    ] );

                    return response()->json( [
                        'status'         => 'ok',
                        'register_token' => $register_token,
                        'otp'            => $otp,
                        'ref_no'         => $ref_no,
                        'ttl_minute'     => $minute,
                    ], 200 );
                }
            } catch ( CBValidationException $e ) {
                return response()->json( [
                    'status'   => 'error',
                    'message'  => $e->getMessage(),
                    'messages' => explode( '; ', $e->getMessage() ),
                ], 500 );
            } catch ( Exception $e ) {
                return response()->json( [
                    'status'  => 'error',
                    'message' => $e->getMessage(),
                ], 500 );
            }
        } );
    } );

    Route::group( ['prefix' => 'forgot-password'], function () {
        Route::post( '/', function ( Request $request ) {
            $validator = [
                'username' => 'required',
            ];

            try {
                cb()->validation( $validator );
                $query = DB::table( 'users' )->select( 'id', 'mobile' )
                    ->where( 'username', $request->input( 'username' ) )
                    ->where( 'status', 1 );

                $count = $query->count();
                if ( $count > 0 ) {
                    $user = $query->first();
                    $uid = $user->id;
                    $mobile = $user->mobile;

                    $minute = tpa_config( 'ttl_otp_forgot_password' );
                    $row = DB::table( 'tpa_otp' )->select( 'id', 'mobile', DB::raw( 'DATEADD(MINUTE, ' . $minute . ', created_at) AS created_at' ) )
                        ->where( 'uid', $uid )
                        ->where( 'otp_type', 3 )
                        ->where( 'verified', 0 )->first();

                    if ( $row != false ) {
                        if ( strtotime( $row->created_at ) > time() ) {
                            $message = 'เกิดข้อผิดพลาดไม่สามารถส่งรหัส OTP ได้ เนื่องจากยังไม่ครบ ' . $minute . ' นาที';
                            if ( App::getLocale() == 'en' ) {
                                $message = 'An error occured. OTP cannot be sent as you need to wait  ' . $minute . ' minutes before resending.';
                            }

                            return response()->json( [
                                'status'  => 'error',
                                'message' => $message,
                            ], 500 );
                        }
                        DB::table( 'tpa_otp' )
                            ->where( 'id', $row->id )
                            ->update( ['verified' => -1] );
                        //$mobile = $row->mobile;
                    } else {
                        $user = DB::table( 'users' )->select( 'mobile' )
                            ->where( 'id', $uid )
                            ->first();

                        if ( $user != false ) {
                            $mobile = $user->mobile;
                        } else {
                            return response()->json( [
                                'status'    => 'error',
                                'message'   => 'เกิดข้อผิดพลาดไม่สามารถส่งรหัส OTP ได้',
                                'exception' => 'token not found.',
                            ], 500 );
                        }
                    }

                    $otp = GenerateOTP();
                    $ref_no = GenerateRefOTP();
                    DB::table( 'tpa_otp' )->insert( [
                        'uid'        => $uid,
                        'mobile'     => $mobile,
                        'otp'        => $otp,
                        'ref_no'     => $ref_no,
                        'otp_type'   => 3,
                        'created_at' => date( "Y-m-d H:i:s" ),
                        'expired_at' => date( "Y-m-d H:i:s", strtotime( "+" . $minute . " min" ) ),
                    ] );

                    $msdata = 'รหัส OTP ในการยืนยันตัวตนเนื่องจากลืมรหัสผ่านของคุณคือ ' . $otp . ' รหัสอ้างอิง ' . $ref_no . ' (มีอายุ ' . $minute . ' นาที)';
                    if ( App::getLocale() == 'en' ) {
                        $msdata = $otp . ' is your OTP (ref. ' . $ref_no . '). OTP will be expired within ' . $minute . ' minute.';
                    }

                    send_sms( [
                        'id'      => date( 'YmdHis' ) . rand( 3, 3 ),
                        'msisdn'  => $mobile,
                        'msgtype' => 'T',
                        'msdata'  => build_otp_message( App::getLocale(), $ref_no, $otp ),
                    ] );

                    return response()->json( [
                        'status'         => 'ok',
                        'recovery_token' => encrypt( $uid ),
                        'mobile'         => $mobile,
                        'otp'            => $otp,
                        'ref_no'         => $ref_no,
                        'ttl_minute'     => $minute,
                    ], 200 );
                } else {
                    return response()->json( [
                        'status'  => 'error',
                        'message' => 'Your username not found.',
                    ], 500 );
                }
            } catch ( CBValidationException $e ) {
                return response()->json( [
                    'status'  => 'error',
                    'message' => $e->getMessage(),
                ], 500 );
            } catch ( Exception $e ) {
                return response()->json( [
                    'status'  => 'error',
                    'message' => $e->getMessage(),
                ], 500 );
            }
        } );

        Route::post( 'resendotp', function ( Request $request ) {

            $validator = [
                'recovery_token' => 'required',
            ];

            try {
                cb()->validation( $validator );

                $uid = decrypt( $request->input( 'recovery_token' ) );

                $query = DB::table( 'users' )->select( 'mobile' )
                    ->where( 'id', $uid )
                    ->where( 'status', 1 );

                $count = $query->count();
                if ( $count <= 0 ) {
                    return response()->json( [
                        'status'  => 'error',
                        'message' => 'Invalid recovery token !',
                    ], 500 );
                } else {
                    $user = $query->first();
                    $mobile = $user->mobile;
                    $minute = tpa_config( 'ttl_otp_forgot_password' );
                    $row = DB::table( 'tpa_otp' )->select( 'id', 'mobile', DB::raw( 'DATEADD(MINUTE, ' . $minute . ', created_at) AS created_at' ) )
                        ->where( 'uid', $uid )
                        ->where( 'otp_type', 3 )
                        ->where( 'verified', 0 )->first();
                    if ( $row != false ) {
                        if ( strtotime( $row->created_at ) > time() ) {
                            $message = 'เกิดข้อผิดพลาดไม่สามารถส่งรหัส OTP ได้ เนื่องจากยังไม่ครบ ' . $minute . ' นาที';
                            if ( App::getLocale() == 'en' ) {
                                $message = 'An error occured. OTP cannot be sent as you need to wait  ' . $minute . ' minutes before resending.';
                            }

                            return response()->json( [
                                'status'  => 'error',
                                'message' => $message,
                            ], 500 );

                        }
                        DB::table( 'tpa_otp' )
                            ->where( 'id', $row->id )
                            ->update( ['verified' => -1] );
                        $mobile = $row->mobile;
                    } else {
                        $user = DB::table( 'users' )->select( 'mobile' )
                            ->where( 'id', $uid )
                            ->first();

                        if ( $user != false ) {
                            $mobile = $user->mobile;
                        } else {
                            return response()->json( [
                                'status'    => 'error',
                                'message'   => 'เกิดข้อผิดพลาดไม่สามารถส่งรหัส OTP ได้',
                                'exception' => 'token not found.',
                            ], 500 );
                        }
                    }

                    $otp = GenerateOTP();
                    $ref_no = GenerateRefOTP();

                    DB::table( 'tpa_otp' )->insert( [
                        'uid'        => $uid,
                        'mobile'     => $mobile,
                        'otp'        => $otp,
                        'ref_no'     => $ref_no,
                        'otp_type'   => 3,
                        'created_at' => date( "Y-m-d H:i:s" ),
                        'expired_at' => date( "Y-m-d H:i:s", strtotime( "+" . $minute . " min" ) ),
                    ] );

                    $msdata = 'รหัส OTP ในการยืนยันตัวตนเนื่องจากลืมรหัสผ่านของคุณคือ ' . $otp . ' รหัสอ้างอิง ' . $ref_no . ' (มีอายุ ' . $minute . ' นาที)';
                    if ( App::getLocale() == 'en' ) {
                        $msdata = $otp . ' is your OTP (ref. ' . $ref_no . '). OTP will be expired within ' . $minute . ' minute.';
                    }

                    send_sms( [
                        'id'      => date( 'YmdHis' ) . rand( 3, 3 ),
                        'msisdn'  => $mobile,
                        'msgtype' => 'T',
                        'msdata'  => build_otp_message( App::getLocale(), $ref_no, $otp ),
                        // 'msdata'  => 'หมายเลข OTP ในการยืนยันตัวตนเนื่องจากลืมรหัสผ่านของคุณคือ ' . $otp,
                    ] );

                    return response()->json( [
                        'status'         => 'ok',
                        'recovery_token' => $request->input( 'recovery_token' ),
                        'mobile'         => $mobile,
                        'otp'            => $otp,
                        'ref_no'         => $ref_no,
                        'ttl_minute'     => $minute,
                    ], 200 );
                }
            } catch ( CBValidationException $e ) {
                return response()->json( [
                    'status'  => 'error',
                    'message' => $e->getMessage(),
                ], 500 );
            } catch ( DecryptException $e ) {
                return response()->json( [
                    'status'  => 'error',
                    'message' => $e->getMessage(),
                ], 500 );
            } catch ( Exception $e ) {
                return response()->json( [
                    'status'  => 'error',
                    'message' => $e->getMessage(),
                ], 500 );
            }
        } );

        Route::post( 'verifyotp', function ( Request $request ) {
            $validator = [
                'otp'            => 'required',
                'recovery_token' => 'required',
            ];

            try {
                cb()->validation( $validator );

                $uid = decrypt( $request->input( 'recovery_token' ) );
                $query = DB::table( 'users' )->select( 'mobile' )
                    ->where( 'id', $uid )
                    ->where( 'status', 1 );

                $count = $query->count();
                if ( $count <= 0 ) {
                    return response()->json( [
                        'status'  => 'error',
                        'message' => 'Invalid recovery token !',
                    ], 500 );
                } else {
                    $otp = DB::table( 'tpa_otp' )->select( 'id' )
                        ->where( 'uid', $uid )
                        ->where( 'otp', $request->input( 'otp' ) )
                        ->where( 'otp_type', 3 )
                        ->where( 'verified', 0 )
                        ->where( 'expired_at', '>=', date( 'Y-m-d H:i:s' ) )
                        ->first();

                    if ( !$otp ) {
                        return response()->json( [
                            'status'  => 'error',
                            'message' => 'OTP ไม่ถูกต้อง',
                        ], 500 );
                    } else {

                        $current_time = date( 'Y-m-d H:i:s' );
                        $affected_row = DB::table( 'tpa_otp' )
                            ->where( 'id', $otp->id )
                            ->update( [
                                'verified'    => 1,
                                'verified_at' => $current_time,
                            ] );

                        if ( $affected_row <= 0 ) {
                            return response()->json( [
                                'status'  => 'error',
                                'message' => 'ไม่สามารถตรวจสอบรหัส OTP ได้',
                            ], 500 );
                        } else {
                            return response()->json( [
                                'status'         => 'ok',
                                'message'        => __( 'messages.success' ),
                                'recovery_token' => $request->input( 'recovery_token' ),
                            ], 200 );
                        }
                    }
                }
            } catch ( CBValidationException $e ) {
                return response()->json( [
                    'status'  => 'error',
                    'message' => $e->getMessage(),
                ], 500 );
            } catch ( DecryptException $e ) {
                return response()->json( [
                    'status'  => 'error',
                    'message' => $e->getMessage(),
                ], 500 );
            } catch ( Exception $e ) {
                return response()->json( [
                    'status'  => 'error',
                    'message' => $e->getMessage(),
                ], 500 );
            }
        } );

        Route::put( 'change-password', function ( Request $request ) {
            $validator = [
                'password'       => 'required|max:15|min:8|required_with:passconf|same:passconf',
                'passconf'       => 'required|max:15|min:8',
                'recovery_token' => 'required',
            ];

            try {
                cb()->validation( $validator );
                $uid = decrypt( $request->input( 'recovery_token' ) );
                $query = DB::table( 'users' )->select( 'mobile', 'ref_id', 'is_new_pass' )
                    ->where( 'id', $uid )
                    ->where( 'status', 1 );

                $count = $query->count();
                if ( $count <= 0 ) {
                    return response()->json( [
                        'status'  => 'error',
                        'message' => 'Invalid recovery token !',
                    ], 500 );
                } else {

                    $data = [
                        'password'   => Hash::make( $request->input( 'password' ) ),
                        'updated_at' => date( 'Y-m-d H:i:s' ),
                    ];

                    $user = $query->first();
                    if ( !empty( $user->ref_id ) && ( $user->is_new_pass != 1 ) ) {
                        $data['is_new_pass'] = 1;
                    }

                    $affected_row = DB::table( 'users' )->where( 'id', $uid )->update( $data );

                    if ( $affected_row <= 0 ) {
                        return response()->json( [
                            'status'  => 'error',
                            'message' => 'ไม่สามารถแก้ไขรหัสผ่านได้',
                        ], 500 );
                    } else {
                        return response()->json( [
                            'status'  => 'ok',
                            'message' => __( 'messages.success' ),
                        ], 200 );
                    }
                }
            } catch ( CBValidationException $e ) {
                return response()->json( [
                    'status'  => 'error',
                    'message' => $e->getMessage(),
                ], 500 );
            } catch ( DecryptException $e ) {
                return response()->json( [
                    'status'  => 'error',
                    'message' => $e->getMessage(),
                ], 500 );
            } catch ( Exception $e ) {
                return response()->json( [
                    'status'  => 'error',
                    'message' => $e->getMessage(),
                ], 500 );
            }
        } );
    } );

    Route::group( ['prefix' => 'forgot-username'], function () {
        Route::post( '/', function ( Request $request ) {
            $validator = [
                'mobile' => 'required',
            ];

            try {
                cb()->validation( $validator );
                $mobile = $request->input( 'mobile' );
                $query = DB::table( 'users' )->select( 'id' )
                    ->where( 'mobile', $mobile )
                    ->where( 'status', 1 );

                $count = $query->count();
                if ( $count > 0 ) {
                    $user = $query->first();
                    $uid = $user->id;

                    $minute = tpa_config( 'ttl_otp_forgot_username' );
                    $row = DB::table( 'tpa_otp' )->select( 'id', 'mobile', DB::raw( 'DATEADD(MINUTE, ' . $minute . ', created_at) AS created_at' ) )
                        ->where( 'uid', $uid )
                        ->where( 'otp_type', 3 )
                        ->where( 'verified', 0 )->first();

                    if ( $row != false ) {
                        if ( strtotime( $row->created_at ) > time() ) {
                            return response()->json( [
                                'status'  => 'error',
                                'message' => 'เกิดข้อผิดพลาดไม่สามารถส่งรหัส OTP ได้ เนื่องจากยังไม่ครบ ' . $minute . ' นาที',
                            ], 500 );
                        }
                        DB::table( 'tpa_otp' )
                            ->where( 'id', $row->id )
                            ->update( ['verified' => -1] );
                    } else {
                        $user = DB::table( 'users' )->select( 'mobile' )
                            ->where( 'id', $uid )
                            ->first();

                        if ( $user != false ) {
                            $mobile = $user->mobile;
                        } else {
                            return response()->json( [
                                'status'    => 'error',
                                'message'   => 'เกิดข้อผิดพลาดไม่สามารถส่งรหัส OTP ได้',
                                'exception' => 'token not found.',
                            ], 500 );
                        }
                    }

                    $otp = GenerateOTP();
                    $ref_no = GenerateRefOTP();

                    DB::table( 'tpa_otp' )->insert( [
                        'uid'        => $uid,
                        'mobile'     => $mobile,
                        'otp'        => $otp,
                        'ref_no'     => $ref_no,
                        'otp_type'   => 4,
                        'created_at' => date( "Y-m-d H:i:s" ),
                        'expired_at' => date( "Y-m-d H:i:s", strtotime( "+" . $minute . " min" ) ),
                    ] );

                    $msdata = 'รหัส OTP ในการยืนยันตัวตนเนื่องจากลืมชื่อผู้ใช้งานของคุณคือ ' . $otp . ' รหัสอ้างอิง ' . $ref_no . ' (มีอายุ ' . $minute . ' นาที)';
                    if ( App::getLocale() == 'en' ) {
                        $msdata = $otp . ' is your OTP (ref. ' . $ref_no . '). OTP will be expired within ' . $minute . ' minute.';
                    }

                    send_sms( [
                        'id'      => date( 'YmdHis' ) . rand( 3, 3 ),
                        'msisdn'  => $mobile,
                        'msgtype' => 'T',
                        'msdata'  => build_otp_message( App::getLocale(), $ref_no, $otp ),
                        // 'msdata'  => 'หมายเลข OTP ในการยืนยันตัวตนเนื่องจากลืมชื่อผู้ใช้งานของคุณคือ ' . $otp,
                    ] );

                    return response()->json( [
                        'status'         => 'ok',
                        'recovery_token' => encrypt( $uid ),
                        'otp'            => $otp,
                        'ref_no'         => $ref_no,
                        'ttl_minute'     => $minute,
                    ], 200 );
                } else {
                    $message = 'ไม่พบเบอร์โทรศัพท์มือถือของคุณในระบบ';
                    if ( App::getLocale() == 'en' ) {
                        $message = 'Your mobile number is not found.';
                    }

                    return response()->json( [
                        'status'  => 'error',
                        'message' => $message,
                    ], 500 );
                }
            } catch ( CBValidationException $e ) {
                return response()->json( [
                    'status'  => 'error',
                    'message' => $e->getMessage(),
                ], 500 );
            } catch ( Exception $e ) {
                return response()->json( [
                    'status'  => 'error',
                    'message' => $e->getMessage(),
                ], 500 );
            }
        } );

        Route::post( 'resendotp', function ( Request $request ) {

            $validator = [
                'recovery_token' => 'required',
            ];

            try {
                cb()->validation( $validator );

                $uid = decrypt( $request->input( 'recovery_token' ) );

                $query = DB::table( 'users' )->select( 'mobile' )
                    ->where( 'id', $uid )
                    ->where( 'status', 1 );

                $count = $query->count();
                if ( $count <= 0 ) {
                    return response()->json( [
                        'status'  => 'error',
                        'message' => 'Invalid recovery token !',
                    ], 500 );
                } else {
                    $user = $query->first();
                    $mobile = $user->mobile;
                    $minute = tpa_config( 'ttl_otp_forgot_username' );
                    $row = DB::table( 'tpa_otp' )->select( 'id', 'mobile', DB::raw( 'DATEADD(MINUTE, ' . $minute . ', created_at) AS created_at' ) )
                        ->where( 'uid', $uid )
                        ->where( 'otp_type', 4 )
                        ->where( 'verified', 0 )->first();
                    if ( $row != false ) {
                        if ( strtotime( $row->created_at ) > time() ) {
                            $message = 'เกิดข้อผิดพลาดไม่สามารถส่งรหัส OTP ได้ เนื่องจากยังไม่ครบ ' . $minute . ' นาที';
                            if ( App::getLocale() == 'en' ) {
                                $message = 'An error occured. OTP cannot be sent as you need to wait  ' . $minute . ' minutes before resending.';
                            }

                            return response()->json( [
                                'status'  => 'error',
                                'message' => $message,
                            ], 500 );

                        }
                        DB::table( 'tpa_otp' )
                            ->where( 'id', $row->id )
                            ->update( ['verified' => -1] );
                        $mobile = $row->mobile;
                    } else {
                        $user = DB::table( 'users' )->select( 'mobile' )
                            ->where( 'id', $uid )
                            ->first();

                        if ( $user != false ) {
                            $mobile = $user->mobile;
                        } else {
                            return response()->json( [
                                'status'    => 'error',
                                'message'   => 'เกิดข้อผิดพลาดไม่สามารถส่งรหัส OTP ได้',
                                'exception' => 'token not found.',
                            ], 500 );
                        }
                    }

                    $otp = GenerateOTP();
                    $ref_no = GenerateRefOTP();

                    DB::table( 'tpa_otp' )->insert( [
                        'uid'        => $uid,
                        'mobile'     => $mobile,
                        'otp'        => $otp,
                        'ref_no'     => $ref_no,
                        'otp_type'   => 4,
                        'created_at' => date( "Y-m-d H:i:s" ),
                        'expired_at' => date( "Y-m-d H:i:s", strtotime( "+" . $minute . " min" ) ),
                    ] );

                    $msdata = 'รหัส OTP ในการยืนยันตัวตนเนื่องจากลืมชื่อผู้ใช้งานของคุณคือ ' . $otp . ' รหัสอ้างอิง ' . $ref_no . ' (มีอายุ ' . $minute . ' นาที)';
                    if ( App::getLocale() == 'en' ) {
                        $msdata = $otp . ' is your OTP (ref. ' . $ref_no . '). OTP will be expired within ' . $minute . ' minute.';
                    }

                    send_sms( [
                        'id'      => date( 'YmdHis' ) . rand( 3, 3 ),
                        'msisdn'  => $mobile,
                        'msgtype' => 'T',
                        'msdata'  => build_otp_message( App::getLocale(), $ref_no, $otp ),
                        // 'msdata'  => 'หมายเลข OTP ในการยืนยันตัวตนเนื่องจากลืมชื่อผู้ใช้งานของคุณคือ ' . $otp,
                    ] );

                    return response()->json( [
                        'status'         => 'ok',
                        'recovery_token' => $request->input( 'recovery_token' ),
                        'otp'            => $otp,
                        'ref_no'         => $ref_no,
                        'ttl_minute'     => $minute,
                    ], 200 );
                }
            } catch ( CBValidationException $e ) {
                return response()->json( [
                    'status'  => 'error',
                    'message' => $e->getMessage(),
                ], 500 );
            } catch ( DecryptException $e ) {
                return response()->json( [
                    'status'  => 'error',
                    'message' => $e->getMessage(),
                ], 500 );
            } catch ( Exception $e ) {
                return response()->json( [
                    'status'  => 'error',
                    'message' => $e->getMessage(),
                ], 500 );
            }
        } );

        Route::post( 'verifyotp', function ( Request $request ) {
            $validator = [
                'otp'            => 'required',
                'recovery_token' => 'required',
            ];

            try {
                cb()->validation( $validator );

                $uid = decrypt( $request->input( 'recovery_token' ) );
                $query = DB::table( 'users' )->select( 'username' )
                    ->where( 'id', $uid )
                    ->where( 'status', 1 );

                $count = $query->count();
                if ( $count <= 0 ) {
                    return response()->json( [
                        'status'  => 'error',
                        'message' => 'Invalid recovery token !',
                    ], 500 );
                } else {
                    $user = $query->first();
                    $otp = DB::table( 'tpa_otp' )->select( 'id' )
                        ->where( 'uid', $uid )
                        ->where( 'otp', $request->input( 'otp' ) )
                        ->where( 'otp_type', 4 )
                        ->where( 'verified', 0 )
                        ->where( 'expired_at', '>=', date( 'Y-m-d H:i:s' ) )
                        ->first();

                    if ( !$otp ) {
                        return response()->json( [
                            'status'  => 'error',
                            'message' => 'OTP ไม่ถูกต้อง',
                        ], 500 );
                    } else {

                        $current_time = date( 'Y-m-d H:i:s' );
                        $affected_row = DB::table( 'tpa_otp' )
                            ->where( 'id', $otp->id )
                            ->update( [
                                'verified'    => 1,
                                'verified_at' => $current_time,
                            ] );

                        if ( $affected_row <= 0 ) {
                            return response()->json( [
                                'status'  => 'error',
                                'message' => 'ไม่สามารถตรวจสอบรหัส OTP ได้',
                            ], 500 );
                        } else {
                            return response()->json( [
                                'status'   => 'ok',
                                'message'  => __( 'messages.success' ),
                                'username' => $user->username,
                            ], 200 );
                        }
                    }
                }
            } catch ( CBValidationException $e ) {
                return response()->json( [
                    'status'  => 'error',
                    'message' => $e->getMessage(),
                ], 500 );
            } catch ( DecryptException $e ) {
                return response()->json( [
                    'status'  => 'error',
                    'message' => $e->getMessage(),
                ], 500 );
            } catch ( Exception $e ) {
                return response()->json( [
                    'status'  => 'error',
                    'message' => $e->getMessage(),
                ], 500 );
            }
        } );
    } );

    Route::group( ['middleware' => 'verify.token'], function () {

        Route::group( ['prefix' => 'agreement'], function () {

            Route::get( 'check/{agreement_type}', function ( $agreement_type, Request $request ) {
                try {
                    $uid = $request->token->uid;
                    $agreement = DB::table( 'tpa_agreement' )->select( 'agreement_id as id', 'agreement_' . App::getLocale() . ' as content', 'agreement_force' )
                        ->where( 'agreement_status', 1 )
                        ->where( 'agreement_type', $agreement_type )
                        ->first();

                    $count = DB::table( 'tpa_consent' )->select( 'consent_id' )
                        ->where( 'agreement_id', $agreement->id )
                        ->where( 'uid', $uid )
                        ->count();

                    if ( $count > 0 ) {
                        $result = [
                            'status'  => 'ok',
                            'message' => __( 'messages.success' ),
                            'force'   => ( $agreement->agreement_force == "1" ) ? true : false,
                        ];
                    } else {
                        $result = [
                            'status'  => 'error',
                            'message' => __( 'Please consent agreement.' ),
                            'data'    => $agreement,
                            'force'   => ( $agreement->agreement_force == "1" ) ? true : false,
                        ];

                    }

                    return response()->json( $result, 200 );

                } catch ( Exception $e ) {
                    return response()->json( [
                        'status'  => 'error',
                        'message' => $e->getMessage(),
                    ], 500 );
                }

            } );

            Route::post( 'consent/{agreement_id}', function ( $agreement_id, Request $request ) {
                try {
                    $uid = $request->token->uid;
                    DB::table( 'tpa_consent' )->insert( [
                        'uid'          => $uid,
                        'agreement_id' => $agreement_id,
                    ] );

                    return response()->json( [
                        'status'  => 'ok',
                        'message' => __( 'messages.success' ),
                    ], 200 );

                } catch ( Exception $e ) {
                    return response()->json( [
                        'status'  => 'error',
                        'message' => $e->getMessage(),
                    ], 500 );
                }

            } );

        } );

        Route::get( 'check-token', function ( Request $request ) {
            $uid = $request->token->uid;
            $user = DB::table( 'users' )->select( 'status', 'pin' )
                ->where( 'id', $uid )
                ->first();
            return response()->json( [
                'status'  => 'ok',
                'message' => __( 'messages.success' ),
                'data'    => [
                    'is_pin' => ( $user->pin ) ? true : false,
                ],
            ], 200 );
        } );

        Route::put( 'setpass', function ( Request $request ) {

            $validator = [
                'passcurr' => 'required|max:15|min:8',
                'password' => 'required|max:15|min:8|required_with:passconf|same:passconf',
                'passconf' => 'required|max:15|min:8',
            ];

            try {
                cb()->validation( $validator );

                $uid = $request->token->uid;
                $credential = [
                    'id'       => $uid,
                    'password' => $request->input( 'passcurr' ),
                    'status'   => 1,
                ];
                $user = [];
                if ( !auth()->attempt( $credential ) ) {
                    return response()->json( [
                        'status'  => 'error',
                        'message' => 'รหัสผ่านปัจจุบันของคุณไม่ถูกต้อง',
                    ], 500 );

                } else {

                    $affected_row = DB::table( 'users' )->where( 'id', $uid )->update( [
                        'password'   => Hash::make( $request->input( 'password' ) ),
                        'updated_at' => date( 'Y-m-d H:i:s' ),
                    ] );

                    if ( $affected_row <= 0 ) {
                        return response()->json( [
                            'status'  => 'error',
                            'message' => 'ไม่สามารถแก้ไขรหัสผ่านได้',
                        ], 500 );
                    } else {
                        return response()->json( [
                            'status'  => 'ok',
                            'message' => __( 'messages.success' ),
                        ], 200 );
                    }

                }

            } catch ( CBValidationException $e ) {
                return response()->json( [
                    'status'   => 'error',
                    'message'  => $e->getMessage(),
                    'messages' => explode( '; ', $e->getMessage() ),
                ], 500 );
            } catch ( Exception $e ) {
                return response()->json( [
                    'status'  => 'error',
                    'message' => $e->getMessage(),
                ], 500 );
            }
        } );

        Route::put( 'setpin', function ( Request $request ) {

            $validator = [
                'pin'     => 'required|max:6|min:6|required_with:pinconf|same:pinconf',
                'pinconf' => 'required|max:6|min:6',
            ];

            try {
                cb()->validation( $validator );

                $uid = $request->token->uid;
                $current_time = date( 'Y-m-d H:i:s' );
                DB::table( 'users' )
                    ->where( 'id', $uid )
                    ->update( [
                        'pin'        => md5( $request->input( 'pin' ) ),
                        'updated_at' => $current_time,
                    ] );

                return response()->json( [
                    'status'  => 'ok',
                    'message' => __( 'messages.success' ),
                ], 200 );
            } catch ( CBValidationException $e ) {
                return response()->json( [
                    'status'   => 'error',
                    'message'  => $e->getMessage(),
                    'messages' => explode( '; ', $e->getMessage() ),
                ], 500 );
            } catch ( Exception $e ) {
                return response()->json( [
                    'status'  => 'error',
                    'message' => $e->getMessage(),
                ], 500 );
            }
        } );

        Route::post( 'verifypin', function ( Request $request ) {

            $validator = [
                'pin' => 'required',
            ];

            try {
                cb()->validation( $validator );

                $uid = $request->token->uid;
                $user = DB::table( 'users' )->select( 'id' )
                    ->where( 'id', $uid )
                    ->where( 'pin', md5( $request->input( 'pin' ) ) )
                    ->first();
                if ( !$user ) {
                    return response()->json( [
                        'status'  => 'error',
                        'message' => 'Invalid pin.',
                    ], 500 );
                } else {
                    return response()->json( [
                        'status'  => 'ok',
                        'message' => __( 'messages.success' ),
                    ], 200 );
                }
            } catch ( CBValidationException $e ) {
                return response()->json( [
                    'status'   => 'error',
                    'message'  => $e->getMessage(),
                    'messages' => explode( '; ', $e->getMessage() ),
                ], 500 );
            } catch ( Exception $e ) {
                return response()->json( [
                    'status'  => 'error',
                    'message' => $e->getMessage(),
                ], 500 );
            }
        } );

        Route::group( ['prefix' => 'profile'], function () {
            Route::get( '/', function ( Request $request ) {
                $uid = $request->token->uid;
                $user = DB::table( 'users' )->select( 'username', 'name', 'surname', 'mobile', 'sex', 'birthdate', 'photo' )
                    ->where( 'id', $uid )
                    ->first();

                if ( !empty( $user->photo ) ) {
                    $user->photo = url( $user->photo );
                }
                return response()->json( [
                    'status'  => 'ok',
                    'message' => __( 'messages.success' ),
                    'data'    => $user,
                ], 200 );
            } );

            Route::post( 'sendotp', function ( Request $request ) {
                try {

                    $validator = [
                        'name'    => 'required',
                        'surname' => 'required',
                        'mobile'  => 'required',
                    ];

                    cb()->validation( $validator );

                    $uid = $request->token->uid;
                    $mobile = $request->input( 'mobile' );

                    $count = DB::table( 'users' )->select( 'mobile' )
                        ->where( 'id', '<>', $uid )
                        ->where( 'mobile', $mobile )
                        ->count();

                    if ( $count > 0 ) {
                        return response()->json( [
                            'status'  => 'error',
                            'message' => __( 'validation.custom.mobile.unique' ),
                        ], 500 );

                    } else {
                        $minute = tpa_config( 'ttl_otp_change_mobile' );
                        $row = DB::table( 'tpa_otp' )->select( 'id', 'mobile', DB::raw( 'DATEADD(MINUTE, ' . $minute . ', created_at) AS created_at' ) )
                            ->where( 'uid', $uid )
                            ->where( 'otp_type', 2 )
                            ->where( 'verified', 0 )->first();

                        if ( $row != false ) {
                            if ( strtotime( $row->created_at ) > time() ) {
                                return response()->json( [
                                    'status'  => 'error',
                                    'message' => 'เกิดข้อผิดพลาดไม่สามารถส่งรหัส OTP ได้ เนื่องจากยังไม่ครบ ' . $minute . ' นาที',
                                ], 500 );
                            }

                            DB::table( 'tpa_otp' )
                                ->where( 'id', $row->id )
                                ->update( ['verified' => -1] );
                        } else {
                            DB::table( 'tpa_otp' )
                                ->where( 'uid', $uid )
                                ->where( 'otp_type', 2 )
                                ->where( 'verified', 0 )
                                ->update( ['verified' => -1] );
                        }

                        $otp = GenerateOTP();
                        $ref_no = GenerateRefOTP();

                        DB::table( 'tpa_otp' )->insert( [
                            'uid'        => $uid,
                            'mobile'     => $mobile,
                            'otp'        => $otp,
                            'ref_no'     => $ref_no,
                            'otp_type'   => 2,
                            'created_at' => date( "Y-m-d H:i:s" ),
                            'expired_at' => date( "Y-m-d H:i:s", strtotime( "+" . $minute . " min" ) ),
                        ] );

                        $msdata = 'รหัส OTP ในการอัพเดทข้อมูลส่วนตัวของคุณคือ ' . $otp . ' รหัสอ้างอิง ' . $ref_no . ' (มีอายุ ' . $minute . ' นาที)';
                        if ( App::getLocale() == 'en' ) {
                            $msdata = $otp . ' is your OTP (ref. ' . $ref_no . '). OTP will be expired within ' . $minute . ' minute.';
                        }

                        send_sms( [
                            'id'      => date( 'YmdHis' ) . rand( 3, 3 ),
                            'msisdn'  => $mobile,
                            'msgtype' => 'T',
                            'msdata'  => build_otp_message( App::getLocale(), $ref_no, $otp ),
                            // 'msdata'  => 'หมายเลข OTP ในการอัพเดทข้อมูลส่วนตัวของคุณคือ ' . $otp,
                        ] );

                        return response()->json( [
                            'status'     => 'ok',
                            'otp'        => $otp,
                            'ref_no'     => $ref_no,
                            'ttl_minute' => $minute,
                        ], 200 );

                    }

                } catch ( CBValidationException $e ) {
                    return response()->json( [
                        'status'   => 'error',
                        'message'  => $e->getMessage(),
                        'messages' => explode( '; ', $e->getMessage() ),
                    ], 500 );
                } catch ( Exception $e ) {
                    return response()->json( [
                        'status'  => 'error',
                        'message' => $e->getMessage(),
                    ], 500 );
                }
            } );

            Route::post( 'resendotp', function ( Request $request ) {
                $uid = $request->token->uid;
                try {
                    $minute = tpa_config( 'sendotp_minute' );
                    $row = DB::table( 'tpa_otp' )->select( 'id', 'mobile', DB::raw( 'DATEADD(MINUTE, ' . $minute . ', created_at) AS created_at' ) )
                        ->where( 'uid', $uid )
                        ->where( 'otp_type', 2 )
                        ->where( 'verified', 0 )->first();

                    if ( $row != false ) {
                        if ( strtotime( $row->created_at ) > time() ) {
                            $message = 'เกิดข้อผิดพลาดไม่สามารถส่งรหัส OTP ได้ เนื่องจากยังไม่ครบ ' . $minute . ' นาที';
                            if ( App::getLocale() == 'en' ) {
                                $message = 'An error occured. OTP cannot be sent as you need to wait  ' . $minute . ' minutes before resending.';
                            }

                            return response()->json( [
                                'status'  => 'error',
                                'message' => $message,
                            ], 500 );

                        }
                        DB::table( 'tpa_otp' )
                            ->where( 'id', $row->id )
                            ->update( ['verified' => -1] );
                        $mobile = $row->mobile;
                    } else {
                        $user = DB::table( 'users' )->select( 'mobile' )
                            ->where( 'id', $uid )
                            ->first();

                        if ( $user != false ) {
                            $mobile = $user->mobile;
                        } else {

                            return response()->json( [
                                'status'    => 'error',
                                'message'   => 'เกิดข้อผิดพลาดไม่สามารถส่งรหัส OTP ได้',
                                'exception' => 'token not found.',
                            ], 500 );
                        }
                    }

                    $otp = GenerateOTP();
                    $ref_no = GenerateRefOTP();

                    DB::table( 'tpa_otp' )->insert( [
                        'uid'        => $uid,
                        'mobile'     => $mobile,
                        'otp'        => $otp,
                        'ref_no'     => $ref_no,
                        'otp_type'   => 2,
                        'created_at' => date( "Y-m-d H:i:s" ),
                        'expired_at' => date( "Y-m-d H:i:s", strtotime( "+" . $minute . " min" ) ),
                    ] );

                    $msdata = 'รหัส OTP ในการอัพเดทข้อมูลส่วนตัวของคุณคือ ' . $otp . ' รหัสอ้างอิง ' . $ref_no . ' (มีอายุ ' . $minute . ' นาที)';
                    if ( App::getLocale() == 'en' ) {
                        $msdata = $otp . ' is your OTP (ref. ' . $ref_no . '). OTP will be expired within ' . $minute . ' minute.';
                    }

                    send_sms( [
                        'id'      => date( 'YmdHis' ) . rand( 3, 3 ),
                        'msisdn'  => $mobile,
                        'msgtype' => 'T',
                        'msdata'  => build_otp_message( App::getLocale(), $ref_no, $otp ),
                        // 'msdata'  => 'หมายเลข OTP ในการอัพเดทข้อมูลส่วนตัวของคุณคือ ' . $otp,
                    ] );

                    return response()->json( [
                        'status'     => 'ok',
                        'otp'        => $otp,
                        'ref_no'     => $ref_no,
                        'ttl_minute' => $minute,
                    ], 200 );
                } catch ( Exception $e ) {
                    return response()->json( [
                        'status'  => 'error',
                        'message' => $e->getMessage(),
                    ], 500 );
                }
            } );

            Route::post( 'verifyotp', function ( Request $request ) {
                try {
                    $uid = $request->token->uid;

                    $validator = [
                        'otp' => 'required',
                    ];
                    cb()->validation( $validator );

                    $otp = DB::table( 'tpa_otp' )->select( 'id' )
                        ->where( 'uid', $uid )
                        ->where( 'otp', $request->input( 'otp' ) )
                        ->where( 'otp_type', 2 )
                        ->where( 'verified', 0 )
                        ->where( 'expired_at', '>=', date( 'Y-m-d H:i:s' ) )
                        ->first();

                    if ( !$otp ) {
                        return response()->json( [
                            'status'  => 'error',
                            'message' => 'OTP ไม่ถูกต้อง',
                        ], 500 );
                    } else {
                        $current_time = date( 'Y-m-d H:i:s' );
                        $affected_row = DB::table( 'tpa_otp' )
                            ->where( 'id', $otp->id )
                            ->update( [
                                'verified'    => 1,
                                'verified_at' => $current_time,
                            ] );

                        if ( $affected_row > 0 ) {
                            return response()->json( [
                                'status'  => 'ok',
                                'message' => __( 'messages.success' ),
                            ], 200 );
                        } else {
                            return response()->json( [
                                'status'  => 'error',
                                'message' => 'Failure !',
                            ], 500 );
                        }
                    }
                } catch ( CBValidationException $e ) {
                    return response()->json( [
                        'status'  => 'error',
                        'message' => $e->getMessage(),
                    ], 500 );
                } catch ( Exception $e ) {
                    return response()->json( [
                        'status'  => 'error',
                        'message' => $e->getMessage(),
                    ], 500 );
                }
            } );

            Route::put( '/', function ( Request $request ) {
                try {

                    $validator = [
                        'name'    => 'required',
                        'surname' => 'required',
                        'mobile'  => 'required',
                    ];

                    cb()->validation( $validator );

                    $uid = $request->token->uid;

                    $current_time = date( 'Y-m-d H:i:s' );
                    $image_64 = $request->input( 'photo' );

                    $user = [
                        'name'       => $request->input( 'name' ),
                        'surname'    => $request->input( 'surname' ),
                        'mobile'     => $request->input( 'mobile' ),
                        'sex'        => $request->input( 'sex' ),
                        'birthdate'  => $request->input( 'birthdate' ),
                        'updated_at' => $current_time,
                    ];

                    if ( !empty( $image_64 ) ) {
                        $user['photo'] = upload_profie_photo( $request->input( 'photo' ) );
                    }

                    $affected_row = DB::table( 'users' )
                        ->where( 'id', $uid )
                        ->update( $user );

                    if ( $affected_row > 0 ) {
                        return response()->json( [
                            'status'  => 'ok',
                            'message' => __( 'messages.success' ),
                        ], 200 );
                    } else {
                        return response()->json( [
                            'status'  => 'error',
                            'message' => 'Failure !',
                        ], 500 );
                    }
                } catch ( CBValidationException $e ) {
                    return response()->json( [
                        'status'   => 'error',
                        'message'  => $e->getMessage(),
                        'messages' => explode( '; ', $e->getMessage() ),
                    ], 500 );
                } catch ( Exception $e ) {
                    return response()->json( [
                        'status'  => 'error',
                        'message' => $e->getMessage(),
                    ], 500 );
                }
            } );
        } );

        Route::group( ['prefix' => 'policy'], function () {
            Route::get( 'list/{citizen_id?}', function ( $citizen_id = null, Request $request ) {
                $insured_type = ( $request->input( 'insured_type' ) ) ? strtolower( $request->input( 'insured_type' ) ) : '';
                $is_claim = ( $request->input( 'is_claim' ) ) ? 1 : 0;

                $uid = $request->token->uid;
                $is_activated = false;
                if ( empty( $citizen_id ) ) {
                    $user = DB::table( 'users' )
                        ->select( 'citizen_id' )
                        ->where( 'id', $uid )
                        ->first();
                    $citizen_id = $user->citizen_id;
                    $is_activated = true;

                }

                if ( empty( $citizen_id ) ) {
                    return response()->json( [
                        'status'  => 'error',
                        'message' => __( 'messages.policy.list.error' ),
                    ], 500 );
                } else {

                    try {
                        $client = new GuzzleHttp\Client();
                        $res = $client->request( 'POST', env( 'TPA_URL' ) . 'login', [
                            'headers' => ['Content-Type' => 'application/json'],
                            'body'    => json_encode( [
                                'Username' => env( 'TPA_USER' ),
                                'Password' => env( 'TPA_PASS' ),
                            ] ),
                            'verify'  => false,
                        ] );

                        $token = json_decode( $res->getBody()->getContents() );

                        if ( empty( $token ) ) {
                            return response()->json( [
                                'status'  => 'error',
                                'message' => __( 'Internal server error.' ),
                            ], 500 );
                        } else {
                            $client = new GuzzleHttp\Client();
                            $res = $client->request( 'POST', env( 'TPA_URL' ) . 'PolicyList', [
                                'headers' => [
                                    'Content-Type'  => 'application/json',
                                    'Authorization' => $token,
                                ],
                                'body'    => json_encode( [
                                    'CITIZEN_ID' => $citizen_id,
                                ] ),
                                'verify'  => false,
                            ] );

                            $httpCode = $res->getStatusCode();
                            if ( $httpCode != 200 ) {
                                return response()->json( [
                                    'status'  => 'error',
                                    'message' => __( 'Internal server error.' ),
                                    'token'   => $token,
                                ], $httpCode );
                            } else {
                                $user = DB::table( 'users' )
                                    ->select( 'mobile', 'username' )
                                    ->where( 'id', $uid )
                                    ->first();

                                $body = json_decode( $res->getBody()->getContents() );
                                $data = [];
                                $tpa_pending = [];
                                $tpa_active = [];
                                $tpa_expired = [];
                                $self_active = [];
                                $self_expired = [];

                                $do_activate = false;
                                foreach ( $body->ListOfPolicy as $row ) {
                                    if ( $row->TelNo == $user->mobile ) {
                                        $do_activate = true;
                                    }
                                }

                                foreach ( $body->ListOfPolicy as $row ) {
                                    $row->EclaimInsurerName = $row->InsurerName;
                                    $row->EclaimInsurerNameEN = $row->InsurerNameEN;

                                    list( $d, $m, $y ) = explode( '/', $row->EffTo );
                                    $exp = $y . '-' . $m . '-' . $d . ' 23:59:59';

                                    list( $d, $m, $y ) = explode( '/', $row->EffFrom );
                                    $start = strtotime( $y . '-' . $m . '-' . $d );
                                    $row->sort = $start;
                                    $row->expired = false;
                                    $newEndingDate = date( "Y-m-d H:i:s", strtotime( date( "Y-m-d H:i:s", strtotime( $exp ) ) . " + 1 year" ) );
                                    $cur_date = date( 'Y-m-d H:i:s' );

                                    if ( strtotime( $exp ) < time() ) {
                                        $row->expired = true;
                                    }

                                    $bg = $row->background;

                                    $query = DB::table( 'tpa_company' )
                                        ->select( 'background' )
                                        ->whereNotNull( 'background' )
                                        ->where( 'company_code', $row->CompanyCode );
                                    if ( $query->count() > 0 ) {
                                        $com = $query->first();
                                        if ( !empty( $com->background ) ) {
                                            $row->background = url( $com->background );
                                            $bg = $com->background;
                                            if ( $row->expired == true ) {
                                                $row->background = url( 'api/img/' . base64_encode( $bg ) );
                                            } else {
                                                if ( $start > time() ) {
                                                    $row->background = url( 'api/img/' . base64_encode( $bg ) );

                                                }
                                            }

                                        }

                                    } else {
                                        $query = DB::table( 'tpa_insurer' )
                                            ->select( 'background' )
                                            ->where( 'insurer_code', $row->InsurerCode );
                                        if ( $query->count() > 0 ) {
                                            $ins = $query->first();

                                            if ( strpos( $ins->background, 'https' ) !== false ) {
                                                $row->background = $ins->background;

                                            } else {
                                                if ( !empty( $ins->background ) ) {
                                                    $row->background = url( $ins->background );
                                                    $bg = $ins->background;
                                                }
                                            }

                                            if ( $row->expired == true ) {
                                                $row->background = url( 'api/img/' . base64_encode( $bg ) );
                                            } else {
                                                if ( $start > time() ) {
                                                    $row->background = url( 'api/img/' . base64_encode( $bg ) );

                                                }
                                            }

                                        } else {
                                            $row->background = '';
                                        }
                                    }

                                    #font color
                                    $row->color = '#FFFFFF';
                                    $query = DB::table( 'tpa_company' )
                                        ->select( 'color' )
                                        ->whereNotNull( 'color' )
                                        ->where( 'company_code', $row->CompanyCode );
                                    if ( $query->count() > 0 ) {
                                        $com = $query->first();
                                        $row->color = $com->color;
                                    } else {
                                        $query = DB::table( 'tpa_insurer' )
                                            ->select( 'color' )
                                            ->where( 'insurer_code', $row->InsurerCode );
                                        if ( $query->count() > 0 ) {
                                            $ins = $query->first();
                                            $row->color = $ins->color;
                                        }
                                    }

                                    $row->inapp = false;
                                    $row->mobile = $user->mobile;
                                    if ( $do_activate == true ) {
//$row->AllowEclaim = 'True';
                                    } else {
                                        $row->PolNo = substr( $row->PolNo, 0, 2 ) . str_pad( '', strlen( substr( $row->PolNo, 2, -2 ) ), 'x' ) . substr( $row->PolNo, -2 );

//$row->AllowEclaim = 'False';
                                    }

                                    $row->InsuredType = strtolower( $row->InsuredType );

                                    if ( $insured_type == strtolower( $row->InsuredType ) || empty( $insured_type ) ) {
                                        if ( App::getLocale() == 'en' ) {
                                            $row->Name = $row->NameEN;
                                            $row->Surname = $row->SurnameEN;
                                            $row->InsurerName = $row->InsurerNameEN;
                                            $row->CompanyName = $row->CompanyNameEN;
                                        }

                                        unset( $row->NameEN );
                                        unset( $row->SurnameEN );
                                        unset( $row->InsurerNameEN );
                                        unset( $row->CompanyNameEN );
                                        if ( $is_claim == 1 ) {
                                            if ( strtolower( $row->AllowEclaim ) == 'true' ) {
                                                if ( $row->expired == true ) {
                                                    array_push( $tpa_expired, $row );
                                                    if ( $newEndingDate > $cur_date ) {
                                                        $row->isEclaimShow = true;
                                                    } else {
                                                        $row->isEclaimShow = false;
                                                    }
                                                } else {
                                                    if ( $start <= time() ) {
                                                        array_push( $tpa_active, $row );
                                                        $row->isEclaimShow = true;
                                                    } else {
                                                        array_push( $tpa_pending, $row );
                                                        $row->isEclaimShow = false;
                                                    }
                                                }
                                            } else {
                                                $row->isEclaimShow = false;
                                            }
                                        } else {
                                            if ( $row->expired == true ) {
                                                array_push( $tpa_expired, $row );
                                            } else {
                                                if ( $start <= time() ) {
                                                    array_push( $tpa_active, $row );
                                                    //$row->isEclaimShow = "z";
                                                } else {
                                                    array_push( $tpa_pending, $row );
                                                }

                                            }

                                            if ( strtolower( $row->AllowEclaim ) == 'true' ) {
                                                if ( $row->expired == true ) {
                                                    if ( $newEndingDate > $cur_date ) {
                                                        $row->isEclaimShow = true;
                                                    } else {
                                                        $row->isEclaimShow = false;
                                                    }
                                                } else {
                                                    if ( $start <= time() ) {
                                                        $row->isEclaimShow = true;
                                                    } else {
                                                        $row->isEclaimShow = false;
                                                    }
                                                }
                                            } else {
                                                $row->isEclaimShow = false;
                                            }
                                        }
                                    }
                                }

                                if (  ( $do_activate == true ) && ( $is_activated == false ) ) {
                                    $affected_row = DB::table( 'users' )
                                        ->where( 'id', $uid )
                                        ->update( [
                                            'citizen_id' => $citizen_id,
                                        ] );

                                    if ( $affected_row > 0 ) {
                                        $client = new GuzzleHttp\Client();
                                        $res = $client->request( 'POST', env( 'TPA_URL' ) . 'PolicyActivate', [
                                            'headers' => [
                                                'Content-Type'  => 'application/json',
                                                'Authorization' => $token,
                                            ],
                                            'body'    => json_encode( [
                                                'CITIZEN_ID' => $citizen_id,
                                                'USERNAME'   => $user->username,
                                            ] ),
                                            'verify'  => false,
                                        ] );

                                    }

                                }

                                if ( $is_claim != 1 ) {

                                    $query = DB::table( 'tpa_policy as p' )
                                        ->join( 'tpa_insurer as i', 'i.insurer_code', '=', 'p.insurer_code' )
                                        ->select( 'p.*', 'i.name_' . App::getLocale() . ' as insurer_name', 'i.name_th AS InsurerName', 'i.name_en AS InsurerNameEN' )
                                        ->where( 'p.deleted_at', null )
                                        ->where( 'p.insured_type', $insured_type )
                                        ->where( 'p.uid', $uid )
                                        ->orderBy( 'p.effective_from', 'asc' );
                                    $policies = $query->get();

                                    if ( $query->count() > 0 ) {
                                        foreach ( $policies as $row ) {
                                            //list( $name, $surname ) = explode( ' ', $row->insured_name );
                                            $insured_name = explode( ' ', $row->insured_name );
                                            if ( count( $insured_name ) < 2 ) {
                                                $insured_name[1] = '';
                                            }

                                            $eff_to = date( 'Y-m-d', strtotime( $row->effective_to ) ) . ' 23:59:59';

                                            if ( strtotime( $eff_to ) < time() ) {
                                                if ( !empty( $row->card_front ) ) {
                                                    $card_front = url( 'api/img/' . base64_encode( $row->card_front ) );
                                                } else {
                                                    $card_front = "";
                                                }

                                                if ( !empty( $row->card_back ) ) {
                                                    $card_back = url( 'api/img/' . base64_encode( $row->card_back ) );
                                                } else {
                                                    $card_back = "";
                                                }

                                            } else {
                                                if ( $start > time() ) {
                                                    $card_front = url( 'api/img/' . base64_encode( $row->card_front ) );

                                                    $card_back = url( 'api/img/' . base64_encode( $row->card_back ) );
                                                } else {
                                                    if ( !empty( $row->card_front ) ) {
                                                        $card_front = url( $row->card_front );
                                                    } else {
                                                        $card_front = "";
                                                    }
                                                    if ( !empty( $row->card_front ) ) {
                                                        $card_back = url( $row->card_back );
                                                    } else {
                                                        $card_back = "";
                                                    }
                                                }
                                            }

                                            $obj = (object) [
                                                'id'                  => (int) $row->id,
                                                'InsuredType'         => $row->insured_type,
                                                'InsurerCode'         => $row->insurer_code,
                                                'InsurerName'         => $row->insurer_name,
                                                'EclaimInsurerName'   => $row->InsurerName,
                                                'EclaimInsurerNameEN' => $row->InsurerNameEN,
                                                'Name'                => $insured_name[0],
                                                'Surname'             => $insured_name[1],
                                                'PolNo'               => $row->policy_no,
                                                'EffFrom'             => date( 'd/m/Y', strtotime( $row->effective_from ) ),
                                                'EffTo'               => date( 'd/m/Y', strtotime( $row->effective_to ) ),
                                                'card_front'          => $card_front,
                                                'card_back'           => $card_back,
                                                'inapp'               => true,
                                                'sort'                => $row->effective_from,
                                            ];

                                            $obj->expired = false;
                                            if ( strtotime( $eff_to ) < time() ) {
                                                $obj->expired = true;
                                                array_push( $self_expired, $obj );
                                            } else {
                                                array_push( $self_active, $obj );
                                            }

                                        }

                                    }
                                }

                                $EffFrom = array_column( $tpa_active, 'sort' );
                                array_multisort( $EffFrom, SORT_ASC, $tpa_active );

                                foreach ( $tpa_active as $r ) {
                                    $r->cc = 'tpa_active';
                                    array_push( $data, $r );
                                }

                                $EffFrom = array_column( $self_active, 'sort' );
                                array_multisort( $EffFrom, SORT_ASC, $self_active );

                                foreach ( $self_active as $r ) {
                                    $r->cc = 'self_active';
                                    array_push( $data, $r );
                                }

                                $EffFrom = array_column( $tpa_pending, 'sort' );
                                array_multisort( $EffFrom, SORT_ASC, $tpa_pending );

                                foreach ( $tpa_pending as $r ) {
                                    $r->cc = 'tpa_pending';
                                    array_push( $data, $r );
                                }

                                foreach ( $tpa_expired as $r ) {
                                    $r->cc = 'tpa_expired';
                                    array_push( $data, $r );
                                }

                                foreach ( $self_expired as $r ) {
                                    $r->cc = 'self_expired';
                                    array_push( $data, $r );
                                }

                                return response()->json( [
                                    'status'  => 'ok',
                                    'message' => __( 'messages.success' ),
                                    'date'    => date( 'Y-m-d H:i:s' ),
                                    'data'    => $data,
                                ], 200 );
                            }
                        }
                    } catch ( RequestException $e ) {
                        return response()->json( [
                            'status'    => 'error',
                            'message'   => __( 'Internal server error.' ),
                            'exception' => $e->getMessage(),
                        ], 500 );
                    } catch ( Exception $e ) {
                        return response()->json( [
                            'status'    => 'error',
                            'message'   => __( 'Internal server error.' ),
                            'exception' => $e->getMessage(),
                        ], 500 );
                    }
                }
            } );

            Route::get( 'detail/{policy_id}', function ( $policy_id, Request $request ) {
                try {
                    $policy = DB::table( 'tpa_policy as p' )
                        ->join( 'tpa_insurer as i', 'i.insurer_code', '=', 'p.insurer_code' )
                        ->select( 'p.id', 'p.insurer_code', 'i.name_' . App::getLocale() . ' as insurer_name', 'p.insured_name', 'p.policy_no', 'p.effective_from', 'p.effective_to', 'p.card_front', 'p.card_back' )
                        ->where( 'p.deleted_at', null )
                        ->where( 'p.id', $policy_id )
                        ->first();

                    $coverages = DB::table( 'tpa_policy_benefit' )
                        ->select( 'cov_index', 'cov_desc', 'cov_limit' )
                        ->where( 'policy_id', $policy_id )
                        ->orderBy( 'cov_index', 'asc' )->get();

                    if ( !empty( $policy ) ) {
                        $policy->card_front = ( $policy->card_front ) ? url( $policy->card_front ) : null;
                        $policy->card_back = ( $policy->card_back ) ? url( $policy->card_back ) : null;
                        $policy->effective_from = date( 'd/m/Y', strtotime( $policy->effective_from ) );
                        $policy->effective_to = date( 'd/m/Y', strtotime( $policy->effective_to ) );
                        $policy->coverages = $coverages;
                    }

                    return response()->json( [
                        'status'  => 'ok',
                        'message' => __( 'messages.success' ),
                        'data'    => $policy,
                    ], 200 );

                } catch ( RequestException $e ) {
                    return response()->json( [
                        'status'    => 'error',
                        'message'   => __( 'Internal server error.' ),
                        'exception' => $e->getMessage(),
                    ], 500 );
                } catch ( Exception $e ) {
                    return response()->json( [
                        'status'    => 'error',
                        'message'   => __( 'Internal server error.' ),
                        'exception' => $e->getMessage(),
                    ], 500 );
                }

            } );

			Route::post( 'detail/', function ( Request $request ) {

				try {

					$validator = [
						'pol_no'   => 'required',
						'member_code'   => 'required',
					];

					cb()->validation( $validator );

					$citizen_id = $request->citizen_id;

					$uid = $request->token->uid;
					if ( empty( $citizen_id ) ) {
						$user = DB::table( 'users' )
							->select( 'citizen_id' )
							->where( 'id', $uid )
							->first();
						$citizen_id = $user->citizen_id;

					}

					if ( empty( $citizen_id ) ) {
						return response()->json( [
							'status'  => 'error',
							'message' => __( 'messages.policy.list.error' ),
						], 500 );
					} else {

						try {
							$client = new GuzzleHttp\Client();
							$res = $client->request( 'POST', env( 'TPA_URL' ) . 'login', [
								'headers' => ['Content-Type' => 'application/json'],
								'body'    => json_encode( [
									'Username' => env( 'TPA_USER' ),
									'Password' => env( 'TPA_PASS' ),
								] ),
								'verify'  => false,
							] );

							$token = json_decode( $res->getBody()->getContents() );
							if ( empty( $token ) ) {
								return response()->json( [
									'status'  => 'error',
									'message' => __( 'Internal server error.' ),
								], 500 );
							} else {
								$client = new GuzzleHttp\Client();
								$res = $client->request( 'POST', env( 'TPA_URL' ) . 'PolicyDetail', [
									'headers' => [
										'Content-Type'  => 'application/json',
										'Authorization' => $token,
									],
									'body'    => json_encode( [
										'CITIZEN_ID'  => $citizen_id,
										'POL_NO'      => $request->pol_no,
										'MEMBER_CODE' => $request->member_code,
									] ),
									'verify'  => false,
								] );

								$httpCode = $res->getStatusCode();
								if ( $httpCode != 200 ) {
									return response()->json( [
										'status'  => 'error',
										'message' => __( 'Internal server error.' ),
										'token'   => $token,
									], $httpCode );
								} else {
									$body = json_decode( $res->getBody()->getContents() );

									$ListOfPolDet = [];
									foreach ( $body->ListOfPolDet as $row ) {
										if ( App::getLocale() == 'en' ) {
											$row->MainBenefit = $row->MainBenefitEN;
										}

										unset( $row->MainBenefitEN );
										$Coverage = $row->Coverage;
										unset( $row->Coverage );

										$coverages = [];
										foreach ( $Coverage as $r ) {
											if ( App::getLocale() == 'en' ) {
												$r->CovNo = $r->CovNoEN;
												$r->CovDesc = $r->CovDescEN;
												$r->CovLimit = $r->CovLimitEN;
												$r->CovUtilized = $r->CovUtilizedEN;
											}
											unset( $r->CovNoEN );
											unset( $r->CovDescEN );
											unset( $r->CovLimitEN );
											unset( $r->CovUtilizedEN );
											array_push( $coverages, $r );
										}
										$row->Coverage = $coverages;
										array_push( $ListOfPolDet, $row );

									}

									$ListOfPolClaim = [];
									foreach ( $body->ListOfPolClaim as $row ) {
										if ( App::getLocale() == 'en' ) {
											$row->ClmInsurer = $row->ClmInsurerEN;
											$row->ClmStatusTxt = $row->ClmStatusTxtEN;
											$row->ClmProvider = $row->ClmProviderEN;
										}
										unset( $row->ClmInsurerEN );
										unset( $row->ClmStatusTxtEN );
										unset( $row->ClmProviderEN );
										array_push( $ListOfPolClaim, $row );

									}
									return response()->json( [
										'status'  => 'ok',
										'message' => __( 'messages.success' ),
										'data'    => [
											'ListOfPolDet'   => $ListOfPolDet,
											'ListOfPolClaim' => $ListOfPolClaim,
										],
									], 200 );
								}
							}
						} catch ( RequestException $e ) {
							return response()->json( [
								'status'    => 'error',
								'message'   => __( 'Internal server error.' ),
								'exception' => $e->getMessage(),
							], 500 );
						}
					}
				} catch ( CBValidationException $e ) {
                    return response()->json( [
                        'status'   => 'error',
                        'message'  => $e->getMessage(),
                        'messages' => explode( '; ', $e->getMessage() ),
                    ], 500 );
                }
            } );

            Route::get( 'detail/{pol_no}/{member_code}', function ( $pol_no, $member_code, Request $request ) {
                $citizen_id = ( $request->input( 'citizen_id' ) ) ? strtolower( $request->input( 'citizen_id' ) ) : null;

                $uid = $request->token->uid;
                if ( empty( $citizen_id ) ) {
                    $user = DB::table( 'users' )
                        ->select( 'citizen_id' )
                        ->where( 'id', $uid )
                        ->first();
                    $citizen_id = $user->citizen_id;

                }

                if ( empty( $citizen_id ) ) {
                    return response()->json( [
                        'status'  => 'error',
                        'message' => __( 'messages.policy.list.error' ),
                    ], 500 );
                } else {

                    try {
                        $client = new GuzzleHttp\Client();
                        $res = $client->request( 'POST', env( 'TPA_URL' ) . 'login', [
                            'headers' => ['Content-Type' => 'application/json'],
                            'body'    => json_encode( [
                                'Username' => env( 'TPA_USER' ),
                                'Password' => env( 'TPA_PASS' ),
                            ] ),
                            'verify'  => false,
                        ] );

                        $token = json_decode( $res->getBody()->getContents() );
                        if ( empty( $token ) ) {
                            return response()->json( [
                                'status'  => 'error',
                                'message' => __( 'Internal server error.' ),
                            ], 500 );
                        } else {
                            $client = new GuzzleHttp\Client();
                            $res = $client->request( 'POST', env( 'TPA_URL' ) . 'PolicyDetail', [
                                'headers' => [
                                    'Content-Type'  => 'application/json',
                                    'Authorization' => $token,
                                ],
                                'body'    => json_encode( [
                                    'CITIZEN_ID'  => $citizen_id,
                                    'POL_NO'      => $pol_no,
                                    'MEMBER_CODE' => $member_code,
                                ] ),
                                'verify'  => false,
                            ] );

                            $httpCode = $res->getStatusCode();
                            if ( $httpCode != 200 ) {
                                return response()->json( [
                                    'status'  => 'error',
                                    'message' => __( 'Internal server error.' ),
                                    'token'   => $token,
                                ], $httpCode );
                            } else {
                                $body = json_decode( $res->getBody()->getContents() );

                                $ListOfPolDet = [];
                                foreach ( $body->ListOfPolDet as $row ) {
                                    if ( App::getLocale() == 'en' ) {
                                        $row->MainBenefit = $row->MainBenefitEN;
                                    }

                                    unset( $row->MainBenefitEN );
                                    $Coverage = $row->Coverage;
                                    unset( $row->Coverage );

                                    $coverages = [];
                                    foreach ( $Coverage as $r ) {
                                        if ( App::getLocale() == 'en' ) {
                                            $r->CovNo = $r->CovNoEN;
                                            $r->CovDesc = $r->CovDescEN;
                                            $r->CovLimit = $r->CovLimitEN;
                                            $r->CovUtilized = $r->CovUtilizedEN;
                                        }
                                        unset( $r->CovNoEN );
                                        unset( $r->CovDescEN );
                                        unset( $r->CovLimitEN );
                                        unset( $r->CovUtilizedEN );
                                        array_push( $coverages, $r );
                                    }
                                    $row->Coverage = $coverages;
                                    array_push( $ListOfPolDet, $row );

                                }

                                $ListOfPolClaim = [];
                                foreach ( $body->ListOfPolClaim as $row ) {
                                    if ( App::getLocale() == 'en' ) {
                                        $row->ClmInsurer = $row->ClmInsurerEN;
                                        $row->ClmStatusTxt = $row->ClmStatusTxtEN;
                                        $row->ClmProvider = $row->ClmProviderEN;
                                    }
                                    unset( $row->ClmInsurerEN );
                                    unset( $row->ClmStatusTxtEN );
                                    unset( $row->ClmProviderEN );
                                    array_push( $ListOfPolClaim, $row );

                                }
                                return response()->json( [
                                    'status'  => 'ok',
                                    'message' => __( 'messages.success' ),
                                    'data'    => [
                                        'ListOfPolDet'   => $ListOfPolDet,
                                        'ListOfPolClaim' => $ListOfPolClaim,
                                    ],
                                ], 200 );
                            }
                        }
                    } catch ( RequestException $e ) {
                        return response()->json( [
                            'status'    => 'error',
                            'message'   => __( 'Internal server error.' ),
                            'exception' => $e->getMessage(),
                        ], 500 );
                    }
                }
            } );

            Route::get( 'detail/{pol_no}/{member_code1}/{member_code2}', function ( $pol_no, $member_code1, $member_code2, Request $request ) {
                $citizen_id = ( $request->input( 'citizen_id' ) ) ? strtolower( $request->input( 'citizen_id' ) ) : null;

                $member_code = $member_code1 . '/' . $member_code2;

                $uid = $request->token->uid;
                if ( empty( $citizen_id ) ) {
                    $user = DB::table( 'users' )
                        ->select( 'citizen_id' )
                        ->where( 'id', $uid )
                        ->first();
                    $citizen_id = $user->citizen_id;

                }

                if ( empty( $citizen_id ) ) {
                    return response()->json( [
                        'status'  => 'error',
                        'message' => __( 'messages.policy.list.error' ),
                    ], 500 );
                } else {

                    try {
                        $client = new GuzzleHttp\Client();
                        $res = $client->request( 'POST', env( 'TPA_URL' ) . 'login', [
                            'headers' => ['Content-Type' => 'application/json'],
                            'body'    => json_encode( [
                                'Username' => env( 'TPA_USER' ),
                                'Password' => env( 'TPA_PASS' ),
                            ] ),
                            'verify'  => false,
                        ] );

                        $token = json_decode( $res->getBody()->getContents() );
                        if ( empty( $token ) ) {
                            return response()->json( [
                                'status'  => 'error',
                                'message' => __( 'Internal server error.' ),
                            ], 500 );
                        } else {
                            $client = new GuzzleHttp\Client();
                            $res = $client->request( 'POST', env( 'TPA_URL' ) . 'PolicyDetail', [
                                'headers' => [
                                    'Content-Type'  => 'application/json',
                                    'Authorization' => $token,
                                ],
                                'body'    => json_encode( [
                                    'CITIZEN_ID'  => $citizen_id,
                                    'POL_NO'      => $pol_no,
                                    'MEMBER_CODE' => $member_code,
                                ] ),
                                'verify'  => false,
                            ] );

                            $httpCode = $res->getStatusCode();
                            if ( $httpCode != 200 ) {
                                return response()->json( [
                                    'status'  => 'error',
                                    'message' => __( 'Internal server error.' ),
                                    'token'   => $token,
                                ], $httpCode );
                            } else {
                                $body = json_decode( $res->getBody()->getContents() );

                                $ListOfPolDet = [];
                                foreach ( $body->ListOfPolDet as $row ) {
                                    if ( App::getLocale() == 'en' ) {
                                        $row->MainBenefit = $row->MainBenefitEN;
                                    }

                                    unset( $row->MainBenefitEN );
                                    $Coverage = $row->Coverage;
                                    unset( $row->Coverage );

                                    $coverages = [];
                                    foreach ( $Coverage as $r ) {
                                        if ( App::getLocale() == 'en' ) {
                                            $r->CovNo = $r->CovNoEN;
                                            $r->CovDesc = $r->CovDescEN;
                                            $r->CovLimit = $r->CovLimitEN;
                                            $r->CovUtilized = $r->CovUtilizedEN;
                                        }
                                        unset( $r->CovNoEN );
                                        unset( $r->CovDescEN );
                                        unset( $r->CovLimitEN );
                                        unset( $r->CovUtilizedEN );
                                        array_push( $coverages, $r );
                                    }
                                    $row->Coverage = $coverages;
                                    array_push( $ListOfPolDet, $row );

                                }

                                $ListOfPolClaim = [];
                                foreach ( $body->ListOfPolClaim as $row ) {
                                    if ( App::getLocale() == 'en' ) {
                                        $row->ClmInsurer = $row->ClmInsurerEN;
                                        $row->ClmStatusTxt = $row->ClmStatusTxtEN;
                                        $row->ClmProvider = $row->ClmProviderEN;
                                    }
                                    unset( $row->ClmInsurerEN );
                                    unset( $row->ClmStatusTxtEN );
                                    unset( $row->ClmProviderEN );
                                    array_push( $ListOfPolClaim, $row );

                                }
                                return response()->json( [
                                    'status'  => 'ok',
                                    'message' => __( 'messages.success' ),
                                    'data'    => [
                                        'ListOfPolDet'   => $ListOfPolDet,
                                        'ListOfPolClaim' => $ListOfPolClaim,
                                    ],
                                ], 200 );
                            }
                        }
                    } catch ( RequestException $e ) {
                        return response()->json( [
                            'status'    => 'error',
                            'message'   => __( 'Internal server error.' ),
                            'exception' => $e->getMessage(),
                        ], 500 );
                    }
                }
            } );

            Route::get( 'detail/{pol_no}/{member_code1}/{member_code2}/{member_code3}', function ( $pol_no, $member_code1, $member_code2, $member_code3, Request $request ) {
                $citizen_id = ( $request->input( 'citizen_id' ) ) ? strtolower( $request->input( 'citizen_id' ) ) : null;

                $member_code = $member_code1 . '/' . $member_code2 . '/' . $member_code3;

                $uid = $request->token->uid;
                if ( empty( $citizen_id ) ) {
                    $user = DB::table( 'users' )
                        ->select( 'citizen_id' )
                        ->where( 'id', $uid )
                        ->first();
                    $citizen_id = $user->citizen_id;

                }

                if ( empty( $citizen_id ) ) {
                    return response()->json( [
                        'status'  => 'error',
                        'message' => __( 'messages.policy.list.error' ),
                    ], 500 );
                } else {

                    try {
                        $client = new GuzzleHttp\Client();
                        $res = $client->request( 'POST', env( 'TPA_URL' ) . 'login', [
                            'headers' => ['Content-Type' => 'application/json'],
                            'body'    => json_encode( [
                                'Username' => env( 'TPA_USER' ),
                                'Password' => env( 'TPA_PASS' ),
                            ] ),
                            'verify'  => false,
                        ] );

                        $token = json_decode( $res->getBody()->getContents() );
                        if ( empty( $token ) ) {
                            return response()->json( [
                                'status'  => 'error',
                                'message' => __( 'Internal server error.' ),
                            ], 500 );
                        } else {
                            $client = new GuzzleHttp\Client();
                            $res = $client->request( 'POST', env( 'TPA_URL' ) . 'PolicyDetail', [
                                'headers' => [
                                    'Content-Type'  => 'application/json',
                                    'Authorization' => $token,
                                ],
                                'body'    => json_encode( [
                                    'CITIZEN_ID'  => $citizen_id,
                                    'POL_NO'      => $pol_no,
                                    'MEMBER_CODE' => $member_code,
                                ] ),
                                'verify'  => false,
                            ] );

                            $httpCode = $res->getStatusCode();
                            if ( $httpCode != 200 ) {
                                return response()->json( [
                                    'status'  => 'error',
                                    'message' => __( 'Internal server error.' ),
                                    'token'   => $token,
                                ], $httpCode );
                            } else {
                                $body = json_decode( $res->getBody()->getContents() );

                                $ListOfPolDet = [];
                                foreach ( $body->ListOfPolDet as $row ) {
                                    if ( App::getLocale() == 'en' ) {
                                        $row->MainBenefit = $row->MainBenefitEN;
                                    }

                                    unset( $row->MainBenefitEN );
                                    $Coverage = $row->Coverage;
                                    unset( $row->Coverage );

                                    $coverages = [];
                                    foreach ( $Coverage as $r ) {
                                        if ( App::getLocale() == 'en' ) {
                                            $r->CovNo = $r->CovNoEN;
                                            $r->CovDesc = $r->CovDescEN;
                                            $r->CovLimit = $r->CovLimitEN;
                                            $r->CovUtilized = $r->CovUtilizedEN;
                                        }
                                        unset( $r->CovNoEN );
                                        unset( $r->CovDescEN );
                                        unset( $r->CovLimitEN );
                                        unset( $r->CovUtilizedEN );
                                        array_push( $coverages, $r );
                                    }
                                    $row->Coverage = $coverages;
                                    array_push( $ListOfPolDet, $row );

                                }

                                $ListOfPolClaim = [];
                                foreach ( $body->ListOfPolClaim as $row ) {
                                    if ( App::getLocale() == 'en' ) {
                                        $row->ClmInsurer = $row->ClmInsurerEN;
                                        $row->ClmStatusTxt = $row->ClmStatusTxtEN;
                                        $row->ClmProvider = $row->ClmProviderEN;
                                    }
                                    unset( $row->ClmInsurerEN );
                                    unset( $row->ClmStatusTxtEN );
                                    unset( $row->ClmProviderEN );
                                    array_push( $ListOfPolClaim, $row );

                                }
                                return response()->json( [
                                    'status'  => 'ok',
                                    'message' => __( 'messages.success' ),
                                    'data'    => [
                                        'ListOfPolDet'   => $ListOfPolDet,
                                        'ListOfPolClaim' => $ListOfPolClaim,
                                    ],
                                ], 200 );
                            }
                        }
                    } catch ( RequestException $e ) {
                        return response()->json( [
                            'status'    => 'error',
                            'message'   => __( 'Internal server error.' ),
                            'exception' => $e->getMessage(),
                        ], 500 );
                    }
                }
            } );

            Route::post( '/', function ( Request $request ) {
                try {

                    $validator = [
                        'insurer_code'   => 'required',
                        'insured_name'   => 'required',
                        'policy_no'      => 'required',
                        'effective_from' => 'required',
                        'effective_to'   => 'required',
                    ];

                    cb()->validation( $validator );

                    $uid = $request->token->uid;

                    $current_time = date( 'Y-m-d H:i:s' );
                    $card_front_64 = $request->input( 'card_front' );
                    $card_back_64 = $request->input( 'card_back' );

                    $insured_type = ( $request->input( 'insured_type' ) ) ? strtolower( $request->input( 'insured_type' ) ) : 'member';

                    $policy = [
                        'uid'            => $uid,
                        'insurer_code'   => $request->input( 'insurer_code' ),
                        'insured_name'   => $request->input( 'insured_name' ),
                        'insured_type'   => $insured_type,
                        'policy_no'      => $request->input( 'policy_no' ),
                        'effective_from' => $request->input( 'effective_from' ),
                        'effective_to'   => $request->input( 'effective_to' ),
                        'created_at'     => $current_time,
                    ];

                    if ( !empty( $card_front_64 ) ) {
                        $policy['card_front'] = upload_card_photo( $request->input( 'card_front' ) );
                    }

                    if ( !empty( $card_back_64 ) ) {
                        $policy['card_back'] = upload_card_photo( $request->input( 'card_back' ) );
                    }

                    $policy_id = DB::table( 'tpa_policy' )->insertGetId( $policy );
                    if ( $policy_id > 0 ) {

                        foreach ( $request->input( 'coverages' ) as $row ) {
                            $benefit = [
                                'policy_id' => $policy_id,
                                'cov_desc'  => $row['cov_desc'],
                                'cov_limit' => $row['cov_limit'],
                                'cov_index' => $row['cov_index'],
                            ];
                            DB::table( 'tpa_policy_benefit' )->insert( $benefit );

                        }

                        return response()->json( [
                            'status'  => 'ok',
                            'message' => __( 'messages.success' ),
                            'data'    => [
                                'policy_id' => $policy_id,
                            ],
                        ], 200 );
                    } else {
                        return response()->json( [
                            'status'  => 'error',
                            'message' => 'Failure !',
                        ], 500 );
                    }
                } catch ( CBValidationException $e ) {
                    return response()->json( [
                        'status'   => 'error',
                        'message'  => $e->getMessage(),
                        'messages' => explode( '; ', $e->getMessage() ),
                    ], 500 );
                } catch ( Exception $e ) {
                    return response()->json( [
                        'status'  => 'error',
                        'message' => $e->getMessage(),
                    ], 500 );
                }
            } );

            Route::put( '/{policy_id}', function ( $policy_id, Request $request ) {
                try {

                    $validator = [
                        'insurer_code'   => 'required',
                        'insured_name'   => 'required',
                        'policy_no'      => 'required',
                        'effective_from' => 'required',
                        'effective_to'   => 'required',
                    ];

                    cb()->validation( $validator );

                    $uid = $request->token->uid;

                    $current_time = date( 'Y-m-d H:i:s' );
                    $card_front_64 = $request->input( 'card_front' );
                    $card_back_64 = $request->input( 'card_back' );

                    $policy = [
                        'uid'            => $uid,
                        'insurer_code'   => $request->input( 'insurer_code' ),
                        'insured_name'   => $request->input( 'insured_name' ),
                        'policy_no'      => $request->input( 'policy_no' ),
                        'effective_from' => $request->input( 'effective_from' ),
                        'effective_to'   => $request->input( 'effective_to' ),
                        'updated_at'     => $current_time,
                    ];

                    if ( !empty( $card_front_64 ) ) {
                        $policy['card_front'] = upload_card_photo( $card_front_64 );
                    }

                    if ( !empty( $card_back_64 ) ) {
                        $policy['card_back'] = upload_card_photo( $card_back_64 );
                    }

                    $affected_row = DB::table( 'tpa_policy' )
                        ->where( 'id', $policy_id )
                        ->update( $policy );

                    if ( $affected_row > 0 ) {
                        DB::table( 'tpa_policy_benefit' )->where( 'policy_id', $policy_id )->delete();

                        foreach ( $request->input( 'coverages' ) as $row ) {
                            $benefit = [
                                'policy_id' => $policy_id,
                                'cov_desc'  => $row['cov_desc'],
                                'cov_limit' => $row['cov_limit'],
                                'cov_index' => $row['cov_index'],
                            ];
                            DB::table( 'tpa_policy_benefit' )->insert( $benefit );

                        }

                    }

                    return response()->json( [
                        'status'  => 'ok',
                        'message' => __( 'messages.success' ),
                    ], 200 );

                } catch ( CBValidationException $e ) {
                    return response()->json( [
                        'status'   => 'error',
                        'message'  => $e->getMessage(),
                        'messages' => explode( '; ', $e->getMessage() ),
                    ], 500 );
                } catch ( Exception $e ) {
                    return response()->json( [
                        'status'  => 'error',
                        'message' => $e->getMessage(),
                    ], 500 );
                }
            } );

            Route::delete( '/{policy_id}', function ( $policy_id, Request $request ) {
                try {
                    $uid = $request->token->uid;

                    $count = DB::table( 'tpa_policy as p' )
                        ->join( 'tpa_insurer as i', 'i.insurer_code', '=', 'p.insurer_code' )
                        ->select( 'p.id' )
                        ->where( 'p.deleted_at', null )
                        ->where( 'p.id', $policy_id )
                        ->where( 'p.uid', $uid )
                        ->count();

                    $affected_row = DB::table( 'tpa_policy' )
                        ->where( 'id', $policy_id )
                        ->update( [
                            'deleted_at' => date( 'Y-m-d H:i:s' ),
                        ] );

                    if ( $affected_row > 0 ) {
                        return response()->json( [
                            'status'  => 'ok',
                            'message' => __( 'messages.success' ),
                        ], 200 );

                    } else {
                        return response()->json( [
                            'status'  => 'error',
                            'message' => __( 'failed' ),
                        ], 500 );

                    }

                } catch ( Exception $e ) {
                    return response()->json( [
                        'status'  => 'error',
                        'message' => $e->getMessage(),
                    ], 500 );
                }
            } );

            Route::post( 'used/{policy_no}', function ( $policy_no, Request $request ) {
                try {
                    $validator = [
                        'provider_code' => 'required',
                        'insurer_code'  => 'required',
                        'name'          => 'required',
                        'surname'       => 'required',
                    ];

                    cb()->validation( $validator );

                    $uid = $request->token->uid;
                    $user = DB::table( 'users' )
                        ->select( 'citizen_id', 'username' )
                        ->where( 'id', $uid )
                        ->first();

                    $citizen_id = $user->citizen_id;
                    $username = $user->username;

                    if ( empty( $citizen_id ) ) {
                        return response()->json( [
                            'status'  => 'error',
                            'message' => __( 'cannot use !' ),
                        ], 500 );

                    } else {
                        $client = new GuzzleHttp\Client();
                        $res = $client->request( 'POST', env( 'TPA_URL' ) . 'login', [
                            'headers' => ['Content-Type' => 'application/json'],
                            'body'    => json_encode( [
                                'Username' => env( 'TPA_USER' ),
                                'Password' => env( 'TPA_PASS' ),
                            ] ),
                            'verify'  => false,
                        ] );

                        $token = json_decode( $res->getBody()->getContents() );
                        if ( empty( $token ) ) {
                            return response()->json( [
                                'status'  => 'error',
                                'message' => __( 'Internal server error.' ),
                            ], 500 );
                        } else {
                            $client = new GuzzleHttp\Client();
                            $res = $client->request( 'POST', env( 'TPA_URL' ) . 'EligibilityCheck', [
                                'headers' => [
                                    'Content-Type'  => 'application/json',
                                    'Authorization' => $token,
                                ],
                                'body'    => json_encode( [
                                    'PERSONAL_ID'   => $citizen_id,
                                    'PROVIDER_CODE' => $request->input( 'provider_code' ),
                                    'NAME'          => $request->input( 'name' ),
                                    'SURNAME'       => $request->input( 'surname' ),
                                    'INSURER_CODE'  => $request->input( 'insurer_code' ),
                                    'POLICY_NO'     => $policy_no,
                                    'TMS_USERNAME'  => $username,
                                ] ),
                                'verify'  => false,
                            ] );

                            $httpCode = $res->getStatusCode();
                            if ( $httpCode != 200 ) {
                                return response()->json( [
                                    'status'  => 'error',
                                    'message' => __( 'Internal server error.' ),
                                ], $httpCode );
                            } else {
                                $rs = json_decode( $res->getBody()->getContents() );
                                $record_id = $rs->ListOfEligibilityCheck[0]->RecordID;
                                DB::table( 'tpa_usedcard' )->insert( [
                                    'uid'           => $uid,
                                    'citizen_id'    => $citizen_id,
                                    'name'          => $request->input( 'name' ),
                                    'surname'       => $request->input( 'surname' ),
                                    'policy_no'     => $policy_no,
                                    'insurer_code'  => $request->input( 'insurer_code' ),
                                    'provider_code' => $request->input( 'provider_code' ),
                                    'username'      => $username,
                                    'record_id'     => $record_id,
                                ] );

                                return response()->json( [
                                    'status'  => 'ok',
                                    'message' => __( 'messages.success' ),
                                    'data'    => [
                                        'record_id' => $record_id,
                                    ],
                                ], 200 );
                            }

                        }
                    }

                } catch ( CBValidationException $e ) {
                    return response()->json( [
                        'status'  => 'error',
                        'message' => $e->getMessage(),
                    ], 500 );
                } catch ( RequestException $e ) {
                    return response()->json( [
                        'status'    => 'error',
                        'message'   => __( 'Internal server error.' ),
                        'exception' => $e->getMessage(),
                    ], 500 );
                } catch ( Exception $e ) {
                    return response()->json( [
                        'status'    => 'error',
                        'message'   => __( 'Internal server error.' ),
                        'exception' => $e->getMessage(),
                    ], 500 );
                }

            } );

            Route::put( 'cancel/{record_id}', function ( $record_id, Request $request ) {

                try {
                    $client = new GuzzleHttp\Client();
                    $res = $client->request( 'POST', env( 'TPA_URL' ) . 'login', [
                        'headers' => ['Content-Type' => 'application/json'],
                        'body'    => json_encode( [
                            'Username' => env( 'TPA_USER' ),
                            'Password' => env( 'TPA_PASS' ),
                        ] ),
                        'verify'  => false,
                    ] );

                    $token = json_decode( $res->getBody()->getContents() );
                    if ( empty( $token ) ) {
                        return response()->json( [
                            'status'  => 'error',
                            'message' => __( 'Internal server error.' ),
                        ], 500 );
                    } else {
                        $client = new GuzzleHttp\Client();
                        $res = $client->request( 'POST', env( 'TPA_URL' ) . 'EligibilityCancelCheck', [
                            'headers' => [
                                'Content-Type'  => 'application/json',
                                'Authorization' => $token,
                            ],
                            'body'    => json_encode( [
                                'RECORD_ID' => $record_id,
                            ] ),
                            'verify'  => false,
                        ] );

                        $httpCode = $res->getStatusCode();
                        if ( $httpCode != 200 ) {
                            return response()->json( [
                                'status'  => 'error',
                                'message' => __( 'Internal server error.' ),
                                'token'   => $token,
                            ], $httpCode );
                        } else {
                            $body = json_decode( $res->getBody()->getContents() );
                            if ( $body->ListOfEligibilityCancelCheck[0]->Status != '1' ) {
                                return response()->json( [
                                    'status'    => 'error',
                                    'message'   => __( 'Internal server error.' ),
                                    'exception' => 'TPA',
                                ], 500 );

                            } else {
                                return response()->json( [
                                    'status'  => 'ok',
                                    'message' => __( 'messages.success' ),
                                ], 200 );

                            }
                        }
                    }
                } catch ( Exception $e ) {
                    return response()->json( [
                        'status'    => 'error',
                        'message'   => __( 'Internal server error.' ),
                        'exception' => $e->getMessage(),
                    ], 500 );
                }

            } );

        } );

        Route::group( ['prefix' => 'claim'], function () {
            Route::post( '/', function ( Request $request ) {
                try {
                    $validator = [
                        'policy_no'       => 'required',
                        'claim_type'      => 'required',
                        'admited_type'    => 'required',
                        'symptom'         => 'required',
                        'admited_date'    => 'required',
                        'provider_code'   => 'required',
                        'medical_expense' => 'required',
                    ];

                    cb()->validation( $validator );

                    $uid = $request->token->uid;
                    $policy_no = $request->input( 'policy_no' );
                    $claim_id = (int) DB::table( 'tpa_claim' )->insertGetId( [
                        'uid'             => $uid,
                        'policy_no'       => $policy_no,
                        'staff_no'        => $request->input( 'staff_no' ),
                        'name'            => $request->input( 'name' ),
                        'surname'         => $request->input( 'surname' ),
                        'mobile'          => $request->input( 'mobile' ),
                        'email'           => $request->input( 'email' ),
                        'card_type'       => $request->input( 'card_type' ),
                        'insurer_code'    => $request->input( 'insurer_code' ),
                        'insurer_name_th' => $request->input( 'insurer_name_th' ),
                        'insurer_name_en' => $request->input( 'insurer_name_en' ),
                        'claim_type'      => $request->input( 'claim_type' ),
                        'admited_type'    => $request->input( 'admited_type' ),
                        'symptom'         => $request->input( 'symptom' ),
                        'medical_expense' => $request->input( 'medical_expense' ),
                        'provider_code'   => $request->input( 'provider_code' ),
                        'admited_date'    => $request->input( 'admited_date' ),
                        'created_at'      => date( 'Y-m-d H:i:s' ),
                    ] );

                    $accident_date = ( $request->input( 'accident_date' ) ) ? $request->input( 'accident_date' ) : '';
                    if ( !empty( $accident_date ) ) {
                        DB::table( 'tpa_claim' )->where( 'claim_id', $claim_id )
                            ->update( [
                                'accident_date' => $accident_date,
                            ] );

                    }

                    $sftp = new Net_SFTP( env( 'FTP_HOST' ) );
                    $sftp->login( env( 'FTP_USER' ), env( 'FTP_PASS' ) );
                    $sftp->mkdir( date( 'Ymd' ) );
                    $sftp->chdir( date( 'Ymd' ) );
                    $sftp->mkdir( $claim_id );
                    $sftp->chdir( $claim_id );

                    $attachment = [
                        'claim_id'        => $claim_id,
                        'attachment_type' => 1,
                    ];
                    foreach ( $request->input( 'receipt_attachment' ) as $receipt64 ) {
                        $attachment['file'] = upload_claim_attachment( $receipt64 );
                        $fid = DB::table( 'tpa_claim_attachment' )->insertGetId( $attachment );
                        list( $file, $ext ) = explode( '.', $attachment['file'] );
                        $sftp->put( $claim_id . '-' . $policy_no . '-receipt-' . $fid . '.' . $ext, public_path() . $attachment['file'], NET_SFTP_LOCAL_FILE );
                    }
                    $attachment['attachment_type'] = 2;

                    foreach ( $request->input( 'medical_certificate_attachment' ) as $medical64 ) {
                        $attachment['file'] = upload_claim_attachment( $medical64 );
                        $fid = DB::table( 'tpa_claim_attachment' )->insertGetId( $attachment );
                        list( $file, $ext ) = explode( '.', $attachment['file'] );
                        $sftp->put( $claim_id . '-' . $policy_no . '-medical-cert-' . $fid . '.' . $ext, public_path() . $attachment['file'], NET_SFTP_LOCAL_FILE );

                    }

                    return response()->json( [
                        'status'  => 'ok',
                        'message' => __( 'messages.success' ),
                        'data'    => [
                            'uid'             => $uid,
                            'policy_no'       => $policy_no,
                            'staff_no'        => $request->input( 'staff_no' ),
                            'name'            => $request->input( 'name' ),
                            'surname'         => $request->input( 'surname' ),
                            'mobile'          => $request->input( 'mobile' ),
                            'email'           => $request->input( 'email' ),
                            'card_type'       => $request->input( 'card_type' ),
                            'insurer_code'    => $request->input( 'insurer_code' ),
                            'insurer_name_th' => $request->input( 'insurer_name_th' ),
                            'insurer_name_en' => $request->input( 'insurer_name_en' ),
                            'claim_type'      => $request->input( 'claim_type' ),
                            'admited_type'    => $request->input( 'admited_type' ),
                            'symptom'         => $request->input( 'symptom' ),
                            'medical_expense' => $request->input( 'medical_expense' ),
                            'provider_code'   => $request->input( 'provider_code' ),
                            'admited_date'    => $request->input( 'admited_date' ),
                            'created_at'      => date( 'Y-m-d H:i:s' ),
                        ],
                    ], 200 );

                } catch ( CBValidationException $e ) {
                    return response()->json( [
                        'status'  => 'error',
                        'message' => $e->getMessage(),
                    ], 500 );
                } catch ( RequestException $e ) {
                    return response()->json( [
                        'status'    => 'error',
                        'message'   => __( 'Internal server error.' ),
                        'exception' => $e->getMessage(),
                    ], 500 );
                } catch ( Exception $e ) {
                    return response()->json( [
                        'status'    => 'error',
                        'message'   => __( 'Internal server error.' ),
                        'exception' => $e->getMessage(),
                    ], 500 );
                }

            } );

            Route::get( '/', function ( Request $request ) {
                try {
                    $uid = $request->token->uid;
                    $user = DB::table( 'users' )
                        ->select( 'username' )
                        ->where( 'id', $uid )
                        ->first();

                    $username = $user->username;

                    $client = new GuzzleHttp\Client();
                    $res = $client->request( 'POST', env( 'TPA_URL' ) . 'login', [
                        'headers' => ['Content-Type' => 'application/json'],
                        'body'    => json_encode( [
                            'Username' => env( 'TPA_USER' ),
                            'Password' => env( 'TPA_PASS' ),
                        ] ),
                        'verify'  => false,
                    ] );

                    $token = json_decode( $res->getBody()->getContents() );
                    if ( empty( $token ) ) {
                        return response()->json( [
                            'status'  => 'error',
                            'message' => __( 'Internal server error.' ),
                        ], 500 );
                    } else {
                        $client = new GuzzleHttp\Client();
                        $res = $client->request( 'POST', env( 'TPA_URL' ) . 'ClaimStatus', [
                            'headers' => [
                                'Content-Type'  => 'application/json',
                                'Authorization' => $token,
                            ],
                            'body'    => json_encode( [
                                'USERNAME' => $username,
                            ] ),
                            'verify'  => false,
                        ] );

                        $httpCode = $res->getStatusCode();
                        if ( $httpCode != 200 ) {
                            return response()->json( [
                                'status'  => 'error',
                                'message' => __( 'Internal server error.' ),
                            ], $httpCode );
                        } else {
                            $rs = json_decode( $res->getBody()->getContents() );
                            $data = [];
                            if ( !empty( $rs ) ) {
                                foreach ( $rs->ListOfClaimPolList as $row ) {
                                    if ( App::getLocale() == 'en' ) {
                                        $row->CardName = $row->CardNameEN;
                                        $row->InsurerName = $row->InsurerNameEN;
                                    }
                                    unset( $row->CardNameEN );
                                    unset( $row->InsurerNameEN );

                                    $row->EnableAddMoreFile = false;

                                    $statuses = [];
                                    foreach ( $row->ClaimStatus as $r ) {
                                        if (  (  ( $r->StatusName == 'อยู่ระหว่างรอเอกสารเพิ่มเติม' ) || ( $r->StatusName == 'ขอข้อมูลเพิ่มเติม' ) ) && ( $r->StatusCondition == 'Completed' ) ) {
                                            $row->EnableAddMoreFile = true;

                                        }
                                        if ( App::getLocale() == 'en' ) {
                                            $r->StatusName = $r->StatusNameEN;
                                        }
                                        unset( $r->StatusNameEN );
                                        array_push( $statuses, $r );
                                    }

                                    $card_type = DB::table( 'tpa_cardtype' )
                                        ->select( 'name_' . App::getLocale() . ' AS name' )
                                        ->where( 'card_type', $row->CardType )
                                        ->first();
                                    list( $d, $m, $y ) = explode( '/', $row->VisitDate );
                                    $dd = [$y, $m, $d];
                                    $admited_date = implode( '-', $dd );

                                    $row->CardTypeName = $card_type->name;
                                    $row->sort = $admited_date . ' 00:00:00';
                                    ;
                                    $row->ClaimStatus = $statuses;

                                    $row->IsEclaim = false;

                                    $count = DB::table( 'tpa_claim' )
                                        ->select( 'claim_id' )
                                        ->where( 'policy_no', $row->PolNo )
                                        ->where( 'admited_date', $admited_date )
                                        ->count();
                                    if ( $count > 0 ) {
                                        $row->IsEclaim = true;
                                    }

                                    array_push( $data, $row );
                                }

                            }

                            $rs = DB::table( 'tpa_claim as c' )
                                ->leftjoin( 'tpa_cardtype as ct', 'ct.card_type', '=', 'c.card_type' )
                                ->where( 'c.uid', $uid )
                                ->orderBy( 'c.created_at', 'desc' )
                                ->get();
                            $list = [];
                            foreach ( $rs as $row ) {
                                $obj = (object) [
                                    'PolNo'             => $row->policy_no,
                                    'VisitDate'         => date( 'd/m/Y', strtotime( $row->admited_date ) ),
                                    'sort'              => date( 'Y-m-d H:i:s', strtotime( $row->admited_date ) ),
                                    'ClaimNo'           => '',
                                    'CardType'          => $row->card_type,
                                    'CardName'          => '',
                                    'InsurerCode'       => '',
                                    'InsurerName'       => ( App::getLocale() == 'th' ) ? $row->insurer_name_th : $row->insurer_name_en,
                                    'CardTypeName'      => ( App::getLocale() == 'th' ) ? $row->name_th : $row->name_en,
                                    'ClaimStatus'       => [
                                        (object) [
                                            'StatusNo'        => '1',
                                            'StatusName'      => ( App::getLocale() == 'th' ) ? 'รับเอกสาร' : 'Received',
                                            'StatusCondition' => 'Completed',
                                            'StatusDate'      => date( 'd/m/Y H:i', strtotime( $row->created_at ) ),
                                        ],
                                        (object) [
                                            'StatusNo'        => '2',
                                            'StatusName'      => ( App::getLocale() == 'th' ) ? 'อยู่ระหว่างพิจารณา' : 'In Progress',
                                            'StatusCondition' => 'None',
                                            'StatusDate'      => '',
                                        ],
                                        (object) [
                                            'StatusNo'        => '3',
                                            'StatusName'      => ( App::getLocale() == 'th' ) ? 'ขอข้อมูลเพิ่มเติม' : 'Request Document',
                                            'StatusCondition' => 'None',
                                            'StatusDate'      => '',
                                        ],
                                        (object) [
                                            'StatusNo'        => '4',
                                            'StatusName'      => ( App::getLocale() == 'th' ) ? 'พิจารณาเรียบร้อย' : 'Completed',
                                            'StatusCondition' => 'None',
                                            'StatusDate'      => '',
                                        ],
                                    ],
                                    'IsEclaim'          => true,
                                    'EnableAddMoreFile' => false,
                                ];

                                array_push( $list, $obj );
                            }
                            $xx = $data;
                            foreach ( $list as $obj ) {
                                $chk = 0;
                                foreach ( $xx as $r ) {
                                    if (  ( $r->PolNo == $obj->PolNo ) && ( $r->VisitDate == $obj->VisitDate ) ) {
                                        $chk = 1;
                                    }
                                }
                                if ( $chk == 0 ) {
                                    array_push( $data, $obj );

                                }
                            }

                            $date = array_column( $data, 'sort' );
                            array_multisort( $date, SORT_ASC, $data );

                            return response()->json( [
                                'status'  => 'ok',
                                'message' => __( 'messages.success' ),
                                'data'    => $data,
                            ], 200 );
                        }

                    }

                } catch ( CBValidationException $e ) {
                    return response()->json( [
                        'status'  => 'error',
                        'message' => $e->getMessage(),
                    ], 500 );
                } catch ( RequestException $e ) {
                    return response()->json( [
                        'status'    => 'error',
                        'message'   => __( 'Internal server error.' ),
                        'exception' => $e->getMessage(),
                    ], 500 );
                } catch ( Exception $e ) {
                    return response()->json( [
                        'status'    => 'error',
                        'message'   => __( 'Internal server error.' ),
                        'exception' => $e->getMessage(),
                    ], 500 );
                }

            } );

            Route::get( 'symptom', function ( Request $request ) {

                $keyword = ( $request->input( 'keyword' ) ) ? $request->input( 'keyword' ) : '';

                try {
                    $data = DB::table( 'tpa_symptom' )
                        ->select( 'name_' . App::getLocale() . ' AS name' )
                        ->where( 'status', 1 )
                        ->where( 'name_th', 'like', '%' . $keyword . '%' )
                        ->orWhere( 'name_en', 'like', '%' . $keyword . '%' )
                        ->get();

                    return response()->json( [
                        'status'  => 'ok',
                        'message' => __( 'messages.success' ),
                        'data'    => $data,
                    ], 200 );
                } catch ( Exception $e ) {
                    return response()->json( [
                        'status'    => 'error',
                        'message'   => __( 'Internal server error.' ),
                        'exception' => $e->getMessage(),
                    ], 500 );

                }

            } );

            Route::post( '/more', function ( Request $request ) {
                try {
                    $validator = [
                        'policy_no' => 'required',
                        'claim_no'  => 'required',
                        'status_no' => 'required',
                    ];

                    cb()->validation( $validator );

                    $uid = $request->token->uid;

                    $attachment = [
                        'uid'             => $uid,
                        'policy_no'       => $request->input( 'policy_no' ),
                        'claim_no'        => $request->input( 'claim_no' ),
                        'status_no'       => $request->input( 'status_no' ),
                        'attachment_type' => 1,
                    ];

                    foreach ( $request->input( 'receipt_attachment' ) as $receipt64 ) {
                        $attachment['file'] = upload_claim_attachment( $receipt64 );
                        DB::table( 'tpa_more_attachment' )->insert( $attachment );
                    }
                    $attachment['attachment_type'] = 2;

                    foreach ( $request->input( 'medical_certificate_attachment' ) as $medical64 ) {
                        $attachment['file'] = upload_claim_attachment( $medical64 );
                        DB::table( 'tpa_more_attachment' )->insert( $attachment );
                    }

                    $attachment['attachment_type'] = 3;

                    foreach ( $request->input( 'other_attachment' ) as $other ) {
                        $attachment['file'] = upload_claim_attachment( $other );
                        DB::table( 'tpa_more_attachment' )->insert( $attachment );
                    }

                    return response()->json( [
                        'status'  => 'ok',
                        'message' => __( 'messages.success' ),
                    ], 200 );

                } catch ( CBValidationException $e ) {
                    return response()->json( [
                        'status'  => 'error',
                        'message' => $e->getMessage(),
                    ], 500 );
                } catch ( RequestException $e ) {
                    return response()->json( [
                        'status'    => 'error',
                        'message'   => __( 'Internal server error.' ),
                        'exception' => $e->getMessage(),
                    ], 500 );
                } catch ( Exception $e ) {
                    return response()->json( [
                        'status'    => 'error',
                        'message'   => __( 'Internal server error.' ),
                        'exception' => $e->getMessage(),
                    ], 500 );
                }

            } );

        } );

    } );

    Route::middleware( 'auth:api' )->group( function () {
        Route::put( '/xxx', function ( Request $request ) {
            return response()->json( [
                'status'  => 'ok',
                'message' => __( 'messages.success' ),
            ], 200 );
        } );

        Route::get( '/user', function ( Request $request ) {
            //return $request->user();
            return response()->json( [
                'status'  => 'ok',
                'message' => __( 'messages.success' ),
            ], 200 );
        } );
    } );
} );
