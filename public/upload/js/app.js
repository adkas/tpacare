$(function () {
    var names = [];
    $(document).on('change', '.picupload', function (event) {
        var files = event.target.files;
        var file_index = parseInt($('input[name=\'file_index\']').val());
        for (var i = 0; i < files.length; i++) {
            var file = files[i];
            names.push($(this).get(0).files[i].name);
            if (file.type.match('image')) {

                var picReader = new FileReader();
                picReader.fileName = file.name
                picReader.addEventListener('load', function (event) {

                    var picFile = event.target;

                    var div = document.createElement("li");

                    div.innerHTML = '<img src=\'' + picFile.result + '\'' +
                        'title=\'' + picFile.name + '\' /><div class=\'post-thumb\'><div class=\'inner-post-thumb\'><a href=\'javascript:void(0);\' data-id=\'' + event.target.fileName + '\' class=\'remove-pic\' data-index=\'' + file_index + '\'><i class=\'fa fa-time\' aria-hidden=\'true\'></i></a><a href=\'javascript:void(0);\' data-id=\'' + event.target.fileName + '\' class=\'view-pic\' data-index=\'' + file_index + '\'><i class=\'fa fa-search\' aria-hidden=\'true\'></i></a><div></div>';

                    $(div).appendTo($('#media-list'));

                    file_index++;
                    $('input[name=\'file_index\']').val(file_index);
                    $('li.myupload').append('<span id=\'row-' + file_index + '\'><i class=\'fa fa-plus\' aria-hidden=\'true\'></i><input name=\'files[]\' type=\'file\' click-type=\'type2\' class=\'picupload\' accept=\'image/*\'></span>');
                    $('#media-list li:last-child').after($('li.myupload'));


                });
            }
            picReader.readAsDataURL(file);

        }

        $(this).parent().hide();

    });

    $('body').on('click', '.remove-pic', function () {
        $(this).parent().parent().parent().remove();
        var removeItem = $(this).data('id');
        var index = $(this).data('index');
        var yet = names.indexOf(removeItem);

        $('#row-' + index).remove();

        if (yet != -1) {
            names.splice(yet, 1);
        }
    });

    $(document).on('click', '.view-pic', function () {
        $('.imagepreview').attr('src', $(this).prev().parents().parents().prev().attr('src'));
        $('#imagemodal').modal('show');
    });

});
