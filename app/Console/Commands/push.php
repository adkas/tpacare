<?php

namespace App\Console\Commands;

use GuzzleHttp\Exception\RequestException;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;

class push extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'cron:push';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'push notification scheduler';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $schedule = DB::table( 'tpa_schedule as s' )
            ->join( 'tpa_notifications as n', 'n.id', '=', 's.notification_id' )
            ->whereNull( 'sent_at' )
            ->select( 'n.*', 's.id as schedule_id', 's.notification_id', 's.users' )
            ->first();

        $client = new \GuzzleHttp\Client();

        $endpoint = env( 'PUSH_URL' ) . env( 'PUSH_APP_ID' ) . '/push/';
        $params = [
            'app_id'             => env( 'PUSH_APP_ID' ),
            'app_secret'         => env( 'PUSH_APP_SECRET' ),
            'user'               => explode( ',', $schedule->users ),
            'data'               => [
                'type'          => $schedule->category,
                'push_title_th' => $schedule->title_th,
                'push_title_en' => $schedule->title_en,
                'push_body_th'  => $schedule->body_th,
                'push_body_en'  => $schedule->body_en,
                'title_th'      => $schedule->title_th,
                'title_en'      => $schedule->title_en,
                'detail_th'     => $schedule->body_th,
                'detail_en'     => $schedule->body_en,
                'url'           => $schedule->url,
                'id'            => $schedule->data_id,
            ],
            'notification'       => [
                'title' => $schedule->title_th,
                'body'  => $schedule->body_th,
            ],
            'android'            => [
                'priority' => 'high',
            ],
            'push_gears_options' => [
                'eta'     => date( 'Y-m-d H:i:s' ),
                'history' => true,
            ],
        ];

        try {
            $res = $client->request( 'POST', $endpoint, [
                'headers' => ['Content-Type' => 'application/json'],
                'body'    => json_encode( $params ),
            ] );
            $response = json_decode( $res->getBody()->getContents() );

            DB::table( 'tpa_schedule' )->where( 'id', $schedule->schedule_id )->update( [
                'response' => $response->detail,
                'sent_at'  => date( 'Y-m-d H:i:s' ),
            ] );

            echo ( $response->detail );

        } catch ( RequestException $e ) {

            echo $e->getMessage();

        }

    }
}
