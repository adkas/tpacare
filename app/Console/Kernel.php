<?php

namespace App\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        Commands\push::class
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule( Schedule $schedule )
    {
        // $schedule->command('inspire')
        //          ->hourly();

        /*$schedule->call( function () {
        DB::table( 'tpa_test' )->insert( [
        'test' => 'xxxx',
        ] );
        echo date( 'Y-m-d H:i:s' );
        } )->everyMinute()->when( function () {
        return true;
        } );*/
        $schedule->command( 'cron:push' )->everyMinute();

    }

    /**
     * Register the commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        $this->load( __DIR__ . '/Commands' );

        require base_path( 'routes/console.php' );
    }
}
