<?php
if ( !function_exists( 'alert' ) ) {
    function alert( $data = [] )
    {
        echo '<pre>';
        print_r( $data );
        echo '</pre>';
    }
}

if ( !function_exists( 'distance' ) ) {
    function distance( $lat1, $lon1, $lat2, $lon2, $unit = 'K' )
    {
        if (  ( $lat1 == $lat2 ) && ( $lon1 == $lon2 ) ) {
            return 0;
        } else {
            $theta = $lon1 - $lon2;
            $dist = sin( deg2rad( $lat1 ) ) * sin( deg2rad( $lat2 ) ) + cos( deg2rad( $lat1 ) ) * cos( deg2rad( $lat2 ) ) * cos( deg2rad( $theta ) );
            $dist = acos( $dist );
            $dist = rad2deg( $dist );
            $miles = $dist * 60 * 1.1515;
            $unit = strtoupper( $unit );

            if ( $unit == "K" ) {
                return ( $miles * 1.609344 );
            } else if ( $unit == "N" ) {
                return ( $miles * 0.8684 );
            } else {
                return $miles;
            }
        }
    }
}

if ( !function_exists( 'cut_num' ) ) {
    function cut_num( $num, $precision = 3 )
    {
        return floor( $num ) . substr( str_replace( floor( $num ), '', $num ), 0, $precision + 1 );
    }
}

if ( !function_exists( 'xml2array' ) ) {
    function xml2array( $n )
    {
        $return = array();
        foreach ( $n->childNodes as $nc ) {
            ( $nc->hasChildNodes() )
            ? ( $n->firstChild->nodeName == $n->lastChild->nodeName && $n->childNodes->length > 1 )
            ? $return[$nc->nodeName][] = xml2array( $item )
            : $return[$nc->nodeName] = xml2array( $nc )
            : $return = $nc->nodeValue;
        }

        return $return;
    }
}

if ( !function_exists( 'send_sms' ) ) {
    function send_sms( $params = [] )
    {
        $authenHeader = base64_encode( env( 'SMS_USER' ) . ':' . env( 'SMS_PASS' ) );
        // Post data
        $postData = "<?xml version=\"1.0\" encoding=\"UTF-8\"?><transaction>
    <id>" . $params['id'] . "</id>
    <msisdn>" . $params['msisdn'] . "</msisdn>
    <msgtype>" . $params['msgtype'] . "</msgtype>
    <msdata>" . $params['msdata'] . "</msdata>
    <sender>SMS</sender>
</transaction>";

        $fp = fsockopen( env( 'SMS_HOST' ), 80, $errno, $errstr, 30 );
        if ( !$fp ) {
            echo "$errstr ($errno)<br />\n";
        } else {
            $reqPkg = "POST " . env( 'SMS_PATH' ) . " HTTP/1.1\r\n";
            $reqPkg .= "Host: " . env( 'SMS_HOST' ) . "\r\n";
            $reqPkg .= "Content-type: text/xml; charset=UTF-8\r\n";
            $reqPkg .= "Authorization: $authenHeader\r\n";
            $reqPkg .= "Connection: close\r\n";
            $reqPkg .= "Content-Length: " . strlen( $postData ) . "\r\n\r\n";
            $reqPkg .= "$postData";

            /*$reqPkg = "POST /tunnel/servlet/sendSMS.do HTTP/1.1\r\n";
            $reqPkg .= "Host: sms.911itwist.com\r\n";
            $reqPkg .= "Content-type: text/xml; charset=UTF-8\r\n";
            $reqPkg .= "Authorization: $authenHeader\r\n";
            $reqPkg .= "Connection: close\r\n";
            $reqPkg .= "Content-Length: " . strlen($postData) . "\r\n\r\n";
            $reqPkg .= "$postData";*/

            $respData = '';
            fwrite( $fp, $reqPkg );
            while ( !feof( $fp ) ) {
                $respData .= fgets( $fp, 128 );
            }

            fclose( $fp );

            if ( !is_null( $respData ) ) {
                $xml = new DOMDocument();
                $xml->preserveWhiteSpace = false;
                $xml->loadXML( trim( substr( $respData, stripos( $respData, '<transaction>' ), strlen( $respData ) - strlen( '<transaction>
        ' ) ) ) );
                return xml2array( $xml );
            } else {
                return [];
            }
        }
    }
}

if ( !function_exists( 'upload_profie_photo' ) ) {
    function upload_profie_photo( $image_64 )
    {
        /*$extension = explode( '/', explode( ':', substr( $image_64, 0, strpos( $image_64, ';' ) ) )[1] )[1]; // .jpg .png .pdf
        $replace = substr( $image_64, 0, strpos( $image_64, ',' ) + 1 );
        $image = str_replace( $replace, '', $image_64 );
        $image = str_replace( ' ', '+', $image );*/

        //$image = base64_decode( $image_64 );
        $image = base64_decode( $image_64 );
        $extension = getImageMimeType( $image );
        $imageName = Str::random( 10 ) . '.' . $extension;

        $photo_dir = '/storage/images/profile/' . $imageName;
        $path = public_path() . $photo_dir;
        file_put_contents( $path, $image );
        return $photo_dir;
    }
}

if ( !function_exists( 'upload_card_photo' ) ) {
    function upload_card_photo( $image_64 )
    {
        $image = base64_decode( $image_64 );
        $extension = getImageMimeType( $image );
        $imageName = Str::random( 10 ) . '.' . $extension;
        $photo_dir = '/storage/images/card/' . $imageName;
        $path = public_path() . $photo_dir;
        file_put_contents( $path, $image );

        return $photo_dir;
    }
}

if ( !function_exists( 'upload_claim_attachment' ) ) {
    function upload_claim_attachment( $image_64 )
    {
        $image = base64_decode( $image_64 );
        //$extension = getImageMimeType( $image );
        $extension = getMimeType( $image );
        $imageName = Str::random( 10 ) . '.' . $extension;
        $photo_dir = '/storage/claim/' . $imageName;
        $path = public_path() . $photo_dir;
        file_put_contents( $path, $image );

        return $photo_dir;
    }
}

if ( !function_exists( 'getBytesFromHexString' ) ) {
    function getBytesFromHexString( $hexdata )
    {
        for ( $count = 0; $count < strlen( $hexdata ); $count += 2 ) {
            $bytes[] = chr( hexdec( substr( $hexdata, $count, 2 ) ) );
        }

        return implode( $bytes );
    }

}

if ( !function_exists( 'getImageMimeType' ) ) {
    function getImageMimeType( $imagedata )
    {
        $imagemimetypes = array(
            "jpeg" => "FFD8",
            "png"  => "89504E470D0A1A0A",
            "gif"  => "474946",
            "bmp"  => "424D",
            "tiff" => "4949",
            "tiff" => "4D4D",
        );

        foreach ( $imagemimetypes as $mime => $hexbytes ) {
            $bytes = getBytesFromHexString( $hexbytes );
            if ( substr( $imagedata, 0, strlen( $bytes ) ) == $bytes ) {
                return $mime;
            }

        }

        return null;
    }

}

if ( !function_exists( 'getMimeType' ) ) {
    function getMimeType( $imagedata )
    {
        $f = finfo_open();
        $mime = finfo_buffer( $f, $imagedata, FILEINFO_MIME_TYPE );
        switch ( $mime ) {
            case "image/png":
                $ext = 'png';
                break;

            case "image/jpg":
            case "image/jpeg":
                $ext = 'jpg';
                break;

            case "application/pdf":
                $ext = 'pdf';
                break;

            default:
                $ext = null;
                break;
        }
        return $ext;
    }

}

if ( !function_exists( 'build_otp_message' ) ) {
    function build_otp_message( $lang, $ref_no, $otp )
    {
        $msdata = 'รหัส OTP ของคุณคือ ' . $otp . ' โปรดยืนยันตัวตนตามรหัสอ้างอิง ' . $ref_no;
        if ( $lang == 'en' ) {
            $msdata = 'Enter OTP ' . $otp . ' to verify yourself for the reference code ' . $ref_no;
        }

        return $msdata;

    }
}

if ( !function_exists( 'image_png_filter' ) ) {
    function image_png_filter( $img )
    {
        $im = imagecreatefrompng( trim( public_path() . '\\' . str_replace( '/', '\\', $img ) ) );
        //applying grascale filter
        if ( $im && imagefilter( $im, IMG_FILTER_GRAYSCALE ) ) {

            header( 'Content-type: image/png' );
            imagepng( $im );

            //Saving and replacing the original png file with the grayscale png
            //imagepng( $im, 'colorful.png' );
        }

    }
}

if ( !function_exists( 'image_jpg_filter' ) ) {
    function image_jpg_filter( $img )
    {
        $im = ImageCreateFromJpeg( trim( public_path() . '\\' . str_replace( '/', '\\', $img ) ) );

        $imgw = imagesx( $im );
        $imgh = imagesy( $im );

        for ( $i = 0; $i < $imgw; $i++ ) {
            for ( $j = 0; $j < $imgh; $j++ ) {

                // get the rgb value for current pixel

                $rgb = ImageColorAt( $im, $i, $j );

                // extract each value for r, g, b

                $rr = ( $rgb >> 16 ) & 0xFF;
                $gg = ( $rgb >> 8 ) & 0xFF;
                $bb = $rgb & 0xFF;

                // get the Value from the RGB value

                $g = round(  ( $rr + $gg + $bb ) / 3 );

                // grayscale values have r=g=b=g

                $val = imagecolorallocate( $im, $g, $g, $g );

                // set the gray value

                imagesetpixel( $im, $i, $j, $val );
            }
        }

        header( 'Content-type: image/jpeg' );
        imagejpeg( $im );

    }
}
