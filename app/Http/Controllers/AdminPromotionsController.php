<?php

namespace App\Http\Controllers;

use crocodicstudio\crudbooster\controllers\CBController;
use DB;

class AdminPromotionsController extends CBController
{

    private $gender = [
        [
            'id'   => 'all',
            'name' => 'ทุกเพศ',
        ],
        [
            'id'   => 'male',
            'name' => 'ชาย',
        ],
        [
            'id'   => 'female',
            'name' => 'หญิง',
        ],
    ];
    public function cbInit()
    {
        $this->setTable( "tpa_promotions" );
        $this->setPermalink( "promotions" );
        $this->setPageTitle( "Promotions" );

        /*$this->addText( "Name Th", "name_th" )->strLimit( 150 )->maxLength( 255 );
        $this->addWysiwyg( "Detail Th", "detail_th" )->strLimit( 150 );
        $this->addText( "Name En", "name_en" )->strLimit( 150 )->maxLength( 255 );
        $this->addWysiwyg( "Detail En", "detail_en" )->strLimit( 150 );
        $this->addDate( "Start Date", "start_date" );
        $this->addDate( "End Date", "end_date" );
        $this->addSelectTable( "Hospital", "hospital_id", ["table" => "tpa_hospital", "value_option" => "id", "display_option" => "name_th", "sql_condition" => ""] );
        $this->addText( "Status", "status" )->strLimit( 150 )->maxLength( 255 );
        $this->addDatetime( "Created At", "created_at" )->required( false )->showIndex( false )->showAdd( false )->showEdit( false );
        $this->addDatetime( "Updated At", "updated_at" )->required( false )->showIndex( false )->showAdd( false )->showEdit( false );*/

        $this->style( function () {
            return "
				table#table-module td:first-child {
					width: 40px;
					text-align: center;
				}

				table#table-module td:nth-child(2) {
					width: 120px;
					text-align: left;
				}

				table#table-module td:nth-child(4) {
					width: 160px;
					text-align: center;
				}

				table#table-module th {
					text-align: center;
					font-weight: normal;
				}

				table#table-module td:last-child {
					width: 100px;
					text-align: center;
				}

				";
        } );

        switch ( request()->segment( 3 ) ) {
            case "add":
            case "edit":
                $this->javascript( function () {
                    return "

            $(function () {
                var toolbar = [
                        ['style', ['bold', 'italic', 'underline', 'clear']],
                        ['fontname', ['fontname']],
                        ['fontsize', ['fontsize']],
                        ['color', ['color']],
                        ['para', ['ul', 'ol', 'paragraph']],
                        ['insert', ['link', 'picture']],
                        ['view', ['codeview']],
                        ['height', ['height']]
                ];



                $('select#hospital_code').select2();

                $('select.category,select.status').select2({
                    minimumResultsForSearch: Infinity
                });



                var names = [];
                $(document).on('change', '.picupload', function (event) {
                    var files = event.target.files;
                    var file_index = parseInt($('input[name=\'file_index\']').val());
                    for (var i = 0; i < files.length; i++) {
                        var file = files[i];
                        names.push($(this).get(0).files[i].name);
                        if (file.type.match('image')) {

                            var picReader = new FileReader();
                            picReader.fileName = file.name
                            picReader.addEventListener('load', function (event) {
                                var picFile = event.target;
                                var div = document.createElement('li');
                                div.innerHTML = '<img src=\'' + picFile.result + '\'' +
                                    'title=\'' + file.name + '\' /><div class=\'post-thumb\'><div class=\'inner-post-thumb\'><a href=\'javascript:void(0);\' data-id=\'' + event.target.fileName + '\' class=\'remove-pic\' data-index=\'' + file_index + '\'><i class=\'fa fa-times\' aria-hidden=\'true\'></i></a><a href=\'javascript:void(0);\' data-id=\'' + event.target.fileName + '\' class=\'view-pic\' data-index=\'' + file_index + '\'><i class=\'fa fa-search\' aria-hidden=\'true\'></i></a><div></div>';

                                $(div).appendTo($('#media-list'));
                                file_index++;
                                $('input[name=\'file_index\']').val(file_index);
                                $('li.myupload').append('<span id=\'row-' + file_index + '\'><i class=\'fa fa-plus\' aria-hidden=\'true\'></i><input name=\'files[]\' type=\'file\' click-type=\'type2\' class=\'picupload\' accept=\'image/*\'></span>');
                                $('#media-list li:last-child').after($('li.myupload'));


                            });
                        }
                        picReader.readAsDataURL(file);
                    }

                    $(this).parent().hide();

                });

                $('body').on('click', '.remove-pic', function () {
                    $(this).parent().parent().parent().remove();
                    var removeItem = $(this).data('id');
                    var index = $(this).data('index');
                    var yet = names.indexOf(removeItem);

                    if($(this).hasClass( 'db' )){
                        var delete_pictures = $('input[name=\'delete_pictures\']').val();
                        if(delete_pictures == ''){
                            $('input[name=\'delete_pictures\']').val(removeItem)
                        }else{
                            $('input[name=\'delete_pictures\']').val(delete_pictures+','+removeItem)
                        }
                    }

                    $('#row-' + index).remove();

                    if (yet != -1) {
                        names.splice(yet, 1);
                    }
                });

                $(document).on('click', '.view-pic', function () {
                    $('.imagepreview').attr('src', $(this).prev().parents().parents().prev().attr('src'));
                    $('#imagemodal').modal('show');
                });


                var detail_th = $('textarea#detail_th');
                var detail_en = $('textarea#detail_en');

                var summernoteValidator = $('#WebForm').validate({
                    errorElement: 'span',
                    errorClass: 'error',
                    validClass: 'is-valid',
                    ignore: ':hidden:not(.summernote),.note-editable.card-block',
                    errorPlacement: function (error, element) {
                        // Add the `help-block` class to the error element
                        error.addClass('invalid-feedback');
                        console.log(element.attr('class'));
                        if (element.prop('type') === 'checkbox') {
                            error.insertAfter(element.siblings('label'));
                        } else if (element.hasClass('summernote')) {
                            error.insertAfter(element.siblings('.note-editor'));
                        } else if(element.hasClass('select2-hidden-accessible')) {
                            error . insertAfter( element . siblings( '.select2-container' ) );

                        } else {
                            error.insertAfter(element);
                        }
                    },
                    rules: {
                        title_th: 'required',
                        detail_th: 'required',
                        title_en: 'required',
                        detail_en: 'required',
                        category_id: 'required',
                        hospital_code: 'required',
                        start_date: 'required',
                        end_date: 'required',
                    },
                    submitHandler: function(form) {
                        $.ajax({
                            url: '" . url( 'admin/promotions/save-' . request()->segment( 3 ) ) . "',
                            type: 'POST',
                            data: new FormData(form),
                            cache: false,
                            processData: false,
                            contentType: false
                        }).done(function(response){
                            if(response.status == 'ok'){

                                swal('บันทึกข้อมูลเรียบร้อยแล้ว', {
                                    buttons: {
                                        catch: {
                                        text: 'OK',
                                        value: 'ok',
                                        }
                                    },
                                    })
                                    .then((value) => {
                                    window.location.href='" . url( 'admin/promotions' ) . "';
                                });
                            }
                        });
                    }
                });

                detail_th.summernote({
                    placeholder: 'รายละเอียดโปรโมชั่นภาษาไทย',
                    height: ($(window).height() - 300),
                    toolbar: toolbar,
                    callbacks: {
                        onChange: function (contents) {
                            detail_th.val(detail_th.summernote('isEmpty') ? '' : contents);
                            summernoteValidator.element(detail_th);
                        }
                    }
                });

                detail_en.summernote({
                    placeholder: 'รายละเอียดโปรโมชั่นภาษาอังกฤษ',
                    height: ($(window).height() - 300),
                    toolbar: toolbar,
                    callbacks: {
                        onChange: function (contents) {
                            detail_en.val(detail_en.summernote('isEmpty') ? '' : contents);
                            summernoteValidator.element(detail_en);
                        }
                    }
				});
				$('.datepicker').datetimepicker({
					format:'Y-m-d',
					onShow:function( ct ){
					this.setOptions({
						minDate:jQuery('#date_timepicker_start').val()?jQuery('#date_timepicker_start').val():false
					})
					},
					timepicker:false
				});

                 });

                            ";
                } );

                break;

            case "send":
                $this->javascript( function () {
                    return "

                    $(function () {

                        $('#WebForm').validate({
                            errorElement: 'span',
                            errorClass: 'error',
                            validClass: 'is-valid',
                            rules: {
                                title_th: 'required',
                                title_en: 'required',
                                body_th: 'required',
                                body_en: 'required'
                            },
                            submitHandler: function (form) {
                                $.ajax({
                                    url: '" . url( 'api/v1/push-notifications' ) . "',
                                    headers: {
                                        'x-api-key':'ykRNP0wziQLCoC64NLpl'
                                    },
                                    type: 'POST',
                                    data: new FormData(form),
                                    cache: false,
                                    processData: false,
                                    contentType: false
                                }).done(function (response) {
                                    console.log(response)
                                    if (response.status == 'ok') {

                                            swal('ส่งการแจ้งเตือนเรียบร้อยแล้ว', {
                                                icon: 'success',
                                                buttons: {
                                                    catch: {
                                                    text: 'OK',
                                                    value: 'ok',
                                                    }
                                                },
                                                })
                                                .then((value) => {
                                                window.location.href='" . url( 'admin/notifications' ) . "';
                                            });
                                    }
                                }).fail(function(response) {
                                    console.log(response.responseJSON);
                                                swal(response.responseJSON.message, {
                                                    icon: 'error',
                                                    dangerMode: true,
                                                })
                                        });
                            }
                        })
                    });

                    ";
                } );

                break;

            default:
                $this->javascript( function () {
                    return "

                    $(function () {
                        $(document).on('click', '.btn-send-noti', function () {
                            var id = $(this).data('id');
                            console.log(id);
                            //$('#imagemodal').modal('show');
                        });
                        $(document).on('click', '.btn-delete', function () {
                                var id = $(this).data('id');
                                swal({
                                    title: 'กรุณายืนยันการลบข้อมูลโปรโมชั่น',
                                    text: '',
                                    icon: 'warning',
                                    dangerMode: true,
                                    buttons: ['ยกเลิก','ยืนยัน']
                                })
                                    .then((willDelete) => {
                                        if (willDelete) {
                                             $.ajax({
                                                url: '" . url( 'admin/promotions/delete' ) . "',
                                                type: 'DELETE',
                                                data: {
                                                    id: id
                                                }
                                            }).done(function(response){
                                                console.log(response);
                                                swal('ลบข้อมูลโปรโมชั่นเรียบร้อยแล้ว', {
                                                    icon: 'success'
                                                }).then(() => {
                                                    location . reload();
                                                });
                                            });

                                        }
                                    });

                            });
                    });

                                    ";
                } );

                break;
        }

    }

    public function getIndex()
    {
        $category_id = ( request()->query( 'category_id' ) ) ? request()->query( 'category_id' ) : 0;
        $seach_status = ( request()->query( 'status' ) ) ? request()->query( 'status' ) : 'all';
        $hospital_name = ( request()->query( 'hospital_name' ) ) ? request()->query( 'hospital_name' ) : '';
        $title_th = ( request()->query( 'title_th' ) ) ? request()->query( 'title_th' ) : '';
        $title_en = ( request()->query( 'title_en' ) ) ? request()->query( 'title_en' ) : '';

        $query = DB::table( 'tpa_promotions as p' )
            ->join( 'tpa_promotions_category as c', 'c.id', '=', 'p.category_id' )
            ->join( 'tpa_hospital as h', 'h.code', '=', 'p.hospital_code' )
            ->select( 'p.id', 'p.title_th', 'p.title_en', 'h.name_th AS hospital_name', 'c.name_th AS category_name', 'p.start_date', 'p.end_date', 'p.created_at', 'p.status' );
        if ( !empty( $category_id ) ) {
            $query->where( 'p.category_id', $category_id );
        }

        if ( !empty( $title_th ) ) {
            $query->where( 'p.title_th', 'like', '%' . $title_th . '%' );
        }

        if ( !empty( $title_en ) ) {
            $query->where( 'p.title_en', 'like', '%' . $title_en . '%' );
        }

        if ( !empty( $hospital_name ) ) {
            $query->where( 'h.name_th', 'like', '%' . $hospital_name . '%' )
                ->orWhere( 'h.name_en', 'like', '%' . $hospital_name . '%' );
        }

        if ( $seach_status == 'all' ) {
            $query->whereIn( 'p.status', [0, 1] );
        } else {
            $status = ( $seach_status == 2 ) ? 0 : $seach_status;
            $query->where( 'p.status', $status );

        }

        $rs = $query->orderby( 'p.id', 'desc' )
            ->paginate( 20 );

        $record = [];
        $no = 1;
        foreach ( $rs as $row ) {
            $img = DB::table( 'tpa_pictures' )->select( 'path' )->where( 'picture_type', 1 )->where( 'ref_id', $row->id )->first();

            $row->no = $no;
            $row->start_date = date( 'd/m/Y', strtotime( $row->start_date ) );
            $row->end_date = date( 'd/m/Y', strtotime( $row->created_at ) );
            $row->created_at = date( 'd/m/Y H:i:s', strtotime( $row->created_at ) );
            $row->status_text = ( $row->status == 1 ) ? 'แสดง' : 'ซ่อน';
            $row->picture = url( $img->path );
            array_push( $record, $row );
            $no++;

        }
        $rr = DB::table( 'tpa_promotions_category' )
            ->select( 'id', 'name_th as name' )
            ->where( 'status', 1 )->get();
        $categories = [];
        foreach ( $rr as $row ) {
            $row->selected = ( $row->id == $category_id ) ? 'selected' : '';
            array_push( $categories, $row );
        }

        $data = [];
        $data['record'] = $record;
        $data['result'] = $rs;
        $data['categories'] = $categories;
        $data['status'] = $seach_status;
        $data['hospital_name'] = $hospital_name;
        $data['title_th'] = $title_th;
        $data['title_en'] = $title_en;

        $data['page_title'] = 'โปรโมชั่นด้านสุขภาพ';
        return view( 'promotions/browse', $data );
    }

    public function getAdd()
    {
        $data = [];
        $data['page_title'] = 'โปรโมชั่นด้านสุขภาพ';
        $data['hospital'] = DB::table( 'tpa_hospital' )->select( 'code', 'name_th AS name' )->get();
        $data['categories'] = DB::table( 'tpa_promotions_category' )->select( 'id', 'name_th AS name' )->where( 'status', 1 )->get();
        $data['age_range'] = DB::table( 'tpa_age_range' )->select( 'id', 'title_th AS name' )->where( 'status', 1 )->get();

        return view( 'promotions/add', $data );

    }

    public function getEdit( $id )
    {
        $row = DB::table( 'tpa_promotions' )->where( 'id', $id )->first();
        $rs_pictures = DB::table( 'tpa_pictures' )->select( 'id', 'path' )->where( 'ref_id', $id )->where( 'picture_type', 1 )->get();
        $pictures = [];
        $index = 1;
        foreach ( $rs_pictures as $r ) {
            $r->index = $index;
            $r->url = url( $r->path );
            array_push( $pictures, $r );
            $index++;
        }

        $row->pictures = $pictures;

        $rs_hospital = DB::table( 'tpa_hospital' )->select( 'code', 'name_th AS name' )->get();
        $hospital = [];
        foreach ( $rs_hospital as $r ) {
            $r->selected = ( $r->code == $row->hospital_code ) ? 'selected' : '';
            array_push( $hospital, $r );
        }

        $rs_category = DB::table( 'tpa_promotions_category' )
            ->select( 'id', 'name_th AS name' )
            ->where( 'status', 1 )
            ->get();

        $categories = [];
        foreach ( $rs_category as $r ) {
            $r->selected = ( $r->id == $row->category_id ) ? 'selected' : '';
            array_push( $categories, $r );
        }

        $rs_age = DB::table( 'tpa_age_range' )->select( 'id', 'title_th AS name' )->where( 'status', 1 )->get();

        $age_range = [];
        foreach ( $rs_age as $r ) {
            $r->selected = ( $r->id == $row->age_range ) ? 'selected' : '';
            array_push( $age_range, $r );
        }

        if ( empty( $row->gender ) ) {
            $row->gender = 'all';
        }

        $gender = [];
        foreach ( (object) $this->gender as $r ) {
            $r = (object) $r;
            $r->selected = ( $r->id == $row->gender ) ? 'selected' : '';
            array_push( $gender, $r );
        }

        $data = [];
        $data['page_title'] = 'โปรโมชั่นด้านสุขภาพ';
        $data['hospital'] = $hospital;
        $data['categories'] = $categories;
        $data['promotion'] = $row;
        $data['age_range'] = $age_range;
        $data['gender'] = $gender;
        $data['file_index'] = count( $row->pictures ) + 1;
        return view( 'promotions/edit', $data );

    }

    public function getSend( $id )
    {
        $row = DB::table( 'tpa_promotions' )->where( 'id', $id )->first();
        $rs_age = DB::table( 'tpa_age_range' )->select( 'id', 'title_th AS name' )->where( 'status', 1 )->get();

        $age_range = [];
        foreach ( $rs_age as $r ) {
            $r->selected = ( $r->id == $row->age_range ) ? 'selected' : '';
            array_push( $age_range, $r );
        }

        if ( empty( $row->gender ) ) {
            $row->gender = 'all';
        }

        $gender = [];
        foreach ( (object) $this->gender as $r ) {
            $r = (object) $r;
            $r->selected = ( $r->id == $row->gender ) ? 'selected' : '';
            array_push( $gender, $r );
        }

        $row->detail_th = trim( strip_tags( $row->detail_th ) );
        $row->detail_en = trim( strip_tags( $row->detail_en ) );

        $data = [];
        $data['page_title'] = 'โปรโมชั่นด้านสุขภาพ';
        $data['promotion'] = $row;
        $data['age_range'] = $age_range;
        $data['gender'] = $gender;
        return view( 'promotions/notify', $data );

    }
}
