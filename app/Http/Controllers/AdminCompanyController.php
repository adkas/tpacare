<?php
namespace App\Http\Controllers;

use crocodicstudio\crudbooster\controllers\CBController;
use DB;

class AdminCompanyController extends CBController
{

    private $page_title = 'ผลิตภัณฑ์';
    public function cbInit()
    {
        $this->setTable( "tpa_company" );
        $this->setPermalink( "company" );
        $this->setPageTitle( "ผลิตภัณฑ์" );

        $this->addText( "Code", "company_code" )->strLimit( 150 )->maxLength( 255 );
        $this->addText( "Name TH", "company_name_th" )->strLimit( 150 )->maxLength( 255 );
        // $this->addText("Policy No","policy_no")->strLimit(150)->maxLength(255);
        // $this->addDatetime("Created At","created_at")->required(false)->showIndex(false)->showAdd(false)->showEdit(false);
        // $this->addDatetime("Updated At","updated_at")->required(false)->showIndex(false)->showAdd(false)->showEdit(false);
        switch ( request()->segment( 3 ) ) {
            case "edit":
                $this->javascript( function () {
                    return "
                        $(function () {
                            $(document).on('click', '.delete', function () {
                                var code = $(this).data('code');
                                swal({
                                    title: 'กรุณายืนยันการลบรูปภาพ',
                                    text: '',
                                    icon: 'warning',
                                    dangerMode: true,
                                    buttons: ['ยกเลิก','ยืนยัน']
                                })
                                    .then((willDelete) => {
                                        if (willDelete) {
                                             $.ajax({
                                                url: '" . url( 'admin/company/delete' ) . "',
                                                type: 'DELETE',
                                                data: {
                                                    code: code
                                                }
                                            }).done(function(response){
                                                console.log(response);
                                                swal('ลบรูปภาพเรียบร้อยแล้ว', {
                                                    icon: 'success'
                                                }).then(() => {
                                                    location . reload();
                                                });
                                            });

                                        }
                                    });

                            });
                        });
                        ";

                });

            break;
        }
    }

    public function getIndex()
    {
        $search_code = ( request()->query( 'search_code' ) ) ? request()->query( 'search_code' ) : '';
        $search_name_th = ( request()->query( 'search_name_th' ) ) ? request()->query( 'search_name_th' ) : '';
        $search_name_en = ( request()->query( 'search_name_en' ) ) ? request()->query( 'search_name_en' ) : '';

        $data = [];
        $data['page_title'] = $this->page_title;
        $query = DB::table( 'tpa_company' )
            ->select( 'company_code as code', 'company_name_th as name_th', 'company_name_en as name_en' );
        if ( !empty( $search_code ) ) {
            $query->where( 'company_code', 'like', '%' . $search_code . '%' );
        }



        if ( !empty( $search_name_th ) ) {
            $query->where( 'company_name_th', 'like', '%' . $search_name_th . '%' );
        }

        if ( !empty( $search_name_en ) ) {
            $query->where( 'company_name_en', 'like', '%' . $search_name_en . '%' );
        }

        $rs = $query->orderby( 'company_name_th', 'asc' )
            ->paginate( 30 );
        $record = [];
        foreach ( $rs as $p => $row ) {
            $row->no = $p + 1;
            array_push( $record, $row );
        }
        $data['result'] = $rs;
        $data['record'] = $record;
        $data['search_code'] = $search_code;
        $data['search_name_th'] = $search_name_th;
        $data['search_name_en'] = $search_name_en;

        return view( 'company/browse', $data );
    }

    public function getAdd()
    {
        $data = [
            'page_title' => $this->page_title
        ];

        return view( 'company/create', $data );

    }

    public function getEdit( $code )
    {
        $row = DB::table( 'tpa_company' )->where( 'company_code', $code )->first();
        $data = [
            'page_title' => $this->page_title,
            'company'    => $row,
        ];

        return view( 'company/update', $data );

    }
}
