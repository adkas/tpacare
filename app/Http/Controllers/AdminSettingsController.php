<?php

namespace App\Http\Controllers;

use crocodicstudio\crudbooster\controllers\CBController;
use DB;

class AdminSettingsController extends CBController
{

    public function cbInit()
    {
        /*$this->setTable( "tpa_promotions" );*/
        $this->setPermalink( "settings" );
        $this->setPageTitle( "Settings" );

        /*$this->addText( "Name Th", "name_th" )->strLimit( 150 )->maxLength( 255 );
    $this->addWysiwyg( "Detail Th", "detail_th" )->strLimit( 150 );
    $this->addText( "Name En", "name_en" )->strLimit( 150 )->maxLength( 255 );
    $this->addWysiwyg( "Detail En", "detail_en" )->strLimit( 150 );
    $this->addDate( "Start Date", "start_date" );
    $this->addDate( "End Date", "end_date" );
    $this->addSelectTable( "Hospital", "hospital_id", ["table" => "tpa_hospital", "value_option" => "id", "display_option" => "name_th", "sql_condition" => ""] );
    $this->addText( "Status", "status" )->strLimit( 150 )->maxLength( 255 );
    $this->addDatetime( "Created At", "created_at" )->required( false )->showIndex( false )->showAdd( false )->showEdit( false );
    $this->addDatetime( "Updated At", "updated_at" )->required( false )->showIndex( false )->showAdd( false )->showEdit( false );*/

    }

    public function getIndex()
    {
        $rs = DB::table( 'tpa_config' )
            ->select( 'config_key', 'config_val' )
            ->get();

            $config = [];
            foreach($rs as $row){
                $config[$row->config_key] = $row->config_val;
            }

        $data = [
            'page_title' => 'ตั้งค่า',
            'config' => (object) $config
        ];
        return view( 'settings/form', $data );
    }
}
