<?php
namespace App\Http\Controllers;

use crocodicstudio\crudbooster\controllers\CBController;
use DB;

class AdminAgreementController extends CBController
{

    public function cbInit()
    {
        $this->setTable( "tpa_agreement" );
        $this->setPermalink( "agreement" );
        $this->setPageTitle( "Agreement" );

        $this->addText( "Agreement Type", "agreement_type" )->strLimit( 150 )->maxLength( 255 );
        $this->addWysiwyg( "Agreement Th", "agreement_th" )->strLimit( 150 );
        $this->addWysiwyg( "Agreement En", "agreement_en" )->strLimit( 150 );
        $this->addText( "Agreement Status", "agreement_status" )->required( false )->strLimit( 150 )->maxLength( 255 );
        $this->addDatetime( "Updated At", "updated_at" )->required( false )->showAdd( false )->showEdit( false );
        $this->addText( "Updated By", "updated_by" )->required( false )->showIndex( false )->showDetail( false )->showAdd( false )->showEdit( false )->strLimit( 150 )->maxLength( 255 );

        if ( request()->segment( 3 ) == 'edit' ) {
            $this->javascript( function () {
                return "$(function () {
                $('select.force').select2({
                    minimumResultsForSearch: Infinity
                });

                var toolbar = [
                        ['style', ['bold', 'italic', 'underline', 'clear']],
                        ['fontname', ['fontname']],
                        ['fontsize', ['fontsize']],
                        ['color', ['color']],
                        ['para', ['ul', 'ol', 'paragraph']],
                        ['view', ['codeview']],
                        ['height', ['height']]
                ];

                var agreement_th = $('textarea#agreement_th');
                var agreement_en = $('textarea#agreement_en');

                 var summernoteValidator = $('#WebForm').validate({
                    errorElement: 'span',
                    errorClass: 'error',
                    validClass: 'is-valid',
                    ignore: ':hidden:not(.summernote),.note-editable.card-block',
                    errorPlacement: function (error, element) {
                        // Add the `help-block` class to the error element
                        error.addClass('invalid-feedback');
                        console.log(element.attr('class'));
                        if (element.prop('type') === 'checkbox') {
                            error.insertAfter(element.siblings('label'));
                        } else if (element.hasClass('summernote')) {
                            error.insertAfter(element.siblings('.note-editor'));
                        } else if(element.hasClass('select2-hidden-accessible')) {
                            error . insertAfter( element . siblings( '.select2-container' ) );

                        } else {
                            error.insertAfter(element);
                        }
                    },
                    rules: {
                        agreement_th: 'required',
                        agreement_en: 'required'
                    },
                    submitHandler: function(form) {
                        $.ajax({
                            url: '" . url( 'admin/agreement/save-edit' ) . "',
                            type: 'POST',
                            data: new FormData(form),
                            cache: false,
                            processData: false,
                            contentType: false
                        }).done(function(response){
                            console.log(response);
                            if(response.status == 'ok'){

                                swal('บันทึกข้อมูลเรียบร้อยแล้ว', {
                                    buttons: {
                                        catch: {
                                        text: 'OK',
                                        value: 'ok',
                                        }
                                    },
                                    })
                                    .then((value) => {
                                    window.location.href='" . url( 'admin/agreement' ) . "';
                                });
                            }
                        });
                    }
                });

                agreement_th.summernote({
                    placeholder: 'เนื้อหาภาษาไทย',
                    height: ($(window).height() - 300),
                    toolbar: toolbar,
                    callbacks: {
                        onChange: function (contents) {
                            agreement_th.val(agreement_th.summernote('isEmpty') ? '' : contents);
                            summernoteValidator.element(agreement_th);
                        }
                    }
                });

                agreement_en.summernote({
                    placeholder: 'เนื้อหาภาษาอังกฤษ',
                    height: ($(window).height() - 300),
                    toolbar: toolbar,
                    callbacks: {
                        onChange: function (contents) {
                            agreement_en.val(agreement_en.summernote('isEmpty') ? '' : contents);
                            summernoteValidator.element(agreement_en);
                        }
                    }
                });

            });";
            } );
        }

    }

    public function getIndex()
    {
        $rs = DB::table( 'tpa_agreement' )->select( 'agreement_id as id', 'agreement_type', 'agreement_force', 'updated_at' )->where( 'agreement_status', 1 )->orderby( 'agreement_type', 'desc' )->get();
        $record = [];
        $no = 1;
        foreach ( $rs as $row ) {
            $row->no = $no;
            $row->updated_text = ( $row->agreement_force == 1 ) ? 'บังคับ' : 'ไม่บังคับ';
            $row->title = ( $row->agreement_type == 1 ) ? 'ข้อกำหนดและเงื่อนไขในการใช้งาน TPA Care' : 'ข้อตกลงและนโยบายส่วนบุคคล';
            $row->updated_at = date( 'd/m/Y H:i:s', strtotime( $row->updated_at ) );
            array_push( $record, $row );
            $no++;
        }

        $data = [];
        $data['page_title'] = 'ข้อตกลงและข้อกำหนด';
        $data['record'] = $record;
        return view( 'agreement/browse', $data );
    }

    public function getEdit( $id )
    {
        $rs = DB::table( 'tpa_agreement' )->where( 'agreement_id', $id )->first();
        $rs->title = ( $rs->agreement_type == 1 ) ? 'ข้อกำหนดและเงื่อนไขในการใช้งาน TPA Care' : 'ข้อตกลงและนโยบายส่วนบุคคล';

        $data = [];
        $data['page_title'] = 'ข้อตกลงและข้อกำหนด';
        $data['row'] = $rs;
        return view( 'agreement/edit', $data );

    }
}
