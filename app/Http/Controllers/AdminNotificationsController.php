<?php

namespace App\Http\Controllers;

use crocodicstudio\crudbooster\controllers\CBController;
use DB;

class AdminNotificationsController extends CBController
{

    private $gender = [
        [
            'id'   => 'all',
            'name' => 'ทุกเพศ',
        ],
        [
            'id'   => 'male',
            'name' => 'ชาย',
        ],
        [
            'id'   => 'female',
            'name' => 'หญิง',
        ],
    ];

    private $category = [
        'normal'   => 'การแจ้งเตือนทั่วไป',
        'promotion' => 'โปรโมชั่น',
        'article' => 'บทความ',
        'claim' => 'สถานะเคลม',
    ];

    public function cbInit()
    {
        $this->setTable( "tpa_promotions" );
        $this->setPermalink( "promotions" );
        $this->setPageTitle( "Promotions" );

        /*$this->addText( "Name Th", "name_th" )->strLimit( 150 )->maxLength( 255 );
    $this->addWysiwyg( "Detail Th", "detail_th" )->strLimit( 150 );
    $this->addText( "Name En", "name_en" )->strLimit( 150 )->maxLength( 255 );
    $this->addWysiwyg( "Detail En", "detail_en" )->strLimit( 150 );
    $this->addDate( "Start Date", "start_date" );
    $this->addDate( "End Date", "end_date" );
    $this->addSelectTable( "Hospital", "hospital_id", ["table" => "tpa_hospital", "value_option" => "id", "display_option" => "name_th", "sql_condition" => ""] );
    $this->addText( "Status", "status" )->strLimit( 150 )->maxLength( 255 );
    $this->addDatetime( "Created At", "created_at" )->required( false )->showIndex( false )->showAdd( false )->showEdit( false );
    $this->addDatetime( "Updated At", "updated_at" )->required( false )->showIndex( false )->showAdd( false )->showEdit( false );*/

    }

    public function getIndex()
    {
        $start_date = ( request()->query( 'start_date' ) ) ? request()->query( 'start_date' ) : '';
        $end_date = ( request()->query( 'end_date' ) ) ? request()->query( 'end_date' ) : '';
        $insurance_name = ( request()->query( 'insurance_name' ) ) ? request()->query( 'insurance_name' ) : '';

        $data = [];
        $data['page_title'] = 'การแจ้งเตือน';
        $query = DB::table( 'tpa_notifications as n' )
            ->leftjoin( 'tpa_age_range as a', 'a.id', '=', 'n.age_range' )
            ->select( 'n.id', 'n.title_th as title', 'n.type', 'n.target_criteria', 'n.age_range', 'n.gender', 'n.created_at', 'a.title_th as age', 'n.category' );

        $rs = $query->orderby( 'n.id', 'desc' )
            ->paginate( 20 );
        $record = [];
        $no = 1;
        foreach ( $rs as $row ) {
            $row->no = $no;
            //$row->status_text = ( $row->status == 1 ) ? 'แสดง' : 'ซ่อน';
            if ( $row->target_criteria == 'gender' ) {
                $row->target_value = $this->gender[$row->gender]['name'];
            } else {
                $row->target_value = $row->age;
            }

            $row->target_criteria = ( $row->target_criteria == 'gender' ) ? 'เพศ' : 'ช่วงอายุ';
            $row->category = $this->category[$row->category];
            $row->created_at = date( 'd/m/Y H:i:s', strtotime( $row->created_at ) );
            array_push( $record, $row );
            $no++;

        }

        $data['record'] = $record;
        $data['result'] = $rs;
        $data['start_date'] = $start_date;
        $data['end_date'] = $end_date;
        $data['insurance_name'] = $insurance_name;

        return view( 'notifications/browse', $data );
    }

    public function getAdd()
    {
        $age_range = DB::table( 'tpa_age_range' )->select( 'id', 'title_th AS name' )->get();
        $gender = [
            0 => 'ทุกเพศ',
            1 => 'ชาย',
            2 => 'หญิง',
        ];

        $data = [
            'page_title' => 'การแจ้งเตือน',
            'age_range'  => $age_range,
            'gender'     => $gender,
        ];
        return view( 'notifications/form', $data );
    }

    public function getDetail( $id )
    {
        $row = DB::table( 'tpa_notifications as n' )
            ->leftjoin( 'tpa_age_range as a', 'a.id', '=', 'n.age_range' )
            ->select( 'n.*', 'a.title_th as age' )
            ->where( 'n.id', $id )->first();
        if ( $row->target_criteria == 'gender' ) {
            $row->target_value = $this->gender[$row->gender]['name'];
        } else {
            $row->target_value = $row->age;
        }

        $row->target_criteria = ( $row->target_criteria == 'gender' ) ? 'เพศ' : 'ช่วงอายุ';
        $row->notification_type = ( $row->type == 'all' ) ? 'ทั้งหมด' : 'ระบุ';

        $row->created_at = date( 'd/m/Y H:i:s', strtotime( $row->created_at ) );
        $data = (array) $row;
        $data['page_title'] = 'การแจ้งเตือน';
        return view( 'notifications/detail', $data );

    }

}
