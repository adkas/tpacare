<?php namespace App\Http\Controllers;

use crocodicstudio\crudbooster\controllers\CBController;
use DB;
class AdminTpaPartnerController extends CBController {


    public function cbInit()
    {
        $this->setTable("tpa_partner");
        $this->setPermalink("tpa_partner");
        $this->setPageTitle("Tpa Partner");

		$this->javascript( function () {
            return "
            $(function () {

				$('#form').validate({
					rules: {
						ins_code: 'required',
						name_th: 'required'
					},
					submitHandler: function(form) {
						$.ajax({
							url: '" . url( 'admin/tpa_partner/save-' . request()->segment( 3 ) ) . "',
							type: 'POST',
							data: new FormData(form),
							cache: false,
							processData: false,
							contentType: false
						}).done(function(response){
							console.log(response);
							if(response.status == 'ok'){

								swal('บันทึกข้อมูลเรียบร้อยแล้ว', {
									buttons: {
										catch: {
											text: 'OK',
											value: 'ok',
										}
									},
									})
									.then((value) => {
									window.location.href='" . url( 'admin/tpa_partner' ) . "';
								});
							}else if(response.status == 'error'){
								swal(response.message, {
									buttons: {
										catch: {
											text: 'OK',
											value: 'ok',
											icon: 'warning',
										}
									},
									})
									.then((value) => {  });
							}
						}).fail(function(response) {
							console.log( response );
						});
					}
				});
				
				$(document).on('click', '.delete', function () {
					var code = $(this).data('id');
					swal({
							title: 'ยืนยันการลบข้อมูล',
							text: '',
							icon: 'warning',
							buttons: [
								'ยกเลิก',
								'ตกลง'
							],
							dangerMode: true,
							})
							.then((willDelete) => {
								if (willDelete) {

									$.ajax({
										url: '".url('admin/tpa_partner/delete')."',
										type: 'POST',
										data: {code: code}
									}).done(function (response) {
										console.log(response)
										if (response.status == 'ok') {

												swal('ลบข้อมูลเรียบร้อยแล้ว', {
													icon: 'success',
													buttons: {
														catch: {
														text: 'ตกลง',
														value: 'ok',
														}
													},
													})
													.then((value) => {
													window.location.href='".url('admin/tpa_partner')."';
												});
										}
									}).fail(function(response) {
												console.log( response );
											});
								}
							});
				});
				
				$(document).on('click', '.reset-secret', function () {
					var ins_code = $('#ins_code').val();
					swal({
						title: 'ยืนยันรีเซ็ท secret key ',
						text: '',
						icon: 'warning',
						buttons: [
							'ยกเลิก',
							'ตกลง'
						],
						dangerMode: true,
						})
						.then((resetSecret) => {
							if (resetSecret) {

								$.ajax({
									url: '".url('admin/tpa_partner/reset-secret')."',
									type: 'POST',
									data: {ins_code: ins_code}
								}).done(function (response) {
									console.log(response)
									if (response.status == 'ok') {
										swal('reset secret key เรียบร้อย', {
											icon: 'success',
											buttons: {
												catch: {
												text: 'ตกลง',
												value: 'ok',
												}
											},
											})
											.then((value) => {
											location.reload();;
										});
									}
								}).fail(function(response) {
											console.log( response );
										});
							}
						});
				});
				
			});";
		} );

    }
	
	public function getDetail( $id )
    {
        $partner = (array) DB::table( 'tpa_partner as pn' )
            ->select( '*' )
            ->where( 'pn.ins_code', $id )->first();
        return view( 'partner/detail', $partner );

    }
	
	public function getIndex()
    {
        $name_th = ( request()->query( 'name' ) ) ? request()->query( 'name' ) : '';
        $name_en = ( request()->query( 'name' ) ) ? request()->query( 'name' ) : '';
        $ins_code = ( request()->query( 'ins_code' ) ) ? request()->query( 'ins_code' ) : '';

        $data = [];
        $data['page_title'] = 'Partner';
        $query = DB::table( 'tpa_partner' )
            ->select( 'ins_code', 'name_th', 'name_en' );
        $ins_code = ( request()->query( 'ins_code' ) ) ? request()->query( 'ins_code' ) : '';
        $search_name_th = ( request()->query( 'name_th' ) ) ? request()->query( 'name_th' ) : '';
        $search_name_en = ( request()->query( 'name_en' ) ) ? request()->query( 'name_en' ) : '';
        if ( !empty( $ins_code ) ) {
            $query->where( 'ins_code', 'like', '%' . $ins_code . '%' );
        }

        if ( !empty( $search_name_th ) ) {
            $query->where( 'name_th', 'like', '%' . $search_name_th . '%' );
        }

        if ( !empty( $search_name_en ) ) {
            $query->where( 'name_en', 'like', '%' . $search_name_en . '%' );
        }

        $rs = $query->orderby( 'created_at', 'desc' )->paginate( 20 );

        $data['result'] = $rs;
		$data['name_th'] = $search_name_th;
		$data['name_en'] = $search_name_en;
		$data['ins_code'] = $ins_code;

        return view( 'partner/browse', $data );
    }
	
	public function getAdd()
    {
        $data = [
            'page_title' => "TPA Partner"
        ];

        return view( 'partner/create', $data );

    }
	
	public function getEdit( $code )
    {
        $row = DB::table( 'tpa_partner' )->where( 'ins_code', $code )->first();

        $data = [
            'page_title' => 'Partner',
            'partner'   => $row
        ];

        return view( 'partner/update', $data );

    }
}
