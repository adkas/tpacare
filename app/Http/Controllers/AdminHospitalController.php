<?php

namespace App\Http\Controllers;

use crocodicstudio\crudbooster\controllers\CBController;
use DB;

class AdminHospitalController extends CBController
{

    public function cbInit()
    {
        $this->setTable( "tpa_hospital" );
        $this->setPermalink( "hospital" );
        $this->setPageTitle( "Hospital" );

        /*$this->addText("Name", "name_th")->placeholder("Name")->strLimit(150)->maxLength(255);
        $this->addText("Branch", "branch")->placeholder("Branch")->strLimit(250)->maxLength(250);
        $this->addText("Telephone", "telephone")->placeholder("Telephone")->help("Ex. 089112345,0882224444")->strLimit(150)->maxLength(255);
        $this->addText("Url", "url")->placeholder("Url")->required(false)->strLimit(150)->maxLength(255);
        $this->addTextArea("Address", "address")->placeholder("Name")->strLimit(150);
        $this->addSelectOption("Type", "type")->options([1 => 'ครอบคลุมทั้งหมด', 2 => 'ครอบคลุมบางรายการ', 3 => 'อื่นๆ']);
        $this->addText("Coordinate", "coordinates")->strLimit(150)->maxLength(255);
        $this->addDatetime("Created At", "created_at")->required(false)->showAdd(false)->showEdit(false);
        $this->addDatetime("Updated At", "updated_at")->required(false)->showAdd(false)->showEdit(false);*/

        $this->style( function () {
            return "
				table#table-module td:first-child {
					width: 150px;
					text-align: center;
				}

				table#table-module th {
					text-align: center;
					font-weight: normal;
				}

				table#table-module td:last-child {
					width: 100px;
					text-align: center;
				}

				#media-list li img {
					width: 180px;
					/* height: 100px */
					height: auto;
				}

				#media-list li {
					min-height: 182px;
					height: auto;
				}


				div#hint_brand .modal-dialog {
					top: 110px;
					width: 567px;
					max-width: 100%
				}

				li.myupload span {
					position: relative;
					width: 180px;
					min-height: 180px;
					height: auto;
					display: block;
					background: #fff
				}

				li.myupload span input {
					opacity: 0;
					position: absolute;
					top: 0;
					bottom: 0;
					left: 0;
					right: 0
				}

				li.myupload span i {
					position: absolute;
					top: 50%;
					left: 50%;
					transform: translate(-50%, -50%);
					color: #ccc;
					font-size: 54px
				}

				#media-list li {
					float: left;
					border: 1px solid #ccc;
					background: #ccc;
					position: relative;
					margin: 0 5px 5px 0;
					width: 182px
				}

				#media-list li:last-child {
					margin-right: 0
				}

				.post-thumb {
					position: absolute;
					background: rgba(0, 0, 0, 0.4);
					left: 0;
					top: 0;
					right: 0;
					bottom: 0;
					display: none;
				}

				#media-list li:hover .post-thumb {
					display: block
				}

				a.remove-pic {
					position: absolute;
					top: 5px;
					right: 5px;
					font-size: 12px;
					color: #fff;
					border: 1px solid #fff;
					border-radius: 50%;
					display: block;
					height: 25px;
					width: 25px;
					text-align: center;
					padding: 3px 0;
					z-index: 9999;
				}

				a.view-pic {
					position: absolute;
					top: 5px;
					right: 35px;

					font-size: 12px;
					color: #fff;
					border: 1px solid #fff;
					border-radius: 50%;
					display: block;
					height: 25px;
					width: 25px;
					text-align: center;
					padding: 3px 0;
				}

				a.close {
					top: 0px;
					right: 35px;

					font-size: 12px;
					font-weight: bold;
					color: #333;
					border: 2px solid #333;
					border-radius: 50%;
					display: block;
					height: 25px;
					width: 25px;
					text-align: center;
					padding: 5px 0;
					margin-bottom: 10px;
				}

				.inner-post-thumb {
					position: relative
				}

				.user-post-text-wrap {
					position: relative
				}

				.user-pic-post {
					position: absolute;
					width: 50px;
					height: 50px;
					top: 0;
					left: 0
				}

				.user-pic-post img {
					width: 100%
				}

				.user-txt-post {
					padding: 0 0 0 65px
				}

				textarea.form-control.upostTextarea {
					border: 0;
					box-shadow: none;
					height: 85px;
					resize: none
				}

				.user-post-text-wrap {
					border-bottom: 1px solid #ccc;
					margin: 0 0 15px
				}

				.user-post-btn-wrap {
					margin: 25px 0 0
				}

				ul.btn-nav {
					list-style: none;
					padding: 0;
					margin: 0
				}

				ul.btn-nav li {
					position: relative;
					float: left;
					margin: 0 10px 0 0
				}

				ul.btn-nav li span input {
					position: absolute;
					left: 0;
					top: 0;
					height: 100%;
					width: 100%;
					z-index: 9;
					opacity: 0;
					filter: alpha(opacity=100)
				}

				ul#media-list {
					list-style: none;
					padding: 0;
					margin: 0
				}

				";
        } );

        $this->javascript( function () {
            return "
            $(function () {
	var names = [];
	var remove_images = [];
	$(document).on('click', '.picupload', function (event) {
		if($('ul#media-list li').length > 3)
		{
			return false;
		}
	});
    $(document).on('change', '.picupload', function (event) {
        var files = event.target.files;
		var file_index = parseInt($('input[name=\'file_index\']').val());


			for (var i = 0; i < files.length; i++) {
				var file = files[i];
				names.push($(this).get(0).files[i].name);
				if (file.type.match('image')) {

					var picReader = new FileReader();
					picReader.fileName = file.name
					picReader.addEventListener('load', function (event) {
						var picFile = event.target;
						var div = document.createElement('li');
						div.innerHTML = '<img src=\'' + picFile.result + '\'' +
							'title=\'' + file.name + '\' /><div class=\'post-thumb\'><div class=\'inner-post-thumb\'><a href=\'javascript:void(0);\' data-id=\'' + event.target.fileName + '\' class=\'remove-pic\' data-index=\'' + file_index + '\'><i class=\'fa fa-times\' aria-hidden=\'true\'></i></a><a href=\'javascript:void(0);\' data-id=\'' + event.target.fileName + '\' class=\'view-pic\' data-index=\'' + file_index + '\'><i class=\'fa fa-search\' aria-hidden=\'true\'></i></a><div></div>';

						$(div).appendTo($('#media-list'));
						file_index++;
						$('input[name=\'file_index\']').val(file_index);
						$('li.myupload').append('<span id=\'row-' + file_index + '\'><i class=\'fa fa-plus\' aria-hidden=\'true\'></i><input name=\'files[]\' type=\'file\' click-type=\'type2\' class=\'picupload\' accept=\'image/*\'></span>');
						$('#media-list li:last-child').after($('li.myupload'));


					});
				}
				picReader.readAsDataURL(file);
			}

			$(this).parent().hide();



    });

    $('body').on('click', '.remove-pic', function () {
        $(this).parent().parent().parent().remove();
        var removeItem = $(this).data('id');
        var index = $(this).data('index');
        var yet = names.indexOf(removeItem);
		remove_images.push(removeItem);
        $('#row-' + index).remove();

        if (yet != -1) {
            names.splice(yet, 1);
		}
		$('input[name=\'remove_images\']').val(remove_images.join(','));
		console.log(remove_images.join(','));
    });

    $(document).on('click', '.view-pic', function () {
        $('.imagepreview').attr('src', $(this).prev().parents().parents().prev().attr('src'));
        $('#imagemodal').modal('show');
	});

	$('input.service-time, input.telephone').tagsinput({
			tagClass: 'label label-primary'
		});

	$(document).on('click', '.btn-clone-service', function () {
		var index = parseInt($(this).data('index'));
		console.log(index);

        $.get('" . url( 'admin/hospital/row' ) . "/' + index, function(html){
			$(html).insertAfter($('div.services-time').last());
			$('input.service-time').tagsinput({
				tagClass: 'label label-primary'
			});
			index++;
			console.log(index);
			$('a.btn-clone-service').data('index', index);
		});
	});

	$(document).on('click', '.delete', function () {
		var code = $(this).data('id');
		swal({
                        title: 'ยืนยันการลบข้อมูล',
                        text: '',
                        icon: 'warning',
                        buttons: [
                            'ยกเลิก',
                            'ตกลง'
                        ],
                        dangerMode: true,
                        })
                        .then((willDelete) => {
                            if (willDelete) {

                                $.ajax({
                                    url: '".url('admin/hospital/delete')."',
                                    type: 'POST',
                                    data: {code: code}
                                }).done(function (response) {
                                    console.log(response)
                                    if (response.status == 'ok') {

                                            swal('ลบข้อมูลเรียบร้อยแล้ว', {
                                                icon: 'success',
                                                buttons: {
                                                    catch: {
                                                    text: 'ตกลง',
                                                    value: 'ok',
                                                    }
                                                },
                                                })
                                                .then((value) => {
                                                window.location.href='".url('admin/hospital')."';
                                            });
                                    }
                                }).fail(function(response) {
                                            console.log( response );
                                        });
                            }
                        });
	});

	$(document).on('click', '.btn-delete-service', function () {
		var row = $(this).data('row');
		console.log(row);
		$('div[data-index=\'' + row + '\']').remove();
	});

	$('#form').validate({
                        errorElement: 'span',
                        errorClass: 'error',
                        validClass: 'is-valid',
                        errorPlacement: function (error, element) {
                            // Add the `help-block` class to the error element
                            error.addClass('invalid-feedback');
                            console.log(element.attr('class'));
                            if (element.prop('type') === 'checkbox') {
                                error.insertAfter(element.siblings('label'));
                            } else if (element.hasClass('summernote')) {
                                error.insertAfter(element.siblings('.note-editor'));
                            } else if(element.hasClass('select2-hidden-accessible')) {
                                error . insertAfter( element . siblings( '.select2-container' ) );

                            } else {
                                error.insertAfter(element);
                            }
                        },
                        rules: {
                            name_th: 'required',
                            name_en: 'required'
                        },
                        submitHandler: function(form) {
                            $.ajax({
                                url: '" . url( 'admin/hospital/save-' . request()->segment( 3 ) ) . "',
                                type: 'POST',
                                data: new FormData(form),
                                cache: false,
                                processData: false,
                                contentType: false
                            }).done(function(response){
								console.log(response);
                                if(response.status == 'ok'){

                                    swal('บันทึกข้อมูลเรียบร้อยแล้ว', {
                                        buttons: {
                                            catch: {
                                            text: 'OK',
                                            value: 'ok',
                                            }
                                        },
                                        })
                                        .then((value) => {
                                        window.location.href='" . url( 'admin/hospital' ) . "';
                                    });
                                }
                            }).fail(function(response) {
                                console.log( response );
                            });
                        }
                    });

});


            ";
        } );
    }

    public function getIndex()
    {
        $search_code = ( request()->query( 'search_code' ) ) ? request()->query( 'search_code' ) : '';
        $search_name_th = ( request()->query( 'search_name_th' ) ) ? request()->query( 'search_name_th' ) : '';
        $search_name_en = ( request()->query( 'search_name_en' ) ) ? request()->query( 'search_name_en' ) : '';

        $data = [];
        $data['page_title'] = 'Hospital';
        $query = DB::table( 'tpa_hospital' )
            ->select( 'code', 'name_th', 'name_en', 'name_th as status' );

        if ( !empty( $search_code ) ) {
            $query->where( 'code', 'like', '%' . $search_code . '%' );
        }

        if ( !empty( $search_name_th ) ) {
            $query->where( 'name_th', 'like', '%' . $search_name_th . '%' );
        }

        if ( !empty( $search_name_en ) ) {
            $query->where( 'name_en', 'like', '%' . $search_name_en . '%' );
        }

        $rs = $query->orderby( 'name_th', 'asc' )
            ->paginate( 30 );

        $record = [];
        foreach ( $rs as $p => $row ) {
            $row->no = $p + 1;
            array_push( $record, $row );
        }
        $data['result'] = $rs;
        $data['record'] = $record;
        $data['search_code'] = $search_code;
        $data['search_name_th'] = $search_name_th;
        $data['search_name_en'] = $search_name_en;

        return view( 'hospital/browse', $data );
    }

    public function getAdd()
    {
        $data = [
            'page_title' => 'Hospital',
        ];

        return view( 'hospital/create', $data );
    }

    public function getEdit( $code )
    {
        $row = DB::table( 'tpa_hospital' )->where( 'code', $code )->first();
        $types = [
            (object) [
                'id'   => 1,
                'name' => 'ครอบคลุมทั้งหมด',
            ],
            (object) [
                'id'   => 2,
                'name' => 'ครอบคลุมบางรายการ',
            ],
            (object) [
                'id'   => 3,
                'name' => 'อื่นๆ',
            ],
        ];

        $size = count( $types );
        for ( $i = 0; $i < $size; $i++ ) {
            $types[$i]->selected = ( $types[$i]->id == $row->type ) ? 'selected' : '';
        }

        $rs_pictures = DB::table( 'tpa_hospital_pictures' )->select( 'id', 'path' )->where( 'hospital_code', $code )->get();
        $pictures = [];
        $index = 1;
        foreach ( $rs_pictures as $r ) {
            $r->index = $index;
            $r->url = url( $r->path );
            array_push( $pictures, $r );
            $index++;
        }

		$row->pictures = $pictures;

        $data = [
            'page_title' => 'Hospital',
            'hospital'   => $row,
			'types'      => $types,
			'file_index' => count( $row->pictures ) + 1
        ];

        return view( 'hospital/update', $data );

    }
}
