<?php

namespace App\Http\Controllers;

use crocodicstudio\crudbooster\controllers\CBController;
use DB;

class AdminUsersController extends CBController
{

    public function cbInit()
    {
        $this->setTable( "users" );
        $this->setPermalink( "users" );
        $this->setPageTitle( "Users" );

        $this->addText( "Name", "name" )->strLimit( 150 )->maxLength( 255 );
        $this->addEmail( "Email", "email" );
        $this->addDatetime( "Email Verified At", "email_verified_at" )->required( false )->showIndex( false )->showDetail( false )->showAdd( false )->showEdit( false );
        $this->addPassword( "Password", "password" )->showIndex( false );
        $this->addText( "Access Token", "access_token" )->required( false )->showIndex( false )->showDetail( false )->showAdd( false )->showEdit( false )->strLimit( 150 )->maxLength( 255 );
        $this->addText( "Remember Token", "remember_token" )->required( false )->showIndex( false )->showDetail( false )->showAdd( false )->showEdit( false )->strLimit( 150 )->maxLength( 255 );
        $this->addDatetime( "Created At", "created_at" )->required( false )->showIndex( false )->showAdd( false )->showEdit( false );
        $this->addDatetime( "Updated At", "updated_at" )->required( false )->showIndex( false )->showAdd( false )->showEdit( false );
        //$this->addImage( "Photo", "photo" )->required( false )->encrypt( true );
        $this->addSelectTable( "Role", "cb_roles_id", ["table" => "cb_roles", "value_option" => "id", "display_option" => "name", "sql_condition" => ""] );

        $this->addText( "Ip Address", "ip_address" )->required( false )->showIndex( false )->showAdd( false )->showEdit( false )->strLimit( 150 )->maxLength( 255 );
        $this->addText( "User Agent", "user_agent" )->required( false )->showIndex( false )->showAdd( false )->showEdit( false )->strLimit( 150 )->maxLength( 255 );
        $this->addDatetime( "Login At", "login_at" )->required( false )->showIndex( false )->showAdd( false )->showEdit( false );

        $this->style( function () {
            return "
				table#table-module td:first-child {
					width: 80px;
					text-align: center;
				}

				table#table-module th {
					text-align: center;
					font-weight: normal;
				}

				table#table-module td:nth-child(4) {
					width: 120px;
					text-align: left;
				}

				table#table-module td:last-child {
					width: 100px;
					text-align: center;
				}

				";
        } );

        switch ( request()->segment( 3 ) ) {
            case "edit":
                $this->javascript( function () {
                    return "

                    $(function () {
                        $('.btn-default').attr('href', '" . url( 'admin/users' ) . "');
                    });

                                    ";

                } );
                break;
            default:
                $this->javascript( function () {
                    return "

                    $(function () {
                        $(document).on('click', '.btn-delete', function () {
                                var id = $(this).data('id');
                                swal({
                                    title: 'กรุณายืนยันการลบข้อมูลผู้ใช้ระบบ',
                                    text: '',
                                    icon: 'warning',
                                    dangerMode: true,
                                    buttons: ['ยกเลิก','ยืนยัน']
                                })
                                    .then((willDelete) => {
                                        if (willDelete) {
                                             $.ajax({
                                                url: '" . url( 'admin/users/delete' ) . "/' + id,
                                                type: 'GET'
                                            }).done(function(response){
                                                console.log(response);
                                                swal('ลบข้อมูลผู้ใช้ระบบเรียบร้อยแล้ว', {
                                                    icon: 'success'
                                                }).then(() => {
                                                    location . reload();
                                                });
                                            });

                                        }
                                    });

                            });

                            $(document).on('click', '.btn-default', function () {
                                $(location).href('" . url( 'admin/users' ) . "')
                            });
                    });

                                    ";
                } );

                break;

        }
    }

    public function getIndex()
    {
        $name = ( request()->query( 'name' ) ) ? request()->query( 'name' ) : '';
        $email = ( request()->query( 'email' ) ) ? request()->query( 'email' ) : '';
        $role = ( request()->query( 'role' ) ) ? request()->query( 'role' ) : 0;

        $data = [];
        $data['page_title'] = 'Users';
        $query = DB::table( 'users as u' )
            ->leftJoin( 'cb_roles as r', 'u.cb_roles_id', '=', 'r.id' )
            ->select( 'u.id', 'u.name', 'u.email', 'u.photo', 'r.name AS role' )
            ->where( 'channel', 1 );

        if ( !empty( $name ) ) {
            $query->where( 'u.name', 'like', '%' . $name . '%' );
        }

        if ( !empty( $email ) ) {
            $query->where( 'u.email', 'like', '%' . $email . '%' );
        }

        if ( !empty( $role ) ) {
            $query->where( 'u.cb_roles_id', $role );
        } else {
            $query->where( 'u.cb_roles_id', '!=', 0 );
        }

        $rs = $query->orderby( 'u.id', 'desc' )
            ->paginate( 20 );

        $record = [];
        foreach ( $rs as $p => $row ) {
            $row->no = $p + 1;
            array_push( $record, $row );
        }

        $rr = DB::table( 'cb_roles' )->get();
        $roles = [];
        foreach ( $rr as $row ) {
            $row->selected = ( $row->id == $role ) ? 'selected' : '';
            array_push( $roles, $row );
        }

        $data['name'] = $name;
        $data['email'] = $email;
        $data['role'] = $role;
        $data['roles'] = $roles;
        $data['result'] = $rs;
        $data['record'] = $record;

        return view( 'users/browse', $data );
    }
}
