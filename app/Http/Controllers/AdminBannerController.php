<?php
namespace App\Http\Controllers;

use crocodicstudio\crudbooster\controllers\CBController;
use DB;

class AdminBannerController extends CBController
{
    private $banner_type = [
        "promotion" => "โปรโมชั่น",
        "article"   => "บทความ",
        "other"     => "อื่นๆ",
    ];

    public function cbInit()
    {
        $this->setTable( "tpa_banner" );
        $this->setPermalink( "banner" );
        $this->setPageTitle( "Banner" );

        $this->addText( "Banner", "banner_id" )->showDetail( false )->showAdd( false )->showEdit( false )->strLimit( 150 )->maxLength( 255 );
        $this->addText( "Title Th", "banner_title_th" )->strLimit( 150 )->maxLength( 255 );
        $this->addText( "Title En", "banner_title_en" )->strLimit( 150 )->maxLength( 255 );
        $this->addImage( "Banner Picture", "banner_picture" )->encrypt( true );
        $this->addText( "Banner Link", "banner_link" )->strLimit( 150 )->maxLength( 255 );

        $this->addText( "Ref", "ref_id" )->strLimit( 150 )->maxLength( 255 );
        //$this->addText( "Banner Status", "banner_status" )->strLimit( 150 )->maxLength( 255 );
        $this->addSelectOption( "Status", "banner_status", [
            1 => "แสดง",
            0 => "ซ่อน",
        ] );

        $this->addDatetime( "Updated At", "updated_at" )->required( false )->showAdd( false )->showEdit( false );

        $this->javascript( function () {
            return "

                $(function () {
                    $(document).on('change', 'input#picture', function (event) {
                        readURL( this );
                    });

                    $('select.reference').select2({templateSelection: function (data, container) {
                        $(data.element).attr('title_en', data.title_en);
                        return data.text;
                    }});

                    $(document).on('change', 'select.type', function () {
                        var type = $(this).val();
                        if(type == 'other'){
                            $('div.ref-input').hide();
                            $('input.banner-name').prop( 'disabled', false );
                        }else{

                            var text = 'บทความด้านสุขภาพ';
                            if(type == 'promotion'){
                                var text = 'โปรโมชั่นด้านสุขภาพ';
                            }
                            $('div.ref-input label').text(text);
                            $('div.ref-input').show();
                            $('input.banner-name').val( '' );
                            $('input.banner-name').prop( 'disabled', true );
                            
                            if(type == 'promotion'){
                                var temp= jQuery.parseJSON($('input.promotions').val());
                            }else{
                                var temp= jQuery.parseJSON($('input.articles').val());

                            }
                            $('select.reference').val('').trigger('change') ;
                                var \$select = $('select.reference');                        
                                \$select.find('option').remove();                          
                                $.each(temp, function(key, value) {              
                                    $('<option>').val(value.id).text(value.name).data('title_en',value.title_en).appendTo(\$select);     
                                });
                        }
                    });

                    $(document).on('change', 'select.reference', function (e) {
                        console.log( $('select.reference').find(':selected').data('title_en') );
                        $('input[name=title_th]').val( $('select.reference').find(':selected').text() );
                        $('input[name=title_en]').val( $('select.reference').find(':selected').data('title_en') );
                    });


                    

                    $('select.type,select.status').select2({
                        minimumResultsForSearch: Infinity
                    });

                    $('#WebForm').validate({
                        errorElement: 'span',
                        errorClass: 'error',
                        validClass: 'is-valid',
                        ignore: ':hidden:not(.summernote),.note-editable.card-block',
                        errorPlacement: function (error, element) {
                            // Add the `help-block` class to the error element
                            error.addClass('invalid-feedback');
                            console.log(element.attr('class'));
                            if (element.prop('type') === 'checkbox') {
                                error.insertAfter(element.siblings('label'));
                            } else if (element.hasClass('summernote')) {
                                error.insertAfter(element.siblings('.note-editor'));
                            } else if(element.hasClass('select2-hidden-accessible')) {
                                error . insertAfter( element . siblings( '.select2-container' ) );

                            } else {
                                error.insertAfter(element);
                            }
                        },
                        rules: {
                            title_th: 'required',
                            title_en: 'required'
                        },
                        submitHandler: function(form) {
                            $('input.banner-name').prop( 'disabled', false );
                            $.ajax({
                                url: '" . url( 'admin/banner/save-' . request()->segment( 3 ) ) . "',
                                type: 'POST',
                                data: new FormData(form),
                                cache: false,
                                processData: false,
                                contentType: false
                            }).done(function(response){
                                if(response.status == 'ok'){

                                    swal('บันทึกข้อมูลเรียบร้อยแล้ว', {
                                        buttons: {
                                            catch: {
                                            text: 'OK',
                                            value: 'ok',
                                            }
                                        },
                                        })
                                        .then((value) => {
                                        window.location.href='" . url( 'admin/banner' ) . "';
                                    });
                                }
                            }).fail(function(response) {
                                console.log( response );
                            });
                        }
                    });
                });


                function readURL(input) {
                    if (input.files && input.files[0]) {
                        var reader = new FileReader();

                        reader.onload = function(e) {
                        $('#preview-picture').attr('src', e.target.result);
                        }

                        reader.readAsDataURL(input.files[0]);
                    }
                }
            ";
        } );

    }

    public function getIndex()
    {
        $rs = DB::table( 'tpa_banner' )
            ->select( 'banner_id as id', 'banner_title_th as title', 'banner_type as type', 'banner_link as link', 'banner_picture as picture', 'ref_id', 'banner_status as status' )
            ->orderby( 'banner_id', 'asc' )->get();

        $record = [];
        foreach ( $rs as $row ) {
            /*if (  ( $row->id == 1 ) || ( $row->id == 2 ) ) {
                $row->status_text = ( $row->status == 1 ) ? 'แสดง' : 'ซ่อน';
                
            } else {
                switch ( $row->type ) {
                    case "promotion":
                        $item = DB::table( 'tpa_promotions' )->select( 'title_th as title' )->where( 'id', $row->ref_id )->first();
                        $row->title = $item->title;
                        break;

                    case "article":
                        $item = DB::table( 'tpa_article' )->select( 'title_th as title' )->where( 'id', $row->ref_id )->first();
                        $row->title = $item->title;
                        break;
                }
                $row->status_text = ($row->status == 1) ? 'แสดง' : 'ซ่อน';
                array_push( $record, $row );

            }*/
            $row->status_text = ( $row->status == 1 ) ? 'แสดง' : 'ซ่อน';

            array_push( $record, $row );

        }

        $data = [];
        $data['record'] = $record;
        $data['page_title'] = 'แบนเนอร์หน้า Home';
        return view( 'banner/browse', $data );

    }

    public function getEdit( $id )
    {
        $row = DB::table( 'tpa_banner' )->where( 'banner_id', $id )->first();

        $rs = DB::table( 'tpa_article' )->select( 'id', 'title_th AS name', 'title_en' )->where( 'status', 1 )
            ->where( 'deleted_at', null )->get();
        $article = [];

        $picture = '';
        $picture = $row->banner_picture;

        foreach ( $rs as $r ) {
            // $img = DB::table( 'tpa_pictures' )->select( 'path' )->where( 'picture_type', 2 )->where( 'ref_id', $r->id )->first();
            // $r->picture = url( $img->path );

            $r->selected = (  ( $r->id == $row->ref_id ) && ( $row->banner_type == 'article' ) ) ? 'selected' : '';

            if ( $r->selected == 'selected' ) {
                //$picture = $r->picture;
                $row->banner_title_th = $r->name;
                $row->banner_title_en = $r->title_en;
            }

            array_push( $article, $r );
        }

        $rs = DB::table( 'tpa_promotions' )
            ->select( 'id', 'title_th AS name', 'title_en' )
            ->where( 'status', 1 )
            ->where( 'deleted_at', null )
            ->get();
        $promotions = [];
        foreach ( $rs as $r ) {
            // $img = DB::table( 'tpa_pictures' )->select( 'path' )->where( 'picture_type', 1 )->where( 'ref_id', $r->id )->first();
            // $r->picture = url( $img->path );
            $r->selected = (  ( $r->id == $row->ref_id ) && ( $row->banner_type == 'promotion' ) ) ? 'selected' : '';
            if ( $r->selected == 'selected' ) {
                //$picture = $r->picture;
                $row->banner_title_th = $r->name;
                $row->banner_title_en = $r->title_en;
            }
            array_push( $promotions, $r );
        }

        $banner_type = [];
        $references = [];
        foreach ( $this->banner_type as $key => $val ) {
            $ref_text = '';
            if ( in_array( $id, [1, 2] ) ) {
                if ( $key == $row->banner_type ) {
                    array_push( $banner_type, (object) [
                        'id'       => $key,
                        'name'     => $val,
                        'selected' => 'selected',
                    ] );

                }
                $is_ref = 'none';
                $is_other = 'block';

            } else {
                array_push( $banner_type, (object) [
                    'id'       => $key,
                    'name'     => $val,
                    'selected' => ( $key == $row->banner_type ) ? 'selected' : '',
                ] );
                $is_ref = '';
                if ( $row->banner_type == 'promotion' ) {
                    $ref_text = 'โปรโมชั่นด้านสุขภาพ';
                    $references = $promotions;
                } else if ( $row->banner_type == 'article' ) {
                    $ref_text = 'บทความด้านสุขภาพ';
                    $references = $article;

                }
                $is_ref = 'block';
                $is_other = 'block';

            }

        }
        $row->banner_picture = !empty( $picture ) ? url( $picture ) : url( $row->banner_picture );

        $statuses = [
            (object) [
                'id' => 1,
                'name' => 'แสดง',
                'selected' => ($row->banner_status == 1) ? 'selected' : ''
            ],
            (object) [
                'id' => 0,
                'name' => 'ซ่อน',
                'selected' => ($row->banner_status == 0) ? 'selected' : ''
            ]
        ];

        $data = [];
        $data['page_title'] = 'แบนเนอร์หน้า Home';
        $data['article'] = $article;
        $data['promotions'] = $promotions;
        $data['references'] = $references;
        $data['picture'] = $picture;
        $data['banner_type'] = $banner_type;
        $data['banner'] = $row;
        $data['ref_text'] = $ref_text;
        $data['is_ref'] = $is_ref;
        $data['is_other'] = $is_other;
        $data['statuses'] = $statuses;
        return view( 'banner/edit', $data );

    }
}
