<?php

namespace App\Http\Controllers;

use crocodicstudio\crudbooster\controllers\CBController;
use DB;

class AdminInsurerController extends CBController
{
    private $page_title = 'บริษัทประกันภัย';
    public function cbInit()
    {
        $this->setTable( "tpa_insurer" );
        $this->setPermalink( "insurer" );
        $this->setPageTitle( $this->page_title );

        /*$this->addText("Name", "name_th")->placeholder("Name")->strLimit(150)->maxLength(255);
        $this->addText("Branch", "branch")->placeholder("Branch")->strLimit(250)->maxLength(250);
        $this->addText("Telephone", "telephone")->placeholder("Telephone")->help("Ex. 089112345,0882224444")->strLimit(150)->maxLength(255);
        $this->addText("Url", "url")->placeholder("Url")->required(false)->strLimit(150)->maxLength(255);
        $this->addTextArea("Address", "address")->placeholder("Name")->strLimit(150);
        $this->addSelectOption("Type", "type")->options([1 => 'ครอบคลุมทั้งหมด', 2 => 'ครอบคลุมบางรายการ', 3 => 'อื่นๆ']);
        $this->addText("Coordinate", "coordinates")->strLimit(150)->maxLength(255);
        $this->addDatetime("Created At", "created_at")->required(false)->showAdd(false)->showEdit(false);
        $this->addDatetime("Updated At", "updated_at")->required(false)->showAdd(false)->showEdit(false);*/

        $this->style( function () {
            return "

				#media-list li img {
					width: 180px;
					/* height: 100px */
					height: auto;
				}

				#media-list li {
					min-height: 182px;
					height: auto;
				}


				div#hint_brand .modal-dialog {
					top: 110px;
					width: 567px;
					max-width: 100%
				}

				li.myupload span {
					position: relative;
					width: 180px;
					min-height: 180px;
					height: auto;
					display: block;
					background: #fff
				}

				li.myupload span input {
					opacity: 0;
					position: absolute;
					top: 0;
					bottom: 0;
					left: 0;
					right: 0
				}

				li.myupload span i {
					position: absolute;
					top: 50%;
					left: 50%;
					transform: translate(-50%, -50%);
					color: #ccc;
					font-size: 54px
				}

				#media-list li {
					float: left;
					border: 1px solid #ccc;
					background: #ccc;
					position: relative;
					margin: 0 5px 5px 0;
					width: 182px
				}

				#media-list li:last-child {
					margin-right: 0
				}

				.post-thumb {
					position: absolute;
					background: rgba(0, 0, 0, 0.4);
					left: 0;
					top: 0;
					right: 0;
					bottom: 0;
					display: none;
				}

				#media-list li:hover .post-thumb {
					display: block
				}

				a.remove-pic {
					position: absolute;
					top: 5px;
					right: 5px;
					font-size: 12px;
					color: #fff;
					border: 1px solid #fff;
					border-radius: 50%;
					display: block;
					height: 25px;
					width: 25px;
					text-align: center;
					padding: 3px 0;
					z-index: 9999;
				}

				a.view-pic {
					position: absolute;
					top: 5px;
					right: 35px;

					font-size: 12px;
					color: #fff;
					border: 1px solid #fff;
					border-radius: 50%;
					display: block;
					height: 25px;
					width: 25px;
					text-align: center;
					padding: 3px 0;
				}

				a.close {
					top: 0px;
					right: 35px;

					font-size: 12px;
					font-weight: bold;
					color: #333;
					border: 2px solid #333;
					border-radius: 50%;
					display: block;
					height: 25px;
					width: 25px;
					text-align: center;
					padding: 5px 0;
					margin-bottom: 10px;
				}

				.inner-post-thumb {
					position: relative
				}

				.user-post-text-wrap {
					position: relative
				}

				.user-pic-post {
					position: absolute;
					width: 50px;
					height: 50px;
					top: 0;
					left: 0
				}

				.user-pic-post img {
					width: 100%
				}

				.user-txt-post {
					padding: 0 0 0 65px
				}

				textarea.form-control.upostTextarea {
					border: 0;
					box-shadow: none;
					height: 85px;
					resize: none
				}

				.user-post-text-wrap {
					border-bottom: 1px solid #ccc;
					margin: 0 0 15px
				}

				.user-post-btn-wrap {
					margin: 25px 0 0
				}

				ul.btn-nav {
					list-style: none;
					padding: 0;
					margin: 0
				}

				ul.btn-nav li {
					position: relative;
					float: left;
					margin: 0 10px 0 0
				}

				ul.btn-nav li span input {
					position: absolute;
					left: 0;
					top: 0;
					height: 100%;
					width: 100%;
					z-index: 9;
					opacity: 0;
					filter: alpha(opacity=100)
				}

				ul#media-list {
					list-style: none;
					padding: 0;
					margin: 0
				}

				";
        } );

        switch ( request()->segment( 3 ) ) {
            case "edit":
                $this->javascript( function () {
                    return "
                        $(function () {
                            console.log('xxxxxxx');

                        });
                        ";

                } );

                break;
        }

    }

    public function getIndex()
    {
        $data = [];
        $data['page_title'] = $this->page_title;
        $query = DB::table( 'tpa_insurer' )
            ->select( 'insurer_code as code', 'name_th', 'name_en' );
        $search_code = ( request()->query( 'search_code' ) ) ? request()->query( 'search_code' ) : '';
        $search_name_th = ( request()->query( 'search_name_th' ) ) ? request()->query( 'search_name_th' ) : '';
        $search_name_en = ( request()->query( 'search_name_en' ) ) ? request()->query( 'search_name_en' ) : '';
        if ( !empty( $search_code ) ) {
            $query->where( 'insurer_code', 'like', '%' . $search_code . '%' );
        }

        if ( !empty( $search_name_th ) ) {
            $query->where( 'name_th', 'like', '%' . $search_name_th . '%' );
        }

        if ( !empty( $search_name_en ) ) {
            $query->where( 'name_en', 'like', '%' . $search_name_en . '%' );
        }

        $rs = $query->orderby( 'name_th', 'asc' )
            ->paginate( 30 );
        $record = [];
        foreach ( $rs as $p => $row ) {
            $row->no = $p + 1;
            array_push( $record, $row );
        }
        $data['result'] = $rs;
        $data['record'] = $record;
        $data['search_code'] = $search_code;
        $data['search_name_th'] = $search_name_th;
        $data['search_name_en'] = $search_name_en;

        return view( 'insurer/browse', $data );
    }

    public function getAdd()
    {
        $rs_type = DB::table( 'tpa_insurer_type' )
            ->select( 'id', 'name_th as name' )
            ->get();

        $insurer_type = [];
        foreach ( $rs_type as $r ) {
            $r->selected = '';
            array_push( $insurer_type, $r );
        }

        $data = [
            'page_title'   => $this->page_title,
            'insurer_type' => $insurer_type,
        ];

        return view( 'insurer/create', $data );
    }

    public function getEdit( $code )
    {
        $rs_type = DB::table( 'tpa_insurer_type' )
            ->select( 'id', 'name_th as name' )
            ->get();
        $row = DB::table( 'tpa_insurer' )->where( 'insurer_code', $code )->first();

        $insurer_type = [];
        foreach ( $rs_type as $r ) {
            $r->selected = ( $r->id == $row->insurer_type ) ? 'selected' : '';
            array_push( $insurer_type, $r );
        }
        $data = [
            'page_title'   => $this->page_title,
            'insurer'      => $row,
            'insurer_type' => $insurer_type,
        ];

        return view( 'insurer/update', $data );

    }
}
