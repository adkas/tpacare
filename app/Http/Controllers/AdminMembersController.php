<?php

namespace App\Http\Controllers;

use Carbon\Carbon;
use crocodicstudio\crudbooster\controllers\CBController;
use DB;

class AdminMembersController extends CBController
{

    public function cbInit()
    {
        $this->setTable( "users" );
        $this->setPermalink( "members" );
        $this->setPageTitle( "Members" );

        $this->addText( "Name", "name" )->strLimit( 150 )->maxLength( 255 );
        $this->addText( "Surname", "surname" )->strLimit( 150 )->maxLength( 255 );
        //$this->addEmail( "Email", "email" );
        //$this->addDatetime( "Email Verified At", "email_verified_at" );
        $this->addText( "Mobile", "mobile" )->strLimit( 150 )->maxLength( 255 );
        $this->addText( "Channel", "channel" )->strLimit( 150 )->maxLength( 255 );
        $this->addText( "Username", "username" )->strLimit( 150 )->maxLength( 255 );
        //$this->addPassword( "Password", "password" )->showIndex( false )->showDetail( false )->showEdit( false );
        //$this->addText( "Pin", "pin" )->strLimit( 150 )->maxLength( 255 );
        //$this->addText( "Access Token", "access_token" )->strLimit( 150 )->maxLength( 255 );
        //$this->addText( "Remember Token", "remember_token" )->strLimit( 150 )->maxLength( 255 );
        //$this->addImage( "Photo", "photo" )->encrypt( true );
        //$this->addSelectTable( "Cb Role", "cb_roles_id", ["table" => "cb_roles", "value_option" => "id", "display_option" => "name", "sql_condition" => ""] );
        $this->addText( "Status", "status" )->strLimit( 150 )->maxLength( 255 );
        $this->addText( "Ip Address", "ip_address" )->strLimit( 150 )->maxLength( 255 );
        $this->addText( "User Agent", "user_agent" )->strLimit( 150 )->maxLength( 255 );
        $this->addDatetime( "Created At", "created_at" )->required( false )->showAdd( false )->showEdit( false );
        $this->addDatetime( "Updated At", "updated_at" )->required( false )->showAdd( false )->showEdit( false );
        $this->addDatetime( "Login At", "login_at" );
        $this->style( function () {
            return "
				table#table-module td:first-child {
					width: 60px;
					text-align: center;
				}

				table#table-module th {
					text-align: center;
					font-weight: normal;
				}

				table#table-module td:nth-child(4) {
					width: 150px;
					text-align: center;
                }

                table#table-module td:nth-child(7) {
					width: 160px;
					text-align: center;
				}

				table#table-module td:last-child {
					width: 100px;
					text-align: center;
				}

				";
        } );

        $this->javascript( function () {
            return "
                $(function () {
                    $('#WebForm').validate({
                        errorElement: 'span',
                        errorClass: 'error',
                        validClass: 'is-valid',
                        rules: {
                            name: 'required',
                            surname: 'required'
                        },
                        submitHandler: function (form) {
                            $.ajax({
                                url: '" . url('admin/members/save-edit'). "',
                                type: 'POST',
                                data: new FormData(form),
                                cache: false,
                                processData: false,
                                contentType: false
                            }).done(function (response) {
                                console.log(response)
                                if (response.status == 'ok') {

                                    swal('บันทึกข้อมูลเรียบร้อยแล้ว', {
                                            buttons: {
                                                catch: {
                                                    text: 'OK',
                                                    value: 'ok',
                                                }
                                            },
                                        })
                                        .then((value) => {
                                        window.location.href = '" . url('admin/members'). "';
                                        });
                                }
                            }).fail(function(response) {
                                        console.log( response );
                                    });
                        }
                    })
                });
            ";
        } );
    }

    public function getIndex()
    {
        $name = ( request()->query( 'name' ) ) ? request()->query( 'name' ) : '';
        $surname = ( request()->query( 'surname' ) ) ? request()->query( 'surname' ) : '';
        $username = ( request()->query( 'username' ) ) ? request()->query( 'username' ) : '';
        $mobile = ( request()->query( 'mobile' ) ) ? request()->query( 'mobile' ) : '';
        $citizen_id = ( request()->query( 'citizen_id' ) ) ? request()->query( 'citizen_id' ) : '';

        $data = [];
        $data['page_title'] = 'Members';
        $query = DB::table( 'users as u' )
            ->leftJoin( 'cb_roles as r', 'u.cb_roles_id', '=', 'r.id' )
            ->select( 'u.id', 'u.name', 'u.surname', 'u.mobile', 'u.citizen_id', 'u.username', 'u.created_at', 'u.status' )
            ->where( 'channel', 2 );
        if ( !empty( $name ) ) {
            $query->where( 'u.name', 'like', '%' . $name . '%' );
        }

        if ( !empty( $surname ) ) {
            $query->where( 'u.surname', 'like', '%' . $surname . '%' );
        }

        if ( !empty( $username ) ) {
            $query->where( 'u.username', 'like', '%' . $username . '%' );
        }

        if ( !empty( $mobile ) ) {
            $query->where( 'u.mobile', 'like', '%' . $mobile . '%' );
        }

        if ( !empty( $citizen_id ) ) {
            $query->where( 'u.citizen_id', 'like', '%' . $citizen_id . '%' );
        }

        $rs = $query->orderby( 'u.id', 'desc' )
            ->paginate( 20 );
        $record = [];
        foreach ( $rs as $p => $row ) {
            $row->no = $p + 1;
            $row->name = $row->name . '  ' . $row->surname;
            $row->status_text = ( $row->status == 1 ) ? 'Active' : 'Inactive';
            $row->created_at = Carbon::parse( $row->created_at )->format( 'd/m/Y H:i:s' );
            array_push( $record, $row );
        }
        $data['result'] = $rs;
        $data['record'] = $record;
        $data['name'] = $name;
        $data['surname'] = $surname;
        $data['username'] = $username;
        $data['mobile'] = $mobile;
        $data['citizen_id'] = $citizen_id;

        return view( 'members/browse', $data );
    }

    public function getEdit( $id )
    {
        $data = (array) DB::table( 'users' )->where( 'id', $id )->first();
        return view( 'members/update', $data );
    }

    public function getDetail( $id )
    {
        $data = (array) DB::table( 'users' )->where( 'id', $id )->first();
        $data['status'] = ($data['status'] == 1) ? 'Active' : 'Inactive';
        $data['is_new_pass'] = ($data['is_new_pass'] == 1) ? 'true' : 'false';
        return view( 'members/detail', $data );
    }
}
