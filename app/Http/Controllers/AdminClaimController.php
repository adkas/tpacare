<?php
namespace App\Http\Controllers;

use crocodicstudio\crudbooster\controllers\CBController;
use DB;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;

class AdminClaimController extends CBController
{

    public function cbInit()
    {
        $this->setTable( "tpa_claim" );
        $this->setPermalink( "claim" );
        $this->setPageTitle( "Claim" );

        $this->addText( "Claim No", "claim_no" )->required( false )->showIndex( false )->showDetail( false )->showAdd( false )->showEdit( false )->strLimit( 150 )->maxLength( 255 );
        $this->addText( "Uid", "uid" )->showAdd( false )->strLimit( 150 )->maxLength( 255 );
        $this->addText( "Policy No", "policy_no" )->strLimit( 150 )->maxLength( 255 );
        $this->addText( "Staff No", "staff_no" )->strLimit( 150 )->maxLength( 255 );
        $this->addEmail( "Email", "email" );
        $this->addText( "Admited Type", "admited_type" )->strLimit( 150 )->maxLength( 255 );
        //$this->addText("Benefit Type","benefit_type")->strLimit(150)->maxLength(255);
        $this->addText( "Symptom", "symptom" )->strLimit( 150 )->maxLength( 255 );
        $this->addText( "Provider Code", "provider_code" )->strLimit( 150 )->maxLength( 255 );
        $this->addText( "Medical Expense", "medical_expense" )->strLimit( 150 )->maxLength( 255 );
        $this->addText( "Claim Status", "claim_status" )->strLimit( 150 )->maxLength( 255 );
        $this->addDate( "Accident Date", "accident_date" );
        $this->addDate( "Admited Date", "admited_date" );
        $this->addDatetime( "Created At", "created_at" )->required( false )->showAdd( false )->showEdit( false );
        $this->addDate( "Updated By", "updated_by" );
        $this->addDatetime( "Updated At", "updated_at" )->required( false )->showAdd( false )->showEdit( false );

        $this->style( function () {
            return "
                table#table-module td{
					text-align: center;
				}

				table#table-module th {
					text-align: center;
					font-weight: normal;
                }

                table#table-module td:nth-child(2) {
                    width: 170px;
					text-align: left;
                }

                table#table-module td:nth-child(3) {
					text-align: left;
                }

                table#table-module td:nth-child(4) {
                    width: 80px;
					text-align: center;
                }

                table#table-module td:nth-child(5) {
                    width: 200px;
					text-align: left;
                }

                table#table-module td:nth-child(6) {
					text-align: right;
                }
            ";
        } );

        $this->javascript( function () {
            return "$(function () {
                $('.datepicker').datetimepicker({
					format:'Y-m-d',
					onShow:function( ct ){
					this.setOptions({
						minDate:jQuery('#date_timepicker_start').val()?jQuery('#date_timepicker_start').val():false
					})
					},
					timepicker:false
                });

                $(document).on('click', 'button#btn-export', function (e) {
                     e.preventDefault();
                    $('#SearchForm').attr('action', '" . url( 'admin/claim/export?' ) . "');
                    console.log($('#SearchForm').attr('action'));
                    $( '#submit' ).trigger( 'click');
                    $('#SearchForm').attr('action', '?');
                    console.log($('#SearchForm').attr('action'));
                });
            });";
        } );

    }

    public function getIndex()
    {
        $start_date = ( request()->query( 'start_date' ) ) ? request()->query( 'start_date' ) : '';
        $end_date = ( request()->query( 'end_date' ) ) ? request()->query( 'end_date' ) : '';
        $claim_start_date = ( request()->query( 'claim_start_date' ) ) ? request()->query( 'claim_start_date' ) : '';
        $claim_end_date = ( request()->query( 'claim_end_date' ) ) ? request()->query( 'claim_end_date' ) : '';

        $insurer_name = ( request()->query( 'insurer_name' ) ) ? request()->query( 'insurer_name' ) : '';
        $insurance_name = ( request()->query( 'insurance_name' ) ) ? request()->query( 'insurance_name' ) : '';
        $staff_no = ( request()->query( 'staff_no' ) ) ? request()->query( 'staff_no' ) : '';
        $admited_type = ( request()->query( 'admited_type' ) ) ? request()->query( 'admited_type' ) : '';

        $data = [];
        $data['page_title'] = 'ข้อมูลการเคลม';
        $query = DB::table( 'tpa_claim as c' )
            ->leftjoin( 'tpa_hospital as h', 'h.code', '=', 'c.provider_code' )
            ->select( 'claim_id as id', 'c.name', 'c.surname', 'c.policy_no', 'c.insurer_name_th as insurer_name', 'c.provider_code', 'h.name_th as provider_name', 'c.admited_date', 'c.accident_date', 'c.created_at', 'c.medical_expense' );
        if ( !empty( $start_date ) && !empty( $end_date ) ) {
            $query->whereBetween( 'c.admited_date', [$start_date, $end_date] );
        }

        if ( !empty( $claim_start_date ) && !empty( $claim_end_date ) ) {
            $query->whereBetween( 'c.created_at', [$claim_start_date . ' 00:00:00', $claim_end_date . ' 23:59:59'] );
        }

        if ( !empty( $staff_no ) ) {
            $query->where( 'c.staff_no', 'like', '%' . $staff_no . '%' );
        }

        if ( !empty( $admited_type ) ) {
            $query->where( DB::raw( 'LOWER(c.admited_type)' ), 'like', '%' . strtolower( $admited_type ) . '%' );
        }

        if ( !empty( $insurer_name ) ) {
            $query->where( 'c.insurer_name_th', 'like', '%' . $insurer_name . '%' )->orWhere( 'c.insurer_name_th', 'like', '%' . $insurer_name . '%' );

        }

        if ( !empty( $insurance_name ) ) {
            $query->where( 'c.name', 'like', '%' . $insurance_name . '%' )->orWhere( 'c.surname', 'like', '%' . $insurance_name . '%' );
        }

        $rs = $query->orderby( 'claim_id', 'desc' )
            ->paginate( 20 );
        $record = [];
        $no = 1;
        foreach ( $rs as $row ) {
            $row->no = $no;
            $row->name .= ' ' . $row->surname;
            $row->medical_expense = number_format( $row->medical_expense, 2, '.', ',' );
            //$row->status_text = ( $row->status == 1 ) ? 'แสดง' : 'ซ่อน';
            $row->created_at = date( 'd/m/Y', strtotime( $row->created_at ) );
            $row->admited_date = date( 'd/m/Y', strtotime( $row->admited_date ) );
            unset( $row->surname );
            array_push( $record, $row );
            $no++;

        }
        //alert($record); die;
        $data['record'] = $record;
        $data['result'] = $rs;
        $data['start_date'] = $start_date;
        $data['end_date'] = $end_date;
        $data['claim_start_date'] = $claim_start_date;
        $data['claim_end_date'] = $claim_end_date;
        $data['insurer_name'] = $insurer_name;
        $data['insurance_name'] = $insurance_name;
        $data['staff_no'] = $staff_no;
        $data['admited_type'] = $admited_type;

        return view( 'claim/browse', $data );
    }

    public function getDetail( $id )
    {
        $claim = (array) DB::table( 'tpa_claim as c' )
            ->leftjoin( 'tpa_hospital as h', 'h.code', '=', 'c.provider_code' )
            ->leftjoin( 'tpa_cardtype as t', 't.card_type', '=', 'c.card_type' )
            ->select( 'claim_id as id', 'c.name', 'c.surname', 'c.mobile', 'c.staff_no', 'c.email', 'c.policy_no', 'c.insurer_name_th as insurer_name', 't.name_th as card_type', 'c.claim_type', 'c.admited_type', 'c.provider_code', 'h.name_th as provider_name', 'c.symptom', 'c.medical_expense', 'c.admited_date', 'c.accident_date', 'c.created_at' )
            ->where( 'c.claim_id', $id )->first();
        $claim['medical_expense'] = number_format( $claim['medical_expense'], 2, ".", "," );
        return view( 'claim/detail', $claim );

    }

    public function export()
    {
        $start_date = ( request()->query( 'start_date' ) ) ? request()->query( 'start_date' ) : '';
        $end_date = ( request()->query( 'end_date' ) ) ? request()->query( 'end_date' ) : '';
        $claim_start_date = ( request()->query( 'claim_start_date' ) ) ? request()->query( 'claim_start_date' ) : '';
        $claim_end_date = ( request()->query( 'claim_end_date' ) ) ? request()->query( 'claim_end_date' ) : '';

        $insurer_name = ( request()->query( 'insurer_name' ) ) ? request()->query( 'insurer_name' ) : '';
        $insurance_name = ( request()->query( 'insurance_name' ) ) ? request()->query( 'insurance_name' ) : '';
        $staff_no = ( request()->query( 'staff_no' ) ) ? request()->query( 'staff_no' ) : '';

        $admited_type = ( request()->query( 'admited_type' ) ) ? request()->query( 'admited_type' ) : '';

        $spreadsheet = new Spreadsheet();
        $sheet = $spreadsheet->getActiveSheet();
        $sheet->getColumnDimension( 'A' )->setWidth( 25 );
        $sheet->getColumnDimension( 'B' )->setWidth( 25 );
        $sheet->getColumnDimension( 'C' )->setWidth( 25 );
        $sheet->getColumnDimension( 'D' )->setWidth( 25 );
        $sheet->getColumnDimension( 'E' )->setWidth( 25 );
        $sheet->getColumnDimension( 'F' )->setWidth( 50 );
        $sheet->getColumnDimension( 'G' )->setWidth( 25 );
        $sheet->getColumnDimension( 'H' )->setWidth( 25 );
        $sheet->getColumnDimension( 'I' )->setWidth( 25 );
        $sheet->getColumnDimension( 'J' )->setWidth( 25 );
        $sheet->getColumnDimension( 'K' )->setWidth( 25 );
        $sheet->getColumnDimension( 'L' )->setWidth( 50 );
        $sheet->getColumnDimension( 'M' )->setWidth( 25 );
        $sheet->getColumnDimension( 'N' )->setWidth( 25 );
        $sheet
            ->setCellValue( 'A1', 'รหัสพนักงาน' )
            ->setCellValue( 'B1', 'ชื่อผู้เข้ารับการรักษา' )
            ->setCellValue( 'C1', 'หมายเลขโทรศัพท์มือถือ' )
            ->setCellValue( 'D1', 'อีเมล์' )
            ->setCellValue( 'E1', 'หมายเลขกรมธรรม์' )
            ->setCellValue( 'F1', 'บริษัทประกัน' )
            ->setCellValue( 'G1', 'ประเภทของการเข้ารับรักษา' )
            ->setCellValue( 'H1', 'ประเภทผลประโยชน์' )
            ->setCellValue( 'I1', 'ประเภทความคุ้มครอง' )
            ->setCellValue( 'J1', 'วันที่เข้ารับรักษา' )
            ->setCellValue( 'K1', 'โรค/อาการ/สาเหตุ' )
            ->setCellValue( 'L1', 'สถานที่เข้ารับการรักษา' )
            ->setCellValue( 'M1', 'จำนวนเงินตามใบเสร็จรับเงิน' )
            ->setCellValue( 'N1', 'วันที่ทำรายการ' );

        $query = DB::table( 'tpa_claim as c' )
            ->leftjoin( 'tpa_hospital as h', 'h.code', '=', 'c.provider_code' )
            ->leftjoin( 'tpa_cardtype as t', 't.card_type', '=', 'c.card_type' )
            ->select( 'claim_id as id', 'c.name', 'c.surname', 'c.mobile', 'c.staff_no', 'c.email', 'c.policy_no', 'c.insurer_name_th as insurer_name', 't.name_th as card_type', 'c.claim_type', 'c.admited_type', 'c.provider_code', 'h.name_th as provider_name', 'c.symptom', 'c.medical_expense', 'c.admited_date', 'c.accident_date', 'c.created_at' );
        if ( !empty( $start_date ) && !empty( $end_date ) ) {
            $query->whereBetween( 'c.admited_date', [$start_date, $end_date] );
        }

        if ( !empty( $claim_start_date ) && !empty( $claim_end_date ) ) {
            $query->whereBetween( 'c.created_at', [$claim_start_date . ' 00:00:00', $claim_end_date . ' 23:59:59'] );
        }

        if ( !empty( $staff_no ) ) {
            $query->where( 'c.staff_no', 'like', '%' . $staff_no . '%' );
        }

        if ( !empty( $insurer_name ) ) {
            $query->where( 'c.insurer_name_th', 'like', '%' . $insurer_name . '%' )->orWhere( 'c.insurer_name_th', 'like', '%' . $insurer_name . '%' );

        }

        if ( !empty( $admited_type ) ) {
            $query->where( DB::raw( 'LOWER(c.admited_type)' ), 'like', '%' . strtolower( $admited_type ) . '%' );
        }

        if ( !empty( $insurance_name ) ) {
            $query->where( 'c.name', 'like', '%' . $insurance_name . '%' )->orWhere( 'c.surname', 'like', '%' . $insurance_name . '%' );
        }

        $rs = $query->orderby( 'claim_id', 'desc' )->get();
        $n = 2;
        foreach ( $rs as $row ) {
            //$sheet->setCellValue( 'A' . $n, $row->staff_no );
            $sheet->setCellValueExplicit(
                'A' . $n,
                $row->staff_no,
                \PhpOffice\PhpSpreadsheet\Cell\DataType::TYPE_STRING
            );
            $sheet->setCellValue( 'B' . $n, $row->name . ' ' . $row->surname );
            $sheet->setCellValue( 'C' . $n, $row->mobile );
            $sheet->setCellValue( 'D' . $n, $row->email );
            $sheet->setCellValueExplicit(
                'E' . $n,
                $row->policy_no,
                \PhpOffice\PhpSpreadsheet\Cell\DataType::TYPE_STRING
            );

            $sheet->setCellValue( 'F' . $n, $row->insurer_name );
            $sheet->setCellValue( 'G' . $n, $row->claim_type );
            $sheet->setCellValue( 'H' . $n, $row->admited_type );
            $sheet->setCellValue( 'I' . $n, $row->card_type );
            $sheet->setCellValue( 'J' . $n, $row->admited_date );
            $sheet->setCellValue( 'K' . $n, $row->symptom );
            $sheet->setCellValue( 'L' . $n, $row->provider_name );
            $sheet->setCellValue( 'M' . $n, number_format( $row->medical_expense, 2, ".", "," ) );
            $sheet->setCellValue( 'N' . $n, $row->created_at );
            $n++;
        }

        $writer = new Xlsx( $spreadsheet );
        $path = 'storage/files/claim-' . date( 'Ymd_His' ) . '.xlsx';
        $writer->save( $path );
        return redirect( $path );

    }
}
