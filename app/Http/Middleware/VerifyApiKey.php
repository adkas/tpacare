<?php

namespace App\Http\Middleware;
use Closure;
use Exception;

class VerifyApiKey
{
    public function handle($request, Closure $next, $guard = null)
    {
        $token = $request->header('x-api-key');

        if (!$token) {
            // Unauthorized response if token not there
            return response()->json([
                'status' => 'error',
                'message' => 'Token not provided.',
            ], 401);
        }else if($token != 'ykRNP0wziQLCoC64NLpl'){
            return response()->json([
                'status' => 'error',
                'message' => 'Invalid API Key.',
            ], 401);
        }



        /*try {
            $credentials = JWT::decode($token, env('JWT_SECRET'), ['HS256']);
        }  catch (Exception $e) {
            return response()->json([
                'error' => 'An error while decoding token.',
            ], 400);
        }
        $user = User::find($credentials->sub);
        
        $request->auth = $user;*/
        return $next($request);
    }
}
