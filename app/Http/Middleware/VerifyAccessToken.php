<?php

namespace App\Http\Middleware;

use Closure;
use \Firebase\JWT\JWT;
use \Firebase\JWT\ExpiredException;
use \Firebase\JWT\SignatureInvalidException;
use Illuminate\Support\Facades\DB;
class VerifyAccessToken
{
    public function handle($request, Closure $next, $guard = null)
    {
        $token = $request->bearerToken();

        if (!$token) {
            // Unauthorized response if token not there
            return response()->json([
                'status' => 'error',
                'message' => 'Token not provided.',
            ], 401);
        }

        try {
            $access_data = JWT::decode($token, env('COLLECTION'), array('HS256'));
            if (!empty($access_data->uid)) {
                $count = DB::table('users')->select('id')->where('id', $access_data->uid)->where('status', 1)->count();
                if($count > 0){
                    $request->token = $access_data;
                    return $next($request);
                }else{
                    return response()->json([
                        'status' => 'error',
                        'message' => 'Token not found !'
                    ], 401);
                }
                
            } else {
                return response()->json([
                    'status' => 'error',
                    'message' => 'Invalid token !'
                ], 401);
            }
        } catch (ExpiredException $e) {
            return response()->json([
                'status' => 'error',
                'message' => $e->getMessage()
            ], 401);
        } catch (SignatureInvalidException $e) {
            return response()->json([
                'status' => 'error',
                'message' => $e->getMessage()
            ], 401);
        }
    }
}
