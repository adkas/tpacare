<?php

namespace App\Http\Middleware;

use App;
use Closure;
use Exception;

class SetLang
{
    public function handle($request, Closure $next, $guard = null)
    {
        $lang = ($request->input('lang')) ? strtolower($request->input('lang')) : 'th';
        if (!in_array($lang, ['en', 'th'])) {
            $lang = 'th';
        }
        App::setLocale($lang);

        return $next($request);
    }
}
