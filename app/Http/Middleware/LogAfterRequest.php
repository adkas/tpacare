<?php

namespace App\Http\Middleware;

use Illuminate\Support\Facades\Log;

final class LogAfterRequest
{


    public function handle($request, \Closure $next)
    {
        return $next($request);
    }
    public function terminate($request, $response)
    {
        $r = new \App\Models\Request();
        $r->ip = $request->ip();
        $r->path = $request->path();
        $r->header = json_encode($request->header());
        $r->request = json_encode($request->all());
        $r->response = $response;
        $r->save();
    }
}
