-- ----------------------------
-- Records of users
-- ----------------------------
INSERT INTO users (name, surname, email,email_verified_at, mobile,username,password,pin,access_token,remember_token,cb_roles_id,status,created_date) VALUES ('Sakda Srijumpa', '', 'sakda@gmail.com', NULL, '', '12345', '$2y$10$rKf8aBT9jdWlF5KyqVYZXeHZu1igIdR9Ki0RY1C6jAx8dgW1E7Are', NULL, 'tpacare2020', NULL, 1, 1, '2020-05-10 11:49:37');
INSERT INTO `users` VALUES (2, 'Sakda', 'Srijumpa', 'sakda', NULL, '0861124455', 'sakda', '$2y$10$KSUOvzQ0Ys3seJbz00eiEuczlW2v5CmX0POIHnQyHnLGZX8.CMy4S', 'e10adc3949ba59abbe56e057f20f883e', 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ1aWQiOjIsInVkaWQiOjEsImlhdCI6MTU4ODYxNzU0MiwiZXhwIjoxNTg4NzAzOTQyfQ.9JW2R6AHVyYMAt8RVy_E1C4Rcli3tRQSIHklZBzEWHM', '', 2, NULL, 0, 1, '2020-04-30 04:51:02', '2020-05-05 08:59:08', '2020-05-05 01:39:02');
INSERT INTO `users` VALUES (3, 'Sakda', 'Srijumpa', 'ddddd', NULL, '0891114455', 'ddddd', '$2y$10$SahbIkq7xgba1epeAVoEl.kvAJaR4QpK2YwS/Al9teUO8d1p3oJjS', NULL, '', 'eyJpdiI6IkVBOEVZMkJzaVBTblNYUkhlbE5xaXc9PSIsInZhbHVlIjoidkxtRWpxdHBhbkgyd3J2b29uakRXZz09IiwibWFjIjoiMjg3NmVjNjg2M2UxYmUwZDIxYmIzNTA1YmFlNzBkNTliMTc5MzBkZjE1YzgyYzdkYjdkMzhiOGE0OGE1Yjc1YyJ9', 2, NULL, 0, 0, '2020-05-05 08:44:59', NULL, NULL);

SET IDENTITY_INSERT users ON;

INSERT INTO users (name, surname, email,email_verified_at, mobile,username,password,pin,access_token,remember_token,cb_roles_id,status,created_date) VALUES ('Sakda Srijumpa', '', 'sakda@gmail.com', NULL, '', NULL, '$2y$10$rKf8aBT9jdWlF5KyqVYZXeHZu1igIdR9Ki0RY1C6jAx8dgW1E7Are', NULL, 'tpacare2020', NULL, 1, 1, '2020-05-10 11:49:37');

SET IDENTITY_INSERT users OFF;
SELECT IDENT_CURRENT('users') AS current_id;
DBCC CHECKIDENT ('users', RESEED, 1);
SELECT IDENT_CURRENT('users') AS current_id;
