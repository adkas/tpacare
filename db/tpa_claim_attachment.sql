IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID(N'[dbo].[tpa_claim_attachment]') AND type IN ('U'))
	DROP TABLE [dbo].[tpa_claim_attachment]
GO

CREATE TABLE [dbo].[tpa_claim_attachment] (
  [id] int  IDENTITY(1,1) NOT NULL,
  [claim_id] int DEFAULT ((0)) NOT NULL,
  [attachment_type] int DEFAULT ((0)) NOT NULL,
  [file] nvarchar(150) COLLATE Thai_CI_AI  NOT NULL
)
GO

ALTER TABLE [dbo].[tpa_claim_attachment] SET (LOCK_ESCALATION = TABLE)
GO
