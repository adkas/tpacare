/*
 Navicat Premium Data Transfer

 Source Server         : tpacare
 Source Server Type    : SQL Server
 Source Server Version : 12002000
 Source Host           : tpacare-v2-db.database.windows.net:1433
 Source Catalog        : tpacare-v2-dev
 Source Schema         : dbo

 Target Server Type    : SQL Server
 Target Server Version : 12002000
 File Encoding         : 65001

 Date: 11/07/2020 20:59:37
*/


-- ----------------------------
-- Table structure for tpa_article
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID(N'[dbo].[tpa_article]') AND type IN ('U'))
	DROP TABLE [dbo].[tpa_article]
GO

CREATE TABLE [dbo].[tpa_article] (
  [id] int  IDENTITY(1,1) NOT NULL,
  [title_th] nvarchar(250) COLLATE Thai_CI_AI  NOT NULL,
  [detail_th] ntext COLLATE Thai_CI_AI  NOT NULL,
  [title_en] nvarchar(250) COLLATE Thai_CI_AI  NOT NULL,
  [detail_en] ntext COLLATE Thai_CI_AI  NOT NULL,
  [hospital_code] varchar(20) COLLATE Thai_CI_AI  NULL,
  [category_id] int DEFAULT ((0)) NULL,
  [age_range] int DEFAULT ((1)) NOT NULL,
  [gender] varchar(10) COLLATE Thai_CI_AI  NULL,
  [status] int DEFAULT ((0)) NOT NULL,
  [created_at] datetime DEFAULT (getdate()) NULL,
  [updated_at] datetime  NULL,
  [deleted_at] datetime  NULL
)
GO


-- ----------------------------
-- Records of tpa_article
-- ----------------------------
SET IDENTITY_INSERT [dbo].[tpa_article] ON
GO

INSERT INTO [dbo].[tpa_article] ([id], [title_th], [detail_th], [title_en], [detail_en], [hospital_code], [category_id], [status], [created_at], [updated_at], [deleted_at]) VALUES (N'1', N'รู้แล้วรอด โรคความดันเลือดสูง ภัยเงียบที่ไม่ควรมองข้าม', N'<div class="blog-description">														
<p>วันที่ 17 พฤษภาคม ของทุกปี ถูกจัดตั้งให้เป็นวัน&nbsp;<strong>ความดันเลือดสูงโลก&nbsp;</strong>(World Hypertension Day) เพื่อสร้างความเข้าใจ ให้กับประชาชน ได้ตื่นตัว และตระหนัก เห็นความสำคัญ ของการป้องกัน โรคความดันเลือดสูง ซึ่งเป็นอีกหนึ่ง โรคฮิตที่คร่าชีวิต คนไทย และคนทั่วโลก</p>
<p><strong>โรคความดันเลือดสูงคือ</strong></p>
<p>สภาวะของระดับความดันเลือด ที่สูงกว่าระดับปกติ โดยทั่วไปแล้ว ผู้ที่มีความดันเลือดปกติ จะวัดค่าความดัน ได้ 120/80 มิลลิเมตรปรอท แต่ผู้ที่มีความดันเลือดสูง จะวัดค่าความดันได้ ตั้งแต่ 140/90 มิลลิเมตรปรอท ขึ้นไป และถือว่าเป็นสภาวะ ที่ต้องได้รับการควบคุม ตั้งแต่เนิ่น ๆ เนื่องจากอาจนำมาซึ่ง ภาวะแทรกซ้อนและโรคต่าง ๆ มากมาย เช่น โรคหัวใจขาดเลือด โรคหัวใจล้มเหลว โรคหลอดเลือดในสมอง โรคไตเสื่อม เป็นต้น</p>
<p><strong>สาเหตุและปัจจัยเสี่ยง</strong></p>
<p>ความน่ากลัว ของโรคความดันเลือดสูง คือผู้ป่วยส่วนใหญ่กว่า 90-95 เปอร์เซ็นต์ ไม่สามารถ ตรวจหาสาเหตุ ที่ชัดเจนได้ว่า เกิดขึ้นจากอะไร ทำให้โรคความดันเลือดสูง ถูกขนานนามว่า “โรคเพชฌฆาตเงียบ” โดยทางการแพทย์นั้น ได้อธิบาย โรคความดันเลือดสูง นี้ว่าเป็นโรค ที่เกิดขึ้นจากธรรมชาติ ของมนุษย์ เช่น เกิดจากกรรมพันธุ์ และอายุที่มากขึ้น&nbsp; โดยส่วนใหญ่ จะพบได้มากใน ผู้หญิงที่มีอายุตั้งแต่ 40-50 ปีขึ้นไป หรือวัยหมดประจำเดือน</p>
<p><strong>วิธีรักษา</strong></p>
<p>แม้โรคความดันเลือดสูง จะเป็นโรคที่อันตราย แต่ก็เป็นโรคที่ สามารถควบคุมได้ในระยะยาว หากได้รับการรักษา ที่ทันท่วงที โดยเบื้องต้นจะรักษา ด้วยวิธีการให้ยาลดความดันเลือด เพื่อรักษาระดับความดัน ให้อยู่ในเกณฑ์ มาตรฐาน นอกจากการ รับประทานยาแล้ว ผู้ป่วยควรปรับเปลี่ยนพฤติกรรม เพื่อให้การรักษา มีประสิทธิภาพมากขึ้น ดังนี้</p>
<p><strong>สิ่งที่ควรทำ</strong></p>
<ul>
<li>หมั่นตรวจวัดความดันเลือดอย่างน้อยปีละ 1 ครั้ง</li>
<li>รับประทานอาการให้ครบ 5 หมู่ เน้นผัก และผลไม้ชนิดที่ไม่หวาน</li>
<li>ควบคุมน้ำหนักตัวให้อยู่ในเกณฑ์ปกติ</li>
<li>ออกกำลังเป็นประจำ</li>
<li>พักผ่อนให้เพียงพอ</li>
<li>รักษาสุขภาพจิตให้ดีอยู่เสมอ ไม่เครียด</li>
</ul>
<p><strong>สิ่งที่ไม่ควรทำ</strong></p>
<ul>
<li>สูบบุหรี่ เพราะสารพิษในควันบุหรี่ ส่งผลให้เกิดการอักเสบ ตีบตัน ของหลอดเลือดต่าง ๆ รวมทั้งหลอดเลือดหัวใจ และหลอดเลือดไต</li>
<li>ดื่มเครื่องดื่มแอลกอฮอล์ เพราะทำให้มีโอกาสเป็นโรคความดันเลือดสูงถึงร้อยละ 50</li>
<li>กินอาหารที่มีรสเค็ม หรืออาหารที่มีโซเดียมมากเกินไป เช่น กะปิ นํ้าปลา ของหมักดอง</li>
<li>กินอาหารที่มีไขมันสูง เช่น เนื้อติดมัน หนังสัตว์ ไข่แดง หอยนางรม อาหารประเภทผัดหรือทอด</li>
<li>กินอาหารที่มีรสหวาน หรือน้ำตาลสูง เช่น น้ำหวาน ขนมหวาน</li>
</ul>
<p>&nbsp;</p>
<p>ข้อมูลโดย ผศ. นพ.ภาวิทย์ เพียรวิจิตร สาขาวิชาโรคหัวใจ ภาควิชาอายุรศาสตร์</p>
<p>คณะแพทยศาสตร์โรงพยาบาลรามาธิบดี มหาวิทยาลัยมหิดล</p>
<p><br></p>																	</div>', N'xxxxx', N'<p>yyyyy</p>', N'PVR201600938', N'3', N'1', N'2020-06-29 12:49:23.663', NULL, NULL)
GO

SET IDENTITY_INSERT [dbo].[tpa_article] OFF
GO


-- ----------------------------
-- Primary Key structure for table tpa_article
-- ----------------------------
ALTER TABLE [dbo].[tpa_article] ADD CONSTRAINT [PK__tpa_arti__3213E83F79CA4640] PRIMARY KEY CLUSTERED ([id])
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
GO

