/*
 Navicat Premium Data Transfer

 Source Server         : tpacare
 Source Server Type    : SQL Server
 Source Server Version : 12002000
 Source Host           : tpacare-v2-db.database.windows.net:1433
 Source Catalog        : tpacare-v2-dev
 Source Schema         : dbo

 Target Server Type    : SQL Server
 Target Server Version : 12002000
 File Encoding         : 65001

 Date: 15/07/2020 21:10:15
*/


-- ----------------------------
-- Table structure for tpa_claim
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID(N'[dbo].[tpa_claim]') AND type IN ('U'))
	DROP TABLE [dbo].[tpa_claim]
GO

CREATE TABLE [dbo].[tpa_claim] (
  [clam_id] int  IDENTITY(1,1) NOT NULL,
  [claim_no] varchar(20) COLLATE Thai_CI_AI  NULL,
  [uid] int DEFAULT ((0)) NOT NULL,
  [policy_no] varchar(20) COLLATE Thai_CI_AI  NOT NULL,
  [staff_no] varchar(20) COLLATE Thai_CI_AI  NOT NULL,
  [email] varchar(50) COLLATE Thai_CI_AI  NULL,
  [claim_type] nvarchar(50) COLLATE Thai_CI_AI  NOT NULL,
  [admited_type] nvarchar(50) COLLATE Thai_CI_AI  NOT NULL,
  [symptom] nvarchar(250) COLLATE Thai_CI_AI  NOT NULL,
  [provider_code] varchar(20) COLLATE Thai_CI_AI  NOT NULL,
  [medical_expense] decimal(15,2) DEFAULT ((0.00)) NOT NULL,
  [claim_status] int DEFAULT ((0)) NOT NULL,
  [admited_date] date  NULL,
  [accident_date] date  NULL,
  [created_at] datetime DEFAULT (getdate()) NULL,
  [updated_by] int DEFAULT ((0)) NOT NULL,
  [updated_at] datetime  NULL
)
GO


-- ----------------------------
-- Records of tpa_claim
-- ----------------------------
SET IDENTITY_INSERT [dbo].[tpa_claim] ON
GO

INSERT INTO [dbo].[tpa_claim] ([clam_id], [claim_no], [uid], [policy_no], [staff_no], [email], [claim_type], [admited_type], [symptom], [provider_code], [medical_expense], [claim_status], [admited_date], [created_at], [updated_by], [updated_at]) VALUES (N'1', NULL, N'2', N'RR185198', N'RR185198', N'sakda.jump@gmail.com', N'OPD', N'OPD', N'เป็นไข้', N'PVR201600909', N'5000.00', N'0', N'2020-06-17', N'2020-06-25 22:08:35.750', N'0', NULL)
GO

INSERT INTO [dbo].[tpa_claim] ([clam_id], [claim_no], [uid], [policy_no], [staff_no], [email], [claim_type], [admited_type], [symptom], [provider_code], [medical_expense], [claim_status], [admited_date], [created_at], [updated_by], [updated_at]) VALUES (N'2', NULL, N'2', N'RR185198', N'RR185198', N'sakda.jump@gmail.com', N'OPD', N'OPD', N'เป็นไข้', N'PVR201600909', N'5000.00', N'0', N'2020-06-17', N'2020-06-25 23:01:47.670', N'0', NULL)
GO

INSERT INTO [dbo].[tpa_claim] ([clam_id], [claim_no], [uid], [policy_no], [staff_no], [email], [claim_type], [admited_type], [symptom], [provider_code], [medical_expense], [claim_status], [admited_date], [created_at], [updated_by], [updated_at]) VALUES (N'3', NULL, N'2', N'RR185198', N'RR185198', N'sakda.jump@gmail.com', N'OPD', N'OPD', N'เป็นไข้', N'PVR201600909', N'5000.00', N'0', N'2020-06-17', N'2020-06-26 11:00:18.380', N'0', NULL)
GO

INSERT INTO [dbo].[tpa_claim] ([clam_id], [claim_no], [uid], [policy_no], [staff_no], [email], [claim_type], [admited_type], [symptom], [provider_code], [medical_expense], [claim_status], [admited_date], [created_at], [updated_by], [updated_at]) VALUES (N'4', NULL, N'2', N'RR185198', N'RR185198', N'sakda.jump@gmail.com', N'OPD', N'OPD', N'เป็นไข้', N'PVR201600909', N'5000.00', N'0', N'2020-06-17', N'2020-06-30 07:10:18.337', N'0', NULL)
GO

INSERT INTO [dbo].[tpa_claim] ([clam_id], [claim_no], [uid], [policy_no], [staff_no], [email], [claim_type], [admited_type], [symptom], [provider_code], [medical_expense], [claim_status], [admited_date], [created_at], [updated_by], [updated_at]) VALUES (N'5', NULL, N'2', N'RR185198', N'RR185198', N'sakda.jump@gmail.com', N'OPD', N'OPD', N'เป็นไข้', N'PVR201600909', N'5000.00', N'0', N'2020-06-17', N'2020-07-01 03:53:37.457', N'0', NULL)
GO

INSERT INTO [dbo].[tpa_claim] ([clam_id], [claim_no], [uid], [policy_no], [staff_no], [email], [claim_type], [admited_type], [symptom], [provider_code], [medical_expense], [claim_status], [admited_date], [created_at], [updated_by], [updated_at]) VALUES (N'6', NULL, N'2', N'RR185198', N'RR185198', NULL, N'accident', N'OPD', N'เป็นไข้', N'PVR201600909', N'5000.00', N'0', N'2020-06-17', N'2020-07-07 08:15:51.843', N'0', NULL)
GO

SET IDENTITY_INSERT [dbo].[tpa_claim] OFF
GO


-- ----------------------------
-- Primary Key structure for table tpa_claim
-- ----------------------------
ALTER TABLE [dbo].[tpa_claim] ADD CONSTRAINT [PK__tpa_clai__2D522A18B762D366] PRIMARY KEY CLUSTERED ([clam_id])
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
GO

