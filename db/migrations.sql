

-- ----------------------------
-- Table structure for migrations
-- ----------------------------

CREATE TABLE migrations (
	id int NOT NULL IDENTITY(1,1),
	migration varchar(255) NOT NULL,
	batch int NOT NULL,
	CONSTRAINT migrations_PK PRIMARY KEY (id)
);

-- ----------------------------
-- Records of migrations
-- ----------------------------
SET IDENTITY_INSERT migrations ON;
INSERT INTO migrations (id, migration, batch) VALUES (1, '2014_10_12_000000_create_users_table', 1);
INSERT INTO migrations (id, migration, batch) VALUES (2, '2014_10_12_100000_create_password_resets_table', 1);
INSERT INTO migrations (id, migration, batch) VALUES (3, '2016_08_07_152421_modify_users', 1);
INSERT INTO migrations (id, migration, batch) VALUES (4, '2016_08_07_152421_table_menus', 1);
INSERT INTO migrations (id, migration, batch) VALUES (5, '2016_08_07_152421_table_modules', 1);
INSERT INTO migrations (id, migration, batch) VALUES (6, '2016_08_07_152421_table_role_privileges', 1);
INSERT INTO migrations (id, migration, batch) VALUES (7, '2016_08_07_152421_table_roles', 1);
INSERT INTO migrations (id, migration, batch) VALUES (8, '2020_04_18_071757_tpa_news', 2);
INSERT INTO migrations (id, migration, batch) VALUES (9, '2020_04_22_150124_tpa_promotions', 3);
INSERT INTO migrations (id, migration, batch) VALUES (10, '2020_04_22_151914_tpa_hospital', 4);
SET IDENTITY_INSERT migrations OFF;
