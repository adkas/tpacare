/*
 Navicat Premium Data Transfer

 Source Server         : tpacare
 Source Server Type    : SQL Server
 Source Server Version : 12002000
 Source Host           : tpacare-v2-db.database.windows.net:1433
 Source Catalog        : tpacare-v2-dev
 Source Schema         : dbo

 Target Server Type    : SQL Server
 Target Server Version : 12002000
 File Encoding         : 65001

 Date: 10/12/2020 00:21:31
*/


-- ----------------------------
-- Table structure for tpa_notifications
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID(N'[dbo].[tpa_notifications]') AND type IN ('U'))
	DROP TABLE [dbo].[tpa_notifications]
GO

CREATE TABLE [dbo].[tpa_notifications] (
  [id] int  IDENTITY(1,1) NOT NULL,
  [title_th] nvarchar(250) COLLATE Thai_CI_AS  NOT NULL,
  [title_en] nvarchar(250) COLLATE Thai_CI_AS  NOT NULL,
  [body_th] nvarchar(500) COLLATE Thai_CI_AS  NOT NULL,
  [body_en] nvarchar(500) COLLATE Thai_CI_AS  NOT NULL,
  [url] nvarchar(150) COLLATE Thai_CI_AS  NULL,
  [category] nvarchar(10) COLLATE Thai_CI_AS  DEFAULT 'normal' NOT NULL,
  [type] nvarchar(10) COLLATE Thai_CI_AS  NULL,
  [target_criteria] nvarchar(10) COLLATE Thai_CI_AS  NULL,
  [age_range] int DEFAULT ((0)) NULL,
  [gender] int DEFAULT ((0)) NULL,
  [created_at] datetime  NULL,
  [created_by] int DEFAULT ((0)) NOT NULL
)
GO


-- ----------------------------
-- Records of tpa_notifications
-- ----------------------------
SET IDENTITY_INSERT [dbo].[tpa_notifications] ON
GO

INSERT INTO [dbo].[tpa_notifications] ([id], [title_th], [title_en], [body_th], [body_en], [url], [type], [target_criteria], [age_range], [gender], [created_at], [created_by]) VALUES (N'1', N'ทดสอบ', N'test', N'ทดสอบ', N'test', N'xxxx', N'target', N'age_range', N'2', N'0', N'2020-10-09 17:41:28.000', N'1')
GO

INSERT INTO [dbo].[tpa_notifications] ([id], [title_th], [title_en], [body_th], [body_en], [url], [type], [target_criteria], [age_range], [gender], [created_at], [created_by]) VALUES (N'2', N'ทดสอบ', N'test', N'ทดสอบ', N'test', N'xxxx', N'target', N'gender', N'2', N'0', N'2020-10-09 17:41:53.000', N'1')
GO

INSERT INTO [dbo].[tpa_notifications] ([id], [title_th], [title_en], [body_th], [body_en], [url], [type], [target_criteria], [age_range], [gender], [created_at], [created_by]) VALUES (N'3', N'วัคซีนป้องกันไข้หวัดใหญ่ 4 สายพันธุ์', N'วัคซีนป้องกันไข้หวัดใหญ่ 4 สายพันธุ์-EN', N'วัคซีนป้องกันไข้หวัดใหญ่ 4 สายพันธุ์ 14-10-2020', N'วัคซีนป้องกันไข้หวัดใหญ่ 4 สายพันธุ์ 14-10-2020-EN', N'', N'all', N'age_range', N'2', N'0', N'2020-10-14 12:04:11.000', N'90304')
GO

INSERT INTO [dbo].[tpa_notifications] ([id], [title_th], [title_en], [body_th], [body_en], [url], [type], [target_criteria], [age_range], [gender], [created_at], [created_by]) VALUES (N'4', N'วัคซีนป้องกันไข้หวัดใหญ่ 4 สายพันธุ์', N'วัคซีนป้องกันไข้หวัดใหญ่ 4 สายพันธุ์-en', N'วัคซีนป้องกันไข้หวัดใหญ่ 4 สายพันธุ์ 14-10-2020', N'วัคซีนป้องกันไข้หวัดใหญ่ 4 สายพันธุ์ 14-10-2020-en', N'', N'all', N'gender', N'1', N'0', N'2020-10-14 12:06:01.000', N'90304')
GO

INSERT INTO [dbo].[tpa_notifications] ([id], [title_th], [title_en], [body_th], [body_en], [url], [type], [target_criteria], [age_range], [gender], [created_at], [created_by]) VALUES (N'5', N'วัคซีนป้องกันไข้หวัดใหญ่ 4 สายพันธุ์', N'วัคซีนป้องกันไข้หวัดใหญ่ 4 สายพันธุ์', N'วัคซีนป้องกันไข้หวัดใหญ่ 4 สายพันธุ์', N'วัคซีนป้องกันไข้หวัดใหญ่ 4 สายพันธุ์', N'', N'all', N'gender', N'1', N'0', N'2020-10-14 17:05:27.000', N'21')
GO

INSERT INTO [dbo].[tpa_notifications] ([id], [title_th], [title_en], [body_th], [body_en], [url], [type], [target_criteria], [age_range], [gender], [created_at], [created_by]) VALUES (N'6', N'ราชประสงค์เริ่มเคลื่อนไหว ตร.ตรึงกำลัง ลุงชู 3 นิ้ว เรียกเสียงฮือลั่น-2 สาวถือป้าย''ปล่อยเพื่อนเรา''', N'ราชประสงค์เริ่มเคลื่อนไหว ตร.ตรึงกำลัง ลุงชู 3 นิ้ว เรียกเสียงฮือลั่น-2 สาวถือป้าย''ปล่อยเพื่อนเรา'' --> EN', N'รายละเอียด --> ราชประสงค์เริ่มเคลื่อนไหว ตร.ตรึงกำลัง ลุงชู 3 นิ้ว เรียกเสียงฮือลั่น-2 สาวถือป้าย''ปล่อยเพื่อนเรา''', N'รายละเอียด --> ราชประสงค์เริ่มเคลื่อนไหว ตร.ตรึงกำลัง ลุงชู 3 นิ้ว เรียกเสียงฮือลั่น-2 สาวถือป้าย''ปล่อยเพื่อนเรา'' --> EN', N'', N'all', N'gender', N'1', N'0', N'2020-10-15 15:54:07.000', N'90304')
GO

INSERT INTO [dbo].[tpa_notifications] ([id], [title_th], [title_en], [body_th], [body_en], [url], [type], [target_criteria], [age_range], [gender], [created_at], [created_by]) VALUES (N'7', N'ทดสอบ', N'Test', N'ทดสอบ notification จาก TPA Care', N'Notification test from TPA Care', N'', N'target', N'gender', N'1', N'1', N'2020-10-16 16:04:10.000', N'90304')
GO

INSERT INTO [dbo].[tpa_notifications] ([id], [title_th], [title_en], [body_th], [body_en], [url], [type], [target_criteria], [age_range], [gender], [created_at], [created_by]) VALUES (N'8', N'โปรแกรมตรวจคัดกรองตับ', N'โปรแกรมตรวจคัดกรองตับ-EN', N'รายละเอียด- โปรแกรมตรวจคัดกรองตับ', N'รายละเอียด- โปรแกรมตรวจคัดกรองตับ-EN', N'', N'all', N'gender', N'1', N'0', N'2020-10-21 17:09:59.000', N'90304')
GO

INSERT INTO [dbo].[tpa_notifications] ([id], [title_th], [title_en], [body_th], [body_en], [url], [type], [target_criteria], [age_range], [gender], [created_at], [created_by]) VALUES (N'21', N'แพ็คเกจวัคซีนสำหรับเด็ก อายุ 2-7 เดือน', N'แพ็คเกจวัคซีนสำหรับเด็ก อายุ 2-7 เดือน-EN', N'รายละเอียดแพ็คเกจวัคซีนสำหรับเด็ก อายุ 2-7 เดือน', N'รายละเอียดแพ็คเกจวัคซีนสำหรับเด็ก อายุ 2-7 เดือน-EN', N'', N'all', N'gender', N'1', N'0', N'2020-11-02 11:35:40.000', N'90304')
GO

INSERT INTO [dbo].[tpa_notifications] ([id], [title_th], [title_en], [body_th], [body_en], [url], [type], [target_criteria], [age_range], [gender], [created_at], [created_by]) VALUES (N'22', N'เทส', N'test', N'เทส..', N'test..', N'', N'all', N'gender', N'1', N'0', N'2020-11-03 10:33:30.000', N'21')
GO

INSERT INTO [dbo].[tpa_notifications] ([id], [title_th], [title_en], [body_th], [body_en], [url], [type], [target_criteria], [age_range], [gender], [created_at], [created_by]) VALUES (N'27', N'6 สัญญาณเตือน นิ่วในไต', N'6 สัญญาณเตือน นิ่วในไต-EN', N'รายละเอียด  6 สัญญาณเตือน นิ่วในไต-EN', N'Detail  6 สัญญาณเตือน นิ่วในไต-EN', N'', N'all', N'gender', N'1', N'0', N'2020-11-05 16:07:48.000', N'90304')
GO

INSERT INTO [dbo].[tpa_notifications] ([id], [title_th], [title_en], [body_th], [body_en], [url], [type], [target_criteria], [age_range], [gender], [created_at], [created_by]) VALUES (N'28', N'แจ้งเตือน', N'แจ้งเตือน', N'TPA ได้รับเอกสารแล้ว', N'TPA ได้รับเอกสารแล้ว', N'', N'all', N'gender', N'1', N'0', N'2020-11-10 16:33:35.000', N'90304')
GO

INSERT INTO [dbo].[tpa_notifications] ([id], [title_th], [title_en], [body_th], [body_en], [url], [type], [target_criteria], [age_range], [gender], [created_at], [created_by]) VALUES (N'29', N'แจ้งเตือน', N'แจ้งเตือน', N'TPA ได้รับเอกสารแล้ว', N'TPA ได้รับเอกสารแล้ว', N'', N'all', N'gender', N'1', N'0', N'2020-11-10 16:34:33.000', N'90304')
GO

INSERT INTO [dbo].[tpa_notifications] ([id], [title_th], [title_en], [body_th], [body_en], [url], [type], [target_criteria], [age_range], [gender], [created_at], [created_by]) VALUES (N'30', N'แจ้งเตือน', N'แจ้งเตือน', N'TPA ได้รับเอกสารแล้ว', N'TPA ได้รับเอกสารแล้ว', N'', N'all', N'gender', N'1', N'0', N'2020-11-10 16:35:22.000', N'90304')
GO

INSERT INTO [dbo].[tpa_notifications] ([id], [title_th], [title_en], [body_th], [body_en], [url], [type], [target_criteria], [age_range], [gender], [created_at], [created_by]) VALUES (N'31', N'แจ้งเตือน', N'แจ้งเตือน', N'TPA ได้รับเอกสารแล้ว', N'TPA ได้รับเอกสารแล้ว', N'', N'all', N'gender', N'1', N'0', N'2020-11-10 16:36:52.000', N'90304')
GO

INSERT INTO [dbo].[tpa_notifications] ([id], [title_th], [title_en], [body_th], [body_en], [url], [type], [target_criteria], [age_range], [gender], [created_at], [created_by]) VALUES (N'32', N'แจ้งเตือน', N'แจ้งเตือน', N'TPA ได้รับเอกสารแล้ว', N'TPA ได้รับเอกสารแล้ว', N'', N'target', N'age_range', N'5', N'0', N'2020-11-18 10:57:47.000', N'90304')
GO

INSERT INTO [dbo].[tpa_notifications] ([id], [title_th], [title_en], [body_th], [body_en], [url], [type], [target_criteria], [age_range], [gender], [created_at], [created_by]) VALUES (N'33', N'แจ้งเตือน', N'แจ้งเตือน', N'TPA ได้รับเอกสารแล้ว', N'TPA ได้รับเอกสารแล้ว', N'', N'target', N'gender', N'1', N'1', N'2020-11-18 10:59:52.000', N'90304')
GO

INSERT INTO [dbo].[tpa_notifications] ([id], [title_th], [title_en], [body_th], [body_en], [url], [type], [target_criteria], [age_range], [gender], [created_at], [created_by]) VALUES (N'34', N'แจ้งเตือน', N'แจ้งเตือน', N'TPA ได้รับเอกสารแล้ว', N'TPA ได้รับเอกสารแล้ว', N'', N'target', N'gender', N'1', N'1', N'2020-11-18 11:00:31.000', N'90304')
GO

INSERT INTO [dbo].[tpa_notifications] ([id], [title_th], [title_en], [body_th], [body_en], [url], [type], [target_criteria], [age_range], [gender], [created_at], [created_by]) VALUES (N'35', N'แจ้งเตือน', N'แจ้งเตือน', N'TPA ได้รับเอกสารแล้ว', N'TPA ได้รับเอกสารแล้ว', N'', N'target', N'gender', N'1', N'1', N'2020-11-18 11:04:06.000', N'90304')
GO

INSERT INTO [dbo].[tpa_notifications] ([id], [title_th], [title_en], [body_th], [body_en], [url], [type], [target_criteria], [age_range], [gender], [created_at], [created_by]) VALUES (N'36', N'แจ้งเตือน', N'แจ้งเตือน', N'TPA ได้รับเอกสารแล้ว', N'TPA ได้รับเอกสารแล้ว', N'', N'target', N'gender', N'1', N'1', N'2020-11-18 11:05:13.000', N'90304')
GO

INSERT INTO [dbo].[tpa_notifications] ([id], [title_th], [title_en], [body_th], [body_en], [url], [type], [target_criteria], [age_range], [gender], [created_at], [created_by]) VALUES (N'37', N'แจ้งเตือน', N'แจ้งเตือน', N'TPA พิจารณาเรียบร้อยแล้ว', N'TPA พิจารณาเรียบร้อยแล้ว', N'', N'target', N'gender', N'1', N'1', N'2020-11-18 11:51:59.000', N'90304')
GO

INSERT INTO [dbo].[tpa_notifications] ([id], [title_th], [title_en], [body_th], [body_en], [url], [type], [target_criteria], [age_range], [gender], [created_at], [created_by]) VALUES (N'38', N'แจ้งเตือน', N'แจ้งเตือน', N'TPA พิจารณาเรียบร้อยแล้ว', N'TPA พิจารณาเรียบร้อยแล้ว', N'', N'target', N'gender', N'1', N'1', N'2020-11-18 11:56:28.000', N'90304')
GO

INSERT INTO [dbo].[tpa_notifications] ([id], [title_th], [title_en], [body_th], [body_en], [url], [type], [target_criteria], [age_range], [gender], [created_at], [created_by]) VALUES (N'39', N'แจ้งเตือน', N'แจ้งเตือน', N'TPA พิจารณาเรียบร้อยแล้ว', N'TPA พิจารณาเรียบร้อยแล้ว', N'', N'target', N'gender', N'1', N'1', N'2020-11-18 11:56:57.000', N'90304')
GO

INSERT INTO [dbo].[tpa_notifications] ([id], [title_th], [title_en], [body_th], [body_en], [url], [type], [target_criteria], [age_range], [gender], [created_at], [created_by]) VALUES (N'40', N'แจ้งเตือน', N'แจ้งเตือน', N'TPA พิจารณาเรียบร้อยแล้ว', N'TPA พิจารณาเรียบร้อยแล้ว', N'', N'target', N'gender', N'1', N'1', N'2020-11-18 11:58:07.000', N'90304')
GO

INSERT INTO [dbo].[tpa_notifications] ([id], [title_th], [title_en], [body_th], [body_en], [url], [type], [target_criteria], [age_range], [gender], [created_at], [created_by]) VALUES (N'41', N'แจ้งเตือน', N'แจ้งเตือน', N'TPA พิจารณาเรียบร้อยแล้ว', N'TPA พิจารณาเรียบร้อยแล้ว', N'', N'target', N'gender', N'1', N'1', N'2020-11-18 11:59:26.000', N'90304')
GO

INSERT INTO [dbo].[tpa_notifications] ([id], [title_th], [title_en], [body_th], [body_en], [url], [type], [target_criteria], [age_range], [gender], [created_at], [created_by]) VALUES (N'42', N'แจ้งเตือน', N'แจ้งเตือน', N'TPA พิจารณาเรียบร้อยแล้ว', N'TPA พิจารณาเรียบร้อยแล้ว', N'', N'target', N'age_range', N'7', N'1', N'2020-11-18 12:01:39.000', N'90304')
GO

INSERT INTO [dbo].[tpa_notifications] ([id], [title_th], [title_en], [body_th], [body_en], [url], [type], [target_criteria], [age_range], [gender], [created_at], [created_by]) VALUES (N'43', N'แจ้งเตือน', N'แจ้งเตือน', N'TPA พิจารณาเรียบร้อยแล้ว', N'TPA พิจารณาเรียบร้อยแล้ว', N'', N'target', N'age_range', N'7', N'1', N'2020-11-18 12:01:48.000', N'90304')
GO

INSERT INTO [dbo].[tpa_notifications] ([id], [title_th], [title_en], [body_th], [body_en], [url], [type], [target_criteria], [age_range], [gender], [created_at], [created_by]) VALUES (N'44', N'แจ้งเตือน', N'แจ้งเตือน', N'TPA พิจารณาเรียบร้อยแล้ว', N'TPA พิจารณาเรียบร้อยแล้ว', N'', N'target', N'age_range', N'7', N'0', N'2020-11-18 12:02:18.000', N'90304')
GO

INSERT INTO [dbo].[tpa_notifications] ([id], [title_th], [title_en], [body_th], [body_en], [url], [type], [target_criteria], [age_range], [gender], [created_at], [created_by]) VALUES (N'45', N'แจ้งเตือน', N'แจ้งเตือน', N'TPA พิจารณาเรียบร้อยแล้ว', N'TPA พิจารณาเรียบร้อยแล้ว', N'', N'target', N'age_range', N'7', N'0', N'2020-11-18 12:02:27.000', N'90304')
GO

INSERT INTO [dbo].[tpa_notifications] ([id], [title_th], [title_en], [body_th], [body_en], [url], [type], [target_criteria], [age_range], [gender], [created_at], [created_by]) VALUES (N'46', N'แจ้งเตือน', N'แจ้งเตือน', N'TPA พิจารณาเรียบร้อยแล้ว', N'TPA พิจารณาเรียบร้อยแล้ว', N'', N'target', N'gender', N'7', N'1', N'2020-11-18 12:02:39.000', N'90304')
GO

INSERT INTO [dbo].[tpa_notifications] ([id], [title_th], [title_en], [body_th], [body_en], [url], [type], [target_criteria], [age_range], [gender], [created_at], [created_by]) VALUES (N'47', N'แจ้งเตือน', N'แจ้งเตือน', N'TPA ได้รับเอกสารแล้ว', N'TPA ได้รับเอกสารแล้ว', N'', N'target', N'gender', N'1', N'1', N'2020-11-18 12:03:29.000', N'90304')
GO

INSERT INTO [dbo].[tpa_notifications] ([id], [title_th], [title_en], [body_th], [body_en], [url], [type], [target_criteria], [age_range], [gender], [created_at], [created_by]) VALUES (N'48', N'วัคซีนป้องกันไข้หวัดใหญ่ 4 สายพันธุ์', N'aaaaa', N'ทดสอบ-วัคซีนป้องกันไข้หวัดใหญ่ 4 สายพันธุ์', N'ทดสอบ-วัคซีนป้องกันไข้หวัดใหญ่ 4 สายพันธุ์ -EN', N'', N'target', N'gender', N'1', N'1', N'2020-12-02 17:14:23.000', N'90304')
GO

INSERT INTO [dbo].[tpa_notifications] ([id], [title_th], [title_en], [body_th], [body_en], [url], [type], [target_criteria], [age_range], [gender], [created_at], [created_by]) VALUES (N'49', N'โปรแกรมตรวจคัดกรองตับ', N'โปรแกรมตรวจคัดกรองตับ-EN', N'รายละเอียด-โปรแกรมตรวจคัดกรองตับ', N'รายละเอียด-โปรแกรมตรวจคัดกรองตับ-EN', N'', N'target', N'gender', N'1', N'1', N'2020-12-04 12:04:45.000', N'90304')
GO

INSERT INTO [dbo].[tpa_notifications] ([id], [title_th], [title_en], [body_th], [body_en], [url], [type], [target_criteria], [age_range], [gender], [created_at], [created_by]) VALUES (N'50', N'แพ็คเกจวัคซีนสำหรับเด็ก อายุ 2-7 เดือน', N'แพ็คเกจวัคซีนสำหรับเด็ก อายุ 2-7 เดือน-EN', N'รายละเอียด-แพ็คเกจวัคซีนสำหรับเด็ก อายุ 2-7 เดือน', N'รายละเอียด-แพ็คเกจวัคซีนสำหรับเด็ก อายุ 2-7 เดือน-EN', N'', N'target', N'gender', N'1', N'1', N'2020-12-04 12:18:45.000', N'90304')
GO

INSERT INTO [dbo].[tpa_notifications] ([id], [title_th], [title_en], [body_th], [body_en], [url], [type], [target_criteria], [age_range], [gender], [created_at], [created_by]) VALUES (N'51', N'โปรโมชั่น สุขภาพดี ผ่อนได้ 0% นาน 6 เดือน', N'โปรโมชั่น สุขภาพดี ผ่อนได้ 0% นาน 6 เดือน-EN', N'รายละเอียด-โปรโมชั่น สุขภาพดี ผ่อนได้ 0% นาน 6 เดือน', N'รายละเอียด-6 โปรโมชั่น สุขภาพดี ผ่อนได้ 0% นาน 6 เดือน-EN', N'', N'target', N'gender', N'1', N'1', N'2020-12-04 12:22:25.000', N'90304')
GO

INSERT INTO [dbo].[tpa_notifications] ([id], [title_th], [title_en], [body_th], [body_en], [url], [type], [target_criteria], [age_range], [gender], [created_at], [created_by]) VALUES (N'52', N'เช็คหัวใจส่งท้ายปี กับโปรโมชั่นตรวจหินปูนในหลอดเลือดหัวใจ', N'เช็คหัวใจส่งท้ายปี กับโปรโมชั่นตรวจหินปูนในหลอดเลือดหัวใจ-EN', N'รายละเอียด - เช็คหัวใจส่งท้ายปี กับโปรโมชั่นตรวจหินปูนในหลอดเลือดหัวใจ', N'รายละเอียด - เช็คหัวใจส่งท้ายปี กับโปรโมชั่นตรวจหินปูนในหลอดเลือดหัวใจ-EN', N'', N'target', N'gender', N'1', N'0', N'2020-12-04 12:26:29.000', N'90304')
GO

INSERT INTO [dbo].[tpa_notifications] ([id], [title_th], [title_en], [body_th], [body_en], [url], [type], [target_criteria], [age_range], [gender], [created_at], [created_by]) VALUES (N'62', N'แพ็คเกจวัคซีนสำหรับเด็ก อายุ 2-7 เดือน', N'Vaccine package for 2-7 months', N'แพ็คเกจวัคซีนสำหรับเด็ก อายุ 2-7 เดือน', N'Vaccine package for 2-7 months', N'http://', N'promotion', N'all', N'2', N'0', N'2020-12-10 00:19:06.000', N'0')
GO

SET IDENTITY_INSERT [dbo].[tpa_notifications] OFF
GO


-- ----------------------------
-- Primary Key structure for table tpa_notifications
-- ----------------------------
ALTER TABLE [dbo].[tpa_notifications] ADD CONSTRAINT [PK__tpa_noti__3213E83F535F5AA0] PRIMARY KEY CLUSTERED ([id])
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
GO

