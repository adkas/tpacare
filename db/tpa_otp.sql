/*
 Navicat Premium Data Transfer

 Source Server         : TPA Care
 Source Server Type    : SQL Server
 Source Server Version : 12002000
 Source Host           : tpacare-v2-db.database.windows.net:1433
 Source Catalog        : tpacare-v2-dev
 Source Schema         : dbo

 Target Server Type    : SQL Server
 Target Server Version : 12002000
 File Encoding         : 65001

 Date: 10/05/2020 20:35:48
*/


-- ----------------------------
-- Table structure for tpa_otp
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID(N'[dbo].[tpa_otp]') AND type IN ('U'))
	DROP TABLE [dbo].[tpa_otp]
GO

CREATE TABLE [dbo].[tpa_otp] (
  [id] int  IDENTITY(1,1) NOT NULL,
  [uid] int DEFAULT ((0)) NOT NULL,
  [otp_type] int DEFAULT ((1)) NOT NULL,
  [mobile] varchar(10) COLLATE Thai_CI_AI  NOT NULL,
  [otp] varchar(6) COLLATE Thai_CI_AI  NOT NULL,
  [verified] int DEFAULT ((0)) NOT NULL,
  [created_at] datetime DEFAULT (getdate()) NULL,
  [expired_at] datetime DEFAULT NULL NULL,
  [verified_at] datetime DEFAULT NULL NULL
)
GO

