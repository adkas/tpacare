/*
 Navicat Premium Data Transfer

 Source Server         : tpacare
 Source Server Type    : SQL Server
 Source Server Version : 12002000
 Source Host           : tpacare-v2-db.database.windows.net:1433
 Source Catalog        : tpacare-v2-dev
 Source Schema         : dbo

 Target Server Type    : SQL Server
 Target Server Version : 12002000
 File Encoding         : 65001

 Date: 31/07/2020 14:26:17
*/


-- ----------------------------
-- Table structure for tpa_policy
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID(N'[dbo].[tpa_policy]') AND type IN ('U'))
	DROP TABLE [dbo].[tpa_policy]
GO

CREATE TABLE [dbo].[tpa_policy] (
  [id] int  IDENTITY(1,1) NOT NULL,
  [uid] int DEFAULT ((0)) NOT NULL,
  [insurer_code] varchar(20) COLLATE Thai_CI_AI  NOT NULL,
  [insured_name] nvarchar(250) COLLATE Thai_CI_AI  NOT NULL,
  [insured_type] nvarchar(10) COLLATE Thai_CI_AI DEFAULT 'member',
  [policy_no] varchar(20) COLLATE Thai_CI_AI  NOT NULL,
  [effective_from] date  NULL,
  [effective_to] date  NULL,
  [card_front] nvarchar(150) COLLATE Thai_CI_AI  NOT NULL,
  [card_back] nvarchar(150) COLLATE Thai_CI_AI  NOT NULL,
  [created_at] datetime  NULL,
  [updated_at] datetime  NULL,
  [deleted_at] datetime  NULL
)
GO


-- ----------------------------
-- Records of tpa_policy
-- ----------------------------
SET IDENTITY_INSERT [dbo].[tpa_policy] ON
GO

INSERT INTO [dbo].[tpa_policy] ([id], [uid], [insurer_code], [insured_name], [policy_no], [effective_from], [effective_to], [card_front], [card_back], [created_at], [updated_at], [deleted_at]) VALUES (N'1', N'2', N'BNK201900030', N'Sakda Srijumpa', N'123456789', N'2020-06-17', N'2023-06-20', N'/storage/images/card/4kOERtdAmj.jpeg', N'/storage/images/card/mzhs6Vf9oz.jpeg', N'2020-06-17 02:34:14.000', N'2020-06-18 00:09:23.000', N'2020-07-13 08:51:59.000')
GO

INSERT INTO [dbo].[tpa_policy] ([id], [uid], [insurer_code], [insured_name], [policy_no], [effective_from], [effective_to], [card_front], [card_back], [created_at], [updated_at], [deleted_at]) VALUES (N'2', N'2', N'BNK201900030', N'ศักดิ์ดา ศรีจำปา', N'123456789', N'2020-06-17', N'2023-06-17', N'/storage/images/card/CyQi0yPV5g.jpeg', N'/storage/images/card/CmtIzGURnr.jpeg', N'2020-06-17 17:03:11.000', N'2020-07-23 16:16:16.000', N'2020-07-30 17:09:43.000')
GO

INSERT INTO [dbo].[tpa_policy] ([id], [uid], [insurer_code], [insured_name], [policy_no], [effective_from], [effective_to], [card_front], [card_back], [created_at], [updated_at], [deleted_at]) VALUES (N'3', N'2', N'BNK201900030', N'ศักดิ์ดา ศรีจำปา', N'123456789', N'2020-06-17', N'2023-06-17', N'/storage/images/card/YP73GCV3lE.jpeg', N'/storage/images/card/kYA2lrwVaL.jpeg', N'2020-06-17 17:05:08.000', N'2020-07-30 17:16:27.000', NULL)
GO

INSERT INTO [dbo].[tpa_policy] ([id], [uid], [insurer_code], [insured_name], [policy_no], [effective_from], [effective_to], [card_front], [card_back], [created_at], [updated_at], [deleted_at]) VALUES (N'5', N'59', N'TMP201900002', N'test test', N'123456789', N'2020-06-18', N'2020-06-19', N'/storage/images/card/1aXo5qKejk.png', N'/storage/images/card/6t4YksficM.png', N'2020-06-18 10:39:26.000', NULL, NULL)
GO

INSERT INTO [dbo].[tpa_policy] ([id], [uid], [insurer_code], [insured_name], [policy_no], [effective_from], [effective_to], [card_front], [card_back], [created_at], [updated_at], [deleted_at]) VALUES (N'8', N'59', N'TMP201900002', N'rds fhfder', N'36633355', N'2020-06-18', N'2020-06-18', N'/storage/images/card/03AM5gFJ4x.png', N'/storage/images/card/ivFV8dAtqm.png', N'2020-06-18 12:09:26.000', NULL, NULL)
GO

INSERT INTO [dbo].[tpa_policy] ([id], [uid], [insurer_code], [insured_name], [policy_no], [effective_from], [effective_to], [card_front], [card_back], [created_at], [updated_at], [deleted_at]) VALUES (N'10', N'2', N'BNK201900030', N'ศักดิ์ดา ทดสอบ', N'123456789', N'2020-06-17', N'2023-06-17', N'/storage/images/card/Tffv5lJRpv.jpeg', N'/storage/images/card/DwgK13l0No.jpeg', N'2020-06-18 12:14:11.000', N'2020-07-26 21:25:08.000', N'2020-07-30 17:07:33.000')
GO

INSERT INTO [dbo].[tpa_policy] ([id], [uid], [insurer_code], [insured_name], [policy_no], [effective_from], [effective_to], [card_front], [card_back], [created_at], [updated_at], [deleted_at]) VALUES (N'11', N'31', N'TMP201900001', N'test test', N'1245889', N'2020-06-19', N'2020-06-20', N'/storage/images/card/fQs8IPaMhq.png', N'/storage/images/card/U2icev9wbx.png', N'2020-06-19 10:52:23.000', NULL, NULL)
GO

INSERT INTO [dbo].[tpa_policy] ([id], [uid], [insurer_code], [insured_name], [policy_no], [effective_from], [effective_to], [card_front], [card_back], [created_at], [updated_at], [deleted_at]) VALUES (N'16', N'82', N'BNK201900030', N'ศักดิ์ดา ศรีจำปา', N'123456789', N'2020-06-17', N'2023-06-17', N'/storage/images/card/ZwkEUbZy7o.jpeg', N'/storage/images/card/ymSfjEYenT.jpeg', N'2020-06-27 16:17:54.000', NULL, N'2020-07-17 12:02:13.000')
GO

INSERT INTO [dbo].[tpa_policy] ([id], [uid], [insurer_code], [insured_name], [policy_no], [effective_from], [effective_to], [card_front], [card_back], [created_at], [updated_at], [deleted_at]) VALUES (N'23', N'82', N'TMP201900001', N'คมกฤช กาวิชัย', N'ABC123456', N'2020-06-28', N'2020-06-29', N'/storage/images/card/SuqBxKyzZB.jpeg', N'/storage/images/card/kPqsrM0CNS.jpeg', N'2020-06-28 16:42:52.000', N'2020-06-29 20:19:00.000', NULL)
GO

INSERT INTO [dbo].[tpa_policy] ([id], [uid], [insurer_code], [insured_name], [policy_no], [effective_from], [effective_to], [card_front], [card_back], [created_at], [updated_at], [deleted_at]) VALUES (N'24', N'82', N'TMP201900002', N'คมกฤช กาวิชัย', N'12341234', N'2020-06-28', N'2021-06-28', N'/storage/images/card/UI49HoHgna.jpeg', N'/storage/images/card/WYfr3wWVPd.jpeg', N'2020-06-28 16:47:05.000', NULL, N'2020-07-18 10:20:37.000')
GO

INSERT INTO [dbo].[tpa_policy] ([id], [uid], [insurer_code], [insured_name], [policy_no], [effective_from], [effective_to], [card_front], [card_back], [created_at], [updated_at], [deleted_at]) VALUES (N'25', N'82', N'BNK201900001', N'Komkrit Kawichai', N'12345678', N'2020-06-28', N'2022-06-28', N'/storage/images/card/FgeiKbwJPy.jpeg', N'/storage/images/card/F7t82ii49H.jpeg', N'2020-06-28 16:51:09.000', N'2020-07-17 12:05:21.000', N'2020-07-17 12:07:40.000')
GO

INSERT INTO [dbo].[tpa_policy] ([id], [uid], [insurer_code], [insured_name], [policy_no], [effective_from], [effective_to], [card_front], [card_back], [created_at], [updated_at], [deleted_at]) VALUES (N'26', N'82', N'BNK201900001', N'Komkrit Kawichai', N'1', N'2020-06-28', N'2019-06-28', N'/storage/images/card/i3giZkFbIY.jpeg', N'/storage/images/card/FOZVdd9Jka.jpeg', N'2020-06-28 16:55:24.000', NULL, N'2020-07-17 12:03:48.000')
GO

INSERT INTO [dbo].[tpa_policy] ([id], [uid], [insurer_code], [insured_name], [policy_no], [effective_from], [effective_to], [card_front], [card_back], [created_at], [updated_at], [deleted_at]) VALUES (N'27', N'82', N'TMP201900001', N'คมกฤช กาวิชัย', N'A001', N'2020-06-28', N'2029-06-29', N'/storage/images/card/wXjGPyeZmd.jpeg', N'/storage/images/card/G5rgp2hIEh.jpeg', N'2020-06-28 16:57:45.000', N'2020-06-29 20:12:23.000', NULL)
GO

INSERT INTO [dbo].[tpa_policy] ([id], [uid], [insurer_code], [insured_name], [policy_no], [effective_from], [effective_to], [card_front], [card_back], [created_at], [updated_at], [deleted_at]) VALUES (N'28', N'76', N'TMP201900003', N'test test2', N'111111111111', N'2020-06-29', N'2020-07-01', N'/storage/images/card/jIwdrUxCAC.png', N'/storage/images/card/16yTpnJwUr.png', N'2020-06-29 12:45:01.000', N'2020-07-10 14:39:45.000', N'2020-07-13 09:46:22.000')
GO

INSERT INTO [dbo].[tpa_policy] ([id], [uid], [insurer_code], [insured_name], [policy_no], [effective_from], [effective_to], [card_front], [card_back], [created_at], [updated_at], [deleted_at]) VALUES (N'29', N'76', N'TMP201900001', N'test test2', N'111111222', N'2020-07-10', N'2020-07-10', N'/storage/images/card/ncjOPUmIBb.png', N'/storage/images/card/mHJ3V0DxHg.png', N'2020-06-29 12:47:48.000', N'2020-07-10 12:33:59.000', N'2020-07-13 09:46:01.000')
GO

INSERT INTO [dbo].[tpa_policy] ([id], [uid], [insurer_code], [insured_name], [policy_no], [effective_from], [effective_to], [card_front], [card_back], [created_at], [updated_at], [deleted_at]) VALUES (N'31', N'76', N'INS201600027', N'test test4', N'888888', N'2020-07-01', N'2020-07-13', N'/storage/images/card/7my6mdp91T.png', N'/storage/images/card/pHp3HlQLLg.png', N'2020-06-29 13:56:24.000', N'2020-07-17 09:43:21.000', NULL)
GO

INSERT INTO [dbo].[tpa_policy] ([id], [uid], [insurer_code], [insured_name], [policy_no], [effective_from], [effective_to], [card_front], [card_back], [created_at], [updated_at], [deleted_at]) VALUES (N'32', N'82', N'BNK201900030', N'ศักดิ์ดา ศรีจำปา', N'123456789', N'2020-06-17', N'2023-06-17', N'/storage/images/card/DuuoNowey1.jpeg', N'/storage/images/card/24qln8ALyV.jpeg', N'2020-07-05 08:11:48.000', NULL, N'2020-07-18 07:59:19.000')
GO

INSERT INTO [dbo].[tpa_policy] ([id], [uid], [insurer_code], [insured_name], [policy_no], [effective_from], [effective_to], [card_front], [card_back], [created_at], [updated_at], [deleted_at]) VALUES (N'33', N'86', N'BNK201900030', N'ศักดิ์ดา ศรีจำปา', N'123456789', N'2020-06-17', N'2023-06-17', N'/storage/images/card/28xoQslNUA.jpeg', N'/storage/images/card/a72BZ8V32h.jpeg', N'2020-07-10 11:11:04.000', NULL, NULL)
GO

INSERT INTO [dbo].[tpa_policy] ([id], [uid], [insurer_code], [insured_name], [policy_no], [effective_from], [effective_to], [card_front], [card_back], [created_at], [updated_at], [deleted_at]) VALUES (N'34', N'76', N'TMP201900003', N'name lastname', N'123456789', N'2020-07-04', N'2020-08-01', N'/storage/images/card/wz3QhS94gB.png', N'/storage/images/card/uO2uUA4AY3.png', N'2020-07-17 10:06:31.000', NULL, NULL)
GO

INSERT INTO [dbo].[tpa_policy] ([id], [uid], [insurer_code], [insured_name], [policy_no], [effective_from], [effective_to], [card_front], [card_back], [created_at], [updated_at], [deleted_at]) VALUES (N'36', N'82', N'BNK201900030', N'ศักดิ์ดา ศรีจำปา', N'123456789', N'2020-06-17', N'2023-06-17', N'/storage/images/card/bigKUVVRU8.jpeg', N'/storage/images/card/bfCr6tb534.jpeg', N'2020-07-17 12:13:20.000', NULL, N'2020-07-18 10:19:28.000')
GO

INSERT INTO [dbo].[tpa_policy] ([id], [uid], [insurer_code], [insured_name], [policy_no], [effective_from], [effective_to], [card_front], [card_back], [created_at], [updated_at], [deleted_at]) VALUES (N'37', N'82', N'TMP201900001', N'q q', N'q', N'2020-07-17', N'2021-07-17', N'/storage/images/card/cg63QVquRx.jpeg', N'/storage/images/card/738mzZPS7V.jpeg', N'2020-07-17 12:14:09.000', NULL, N'2020-07-18 10:20:15.000')
GO

INSERT INTO [dbo].[tpa_policy] ([id], [uid], [insurer_code], [insured_name], [policy_no], [effective_from], [effective_to], [card_front], [card_back], [created_at], [updated_at], [deleted_at]) VALUES (N'41', N'81', N'INS201600006', N'นางสาวน้ำค้าง กลางถนน', N'1234rty1234', N'1970-01-01', N'2026-07-20', N'/storage/images/card/EXeabWLP1Z.png', N'/storage/images/card/5btogZtkqa.png', N'2020-07-17 15:46:37.000', N'2020-07-20 10:48:20.000', NULL)
GO

INSERT INTO [dbo].[tpa_policy] ([id], [uid], [insurer_code], [insured_name], [policy_no], [effective_from], [effective_to], [card_front], [card_back], [created_at], [updated_at], [deleted_at]) VALUES (N'46', N'83', N'TMP201900001', N'นางสาวมลทิพย์ เชื่อม่วง', N'12345678', N'2006-07-22', N'2022-07-22', N'/storage/images/card/8CNyhnGuQz.jpeg', N'/storage/images/card/qxWgYWioxb.jpeg', N'2020-07-22 14:06:55.000', N'2020-07-30 16:08:19.000', NULL)
GO

INSERT INTO [dbo].[tpa_policy] ([id], [uid], [insurer_code], [insured_name], [policy_no], [effective_from], [effective_to], [card_front], [card_back], [created_at], [updated_at], [deleted_at]) VALUES (N'52', N'81', N'BNK201900002', N'นางสาวสมควร พักดี', N'Aa123456789', N'2013-07-23', N'2025-07-23', N'/storage/images/card/TmoBVIl8hX.jpeg', N'/storage/images/card/oYjRB69WOK.jpeg', N'2020-07-23 21:50:26.000', NULL, NULL)
GO

INSERT INTO [dbo].[tpa_policy] ([id], [uid], [insurer_code], [insured_name], [policy_no], [effective_from], [effective_to], [card_front], [card_back], [created_at], [updated_at], [deleted_at]) VALUES (N'53', N'76', N'TMP201900001', N'tttttt ddd', N'1111111111111', N'2015-07-24', N'2020-07-24', N'/storage/images/card/8mN0PZnL1V.png', N'/storage/images/card/64GYOvAERa.png', N'2020-07-24 10:09:12.000', NULL, NULL)
GO

INSERT INTO [dbo].[tpa_policy] ([id], [uid], [insurer_code], [insured_name], [policy_no], [effective_from], [effective_to], [card_front], [card_back], [created_at], [updated_at], [deleted_at]) VALUES (N'54', N'2', N'TMP201900001', N'น้ำฝน วรรณคำ', N'aaa123456', N'2010-07-31', N'2022-07-26', N'/storage/images/card/6m4GEAuYlm.png', N'/storage/images/card/pYprSdgwo1.png', N'2020-07-26 17:51:52.000', N'2020-07-31 14:11:14.000', NULL)
GO

INSERT INTO [dbo].[tpa_policy] ([id], [uid], [insurer_code], [insured_name], [policy_no], [effective_from], [effective_to], [card_front], [card_back], [created_at], [updated_at], [deleted_at]) VALUES (N'57', N'2', N'TMP201900001', N'นายทดสอบ ระบบ', N'12345678', N'2012-07-30', N'2019-07-30', N'/storage/images/card/M7BX1VaWUJ.png', N'/storage/images/card/QG9nU2vMeg.png', N'2020-07-30 15:29:47.000', NULL, NULL)
GO

INSERT INTO [dbo].[tpa_policy] ([id], [uid], [insurer_code], [insured_name], [policy_no], [effective_from], [effective_to], [card_front], [card_back], [created_at], [updated_at], [deleted_at]) VALUES (N'58', N'83', N'TMP201900004', N'max chonlakan', N'123456789', N'2020-07-30', N'2020-09-30', N'/storage/images/card/vnNsGvvb86.png', N'/storage/images/card/X00T0dTM2c.png', N'2020-07-30 15:35:12.000', N'2020-07-30 16:50:46.000', NULL)
GO

INSERT INTO [dbo].[tpa_policy] ([id], [uid], [insurer_code], [insured_name], [policy_no], [effective_from], [effective_to], [card_front], [card_back], [created_at], [updated_at], [deleted_at]) VALUES (N'59', N'83', N'INS201600021', N'max za', N'12222', N'2020-07-30', N'2020-11-30', N'/storage/images/card/d0AkW36gQh.png', N'/storage/images/card/8ixUuqXLCq.png', N'2020-07-30 16:35:05.000', N'2020-07-30 16:52:55.000', NULL)
GO

INSERT INTO [dbo].[tpa_policy] ([id], [uid], [insurer_code], [insured_name], [policy_no], [effective_from], [effective_to], [card_front], [card_back], [created_at], [updated_at], [deleted_at]) VALUES (N'60', N'83', N'TMP201900003', N'max max', N'10000000', N'2020-07-30', N'2020-07-31', N'/storage/images/card/KKSRBghRcZ.png', N'/storage/images/card/Eh4dW3sEfN.png', N'2020-07-30 16:42:36.000', N'2020-07-30 16:55:00.000', NULL)
GO

INSERT INTO [dbo].[tpa_policy] ([id], [uid], [insurer_code], [insured_name], [policy_no], [effective_from], [effective_to], [card_front], [card_back], [created_at], [updated_at], [deleted_at]) VALUES (N'61', N'83', N'TMP201900002', N'max za', N'11112', N'2020-07-30', N'2020-09-04', N'/storage/images/card/R4kBlF3kV6.png', N'/storage/images/card/sgwFruT3Yi.png', N'2020-07-30 16:48:29.000', N'2020-07-30 17:00:14.000', NULL)
GO

INSERT INTO [dbo].[tpa_policy] ([id], [uid], [insurer_code], [insured_name], [policy_no], [effective_from], [effective_to], [card_front], [card_back], [created_at], [updated_at], [deleted_at]) VALUES (N'6', N'59', N'TMP201900001', N'test test', N'1234567', N'2020-06-19', N'2020-06-20', N'/storage/images/card/gWZz4lZWD2.png', N'/storage/images/card/vjxcIiEJ0f.png', N'2020-06-18 10:44:38.000', NULL, NULL)
GO

INSERT INTO [dbo].[tpa_policy] ([id], [uid], [insurer_code], [insured_name], [policy_no], [effective_from], [effective_to], [card_front], [card_back], [created_at], [updated_at], [deleted_at]) VALUES (N'7', N'59', N'TMP201900001', N'test test2', N'123456789', N'2020-06-20', N'2020-06-21', N'/storage/images/card/nFnm3yaIMT.png', N'/storage/images/card/Erlbklmktm.png', N'2020-06-18 11:16:51.000', NULL, NULL)
GO

INSERT INTO [dbo].[tpa_policy] ([id], [uid], [insurer_code], [insured_name], [policy_no], [effective_from], [effective_to], [card_front], [card_back], [created_at], [updated_at], [deleted_at]) VALUES (N'9', N'59', N'TMP201900002', N'rdsf hfder', N'36633355', N'2020-06-18', N'2020-06-18', N'/storage/images/card/9PQQ2uKrqT.png', N'/storage/images/card/c2xdQPQUWf.png', N'2020-06-18 12:09:27.000', NULL, NULL)
GO

INSERT INTO [dbo].[tpa_policy] ([id], [uid], [insurer_code], [insured_name], [policy_no], [effective_from], [effective_to], [card_front], [card_back], [created_at], [updated_at], [deleted_at]) VALUES (N'12', N'31', N'TMP201900003', N'test tests', N'123456789', N'2020-06-19', N'2020-06-23', N'/storage/images/card/uKL4tPIZpZ.png', N'/storage/images/card/ODKbM3JTk2.png', N'2020-06-19 11:36:29.000', NULL, NULL)
GO

INSERT INTO [dbo].[tpa_policy] ([id], [uid], [insurer_code], [insured_name], [policy_no], [effective_from], [effective_to], [card_front], [card_back], [created_at], [updated_at], [deleted_at]) VALUES (N'13', N'82', N'BNK201900030', N'ศักดิ์ดา ศรีจำปา', N'123456789', N'2020-06-17', N'2023-06-17', N'/storage/images/card/JqWnaX0Kfb.jpeg', N'/storage/images/card/AF1XyknkTc.jpeg', N'2020-06-27 15:29:52.000', NULL, N'2020-07-17 12:01:25.000')
GO

INSERT INTO [dbo].[tpa_policy] ([id], [uid], [insurer_code], [insured_name], [policy_no], [effective_from], [effective_to], [card_front], [card_back], [created_at], [updated_at], [deleted_at]) VALUES (N'19', N'82', N'TMP201900001', N'คมกฤช กาวิชัย', N'12345678', N'2020-06-27', N'2023-06-27', N'/storage/images/card/ScIfIHiCYe.jpeg', N'/storage/images/card/FZRvgI14oR.jpeg', N'2020-06-27 17:19:46.000', N'2020-06-29 19:42:18.000', N'2020-07-18 10:21:13.000')
GO

INSERT INTO [dbo].[tpa_policy] ([id], [uid], [insurer_code], [insured_name], [policy_no], [effective_from], [effective_to], [card_front], [card_back], [created_at], [updated_at], [deleted_at]) VALUES (N'20', N'82', N'TMP201900001', N'คมกฤช กาวิชัย', N'AS123456', N'2020-06-28', N'2025-06-28', N'/storage/images/card/BlRU8uUhPE.jpeg', N'/storage/images/card/V3Poyb1t7k.jpeg', N'2020-06-28 16:25:04.000', NULL, N'2020-07-18 10:20:25.000')
GO

INSERT INTO [dbo].[tpa_policy] ([id], [uid], [insurer_code], [insured_name], [policy_no], [effective_from], [effective_to], [card_front], [card_back], [created_at], [updated_at], [deleted_at]) VALUES (N'21', N'82', N'TMP201900001', N'คมกฤช กาวิชัย', N'AS123456', N'2020-06-28', N'2025-06-28', N'/storage/images/card/KToU8lx6gA.jpeg', N'/storage/images/card/5bypUeToSD.jpeg', N'2020-06-28 16:26:53.000', NULL, N'2020-07-18 10:21:00.000')
GO

INSERT INTO [dbo].[tpa_policy] ([id], [uid], [insurer_code], [insured_name], [policy_no], [effective_from], [effective_to], [card_front], [card_back], [created_at], [updated_at], [deleted_at]) VALUES (N'22', N'82', N'TMP201900001', N'คมกฤช กาวิชัย', N'AS123456', N'2020-06-28', N'2025-06-28', N'/storage/images/card/2GsWEa2ht8.jpeg', N'/storage/images/card/j4PgViXIZQ.jpeg', N'2020-06-28 16:29:36.000', NULL, N'2020-07-20 10:15:04.000')
GO

INSERT INTO [dbo].[tpa_policy] ([id], [uid], [insurer_code], [insured_name], [policy_no], [effective_from], [effective_to], [card_front], [card_back], [created_at], [updated_at], [deleted_at]) VALUES (N'30', N'76', N'INS201600027', N'test test3', N'999999997', N'2020-06-29', N'2020-06-29', N'/storage/images/card/dvpgIK8ENE.png', N'/storage/images/card/q3GCVzKHZF.png', N'2020-06-29 13:47:08.000', N'2020-07-10 12:59:17.000', N'2020-07-13 10:01:33.000')
GO

INSERT INTO [dbo].[tpa_policy] ([id], [uid], [insurer_code], [insured_name], [policy_no], [effective_from], [effective_to], [card_front], [card_back], [created_at], [updated_at], [deleted_at]) VALUES (N'35', N'82', N'BNK201900001', N'd d', N'1', N'2020-07-17', N'2021-07-17', N'/storage/images/card/nXH2YzAWoX.jpeg', N'/storage/images/card/LdOOz2MbEt.jpeg', N'2020-07-17 12:12:35.000', NULL, N'2020-07-18 10:19:45.000')
GO

INSERT INTO [dbo].[tpa_policy] ([id], [uid], [insurer_code], [insured_name], [policy_no], [effective_from], [effective_to], [card_front], [card_back], [created_at], [updated_at], [deleted_at]) VALUES (N'38', N'88', N'BNK201900002', N'นายทดสอบ ระบบประกัน', N'123456789', N'2020-07-24', N'2025-07-05', N'/storage/images/card/TIBAnt2zFu.jpeg', N'/storage/images/card/fvEujhKNaF.png', N'2020-07-17 15:09:56.000', N'2020-07-24 15:32:12.000', NULL)
GO

INSERT INTO [dbo].[tpa_policy] ([id], [uid], [insurer_code], [insured_name], [policy_no], [effective_from], [effective_to], [card_front], [card_back], [created_at], [updated_at], [deleted_at]) VALUES (N'39', N'88', N'TMP201900001', N'นายทดสอบ ระบบประกัน', N'123456789', N'2019-07-23', N'2019-07-24', N'/storage/images/card/jr0IeLYpQn.png', N'/storage/images/card/7E8rmchJvI.png', N'2020-07-17 15:11:33.000', N'2020-07-24 16:29:51.000', NULL)
GO

INSERT INTO [dbo].[tpa_policy] ([id], [uid], [insurer_code], [insured_name], [policy_no], [effective_from], [effective_to], [card_front], [card_back], [created_at], [updated_at], [deleted_at]) VALUES (N'40', N'88', N'TMP201900002', N'นางสาวน้ำมน คนงาม', N'987654321', N'2019-07-23', N'2019-07-24', N'/storage/images/card/kehAFg2azX.png', N'/storage/images/card/vxxkBjbbPC.png', N'2020-07-17 15:18:55.000', N'2020-07-24 16:30:09.000', NULL)
GO

INSERT INTO [dbo].[tpa_policy] ([id], [uid], [insurer_code], [insured_name], [policy_no], [effective_from], [effective_to], [card_front], [card_back], [created_at], [updated_at], [deleted_at]) VALUES (N'42', N'81', N'TMP201900002', N'นางสาวเสาวคน ท่าอากาศ', N'987654321', N'2014-07-20', N'2027-07-20', N'/storage/images/card/pI6diSOdQk.png', N'/storage/images/card/AE33OQFSh9.png', N'2020-07-17 15:50:52.000', N'2020-07-20 10:48:48.000', NULL)
GO

INSERT INTO [dbo].[tpa_policy] ([id], [uid], [insurer_code], [insured_name], [policy_no], [effective_from], [effective_to], [card_front], [card_back], [created_at], [updated_at], [deleted_at]) VALUES (N'43', N'76', N'TMP201900002', N'test testttt', N'1234567', N'2020-07-17', N'2020-07-17', N'/storage/images/card/RC9VhxR7BU.png', N'/storage/images/card/oLQZm0xw7J.png', N'2020-07-17 15:54:57.000', NULL, NULL)
GO

INSERT INTO [dbo].[tpa_policy] ([id], [uid], [insurer_code], [insured_name], [policy_no], [effective_from], [effective_to], [card_front], [card_back], [created_at], [updated_at], [deleted_at]) VALUES (N'44', N'81', N'TMP201900001', N'นางสาวน้ำมน รักชาติ', N'123456789', N'2014-07-20', N'2022-07-20', N'/storage/images/card/BqGYcS6i4J.jpeg', N'/storage/images/card/fjqMmgTeBS.jpeg', N'2020-07-20 10:47:33.000', NULL, NULL)
GO

INSERT INTO [dbo].[tpa_policy] ([id], [uid], [insurer_code], [insured_name], [policy_no], [effective_from], [effective_to], [card_front], [card_back], [created_at], [updated_at], [deleted_at]) VALUES (N'45', N'2', N'TMP201900001', N'นางสาวน้ำมน ไหว้สวย', N'123456789', N'2014-07-22', N'2021-07-22', N'/storage/images/card/Cpr9dEM8Ls.png', N'/storage/images/card/1qgTJwJTUA.png', N'2020-07-22 10:26:46.000', N'2020-07-30 17:34:53.000', NULL)
GO

INSERT INTO [dbo].[tpa_policy] ([id], [uid], [insurer_code], [insured_name], [policy_no], [effective_from], [effective_to], [card_front], [card_back], [created_at], [updated_at], [deleted_at]) VALUES (N'47', N'2', N'TMP201900001', N'Thasaphol Mungmee', N'12345678', N'2018-07-23', N'2020-12-03', N'/storage/images/card/FdEiQztRVN.jpeg', N'/storage/images/card/sBgpR7DWRF.jpeg', N'2020-07-23 18:15:04.000', N'2020-07-30 17:17:31.000', NULL)
GO

INSERT INTO [dbo].[tpa_policy] ([id], [uid], [insurer_code], [insured_name], [policy_no], [effective_from], [effective_to], [card_front], [card_back], [created_at], [updated_at], [deleted_at]) VALUES (N'48', N'2', N'TMP201900001', N'Test dd', N'12345678', N'2017-07-23', N'2020-07-23', N'/storage/images/card/cFCI1vFE64.jpeg', N'/storage/images/card/Jl97w6VaF9.jpeg', N'2020-07-23 18:16:23.000', N'2020-07-23 21:30:08.000', NULL)
GO

INSERT INTO [dbo].[tpa_policy] ([id], [uid], [insurer_code], [insured_name], [policy_no], [effective_from], [effective_to], [card_front], [card_back], [created_at], [updated_at], [deleted_at]) VALUES (N'49', N'2', N'BNK201900030', N'ศักดิ์ดา ศรีจำปา', N'123456789', N'2020-06-17', N'2023-06-17', N'/storage/images/card/4SvC2iVS7f.jpeg', N'/storage/images/card/M4p4Ks8qYX.jpeg', N'2020-07-23 20:43:55.000', NULL, N'2020-07-30 17:10:21.000')
GO

INSERT INTO [dbo].[tpa_policy] ([id], [uid], [insurer_code], [insured_name], [policy_no], [effective_from], [effective_to], [card_front], [card_back], [created_at], [updated_at], [deleted_at]) VALUES (N'50', N'2', N'BNK201900030', N'ศักดิ์ดา ศรีจำปา', N'123456789', N'2020-06-17', N'2023-06-17', N'/storage/images/card/ITl4VN6HEV.jpeg', N'/storage/images/card/Yypt9jBX1T.jpeg', N'2020-07-23 20:44:09.000', NULL, N'2020-07-30 17:09:32.000')
GO

INSERT INTO [dbo].[tpa_policy] ([id], [uid], [insurer_code], [insured_name], [policy_no], [effective_from], [effective_to], [card_front], [card_back], [created_at], [updated_at], [deleted_at]) VALUES (N'51', N'2', N'BNK201900030', N'ศักดิ์ดาศรีจำปา', N'123456789', N'2020-06-17', N'2023-06-17', N'/storage/images/card/ms6wzah3X7.jpeg', N'/storage/images/card/ZqLp95wYEg.jpeg', N'2020-07-23 20:44:39.000', N'2020-07-30 17:33:23.000', NULL)
GO

INSERT INTO [dbo].[tpa_policy] ([id], [uid], [insurer_code], [insured_name], [policy_no], [effective_from], [effective_to], [card_front], [card_back], [created_at], [updated_at], [deleted_at]) VALUES (N'55', N'88', N'BNK201900002', N'นายทดสอบ', N'123456', N'2016-07-26', N'2020-07-26', N'/storage/images/card/KFQCUVtby7.png', N'/storage/images/card/bRd5KFtKdA.png', N'2020-07-26 19:21:46.000', NULL, NULL)
GO

INSERT INTO [dbo].[tpa_policy] ([id], [uid], [insurer_code], [insured_name], [policy_no], [effective_from], [effective_to], [card_front], [card_back], [created_at], [updated_at], [deleted_at]) VALUES (N'56', N'85', N'TMP201900001', N'ทดสอบtest', N'23กa-&', N'2020-07-29', N'2021-07-29', N'/storage/images/card/u12WsTCOAG.png', N'/storage/images/card/KWd6KkRklv.png', N'2020-07-29 12:15:38.000', N'2020-07-29 13:45:38.000', NULL)
GO

INSERT INTO [dbo].[tpa_policy] ([id], [uid], [insurer_code], [insured_name], [policy_no], [effective_from], [effective_to], [card_front], [card_back], [created_at], [updated_at], [deleted_at]) VALUES (N'62', N'2', N'BNK201900001', N'namfon wan', N'123456789', N'2020-07-30', N'2021-07-30', N'/storage/images/card/gWEmI7frLa.png', N'/storage/images/card/9PCNwn8pJO.png', N'2020-07-30 22:39:13.000', N'2020-07-30 22:41:18.000', NULL)
GO

INSERT INTO [dbo].[tpa_policy] ([id], [uid], [insurer_code], [insured_name], [policy_no], [effective_from], [effective_to], [card_front], [card_back], [created_at], [updated_at], [deleted_at]) VALUES (N'64', N'88', N'TMP201900001', N'นายสดใส ชัยชนะ', N'12345670', N'2013-07-31', N'2020-07-31', N'/storage/images/card/9VkMAbNJ1R.png', N'/storage/images/card/tkH1MzjlF2.png', N'2020-07-31 12:00:55.000', NULL, NULL)
GO

INSERT INTO [dbo].[tpa_policy] ([id], [uid], [insurer_code], [insured_name], [policy_no], [effective_from], [effective_to], [card_front], [card_back], [created_at], [updated_at], [deleted_at]) VALUES (N'63', N'118', N'BNK201900030', N'ศักดิ์ดา ศรีจำปา', N'123456789', N'2020-06-17', N'2023-06-17', N'/storage/images/card/vjkktdt95k.jpeg', N'/storage/images/card/1ISMi6Oa78.jpeg', N'2020-07-31 11:06:02.000', NULL, NULL)
GO

SET IDENTITY_INSERT [dbo].[tpa_policy] OFF
GO

