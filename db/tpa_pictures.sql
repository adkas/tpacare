IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID(N'[dbo].[tpa_pictures]') AND type IN ('U'))
	DROP TABLE [dbo].[tpa_pictures]
GO

CREATE TABLE [dbo].[tpa_pictures] (
  [id] int  IDENTITY(1,1) NOT NULL,
  [ref_id] int DEFAULT ((0)) NOT NULL,
  [picture_type] int DEFAULT ((0)) NOT NULL,
  [path] nvarchar(150) COLLATE Thai_CI_AI  NOT NULL
)
GO

ALTER TABLE [dbo].[tpa_pictures] SET (LOCK_ESCALATION = TABLE)
GO
