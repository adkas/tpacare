IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID(N'[dbo].[tpa_company]') AND type IN ('U'))
	DROP TABLE [dbo].[tpa_company]
GO

CREATE TABLE [dbo].[tpa_company] (
  [id] int  IDENTITY(1,1) NOT NULL,
  [name] nvarchar(250) COLLATE Thai_CI_AI  NOT NULL,
  [policy_no] varchar(20) COLLATE Thai_CI_AI  NOT NULL,
  [created_at] datetime  NULL,
  [updated_at] datetime  NULL,
  [deleted_at] datetime  NULL
)
GO

ALTER TABLE [dbo].[tpa_company] SET (LOCK_ESCALATION = TABLE)
GO
