-- ----------------------------
-- Table structure for tpa_consent
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID(N'[dbo].[tpa_consent]') AND type IN ('U'))
	DROP TABLE [dbo].[tpa_consent]
GO

CREATE TABLE [dbo].[tpa_consent] (
  [consent_id] int  IDENTITY(1,1) NOT NULL,
  [agreement_id] int DEFAULT 0 NOT NULL,
  [uid] int DEFAULT 0 NOT NULL,
  [created_date] datetime  DEFAULT GETDATE()
)
GO

ALTER TABLE [dbo].[tpa_consent] SET (LOCK_ESCALATION = TABLE)
GO
