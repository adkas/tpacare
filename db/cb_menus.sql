/*
 Navicat Premium Data Transfer

 Source Server         : SQL Server
 Source Server Type    : SQL Server
 Source Server Version : 14001000
 Source Host           : localhost\SQLEXPRESS:1433
 Source Catalog        : tpacare
 Source Schema         : dbo

 Target Server Type    : SQL Server
 Target Server Version : 14001000
 File Encoding         : 65001

 Date: 10/05/2020 19:50:23
*/


-- ----------------------------
-- Table structure for cb_menus
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID(N'[dbo].[cb_menus]') AND type IN ('U'))
	DROP TABLE [dbo].[cb_menus]
GO

CREATE TABLE [dbo].[cb_menus] (
  [id] int  IDENTITY(1,1) NOT NULL,
  [name] nvarchar(255) COLLATE Thai_CI_AI  NOT NULL,
  [icon] varchar(150) COLLATE Thai_CI_AI  NOT NULL,
  [path] varchar(50) COLLATE Thai_CI_AI  NULL,
  [type] varchar(50) COLLATE Thai_CI_AI  NOT NULL,
  [sort_number] int DEFAULT ((0)) NOT NULL,
  [cb_modules_id] int DEFAULT NULL,
  [parent_cb_menus_id] int DEFAULT NULL
)
GO

ALTER TABLE [dbo].[cb_menus] SET (LOCK_ESCALATION = TABLE)
GO


-- ----------------------------
-- Records of cb_menus
-- ----------------------------
SET IDENTITY_INSERT [dbo].[cb_menus] ON
GO

INSERT INTO [dbo].[cb_menus] ([id], [name], [icon], [path], [type], [sort_number], [cb_modules_id], [parent_cb_menus_id]) VALUES (N'3', N'Hospital', N'fa fa-hospital-o', NULL, N'module', N'2', N'3', NULL)
GO

INSERT INTO [dbo].[cb_menus] ([id], [name], [icon], [path], [type], [sort_number], [cb_modules_id], [parent_cb_menus_id]) VALUES (N'4', N'Promotions', N'fa fa-gift', NULL, N'module', N'3', N'4', NULL)
GO

INSERT INTO [dbo].[cb_menus] ([id], [name], [icon], [path], [type], [sort_number], [cb_modules_id], [parent_cb_menus_id]) VALUES (N'5', N'Company', N'fa fa-institution', NULL, N'module', N'1', N'2', NULL)
GO

INSERT INTO [dbo].[cb_menus] ([id], [name], [icon], [path], [type], [sort_number], [cb_modules_id], [parent_cb_menus_id]) VALUES (N'6', N'Users', N'fa fa-user-circle', NULL, N'module', N'0', N'1', NULL)
GO

INSERT INTO [dbo].[cb_menus] ([id], [name], [icon], [path], [type], [sort_number], [cb_modules_id], [parent_cb_menus_id]) VALUES (N'7', N'Article', N'fa fa-edit', NULL, N'module', N'4', N'5', NULL)
GO

SET IDENTITY_INSERT [dbo].[cb_menus] OFF
GO


-- ----------------------------
-- Primary Key structure for table cb_menus
-- ----------------------------
ALTER TABLE [dbo].[cb_menus] ADD CONSTRAINT [cb_menus_PK] PRIMARY KEY CLUSTERED ([id])
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)  
ON [PRIMARY]
GO

