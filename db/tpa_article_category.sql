

-- ----------------------------
-- Table structure for tpa_article_category
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID(N'[dbo].[tpa_article_category]') AND type IN ('U'))
	DROP TABLE [dbo].[tpa_article_category]
GO

CREATE TABLE [dbo].[tpa_article_category] (
  [id] int  IDENTITY(1,1) NOT NULL,
  [name_th] nvarchar(150) COLLATE Thai_CI_AI  NOT NULL,
  [name_en] nvarchar(150) COLLATE Thai_CI_AI  NOT NULL,
  [status] int DEFAULT 1 NOT NULL,
);
GO

ALTER TABLE [dbo].[tpa_article_category] SET (LOCK_ESCALATION = TABLE)
GO
