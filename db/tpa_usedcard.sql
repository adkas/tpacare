IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID(N'[dbo].[tpa_usedcard]') AND type IN ('U'))
	DROP TABLE [dbo].[tpa_usedcard]
GO

CREATE TABLE [dbo].[tpa_usedcard] (
  [id] int  IDENTITY(1,1) NOT NULL,
  [uid] int DEFAULT ((0)) NOT NULL,
  [citizen_id] varchar(13) COLLATE Thai_CI_AI  NOT NULL,
  [policy_no] varchar(20) COLLATE Thai_CI_AI  NOT NULL,
  [name] nvarchar(50) COLLATE Thai_CI_AI  NOT NULL,
  [surname] nvarchar(50) COLLATE Thai_CI_AI  NOT NULL,
  [provider_code] varchar(20) COLLATE Thai_CI_AI  NOT NULL,
  [insurer_code] varchar(20) COLLATE Thai_CI_AI  NOT NULL,
  [username] varchar(20) COLLATE Thai_CI_AI  NOT NULL,
  [record_id] varchar(20) COLLATE Thai_CI_AI  NOT NULL,
  [used_date] datetime  DEFAULT GETDATE()
)
GO

ALTER TABLE [dbo].[tpa_usedcard] SET (LOCK_ESCALATION = TABLE)
GO
