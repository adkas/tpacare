/*
 Navicat Premium Data Transfer

 Source Server         : tpacare
 Source Server Type    : SQL Server
 Source Server Version : 12002000
 Source Host           : tpacare-v2-db.database.windows.net:1433
 Source Catalog        : tpacare-v2-dev
 Source Schema         : dbo

 Target Server Type    : SQL Server
 Target Server Version : 12002000
 File Encoding         : 65001

 Date: 20/07/2020 15:15:41
*/


-- ----------------------------
-- Table structure for tpa_more_attachment
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID(N'[dbo].[tpa_more_attachment]') AND type IN ('U'))
	DROP TABLE [dbo].[tpa_more_attachment]
GO

CREATE TABLE [dbo].[tpa_more_attachment] (
  [id] int  IDENTITY(1,1) NOT NULL,
  [uid] int DEFAULT ((0)) NOT NULL,
  [policy_no] varchar(20) COLLATE Thai_CI_AI  NULL,
  [claim_no] varchar(20) COLLATE Thai_CI_AI  NULL,
  [status_no] int DEFAULT ((0)) NOT NULL,
  [attachment_type] int DEFAULT ((0)) NOT NULL,
  [file] nvarchar(150) COLLATE Thai_CI_AI  NOT NULL,
  [created_at] datetime DEFAULT (getdate()) NULL
)
GO


-- ----------------------------
-- Primary Key structure for table tpa_more_attachment
-- ----------------------------
ALTER TABLE [dbo].[tpa_more_attachment] ADD CONSTRAINT [PK__tpa_more__3213E83FE4D2ABF0] PRIMARY KEY CLUSTERED ([id])
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
GO

