-- ----------------------------
-- Table structure for tpa_banner
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID(N'[dbo].[tpa_banner]') AND type IN ('U'))
	DROP TABLE [dbo].[tpa_banner]
GO

CREATE TABLE [dbo].[tpa_banner] (
  [banner_id] int  IDENTITY(1,1) NOT NULL,
  [banner_title_th] nvarchar(250) COLLATE Thai_CI_AI  NOT NULL,
  [banner_title_en] nvarchar(250) COLLATE Thai_CI_AI  NOT NULL,
  [banner_picture] nvarchar(150) COLLATE Thai_CI_AI  NOT NULL,
  [banner_link] nvarchar(150) COLLATE Thai_CI_AI  NOT NULL,
  [banner_type] nvarchar(15) COLLATE Thai_CI_AI  NULL,
  [ref_id] int DEFAULT 0 NOT NULL,
  [banner_status] int DEFAULT 0 NOT NULL,
  [updated_at] datetime  NULL,
  [deleted_at] datetime  NULL
)
GO

ALTER TABLE [dbo].[tpa_banner] SET (LOCK_ESCALATION = TABLE)
GO
