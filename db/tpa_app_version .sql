
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID(N'[dbo].[tpa_app_version ]') AND type IN ('U'))
	DROP TABLE [dbo].[tpa_app_version ]
GO

CREATE TABLE [dbo].[tpa_app_version ] (
  [id] int  IDENTITY(1,1) NOT NULL,
  [platform] varchar(255) COLLATE Thai_CI_AI  NULL,
  [current_version] varchar(255) COLLATE Thai_CI_AI  NULL,
  [force_version] nvarchar(255) COLLATE Thai_CI_AI  NULL,
  [created_date] datetime  NULL,
  [updated_date] datetime  NULL
)
GO

ALTER TABLE [dbo].[tpa_app_version ] SET (LOCK_ESCALATION = TABLE)
GO


