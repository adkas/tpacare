IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID(N'[dbo].[tpa_policy_benefit]') AND type IN ('U'))
	DROP TABLE [dbo].[tpa_policy_benefit]
GO

CREATE TABLE [dbo].[tpa_policy_benefit] (
  [id] int  IDENTITY(1,1) NOT NULL,
  [policy_id] int DEFAULT ((0)) NOT NULL,
  [cov_desc] nvarchar(250) COLLATE Thai_CI_AI  NOT NULL,
  [cov_limit] varchar(30) COLLATE Thai_CI_AI  NOT NULL,
)
GO

ALTER TABLE [dbo].[tpa_policy_benefit] SET (LOCK_ESCALATION = TABLE)
GO
