

-- ----------------------------
-- Table structure for tpa_config
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID(N'[dbo].[tpa_config]') AND type IN ('U'))
	DROP TABLE [dbo].[tpa_config]
GO

CREATE TABLE [dbo].[tpa_config] (
  [id] int  IDENTITY(1,1) NOT NULL,
  [config_key] varchar(30) COLLATE Thai_CI_AI  NOT NULL,
  [config_val] varchar(150) COLLATE Thai_CI_AI  NOT NULL
);
GO

ALTER TABLE [dbo].[tpa_config] SET (LOCK_ESCALATION = TABLE)
GO

-- ----------------------------
-- Records of tpa_config
-- ----------------------------
SET IDENTITY_INSERT [dbo].[tpa_config] ON
GO

INSERT INTO [dbo].[tpa_config] (id, config_key, config_val) VALUES (1, 'rendotp_minute', '5');

SET IDENTITY_INSERT [dbo].[tpa_config] OFF
GO
