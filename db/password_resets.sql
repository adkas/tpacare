
-- ----------------------------
-- Table structure for password_resets
-- ----------------------------
CREATE TABLE password_resets (
	email varchar(255) NOT NULL,
	token varchar(255) NOT NULL,
  created_at datetime NULL
);

CREATE INDEX password_resets_email_index
   ON password_resets ( email );
