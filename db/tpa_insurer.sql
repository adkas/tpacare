/*
 Navicat Premium Data Transfer

 Source Server         : tpacare
 Source Server Type    : SQL Server
 Source Server Version : 12002000
 Source Host           : tpacare-v2-db.database.windows.net:1433
 Source Catalog        : tpacare-v2-dev
 Source Schema         : dbo

 Target Server Type    : SQL Server
 Target Server Version : 12002000
 File Encoding         : 65001

 Date: 03/08/2020 10:10:36
*/


-- ----------------------------
-- Table structure for tpa_insurer
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID(N'[dbo].[tpa_insurer]') AND type IN ('U'))
	DROP TABLE [dbo].[tpa_insurer]
GO

CREATE TABLE [dbo].[tpa_insurer] (
  [id] int  IDENTITY(1,1) NOT NULL,
  [insurer_code] varchar(20) COLLATE Thai_CI_AI  NULL,
  [name_th] nvarchar(255) COLLATE Thai_CI_AI  NULL,
  [name_en] nvarchar(255) COLLATE Thai_CI_AI  NULL,
  [short_name] nvarchar(30) COLLATE Thai_CI_AI  NULL,
  [website] nvarchar(100) COLLATE Thai_CS_AI  NULL,
  [insurer_type] int  NULL,
  [background] varchar(255) COLLATE Thai_CI_AS  NULL,
  [updated_at] datetime  NULL,
  [updated_by] int DEFAULT ((0)) NULL
)
GO

SET IDENTITY_INSERT [dbo].[tpa_insurer]  ON
GO
-- ----------------------------
-- Records of tpa_insurer
-- ----------------------------
INSERT INTO [dbo].[tpa_insurer]  VALUES (N'1', N'TMP201900001', N'บริษัท กรุงเทพประกันชีวิต จำกัด (มหาชน)', N'Bangkok Life Assurance Company Limited', N'BLA', N'www.bla.co.th', N'1', N'https://via.placeholder.com/800x600/DAF7A6/FFFFFF?text=Bangkok Life Assurance Company Limited', NULL, NULL)
GO

INSERT INTO [dbo].[tpa_insurer]  VALUES (N'2', N'TMP201900002', N'บริษัท กรุงไทย-แอกซ่า ประกันชีวิต จำกัด (มหาชน)', N'Krungthai-AXA Life Insurance Public Company Limited', N'KTAL', N'www.krungthai-axa.co.th', N'1', N'https://via.placeholder.com/800x600/DAF7A6/FFFFFF?text=Krungthai-AXA Life Insurance Public Company Limited', NULL, NULL)
GO

INSERT INTO [dbo].[tpa_insurer]  VALUES (N'3', N'TMP201900003', N'บริษัท เจนเนอราลี่ ประกันชีวิต (ไทยแลนด์) จำกัด (มหาชน)', N'Generali Life Assurance (Thailand) Public Company Limited', N'Generali Life', N'www.generali.co.th', N'1', N'https://via.placeholder.com/800x600/DAF7A6/FFFFFF?text=Generali Life Assurance (Thailand) Public Company Limited', NULL, NULL)
GO

INSERT INTO [dbo].[tpa_insurer]  VALUES (N'4', N'INS201600027', N'บริษัท ชับบ์ ไลฟ์ แอสชัวรันซ์ จำกัด (มหาชน)', N'Chubb Life Assurance Public Company Limited', N'CBL', N'www.chubb.com/th', N'1', N'https://via.placeholder.com/800x600/DAF7A6/FFFFFF?text=Chubb Life Assurance Public Company Limited', NULL, NULL)
GO

INSERT INTO [dbo].[tpa_insurer]  VALUES (N'5', N'TMP201900004', N'บริษัท โตเกียวมารีนประกันชีวิต (ประเทศไทย) จำกัด (มหาชน)', N'Tokio Marine Life Insurance (Thailand) Public Company Limited', N'TML', N'www.tokiomarine.co.th/', N'1', N'https://via.placeholder.com/800x600/DAF7A6/FFFFFF?text=Tokio Marine Life Insurance (Thailand) Public Company Limited', NULL, NULL)
GO

INSERT INTO [dbo].[tpa_insurer]  VALUES (N'6', N'INS201600021', N'บริษัท ทิพยประกันชีวิต จำกัด (มหาชน)', N'Dhipaya Life Assurance Public Company Limited', N'DHIPAYA', N'www.dhipayalife.co.th', N'1', N'https://via.placeholder.com/800x600/DAF7A6/FFFFFF?text=Dhipaya Life Assurance Public Company Limited', NULL, NULL)
GO

INSERT INTO [dbo].[tpa_insurer]  VALUES (N'7', N'TMP201900005', N'บริษัท ไทยคาร์ดิฟ ประกันชีวิต จำกัด (มหาชน)', N'Thai Cardif Assurance Public Company Limited', N'Thai Cardif', N'www.thaicardif.com', N'1', N'https://via.placeholder.com/800x600/DAF7A6/FFFFFF?text=Thai Cardif Assurance Public Company Limited', NULL, NULL)
GO

INSERT INTO [dbo].[tpa_insurer]  VALUES (N'8', N'TMP201900006', N'บริษัท ไทยซัมซุง ประกันชีวิต จำกัด (มหาชน)', N'Thai Samsung Life Insurance Public Company Limited', N'TSLI', N'www.thaisamsunglife.co.th', N'1', N'https://via.placeholder.com/800x600/DAF7A6/FFFFFF?text=Thai Samsung Life Insurance Public Company Limited', NULL, NULL)
GO

INSERT INTO [dbo].[tpa_insurer]  VALUES (N'9', N'INS201600006', N'บริษัท ไทยประกันชีวิต จำกัด (มหาชน)', N'Thai Life Insurance Public Company Limited', N'TLI', N'www.thailife.com', N'1', N'https://via.placeholder.com/800x600/DAF7A6/FFFFFF?text=Thai Life Insurance Public Company Limited', NULL, NULL)
GO

INSERT INTO [dbo].[tpa_insurer]  VALUES (N'10', N'TMP201900007', N'บริษัท ไทยพาณิชย์ประกันชีวิต จำกัด (มหาชน)', N'SCB Life Assurance Public Company Limited', N'SCBLIFE', N'www.scblife.co.th', N'1', N'https://via.placeholder.com/800x600/DAF7A6/FFFFFF?text=SCB Life Assurance Public Company Limited', NULL, NULL)
GO

INSERT INTO [dbo].[tpa_insurer]  VALUES (N'11', N'TMP201900008', N'บริษัท ไทยรีประกันชีวิต จำกัด (มหาชน)', N'THAIRE Life Assurance Public Company Limited (Thailand)', N'THREL', N'www.thairelife.co.th', N'1', N'https://via.placeholder.com/800x600/DAF7A6/FFFFFF?text=THAIRE Life Assurance Public Company Limited (Thailand)', NULL, NULL)
GO

INSERT INTO [dbo].[tpa_insurer]  VALUES (N'12', N'TMP201900009', N'บริษัท ไทยสมุทรประกันชีวิต จำกัด (มหาชน)', N'Ocean Life Insurance Public Company Limited', N'OLIC', N'www.ocean.co.th', N'1', N'https://via.placeholder.com/800x600/DAF7A6/FFFFFF?text=Ocean Life Insurance Public Company Limited', NULL, NULL)
GO

INSERT INTO [dbo].[tpa_insurer]  VALUES (N'13', N'TMP201900010', N'บริษัท ธนชาตประกันชีวิต จำกัด (มหาชน)', N'Thanachart Life Assurance Public Company Limited', N'ZNLA', N'http://www.thanachart.co.th', N'1', N'https://via.placeholder.com/800x600/DAF7A6/FFFFFF?text=Thanachart Life Assurance Public Company Limited', NULL, NULL)
GO

INSERT INTO [dbo].[tpa_insurer]  VALUES (N'14', N'TMP201900011', N'บริษัท บางกอกสหประกันชีวิต จำกัด (มหาชน)', N'BUI Life Insurance Public Company Limited', N'BUI life', N'www.builife.com', N'1', N'https://via.placeholder.com/800x600/DAF7A6/FFFFFF?text=BUI Life Insurance Public Company Limited', NULL, NULL)
GO

INSERT INTO [dbo].[tpa_insurer]  VALUES (N'15', N'TMP201900012', N'บริษัท ประกันชีวิตนครหลวงไทย จำกัด (มหาชน)', N'Siam City Life Assurance Public Company Limited', N'SIAM CITY LIFE ASSUR', N'www.scilife.co.th', N'1', N'https://via.placeholder.com/800x600/DAF7A6/FFFFFF?text=Siam City Life Assurance Public Company Limited', NULL, NULL)
GO

INSERT INTO [dbo].[tpa_insurer]  VALUES (N'16', N'INS201600028', N'บริษัท พรูเด็นเชียล ประกันชีวิต (ประเทศไทย) จำกัด (มหาชน)', N'Prudential Life Assurance (Thailand) Co.,Ltd', N'PTSL', N'www.prudential.co.th', N'1', N'https://via.placeholder.com/800x600/DAF7A6/FFFFFF?text=Prudential Life Assurance (Thailand) Co.,Ltd', NULL, NULL)
GO

INSERT INTO [dbo].[tpa_insurer]  VALUES (N'17', N'TMP201900013', N'บริษัท ฟิลลิปประกันชีวิต จำกัด (มหาชน)', N'Philip Life Assurance Public Company Limited', N'PLA', N'www.Philliplife.com', N'1', N'https://via.placeholder.com/800x600/DAF7A6/FFFFFF?text=Philip Life Assurance Public Company Limited', NULL, NULL)
GO

INSERT INTO [dbo].[tpa_insurer]  VALUES (N'18', N'TMP201900014', N'บริษัท เมืองไทยประกันชีวิต จำกัด (มหาชน)', N'Muang Thai Life Assurance Public Company Limited', N'MTI', N'www.muangthai.co.th', N'1', N'https://via.placeholder.com/800x600/DAF7A6/FFFFFF?text=Muang Thai Life Assurance Public Company Limited', NULL, NULL)
GO

INSERT INTO [dbo].[tpa_insurer]  VALUES (N'19', N'INS201600019', N'บริษัท แมนูไลฟ์  ประกันชีวิต (ประเทศไทย) จำกัด (มหาชน)', N'Manulife Insurance (Thailand) Public Company Limited', N'Manulife', N'www.manulife.co.th', N'1', N'https://via.placeholder.com/800x600/DAF7A6/FFFFFF?text=Manulife Insurance (Thailand) Public Company Limited', NULL, NULL)
GO

INSERT INTO [dbo].[tpa_insurer]  VALUES (N'20', N'TMP201900015', N'บริษัท สหประกันชีวิต จำกัด (มหาชน)', N'Union Life Insurance Public Company Limited', N'UNION LIFE', N'www.ulife.in.th', N'1', N'https://via.placeholder.com/800x600/DAF7A6/FFFFFF?text=Union Life Insurance Public Company Limited', NULL, NULL)
GO

INSERT INTO [dbo].[tpa_insurer]  VALUES (N'21', N'TMP201900016', N'บริษัท อลิอันซ์ อยุธยา ประกันชีวิต จำกัด (มหาชน)', N'Allianz Ayudhya Assurance Public Company Limited', N'AZAY', N'www.azay.co.th', N'1', N'https://via.placeholder.com/800x600/DAF7A6/FFFFFF?text=Allianz Ayudhya Assurance Public Company Limited', NULL, NULL)
GO

INSERT INTO [dbo].[tpa_insurer]  VALUES (N'22', N'TMP201900017', N'บริษัท อาคเนย์ประกันชีวิต จำกัด (มหาชน)', N'Southeast Life Insurance Public Company Limited', N'SOUT', N'www.southeastlife.com', N'1', N'https://via.placeholder.com/800x600/DAF7A6/FFFFFF?text=Southeast Life Insurance Public Company Limited', NULL, NULL)
GO

INSERT INTO [dbo].[tpa_insurer]  VALUES (N'23', N'INS201600020', N'บริษัท เอฟดับบลิวดี ประกันชีวิต จำกัด (มหาชน)', N'FWD Life Insurance Public Company Limited', N'FWD', N'www.fwd.co.th', N'1', N'https://via.placeholder.com/800x600/DAF7A6/FFFFFF?text=FWD Life Insurance Public Company Limited', NULL, NULL)
GO

INSERT INTO [dbo].[tpa_insurer]  VALUES (N'24', N'TMP201900018', N'บริษัท เอไอเอ จำกัด', N'AIA Company Limited', N'AIA', N'www.aia.co.th', N'1', N'https://via.placeholder.com/800x600/DAF7A6/FFFFFF?text=AIA Company Limited', NULL, NULL)
GO

INSERT INTO [dbo].[tpa_insurer]  VALUES (N'25', N'INS201600016', N'บริษัท เอ็ม บี เค ไลฟ์ ประกันชีวิต จำกัด (มหาชน)', N'MBK Life Assurance Public Company Limited', N'MBK', N'www.mbklife.co.th', N'1', N'https://via.placeholder.com/800x600/DAF7A6/FFFFFF?text=MBK Life Assurance Public Company Limited', NULL, NULL)
GO

INSERT INTO [dbo].[tpa_insurer]  VALUES (N'26', N'TMP201900019', N'บริษัท แอ๊ดวานซ์ ไลฟ์ ประกันชีวิต จำกัด (มหาชน)', N'Advance Life Assurance Public Company Limited', N'AL', N'www.alife.co.th', N'1', N'https://via.placeholder.com/800x600/DAF7A6/FFFFFF?text=Advance Life Assurance Public Company Limited', NULL, NULL)
GO

INSERT INTO [dbo].[tpa_insurer]  VALUES (N'27', N'INS201800003', N'TKI LIFE INSURANCE COMPANY LIMITED', N'TKI LIFE INSURANCE COMPANY LIMITED', N'TKI', N' ', N'1', N'https://via.placeholder.com/800x600/DAF7A6/FFFFFF?text=TKI LIFE INSURANCE COMPANY LIMITED', NULL, NULL)
GO

INSERT INTO [dbo].[tpa_insurer]  VALUES (N'28', N'INS201600023', N'บริษัท กรุงเทพประกันภัย จำกัด (มหาชน)', N'Bangkok Insurance Public Co., Ltd.', N'BKI', N'www.bangkokinsurance.com', N'2', N'https://via.placeholder.com/800x600/DAF7A6/FFFFFF?text=Bangkok Insurance Public Co., Ltd.', NULL, NULL)
GO

INSERT INTO [dbo].[tpa_insurer]  VALUES (N'29', N'INS201600015', N'บริษัท กรุงเทพประกันสุขภาพ จำกัด (มหาชน)', N'Bangkok Health Insurance Public Company Limited', N'BHI', N'www.bhi.co.th', N'2', N'https://via.placeholder.com/800x600/DAF7A6/FFFFFF?text=Bangkok Health Insurance Public Company Limited', NULL, NULL)
GO

INSERT INTO [dbo].[tpa_insurer]  VALUES (N'30', N'TMP201900020', N'บริษัท กรุงไทยพานิชประกันภัย จำกัด (มหาชน)', N'Krungthai Panich Insurance Public Company Limited', N'KPI', N'www.kpi.co.th', N'2', N'https://via.placeholder.com/800x600/DAF7A6/FFFFFF?text=Krungthai Panich Insurance Public Company Limited', NULL, NULL)
GO

INSERT INTO [dbo].[tpa_insurer]  VALUES (N'31', N'TMP201900021', N'บริษัท กลางคุ้มครองผู้ประสบภัยจากรถ จำกัด', N'Road Accident Victims Protection Co., Ltd.', N'RVP', N'ww.rvp.co.th', N'2', N'https://via.placeholder.com/800x600/DAF7A6/FFFFFF?text=Road Accident Victims Protection Co., Ltd.', NULL, NULL)
GO

INSERT INTO [dbo].[tpa_insurer]  VALUES (N'32', N'INS201600005', N'บริษัท คิง ไว  ประกันภัย จำกัด (มหาชน)', N'King Wai Group (Thailand) Public Company Limited', N'KWG', N'www.kwgi.co.th', N'2', N'https://via.placeholder.com/800x600/DAF7A6/FFFFFF?text=King Wai Group (Thailand) Public Company Limited', NULL, NULL)
GO

INSERT INTO [dbo].[tpa_insurer]  VALUES (N'33', N'INS201600030', N'บริษัท เคเอสเค ประกันภัย (ประเทศไทย) จำกัด (มหาชน)', N'KSK Insurance (Thailand) Public Company Limited', N'KSK', N'http://www.kskinsurance.co.th', N'2', N'https://via.placeholder.com/800x600/DAF7A6/FFFFFF?text=KSK Insurance (Thailand) Public Company Limited', NULL, NULL)
GO

INSERT INTO [dbo].[tpa_insurer]  VALUES (N'34', N'INS201700002', N'บริษัท จรัญประกันภัย จำกัด (มหาชน)', N'Charan Insurance Public Co., Ltd.', N'CHR', N'www.charaninsurance.com', N'2', N'https://via.placeholder.com/800x600/DAF7A6/FFFFFF?text=Charan Insurance Public Co., Ltd.', NULL, NULL)
GO

INSERT INTO [dbo].[tpa_insurer]  VALUES (N'35', N'TMP201900022', N'บริษัท เจนเนอราลี่ ประกันภัย (ไทยแลนด์) จำกัด (มหาชน)', N'Generali Insurance (Thailand) Public Company Limited', N'PIC', N'www.generali.co.th', N'2', N'https://via.placeholder.com/800x600/DAF7A6/FFFFFF?text=Generali Insurance (Thailand) Public Company Limited', NULL, NULL)
GO

INSERT INTO [dbo].[tpa_insurer]  VALUES (N'36', N'TMP201900023', N'บริษัท เจ้าพระยาประกันภัย จำกัด (มหาชน)', N'Chao Phaya Insurance Co.,Ltd.', N'CPY', N'www.cpyins.com', N'2', N'https://via.placeholder.com/800x600/DAF7A6/FFFFFF?text=Chao Phaya Insurance Co.,Ltd.', NULL, NULL)
GO

INSERT INTO [dbo].[tpa_insurer]  VALUES (N'37', N'TMP201900024', N'บริษัท ไชน่าอินชัวรันส์ (ไทย) จำกัด (มหาชน)', N'China Insurance (Thai) Public Company Limited', N'CIC', N'www.chinains.co.th', N'2', N'https://via.placeholder.com/800x600/DAF7A6/FFFFFF?text=China Insurance (Thai) Public Company Limited', NULL, NULL)
GO

INSERT INTO [dbo].[tpa_insurer]  VALUES (N'38', N'INS201600012', N'บริษัท ซิกน่า ประกันภัย จำกัด (มหาชน)', N'Cigna Insurance Publice Company Limited', N'CIGNA', N'www.cigna.co.th', N'2', N'https://via.placeholder.com/800x600/DAF7A6/FFFFFF?text=Cigna Insurance Publice Company Limited', NULL, NULL)
GO

INSERT INTO [dbo].[tpa_insurer]  VALUES (N'39', N'INS201600007', N'บริษัท โตเกียวมารีนประกันภัย (ประเทศไทย) จำกัด (มหาชน)', N'Tokio Marine Insurance (Thailand) Public Company Limited', N'TMI', N'www.tokiomarine.co.th', N'2', N'https://via.placeholder.com/800x600/DAF7A6/FFFFFF?text=Tokio Marine Insurance (Thailand) Public Company Limited', NULL, NULL)
GO

INSERT INTO [dbo].[tpa_insurer]  VALUES (N'40', N'INS201600024', N'บริษัท ทิพยประกันภัย จำกัด (มหาชน)', N'Dhipaya Insurance Public Co., Ltd.', N'DHP', N'www.dhipaya.co.th', N'2', N'https://via.placeholder.com/800x600/DAF7A6/FFFFFF?text=Dhipaya Insurance Public Co., Ltd.', NULL, NULL)
GO

INSERT INTO [dbo].[tpa_insurer]  VALUES (N'41', N'TMP201900025', N'บริษัท ทูนประกันภัย จำกัด (มหาชน)', N'Tune Insurance Public Company Limited', N'Tune', N'www.tuneinsurance.com', N'2', N'https://via.placeholder.com/800x600/DAF7A6/FFFFFF?text=Tune Insurance Public Company Limited', NULL, NULL)
GO

INSERT INTO [dbo].[tpa_insurer]  VALUES (N'42', N'TMP201900026', N'บริษัท เทเวศประกันภัย จำกัด (มหาชน)', N'The Deves Insurance Public Co., Ltd.', N'DVS', N'www.deves.co.th', N'2', N'https://via.placeholder.com/800x600/DAF7A6/FFFFFF?text=The Deves Insurance Public Co., Ltd.', NULL, NULL)
GO

INSERT INTO [dbo].[tpa_insurer]  VALUES (N'43', N'INS201600018', N'บริษัท ไทยประกันภัย จำกัด (มหาชน)', N'Thai Insurance Public Co., Ltd.', N'TIC', N'www.thaiins.com', N'2', N'https://via.placeholder.com/800x600/DAF7A6/FFFFFF?text=Thai Insurance Public Co., Ltd.', NULL, NULL)
GO

INSERT INTO [dbo].[tpa_insurer]  VALUES (N'44', N'TMP201900027', N'บริษัท ไทยประกันสุขภาพ จำกัด (มหาชน)', N'Thai Health Insurance Public Company Limited', NULL, N'www.thaihealth.co.th', N'2', N'https://via.placeholder.com/800x600/DAF7A6/FFFFFF?text=Thai Health Insurance Public Company Limited', NULL, NULL)
GO

INSERT INTO [dbo].[tpa_insurer]  VALUES (N'45', N'TMP201900028', N'บริษัท ไทยพัฒนาประกันภัย จำกัด (มหาชน)', N'The Thai United Insurance Public Company Limited', N'TPI', N'www.thaipat.co.th', N'2', N'https://via.placeholder.com/800x600/DAF7A6/FFFFFF?text=The Thai United Insurance Public Company Limited', NULL, NULL)
GO

INSERT INTO [dbo].[tpa_insurer]  VALUES (N'46', N'TMP201900029', N'บริษัท ไทยไพบูลย์ประกันภัย จำกัด (มหาชน)', N'Thai Paiboon Insurance Public Company Limited', N'TPI', N'www.thaipaiboon.com', N'2', N'https://via.placeholder.com/800x600/DAF7A6/FFFFFF?text=Thai Paiboon Insurance Public Company Limited', NULL, NULL)
GO

INSERT INTO [dbo].[tpa_insurer]  VALUES (N'47', N'TMP201900030', N'บริษัท ไทยรับประกันภัยต่อ จำกัด (มหาชน)', N'Thai Reinsurance Public Company Limited (Thailand)', N'THRE', N'www.thaire.co.th', N'2', N'https://via.placeholder.com/800x600/DAF7A6/FFFFFF?text=Thai Reinsurance Public Company Limited (Thailand)', NULL, NULL)
GO

INSERT INTO [dbo].[tpa_insurer]  VALUES (N'48', N'INS201600009', N'บริษัท ไทยศรีประกันภัย จำกัด (มหาชน)', N'Thaisri Insurance Co.,Ltd', N'TMW', N'www.thaisri.com', N'2', N'https://via.placeholder.com/800x600/DAF7A6/FFFFFF?text=Thaisri Insurance Co.,Ltd', NULL, NULL)
GO

INSERT INTO [dbo].[tpa_insurer]  VALUES (N'49', N'TMP201900031', N'บริษัท ไทยเศรษฐกิจประกันภัย จำกัด (มหาชน)', N'Thai Setakij Insurance Public Co., Td.', N'TSK', N'www.tsi.co.th', N'2', N'https://via.placeholder.com/800x600/DAF7A6/FFFFFF?text=Thai Setakij Insurance Public Co., Td.', NULL, NULL)
GO

INSERT INTO [dbo].[tpa_insurer]  VALUES (N'50', N'INS201600022', N'บริษัท ธนชาตประกันภัย จำกัด (มหาชน)', N'Thanachart Insurance Public Company Limited', N'TNI', N'www.thanachartinsurance.co.th', N'2', N'https://via.placeholder.com/800x600/DAF7A6/FFFFFF?text=Thanachart Insurance Public Company Limited', NULL, NULL)
GO

INSERT INTO [dbo].[tpa_insurer]  VALUES (N'51', N'INS201600010', N'บริษัท นวกิจประกันภัย จำกัด (มหาชน)', N'Navakij Insurance Public Co.,Ltd.', N'NKI', N'www.navakij.co.th', N'2', N'https://via.placeholder.com/800x600/DAF7A6/FFFFFF?text=Navakij Insurance Public Co.,Ltd.', NULL, NULL)
GO

INSERT INTO [dbo].[tpa_insurer]  VALUES (N'52', N'TMP201900032', N'บริษัท นำสินประกันภัย จำกัด (มหาชน)', N'Nam Seng Insurance (Public) Co., Ltd.', N'NSI', N'www.namsengins.co.th', N'2', N'https://via.placeholder.com/800x600/DAF7A6/FFFFFF?text=Nam Seng Insurance (Public) Co., Ltd.', NULL, NULL)
GO

INSERT INTO [dbo].[tpa_insurer]  VALUES (N'53', N'TMP201900033', N'บริษัท นิวอินเดีย แอสชัวรันซ์ จำกัด', N'The New India Assurance Co., Ltd.', N'NIA', N'http://newindia.co.in', N'2', N'https://via.placeholder.com/800x600/DAF7A6/FFFFFF?text=The New India Assurance Co., Ltd.', NULL, NULL)
GO

INSERT INTO [dbo].[tpa_insurer]  VALUES (N'54', N'TMP201900034', N'บริษัท นิวแฮมพ์เชอร์อินชัวรันส์ ', N'New Hampshire Insurance Company', N'NHI', N'www.aiggeneral.co.th', N'2', N'https://via.placeholder.com/800x600/DAF7A6/FFFFFF?text=New Hampshire Insurance Company', NULL, NULL)
GO

INSERT INTO [dbo].[tpa_insurer]  VALUES (N'55', N'TMP201900035', N'บริษัท บางกอกสหประกันภัย จำกัด (มหาชน)', N'Bangkok Union Insurance Public Co., Ltd.', N'BUN', N'www.bui.co.th', N'2', N'https://via.placeholder.com/800x600/DAF7A6/FFFFFF?text=Bangkok Union Insurance Public Co., Ltd.', NULL, NULL)
GO

INSERT INTO [dbo].[tpa_insurer]  VALUES (N'56', N'TMP201900036', N'บริษัท เอ็ทน่า ประกันสุขภาพ (ประเทศไทย) จำกัด (มหาชน)', N'Aetna Health Insurance (Thailand) Public Company Limited', N' ', N'www.aetna.co.th', N'2', N'https://via.placeholder.com/800x600/DAF7A6/FFFFFF?text=Aetna Health Insurance (Thailand) Public Company Limited', NULL, NULL)
GO

INSERT INTO [dbo].[tpa_insurer]  VALUES (N'57', N'INS201700001', N'บริษัท ประกันคุ้มภัย จำกัด (มหาชน)', N'Safety Insurance Public Co., Ltd.', N'STY', N'www.nzi.co.th', N'2', N'https://via.placeholder.com/800x600/DAF7A6/FFFFFF?text=Safety Insurance Public Co., Ltd.', NULL, NULL)
GO

INSERT INTO [dbo].[tpa_insurer]  VALUES (N'58', N'TMP201900037', N'บริษัท ประกันภัยไทยวิวัฒน์ จำกัด (มหาชน)', N'Thaivivat Insurance Public Co.,Ltd.', N'TVI', N'www.thaivivat.co.th', N'2', N'https://via.placeholder.com/800x600/DAF7A6/FFFFFF?text=Thaivivat Insurance Public Co.,Ltd.', NULL, NULL)
GO

INSERT INTO [dbo].[tpa_insurer]  VALUES (N'59', N'TMP201900038', N'บริษัท แปซิฟิค ครอส ประกันสุขภาพ จำกัด (มหาชน)', N'Pacific Cross Health Insurance Public Company Limited', N'PCHI', N'www.pacificcrosshealth.com', N'2', N'https://via.placeholder.com/800x600/DAF7A6/FFFFFF?text=Pacific Cross Health Insurance Public Company Limited', NULL, NULL)
GO

INSERT INTO [dbo].[tpa_insurer]  VALUES (N'60', N'TMP201900039', N'บริษัท พุทธธรรมประกันภัย จำกัด (มหาชน)', N'Phutthatham Insurance Public Company Limited', N'PTI', N'www.phutthatham.com', N'2', N'https://via.placeholder.com/800x600/DAF7A6/FFFFFF?text=Phutthatham Insurance Public Company Limited', NULL, NULL)
GO

INSERT INTO [dbo].[tpa_insurer]  VALUES (N'61', N'INS201800001', N'บริษัท ฟอลคอนประกันภัย จำกัด (มหาชน)', N'The Falcon Insurance Public Company Limited', N'FCI', N'www.falconinsurance.co.th', N'2', N'/storage/insurer/Ml8KwcYcpS.png', N'2020-08-02 22:54:21.000', NULL)
GO

INSERT INTO [dbo].[tpa_insurer]  VALUES (N'62', N'TMP201900040', N'บริษัท เจพี ประกันภัย (ประเทศไทย) จำกัด', N'JP Insurance (Thailand) Company Limited', N' ', N'www.jpinsurance.co.th', N'2', N'https://via.placeholder.com/800x600/DAF7A6/FFFFFF?text=JP Insurance (Thailand) Company Limited', NULL, NULL)
GO

INSERT INTO [dbo].[tpa_insurer]  VALUES (N'63', N'INS201600029', N'บริษัท มิตซุย สุมิโตโม อินชัวรันซ์ จำกัด', N'Mitsui Sumitomo Insurance Co., Ltd.', N'MMF', N'www.ms-ins.co.th', N'2', N'https://via.placeholder.com/800x600/DAF7A6/FFFFFF?text=Mitsui Sumitomo Insurance Co., Ltd.', NULL, NULL)
GO

INSERT INTO [dbo].[tpa_insurer]  VALUES (N'64', N'TMP201900041', N'บริษัท มิตรแท้ประกันภัย จำกัด (มหาชน)', N'Mittare Insurance Public Company Limited', N'MIT', N'www.mittare.com', N'2', N'https://via.placeholder.com/800x600/DAF7A6/FFFFFF?text=Mittare Insurance Public Company Limited', NULL, NULL)
GO

INSERT INTO [dbo].[tpa_insurer]  VALUES (N'65', N'INS201600004', N'บริษัท เมืองไทยประกันภัย จำกัด (มหาชน)', N'Muang Thai Insurance Public Company Limited', N'MTI', N'www.muangthaiinsurance.com', N'2', N'https://via.placeholder.com/800x600/DAF7A6/FFFFFF?text=Muang Thai Insurance Public Company Limited', NULL, NULL)
GO

INSERT INTO [dbo].[tpa_insurer]  VALUES (N'66', N'INS201600031', N'บริษัท วิริยะประกันภัย จำกัด (มหาชน)', N'The Viriyah Insurance Public Company Limited', N'VIB', N'www.viriyah.co.th', N'2', N'https://via.placeholder.com/800x600/DAF7A6/FFFFFF?text=The Viriyah Insurance Public Company Limited', NULL, NULL)
GO

INSERT INTO [dbo].[tpa_insurer]  VALUES (N'67', N'INS201600017', N'บริษัท ศรีอยุธยา เจนเนอรัล ประกันภัย จำกัด (มหาชน)', N'Sri Ayudhya General Insurance Public Company Limited', N'SAGI', N'www.sagi.co.th', N'2', N'https://via.placeholder.com/800x600/DAF7A6/FFFFFF?text=Sri Ayudhya General Insurance Public Company Limited', NULL, NULL)
GO

INSERT INTO [dbo].[tpa_insurer]  VALUES (N'68', N'INS201600008', N'บริษัท ซมโปะ ประกันภัย (ประเทศไทย) จำกัด (มหาชน)', N'Sompo Insurance (Thailand) Public Company Limited', N'SOMPO', N'www.sompo.co.th', N'2', N'https://via.placeholder.com/800x600/DAF7A6/FFFFFF?text=Sompo Insurance (Thailand) Public Company Limited', NULL, NULL)
GO

INSERT INTO [dbo].[tpa_insurer]  VALUES (N'69', N'INS201600013', N'บริษัท สยามซิตี้ประกันภัย จำกัด (มหาชน)', N'Siam City Insurance Public Company Limited', N'SIL', N'www.siamcityinsurance.com', N'2', N'https://via.placeholder.com/800x600/DAF7A6/FFFFFF?text=Siam City Insurance Public Company Limited', NULL, NULL)
GO

INSERT INTO [dbo].[tpa_insurer]  VALUES (N'70', N'TMP201900042', N'บริษัท สหนิรภัยประกันภัย จำกัด (มหาชน)', N'Union Insurance Public Company Limited', N'UNN', N'www.union-insurance.com', N'2', N'https://via.placeholder.com/800x600/DAF7A6/FFFFFF?text=Union Insurance Public Company Limited', NULL, NULL)
GO

INSERT INTO [dbo].[tpa_insurer]  VALUES (N'71', N'TMP201900043', N'บริษัท สหมงคลประกันภัย จำกัด (มหาชน)', N'The Union Prospers Insurance Public Company Limited', N'UPP', N'www.upp.co.th', N'2', N'https://via.placeholder.com/800x600/DAF7A6/FFFFFF?text=The Union Prospers Insurance Public Company Limited', NULL, NULL)
GO

INSERT INTO [dbo].[tpa_insurer]  VALUES (N'72', N'TMP201900044', N'บริษัท สัญญาประกันภัย จำกัด (มหาชน)', N'Promise Insurance Public Company Limited', N'PROMISE', N'www.promiseins.com', N'2', N'https://via.placeholder.com/800x600/DAF7A6/FFFFFF?text=Promise Insurance Public Company Limited', NULL, NULL)
GO

INSERT INTO [dbo].[tpa_insurer]  VALUES (N'73', N'TMP201900045', N'บริษัท ชับบ์สามัคคีประกันภัย จำกัด (มหาชน)', N'Chubb Samaggi Insurance Public Company Limited', N' ', N'https://www2.chubb.com/th-th/', N'2', N'https://via.placeholder.com/800x600/DAF7A6/FFFFFF?text=Chubb Samaggi Insurance Public Company Limited', NULL, NULL)
GO

INSERT INTO [dbo].[tpa_insurer]  VALUES (N'74', N'TMP201900046', N'บริษัท สินทรัพย์ประกันภัย จำกัด (มหาชน)', N'Assets Insurance Public Company Limited', N'AIC', N'www.asset.co.th', N'2', N'https://via.placeholder.com/800x600/DAF7A6/FFFFFF?text=Assets Insurance Public Company Limited', NULL, NULL)
GO

INSERT INTO [dbo].[tpa_insurer]  VALUES (N'75', N'INS201600014', N'บริษัท สินมั่นคงประกันภัย จำกัด (มหาชน)', N'Synmunkong Insurance Public Co., Ltd.', N'SMK', N'www.smk.co.th', N'2', N'https://via.placeholder.com/800x600/DAF7A6/FFFFFF?text=Synmunkong Insurance Public Co., Ltd.', NULL, NULL)
GO

INSERT INTO [dbo].[tpa_insurer]  VALUES (N'76', N'INS201600011', N'บริษัท อลิอันซ์ ประกันภัย จำกัด (มหาชน)', N'Allianz General Insurance Public Company Limited', N'CPI', N'www.allianz.co.th', N'2', N'https://via.placeholder.com/800x600/DAF7A6/FFFFFF?text=Allianz General Insurance Public Company Limited', NULL, NULL)
GO

INSERT INTO [dbo].[tpa_insurer]  VALUES (N'77', N'TMP201900047', N'บริษัท อาคเนย์ประกันภัย จำกัด (มหาชน)', N'Southeast Insurance Public Company Limited', N'SEI', N'www.seic.co.th', N'2', N'https://via.placeholder.com/800x600/DAF7A6/FFFFFF?text=Southeast Insurance Public Company Limited', NULL, NULL)
GO

INSERT INTO [dbo].[tpa_insurer]  VALUES (N'78', N'TMP201900048', N'บริษัท อินทรประกันภัย จำกัด (มหาชน)', N'Indara Insurance Public Co., Ltd.', N'IIC', N'www.indara.co.th', N'2', N'https://via.placeholder.com/800x600/DAF7A6/FFFFFF?text=Indara Insurance Public Co., Ltd.', NULL, NULL)
GO

INSERT INTO [dbo].[tpa_insurer]  VALUES (N'79', N'TMP201900049', N'บริษัท เอเชียประกันภัย 1950 จำกัด (มหาชน)', N'Asia Insurance 1950 Public Company Limited', N'AII', N'www.asiainsurance.co.th', N'2', N'https://via.placeholder.com/800x600/DAF7A6/FFFFFF?text=Asia Insurance 1950 Public Company Limited', NULL, NULL)
GO

INSERT INTO [dbo].[tpa_insurer]  VALUES (N'80', N'TMP201900050', N'บริษัท เอซ ไอเอ็นเอ โอเวอร์ซีส์ อินชัวรันซ์ จำกัด สาขาประเทศไทย', N'Ace Ina Overseas Insurance Company Ltd.,', N'AIOIC', N'www.aceinsurance.co.th', N'2', N'https://via.placeholder.com/800x600/DAF7A6/FFFFFF?text=Ace Ina Overseas Insurance Company Ltd.,', NULL, NULL)
GO

INSERT INTO [dbo].[tpa_insurer]  VALUES (N'81', N'TMP201900051', N'บริษัท เอฟพีจี ประกันภัย (ประเทศไทย) จำกัด (มหาชน)', N'Fpg Insurance (Thailand) Public Company Limited', N'FPG', N'www.fpgins.co.th', N'2', N'https://via.placeholder.com/800x600/DAF7A6/FFFFFF?text=Fpg Insurance (Thailand) Public Company Limited', NULL, NULL)
GO

INSERT INTO [dbo].[tpa_insurer]  VALUES (N'82', N'TMP201900052', N'บริษัท เอ็ม เอส ไอ จี ประกันภัย (ประเทศไทย) จำกัด (มหาชน)', N'Msig Insurance (Thailand) Public Company Limited', N'MSIG', N'www.msig-thai.com', N'2', N'https://via.placeholder.com/800x600/DAF7A6/FFFFFF?text=Msig Insurance (Thailand) Public Company Limited', NULL, NULL)
GO

INSERT INTO [dbo].[tpa_insurer]  VALUES (N'83', N'TMP201900053', N'บริษัท เอราวัณประกันภัย จำกัด (มหาชน)', N'Erawan Insurance Public Company Limited', N'EIC', N'www.erawanins.com', N'2', N'https://via.placeholder.com/800x600/DAF7A6/FFFFFF?text=Erawan Insurance Public Company Limited', NULL, NULL)
GO

INSERT INTO [dbo].[tpa_insurer]  VALUES (N'84', N'TMP201900054', N'บริษัท เอไอจี ประกันภัย (ประเทศไทย) จำกัด (มหาชน)', N'Aig Insurance (Thailand) Public Company Limited', N'AIG', N'www.aig.com', N'2', N'https://via.placeholder.com/800x600/DAF7A6/FFFFFF?text=Aig Insurance (Thailand) Public Company Limited', NULL, NULL)
GO

INSERT INTO [dbo].[tpa_insurer]  VALUES (N'85', N'TMP201900055', N'บริษัท เอไอเอ จำกัด (ประกันวินาศภัย) สาขาประเทศไทย', N'Aia Company Limited (Non-Life Insurance) Thailand Branch', N'AIA (NON-LIFE)', N'www.aianonlife.co.th', N'2', N'https://via.placeholder.com/800x600/DAF7A6/FFFFFF?text=Aia Company Limited (Non-Life Insurance) Thailand Branch', NULL, NULL)
GO

INSERT INTO [dbo].[tpa_insurer]  VALUES (N'86', N'TMP201900056', N'บริษัท แอกซ่าประกันภัย จำกัด (มหาชน)', N'Axa Insurace Public Co., Ltd.', N'AXA', N'www.axa.co.th', N'2', N'https://via.placeholder.com/800x600/DAF7A6/FFFFFF?text=Axa Insurace Public Co., Ltd.', NULL, NULL)
GO

INSERT INTO [dbo].[tpa_insurer]  VALUES (N'87', N'TMP201900057', N'บริษัท แอลเอ็มจี ประกันภัย จำกัด (มหาชน)', N'Lmg Insurance Public Company Limited', N'LMG', N'www.lmginsurance.co.th', N'2', N'https://via.placeholder.com/800x600/DAF7A6/FFFFFF?text=Lmg Insurance Public Company Limited', NULL, NULL)
GO

INSERT INTO [dbo].[tpa_insurer]  VALUES (N'88', N'TMP201900058', N'บริษัท ไอโออิ กรุงเทพ ประกันภัย จำกัด (มหาชน)', N'Aioi Bangkok Insurance Public Company Limited', N'WSN', N'www.aioibkkins.co.th', N'2', N'https://via.placeholder.com/800x600/DAF7A6/FFFFFF?text=Aioi Bangkok Insurance Public Company Limited', NULL, NULL)
GO

INSERT INTO [dbo].[tpa_insurer]  VALUES (N'89', N'INS201800004', N'Sokxay Insurance Company Limited', N'Sokxay Insurance Company Limited', N'SOKXAY', NULL, N'2', N'/storage/insurer/imeTpnmOif.png', N'2020-08-01 23:03:18.000', NULL)
GO

INSERT INTO [dbo].[tpa_insurer]  VALUES (N'90', N'INS201700003', N'บริษัท ซาปุระ เอ็นเนอร์จี (ประเทศไทย) จำกัด', N'SAPURA ENERGY (THAILAND) COMPANY LIMITED', N'SKP', N' ', N'3', N'https://via.placeholder.com/800x600/DAF7A6/FFFFFF?text=SAPURA ENERGY (THAILAND) COMPANY LIMITED', NULL, NULL)
GO

INSERT INTO [dbo].[tpa_insurer]  VALUES (N'91', N'INS201800006', N'บริษัท ดูโฮม จำกัด (มหาชน)', N'บริษัท ดูโฮม จำกัด (มหาชน)', N' ', N'www.dohome.co.th', N'3', N'https://via.placeholder.com/800x600/DAF7A6/FFFFFF?text=บริษัท ดูโฮม จำกัด (มหาชน)', NULL, NULL)
GO

INSERT INTO [dbo].[tpa_insurer]  VALUES (N'92', N'INS201800005', N'บริษัท เอลก้า (ประเทศไทย) จำกัด', N'บริษัท เอลก้า (ประเทศไทย) จำกัด', N'ELCA', N' ', N'3', N'https://via.placeholder.com/800x600/DAF7A6/FFFFFF?text=บริษัท เอลก้า (ประเทศไทย) จำกัด', NULL, NULL)
GO

INSERT INTO [dbo].[tpa_insurer]  VALUES (N'93', N'INS201600002', N'บริษัท ซาปุระ ดริลลิ่ง เอเซีย ลิมิเต็ด', N'บริษัท ซาปุระ ดริลลิ่ง เอเซีย ลิมิเต็ด', N'SKD', N' ', N'3', N'https://via.placeholder.com/800x600/DAF7A6/FFFFFF?text=บริษัท ซาปุระ ดริลลิ่ง เอเซีย ลิมิเต็ด', NULL, NULL)
GO

INSERT INTO [dbo].[tpa_insurer]  VALUES (N'94', N'INS201600025', N'บริษัท เอ็นซีอาร์ (ประเทศไทย) จำกัด', N'บริษัท เอ็นซีอาร์ (ประเทศไทย) จำกัด', N'NCR', N' ', N'3', N'https://via.placeholder.com/800x600/DAF7A6/FFFFFF?text=บริษัท เอ็นซีอาร์ (ประเทศไทย) จำกัด', NULL, NULL)
GO

INSERT INTO [dbo].[tpa_insurer]  VALUES (N'95', N'INS201600001', N'บริษัท เอบีบี (ไทยแลนด์) จำกัด', N'บริษัท เอบีบี (ไทยแลนด์) จำกัด', N'ABB', N' ', N'3', N'https://via.placeholder.com/800x600/DAF7A6/FFFFFF?text=บริษัท เอบีบี (ไทยแลนด์) จำกัด', NULL, NULL)
GO

INSERT INTO [dbo].[tpa_insurer]  VALUES (N'96', N'INS201600003', N'บริษัท ซีดริล อินเตอร์เนชั่นแนล ลิมิเต็ด', N'บริษัท ซีดริล อินเตอร์เนชั่นแนล ลิมิเต็ด', N'SDRL', N' ', N'3', N'https://via.placeholder.com/800x600/DAF7A6/FFFFFF?text=บริษัท ซีดริล อินเตอร์เนชั่นแนล ลิมิเต็ด', NULL, NULL)
GO

INSERT INTO [dbo].[tpa_insurer]  VALUES (N'97', N'INS201600026', N'บริษัท เวสเทิร์นดิจิตอล (ประเทศไทย) จำกัด', N'บริษัท เวสเทิร์นดิจิตอล (ประเทศไทย) จำกัด', N'WD', N' ', N'3', N'https://via.placeholder.com/800x600/DAF7A6/FFFFFF?text=บริษัท เวสเทิร์นดิจิตอล (ประเทศไทย) จำกัด', NULL, NULL)
GO

INSERT INTO [dbo].[tpa_insurer]  VALUES (N'98', N'BNK201900001', N'ธนาคารแห่งประเทศไทย', N'BANK OF THAILAND', N' ', N' ', N'4', N'https://via.placeholder.com/800x600/DAF7A6/FFFFFF?text=BANK OF THAILAND', NULL, NULL)
GO

INSERT INTO [dbo].[tpa_insurer]  VALUES (N'99', N'BNK201900002', N'ธนาคารกรุงเทพ จำกัด (มหาชน)', N'BANGKOK BANK PUBLIC COMPANY LTD.', N' ', N' ', N'4', N'https://via.placeholder.com/800x600/DAF7A6/FFFFFF?text=BANGKOK BANK PUBLIC COMPANY LTD.', NULL, NULL)
GO

INSERT INTO [dbo].[tpa_insurer]  VALUES (N'100', N'BNK201900004', N'ธนาคารกสิกรไทย จำกัด (มหาชน)', N'KASIKORNBANK PUBLIC COMPANY LTD.', N' ', N' ', N'4', N'https://via.placeholder.com/800x600/DAF7A6/FFFFFF?text=KASIKORNBANK PUBLIC COMPANY LTD.', NULL, NULL)
GO

INSERT INTO [dbo].[tpa_insurer]  VALUES (N'101', N'BNK201900006', N'ธนาคารกรุงไทย จำกัด (มหาชน)', N'KRUNG THAI BANK PUBLIC COMPANY LTD.', NULL, NULL, N'4', N'/storage/insurer/xnDrjyqDon.png', N'2020-07-30 23:57:00.000', NULL)
GO

INSERT INTO [dbo].[tpa_insurer]  VALUES (N'102', N'BNK201900008', N'ธนาคารเจพีมอร์แกน เชส', N'JPMORGAN CHASE BANK, NATIONAL ASSOCIATION', N' ', N' ', N'4', N'https://via.placeholder.com/800x600/DAF7A6/FFFFFF?text=JPMORGAN CHASE BANK, NATIONAL ASSOCIATION', NULL, NULL)
GO

INSERT INTO [dbo].[tpa_insurer]  VALUES (N'103', N'BNK201900009', N'ธนาคารโอเวอร์ซี-ไชนีสแบงกิ้งคอร์ปอเรชั่น จำกัด', N'OVER SEA-CHINESE BANKING CORPORATION LIMITED', N' ', N' ', N'4', N'https://via.placeholder.com/800x600/DAF7A6/FFFFFF?text=OVER SEA-CHINESE BANKING CORPORATION LIMITED', NULL, NULL)
GO

INSERT INTO [dbo].[tpa_insurer]  VALUES (N'104', N'BNK201900011', N'ธนาคารทหารไทย จำกัด (มหาชน)', N'TMB BANK PUBLIC COMPANY LIMITED', N' ', N' ', N'4', N'https://via.placeholder.com/800x600/DAF7A6/FFFFFF?text=TMB BANK PUBLIC COMPANY LIMITED', NULL, NULL)
GO

INSERT INTO [dbo].[tpa_insurer]  VALUES (N'105', N'BNK201900014', N'ธนาคารไทยพาณิชย์ จำกัด (มหาชน)', N'SIAM COMMERCIAL BANK PUBLIC COMPANY LTD.', N' ', N' ', N'4', N'https://via.placeholder.com/800x600/DAF7A6/FFFFFF?text=SIAM COMMERCIAL BANK PUBLIC COMPANY LTD.', NULL, NULL)
GO

INSERT INTO [dbo].[tpa_insurer]  VALUES (N'106', N'BNK201900017', N'ธนาคารซิตี้แบงก์ เอ็น.เอ.', N'CITIBANK, N.A.', N' ', N' ', N'4', N'https://via.placeholder.com/800x600/DAF7A6/FFFFFF?text=CITIBANK, N.A.', NULL, NULL)
GO

INSERT INTO [dbo].[tpa_insurer]  VALUES (N'107', N'BNK201900018', N'ธนาคารซูมิโตโม มิตซุย แบงกิ้ง คอร์ปอเรชั่น', N'SUMITOMO MITSUI BANKING CORPORATION', N' ', N' ', N'4', N'https://via.placeholder.com/800x600/DAF7A6/FFFFFF?text=SUMITOMO MITSUI BANKING CORPORATION', NULL, NULL)
GO

INSERT INTO [dbo].[tpa_insurer]  VALUES (N'108', N'BNK201900020', N'ธนาคารสแตนดาร์ดชาร์เตอร์ด (ไทย) จำกัด (มหาชน)', N'STANDARD CHARTERED BANK (THAI) PUBLIC COMPANY LIMITED', N' ', N' ', N'4', N'https://via.placeholder.com/800x600/DAF7A6/FFFFFF?text=STANDARD CHARTERED BANK (THAI) PUBLIC COMPANY LIMITED', NULL, NULL)
GO

INSERT INTO [dbo].[tpa_insurer]  VALUES (N'109', N'BNK201900022', N'ธนาคารซีไอเอ็มบี ไทย จำกัด (มหาชน)', N'CIMB THAI BANK PUBLIC COMPANY LIMITED', N' ', N' ', N'4', N'https://via.placeholder.com/800x600/DAF7A6/FFFFFF?text=CIMB THAI BANK PUBLIC COMPANY LIMITED', NULL, NULL)
GO

INSERT INTO [dbo].[tpa_insurer]  VALUES (N'110', N'BNK201900023', N'ธนาคารอาร์ เอช บี จำกัด', N'RHB BANK BERHAD', N' ', N' ', N'4', N'https://via.placeholder.com/800x600/DAF7A6/FFFFFF?text=RHB BANK BERHAD', NULL, NULL)
GO

INSERT INTO [dbo].[tpa_insurer]  VALUES (N'111', N'BNK201900024', N'ธนาคารยูโอบี จำกัด (มหาชน)', N'UNITED OVERSEAS BANK (THAI) PUBLIC COMPANY LIMITED', N' ', N' ', N'4', N'https://via.placeholder.com/800x600/DAF7A6/FFFFFF?text=UNITED OVERSEAS BANK (THAI) PUBLIC COMPANY LIMITED', NULL, NULL)
GO

INSERT INTO [dbo].[tpa_insurer]  VALUES (N'112', N'BNK201900025', N'ธนาคารกรุงศรีอยุธยา จำกัด (มหาชน)', N'BANK OF AYUDHYA PUBLIC COMPANY LTD.', N' ', N' ', N'4', N'https://via.placeholder.com/800x600/DAF7A6/FFFFFF?text=BANK OF AYUDHYA PUBLIC COMPANY LTD.', NULL, NULL)
GO

INSERT INTO [dbo].[tpa_insurer]  VALUES (N'113', N'BNK201900026', N'ธนาคารเมกะ สากลพาณิชย์ จำกัด (มหาชน)', N'MEGA  INTERNATIONAL COMMERCIAL BANK PUBLIC COMPANY LIMITED', N' ', N' ', N'4', N'https://via.placeholder.com/800x600/DAF7A6/FFFFFF?text=MEGA  INTERNATIONAL COMMERCIAL BANK PUBLIC COMPANY LIMITED', NULL, NULL)
GO

INSERT INTO [dbo].[tpa_insurer]  VALUES (N'114', N'BNK201900027', N'ธนาคารแห่งอเมริกาเนชั่นแนลแอสโซซิเอชั่น', N'BANK OF AMERICA, NATIONAL ASSOCIATION', N' ', N' ', N'4', N'https://via.placeholder.com/800x600/DAF7A6/FFFFFF?text=BANK OF AMERICA, NATIONAL ASSOCIATION', NULL, NULL)
GO

INSERT INTO [dbo].[tpa_insurer]  VALUES (N'115', N'BNK201900029', N'ธนาคารอินเดียนโอเวอร์ซีส์', N'INDIAN OVERSEA BANK', N' ', N' ', N'4', N'https://via.placeholder.com/800x600/DAF7A6/FFFFFF?text=INDIAN OVERSEA BANK', NULL, NULL)
GO

INSERT INTO [dbo].[tpa_insurer]  VALUES (N'116', N'BNK201900030', N'ธนาคารออมสิน', N'THE GOVERNMENT SAVINGS BANK', N' ', N' ', N'4', N'https://via.placeholder.com/800x600/DAF7A6/FFFFFF?text=THE GOVERNMENT SAVINGS BANK', NULL, NULL)
GO

INSERT INTO [dbo].[tpa_insurer]  VALUES (N'117', N'BNK201900031', N'ธนาคารฮ่องกงและเซี่ยงไฮ้แบงกิ้งคอร์ปอเรชั่น จำกัด', N'THE HONGKONG AND SHANGHAI BANKING CORPORATION LTD.', N' ', N' ', N'4', N'https://via.placeholder.com/800x600/DAF7A6/FFFFFF?text=THE HONGKONG AND SHANGHAI BANKING CORPORATION LTD.', NULL, NULL)
GO

INSERT INTO [dbo].[tpa_insurer]  VALUES (N'118', N'BNK201900032', N'ธนาคารดอยซ์แบงก์', N'DEUTSCHE BANK AG.', N' ', N' ', N'4', N'https://via.placeholder.com/800x600/DAF7A6/FFFFFF?text=DEUTSCHE BANK AG.', NULL, NULL)
GO

INSERT INTO [dbo].[tpa_insurer]  VALUES (N'119', N'BNK201900033', N'ธนาคารอาคารสงเคราะห์', N'THE GOVERNMENT HOUSING BANK', N' ', N' ', N'4', N'https://via.placeholder.com/800x600/DAF7A6/FFFFFF?text=THE GOVERNMENT HOUSING BANK', NULL, NULL)
GO

INSERT INTO [dbo].[tpa_insurer]  VALUES (N'120', N'BNK201900034', N'ธนาคารเพื่อการเกษตรและสหกรณ์การเกษตร', N'BANK FOR AGRICULTURE AND AGRICULTURAL COOPERATIVES', N' ', N' ', N'4', N'https://via.placeholder.com/800x600/DAF7A6/FFFFFF?text=BANK FOR AGRICULTURE AND AGRICULTURAL COOPERATIVES', NULL, NULL)
GO

INSERT INTO [dbo].[tpa_insurer]  VALUES (N'121', N'BNK201900035', N'ธนาคารเพื่อการส่งออกและนำเข้าแห่งประเทศไทย', N'EXPORT-IMPORT BANK OF THAILAND', N' ', N' ', N'4', N'https://via.placeholder.com/800x600/DAF7A6/FFFFFF?text=EXPORT-IMPORT BANK OF THAILAND', NULL, NULL)
GO

INSERT INTO [dbo].[tpa_insurer]  VALUES (N'122', N'BNK201900039', N'ธนาคารมิซูโฮ จำกัด สาขากรุงเทพฯ', N'MIZUHO BANK, LTD. BANGKOK BRANCH', N' ', N' ', N'4', N'https://via.placeholder.com/800x600/DAF7A6/FFFFFF?text=MIZUHO BANK, LTD. BANGKOK BRANCH', NULL, NULL)
GO

INSERT INTO [dbo].[tpa_insurer]  VALUES (N'123', N'BNK201900045', N'ธนาคารบีเอ็นพี พารีบาส์', N'BNP PARIBAS', N' ', N' ', N'4', N'https://via.placeholder.com/800x600/DAF7A6/FFFFFF?text=BNP PARIBAS', NULL, NULL)
GO

INSERT INTO [dbo].[tpa_insurer]  VALUES (N'124', N'BNK201900052', N'ธนาคารแห่งประเทศจีน (ไทย) จำกัด (มหาชน)', N'BANK OF CHINA (THAI) PUBLIC COMPANY LIMITED', N' ', N' ', N'4', N'https://via.placeholder.com/800x600/DAF7A6/FFFFFF?text=BANK OF CHINA (THAI) PUBLIC COMPANY LIMITED', NULL, NULL)
GO

INSERT INTO [dbo].[tpa_insurer]  VALUES (N'125', N'BNK201900065', N'ธนาคารธนชาต จำกัด (มหาชน)', N'THANACHART BANK PUBLIC COMPANY LTD.', N' ', N' ', N'4', N'https://via.placeholder.com/800x600/DAF7A6/FFFFFF?text=THANACHART BANK PUBLIC COMPANY LTD.', NULL, NULL)
GO

INSERT INTO [dbo].[tpa_insurer]  VALUES (N'126', N'BNK201900066', N'ธนาคารอิสลามแห่งประเทศไทย', N'ISLAMIC BANK OF THAILAND', N' ', N' ', N'4', N'https://via.placeholder.com/800x600/DAF7A6/FFFFFF?text=ISLAMIC BANK OF THAILAND', NULL, NULL)
GO

INSERT INTO [dbo].[tpa_insurer]  VALUES (N'127', N'BNK201900067', N'ธนาคารทิสโก้ จำกัด (มหาชน)', N'TISCO BANK PUBLIC COMPANY LIMITED', N' ', N' ', N'4', N'https://via.placeholder.com/800x600/DAF7A6/FFFFFF?text=TISCO BANK PUBLIC COMPANY LIMITED', NULL, NULL)
GO

INSERT INTO [dbo].[tpa_insurer]  VALUES (N'128', N'BNK201900069', N'ธนาคารเกียรตินาคิน จำกัด (มหาชน)', N'KIATNAKIN BANK PUBLIC COMPANY LIMITED', N' ', N' ', N'4', N'https://via.placeholder.com/800x600/DAF7A6/FFFFFF?text=KIATNAKIN BANK PUBLIC COMPANY LIMITED', NULL, NULL)
GO

INSERT INTO [dbo].[tpa_insurer]  VALUES (N'129', N'BNK201900070', N'ธนาคารไอซีบีซี (ไทย) จำกัด (มหาชน)', N'INDUSTRIAL AND COMMERCIAL BANK OF CHINA (THAI) PUBLIC COMPANY LIMITED', N' ', N' ', N'4', N'https://via.placeholder.com/800x600/DAF7A6/FFFFFF?text=INDUSTRIAL AND COMMERCIAL BANK OF CHINA (THAI) PUBLIC COMPANY LIMITED', NULL, NULL)
GO

INSERT INTO [dbo].[tpa_insurer]  VALUES (N'130', N'BNK201900071', N'ธนาคารไทยเครดิต เพื่อรายย่อย จำกัด (มหาชน)', N'THE THAI CREDIT RETAIL BANK PUBLIC COMPANY LIMITED', N' ', N' ', N'4', N'https://via.placeholder.com/800x600/DAF7A6/FFFFFF?text=THE THAI CREDIT RETAIL BANK PUBLIC COMPANY LIMITED', NULL, NULL)
GO

INSERT INTO [dbo].[tpa_insurer]  VALUES (N'131', N'BNK201900073', N'ธนาคารแลนด์ แอนด์ เฮ้าส์ จำกัด (มหาชน)', N'LAND AND HOUSES PUBLIC COMPANY LIMITED', N' ', N' ', N'4', N'https://via.placeholder.com/800x600/DAF7A6/FFFFFF?text=LAND AND HOUSES PUBLIC COMPANY LIMITED', NULL, NULL)
GO

INSERT INTO [dbo].[tpa_insurer]  VALUES (N'132', N'BNK201900079', N'ธนาคารเอเอ็นแซด (ไทย) จำกัด (มหาชน)', N'ANZ BANK (THAI) PUBLIC COMPANY LIMITED', N' ', N' ', N'4', N'https://via.placeholder.com/800x600/DAF7A6/FFFFFF?text=ANZ BANK (THAI) PUBLIC COMPANY LIMITED', NULL, NULL)
GO

INSERT INTO [dbo].[tpa_insurer]  VALUES (N'133', N'BNK201900080', N'ธนาคารซูมิโตโม มิตซุย ทรัสต์ (ไทย) จำกัด (มหาชน)', N'SUMITOMO MITSUI TRUST BANK (THAI) PUBLIC COMPANY LIMITED', N' ', N' ', N'4', N'https://via.placeholder.com/800x600/DAF7A6/FFFFFF?text=SUMITOMO MITSUI TRUST BANK (THAI) PUBLIC COMPANY LIMITED', NULL, NULL)
GO

INSERT INTO [dbo].[tpa_insurer]  VALUES (N'134', N'BNK201900098', N'ธนาคารพัฒนาวิสาหกิจขนาดกลางและขนาดย่อมแห่งประเทศไทย', N'SMALL AND MEDIUM ENTERPRISE DEVELOPMENT BANK OF THAILAND', N' ', N' ', N'4', N'https://via.placeholder.com/800x600/DAF7A6/FFFFFF?text=SMALL AND MEDIUM ENTERPRISE DEVELOPMENT BANK OF THAILAND', NULL, NULL)
GO

SET IDENTITY_INSERT [dbo].[tpa_insurer] OFF
GO


-- ----------------------------
-- Primary Key structure for table tpa_insurer
-- ----------------------------
ALTER TABLE [dbo].[tpa_insurer] ADD CONSTRAINT [PK__tpa_insu__3213E83E7DC55724] PRIMARY KEY NONCLUSTERED ([id])
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = OFF, ALLOW_PAGE_LOCKS = OFF)
GO

