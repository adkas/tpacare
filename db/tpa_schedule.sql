/*
 Navicat Premium Data Transfer

 Source Server         : MSSQLSERVER
 Source Server Type    : SQL Server
 Source Server Version : 14001000
 Source Host           : SAKDA-INSPIRON:1433
 Source Catalog        : tpacare
 Source Schema         : dbo

 Target Server Type    : SQL Server
 Target Server Version : 14001000
 File Encoding         : 65001

 Date: 15/12/2020 23:41:06
*/


-- ----------------------------
-- Table structure for tpa_schedule
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID(N'[dbo].[tpa_schedule]') AND type IN ('U'))
	DROP TABLE [dbo].[tpa_schedule]
GO

CREATE TABLE [dbo].[tpa_schedule] (
  [id] int  IDENTITY(1,1) NOT NULL,
  [notification_id] int  NOT NULL,
  [users] ntext NOT NULL,
  [response] ntext NULL,
  [created_at] datetime DEFAULT (getdate()) NULL,
  [sent_at] datetime  NULL
)
GO

ALTER TABLE [dbo].[tpa_schedule] SET (LOCK_ESCALATION = TABLE)
GO


-- ----------------------------
-- Primary Key structure for table tpa_schedule
-- ----------------------------
ALTER TABLE [dbo].[tpa_schedule] ADD CONSTRAINT [PK__tpa_sche__3213E83F0615F1E2] PRIMARY KEY CLUSTERED ([id])
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
ON [PRIMARY]
GO

