

-- ----------------------------
-- Table structure for log_request_response
-- ----------------------------

CREATE TABLE log_request_response (
	id int NOT NULL IDENTITY(1,1),
	header text NOT NULL,
	request ntext NOT NULL,
	response ntext NOT NULL,
  path varchar(250) NULL,
	ip varchar(16) NOT NULL,
	created_at datetime NULL,
  updated_at datetime NULL,
	CONSTRAINT log_request_response_PK PRIMARY KEY (id)
);


