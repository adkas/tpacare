/*
 Navicat Premium Data Transfer

 Source Server         : SQL Server
 Source Server Type    : SQL Server
 Source Server Version : 14001000
 Source Host           : localhost\SQLEXPRESS:1433
 Source Catalog        : tpacare
 Source Schema         : dbo

 Target Server Type    : SQL Server
 Target Server Version : 14001000
 File Encoding         : 65001

 Date: 10/05/2020 18:30:48
*/


-- ----------------------------
-- Table structure for users
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID(N'[dbo].[users]') AND type IN ('U'))
	DROP TABLE [dbo].[users]
GO

CREATE TABLE [dbo].[users] (
  [id] int  IDENTITY(1,1) NOT NULL,
  [name] nvarchar(250) COLLATE Thai_CI_AI  NOT NULL,
  [surname] nvarchar(250) COLLATE Thai_CI_AI  NOT NULL,
  [email] varchar(150) COLLATE Thai_CI_AI  NULL,
  [email_verified_at] datetime  NULL,
  [mobile] varchar(150) COLLATE Thai_CI_AI  NOT NULL,
  [username] varchar(150) COLLATE Thai_CI_AI  NULL,
  [password] varchar(255) COLLATE Thai_CI_AI  NOT NULL,
  [pin] varchar(32) COLLATE Thai_CI_AI  NULL,
  [access_token] varchar(150) COLLATE Thai_CI_AI  NULL,
  [remember_token] varchar(150) COLLATE Thai_CI_AI  NULL,
  [photo] varchar(150) COLLATE Thai_CI_AI  NULL,
  [cb_roles_id] int DEFAULT ((0)) NOT NULL,
  [status] int DEFAULT ((0)) NOT NULL,
  [created_date] datetime  NULL,
  [updated_at] datetime  NULL,
  [login_at] datetime  NULL
)
GO

ALTER TABLE [dbo].[users] SET (LOCK_ESCALATION = TABLE)
GO


-- ----------------------------
-- Records of users
-- ----------------------------
SET IDENTITY_INSERT [dbo].[users] ON
GO

INSERT INTO [dbo].[users] ([id], [name], [surname], [email], [email_verified_at], [mobile], [username], [password], [pin], [access_token], [remember_token], [photo], [cb_roles_id], [status], [created_date], [updated_at], [login_at]) VALUES (N'1', N'Sakda Srijumpa', N'', N'sakda@gmail.com', NULL, N'', N'sakda@gmail.com', N'$2y$10$rKf8aBT9jdWlF5KyqVYZXeHZu1igIdR9Ki0RY1C6jAx8dgW1E7Are', NULL, N'tpacare2020', NULL, NULL, N'1', N'1', N'2020-05-10 11:49:37.000', NULL, NULL)
GO

SET IDENTITY_INSERT [dbo].[users] OFF
GO


-- ----------------------------
-- Triggers structure for table users
-- ----------------------------
CREATE TRIGGER [dbo].[trg_users_before_insert]
ON [dbo].[users]
WITH EXECUTE AS CALLER
FOR INSERT
AS
BEGIN
      UPDATE users
      SET email = inserted.username
      FROM inserted
      WHERE users.id = inserted.id AND users.email IS NULL;

      UPDATE users
      SET username = inserted.email
      FROM inserted
      WHERE users.id = inserted.id AND users.username IS NULL;
    END
GO


-- ----------------------------
-- Primary Key structure for table users
-- ----------------------------
ALTER TABLE [dbo].[users] ADD CONSTRAINT [users_PK] PRIMARY KEY CLUSTERED ([id])
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)  
ON [PRIMARY]
GO

