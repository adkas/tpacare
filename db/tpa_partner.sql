SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tpa_partner](
	[ins_code] [nvarchar](20) NOT NULL,
	[name_th] [nvarchar](255) NULL,
	[name_en] [nvarchar](255) NULL,
	[status_cf1] [nvarchar](7) NULL,
	[status_cf1_c] [nvarchar](7) NULL,
	[status_cf2] [nvarchar](7) NULL,
	[status_cf2_c] [nvarchar](7) NULL,
	[status_ccb1] [nvarchar](7) NULL,
	[status_ccb1_c] [nvarchar](7) NULL,
	[status_cb1] [nvarchar](7) NULL,
	[status_cb1_c] [nvarchar](7) NULL,
	[detail_cf1] [nvarchar](7) NULL,
	[detail_cf1_c] [nvarchar](7) NULL,
	[detail_cf2] [nvarchar](7) NULL,
	[detail_cf2_c] [nvarchar](7) NULL,
	[detail_cf3] [nvarchar](7) NULL,
	[detail_cf3_c] [nvarchar](7) NULL,
	[detail_cb] [nvarchar](7) NULL,
	[detail_cb_c] [nvarchar](7) NULL,
	[history_cf1] [nvarchar](7) NULL,
	[history_cf1_c] [nvarchar](7) NULL,
	[history_cf2] [nvarchar](7) NULL,
	[history_cf2_c] [nvarchar](7) NULL,
	[history_cb] [nvarchar](7) NULL,
	[history_cb_c] [nvarchar](7) NULL,
	[secret_key] [nvarchar](255) NULL,
	[partner_id] [nvarchar](255) NULL,
	[cn_flg] [int] NULL,
	[created_at] [datetime] NULL,
	[updated_by] [int] NULL,
	[updated_at] [datetime] NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING ON
GO
ALTER TABLE [dbo].[tpa_partner] ADD PRIMARY KEY NONCLUSTERED 
(
	[ins_code] ASC
)WITH (STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ONLINE = OFF, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tpa_partner] ADD  DEFAULT ((1)) FOR [cn_flg]
GO
