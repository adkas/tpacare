-- ----------------------------
-- Table structure for tpa_article
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID(N'[dbo].[tpa_article]') AND type IN ('U'))
	DROP TABLE [dbo].[tpa_article]
GO

CREATE TABLE [dbo].[tpa_article] (
  [id] int  IDENTITY(1,1) NOT NULL,
  [title_th] nvarchar(250) COLLATE Thai_CI_AI  NOT NULL,
  [detail_th] ntext COLLATE Thai_CI_AI  NOT NULL,
  [title_en] nvarchar(250) COLLATE Thai_CI_AI  NOT NULL,
  [detail_en] ntext COLLATE Thai_CI_AI  NOT NULL,
  [type] int DEFAULT 0 NOT NULL,
  [coordinates] varchar(255) COLLATE Thai_CI_AI  NULL,
  [status] int DEFAULT 0 NOT NULL,
  [created_at] datetime  NULL DEFAULT GETDATE(),
  [updated_at] datetime  NULL,
  [deleted_at] datetime  NULL
)
GO

ALTER TABLE [dbo].[tpa_article] SET (LOCK_ESCALATION = TABLE)
GO
