-- ----------------------------
-- Table structure for tpa_users_device
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID(N'[dbo].[tpa_users_device]') AND type IN ('U'))
	DROP TABLE [dbo].[tpa_users_device]
GO

CREATE TABLE [dbo].[tpa_users_device] (
  [udid] int  IDENTITY(1,1) NOT NULL,
  [uid] int DEFAULT 0,
  [device_type] varchar(10) COLLATE Thai_CI_AI  NOT NULL,
  [device_token] varchar(250) COLLATE Thai_CI_AI  NOT NULL,
  [ip_address] varchar(30) COLLATE Thai_CI_AI  NOT NULL,
  [user_agent] varchar(250) COLLATE Thai_CI_AI  NOT NULL,
  [created_at] datetime NULL DEFAULT GETDATE(),
  [login_at] datetime NULL DEFAULT GETDATE(),
  CONSTRAINT AK_tpa_users_device UNIQUE(uid, device_type, device_token)  
)
GO

ALTER TABLE [dbo].[tpa_users_device] SET (LOCK_ESCALATION = TABLE)
GO

