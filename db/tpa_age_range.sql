/*
 Navicat Premium Data Transfer

 Source Server         : tpacare
 Source Server Type    : SQL Server
 Source Server Version : 12002000
 Source Host           : tpacare-v2-db.database.windows.net:1433
 Source Catalog        : tpacare-v2-dev
 Source Schema         : dbo

 Target Server Type    : SQL Server
 Target Server Version : 12002000
 File Encoding         : 65001

 Date: 09/07/2020 16:25:21
*/


-- ----------------------------
-- Table structure for tpa_age_range
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID(N'[dbo].[tpa_age_range]') AND type IN ('U'))
	DROP TABLE [dbo].[tpa_age_range]
GO

CREATE TABLE [dbo].[tpa_age_range] (
  [id] int  IDENTITY(1,1) NOT NULL,
  [title_th] nvarchar(100) COLLATE Thai_CI_AI  NOT NULL,
  [title_en] nvarchar(100) COLLATE Thai_CI_AI  NOT NULL,
  [start_age] int DEFAULT NULL NULL,
  [end_age] int DEFAULT NULL NULL,
  [status] int DEFAULT ((1)) NOT NULL
)
GO


-- ----------------------------
-- Primary Key structure for table tpa_age_range
-- ----------------------------
ALTER TABLE [dbo].[tpa_age_range] ADD CONSTRAINT [PK__tpa_age___3213E83F4F4192A7] PRIMARY KEY CLUSTERED ([id])
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
GO

