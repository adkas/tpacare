/*
 Navicat Premium Data Transfer

 Source Server         : SQL Server
 Source Server Type    : SQL Server
 Source Server Version : 14001000
 Source Host           : localhost\SQLEXPRESS:1433
 Source Catalog        : tpacare
 Source Schema         : dbo

 Target Server Type    : SQL Server
 Target Server Version : 14001000
 File Encoding         : 65001

 Date: 10/05/2020 19:49:48
*/


-- ----------------------------
-- Table structure for cb_role_privileges
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID(N'[dbo].[cb_role_privileges]') AND type IN ('U'))
	DROP TABLE [dbo].[cb_role_privileges]
GO

CREATE TABLE [dbo].[cb_role_privileges] (
  [id] int  IDENTITY(1,1) NOT NULL,
  [cb_roles_id] int DEFAULT NULL NULL,
  [cb_menus_id] int DEFAULT NULL NULL,
  [can_browse] int DEFAULT ((1)) NOT NULL,
  [can_create] int DEFAULT ((1)) NOT NULL,
  [can_read] int DEFAULT ((1)) NOT NULL,
  [can_update] int DEFAULT ((1)) NOT NULL,
  [can_delete] int DEFAULT ((1)) NOT NULL
)
GO

ALTER TABLE [dbo].[cb_role_privileges] SET (LOCK_ESCALATION = TABLE)
GO


-- ----------------------------
-- Records of cb_role_privileges
-- ----------------------------
SET IDENTITY_INSERT [dbo].[cb_role_privileges] ON
GO

INSERT INTO [dbo].[cb_role_privileges] ([id], [cb_roles_id], [cb_menus_id], [can_browse], [can_create], [can_read], [can_update], [can_delete]) VALUES (N'2', N'1', N'2', N'1', N'1', N'1', N'1', N'1')
GO

INSERT INTO [dbo].[cb_role_privileges] ([id], [cb_roles_id], [cb_menus_id], [can_browse], [can_create], [can_read], [can_update], [can_delete]) VALUES (N'3', N'1', N'3', N'1', N'1', N'1', N'1', N'1')
GO

INSERT INTO [dbo].[cb_role_privileges] ([id], [cb_roles_id], [cb_menus_id], [can_browse], [can_create], [can_read], [can_update], [can_delete]) VALUES (N'4', N'1', N'4', N'1', N'1', N'1', N'1', N'1')
GO

INSERT INTO [dbo].[cb_role_privileges] ([id], [cb_roles_id], [cb_menus_id], [can_browse], [can_create], [can_read], [can_update], [can_delete]) VALUES (N'5', N'1', N'5', N'1', N'1', N'1', N'1', N'1')
GO

INSERT INTO [dbo].[cb_role_privileges] ([id], [cb_roles_id], [cb_menus_id], [can_browse], [can_create], [can_read], [can_update], [can_delete]) VALUES (N'6', N'1', N'6', N'1', N'1', N'1', N'1', N'1')
GO

INSERT INTO [dbo].[cb_role_privileges] ([id], [cb_roles_id], [cb_menus_id], [can_browse], [can_create], [can_read], [can_update], [can_delete]) VALUES (N'7', N'2', N'5', N'0', N'0', N'0', N'0', N'0')
GO

INSERT INTO [dbo].[cb_role_privileges] ([id], [cb_roles_id], [cb_menus_id], [can_browse], [can_create], [can_read], [can_update], [can_delete]) VALUES (N'8', N'2', N'3', N'0', N'0', N'0', N'0', N'0')
GO

INSERT INTO [dbo].[cb_role_privileges] ([id], [cb_roles_id], [cb_menus_id], [can_browse], [can_create], [can_read], [can_update], [can_delete]) VALUES (N'10', N'2', N'4', N'0', N'0', N'0', N'0', N'0')
GO

INSERT INTO [dbo].[cb_role_privileges] ([id], [cb_roles_id], [cb_menus_id], [can_browse], [can_create], [can_read], [can_update], [can_delete]) VALUES (N'11', N'2', N'6', N'0', N'0', N'0', N'0', N'0')
GO

INSERT INTO [dbo].[cb_role_privileges] ([id], [cb_roles_id], [cb_menus_id], [can_browse], [can_create], [can_read], [can_update], [can_delete]) VALUES (N'12', N'3', N'5', N'0', N'0', N'0', N'0', N'0')
GO

INSERT INTO [dbo].[cb_role_privileges] ([id], [cb_roles_id], [cb_menus_id], [can_browse], [can_create], [can_read], [can_update], [can_delete]) VALUES (N'13', N'3', N'3', N'0', N'0', N'0', N'0', N'0')
GO

INSERT INTO [dbo].[cb_role_privileges] ([id], [cb_roles_id], [cb_menus_id], [can_browse], [can_create], [can_read], [can_update], [can_delete]) VALUES (N'15', N'3', N'4', N'0', N'0', N'0', N'0', N'0')
GO

INSERT INTO [dbo].[cb_role_privileges] ([id], [cb_roles_id], [cb_menus_id], [can_browse], [can_create], [can_read], [can_update], [can_delete]) VALUES (N'16', N'3', N'6', N'0', N'0', N'0', N'0', N'0')
GO

SET IDENTITY_INSERT [dbo].[cb_role_privileges] OFF
GO


-- ----------------------------
-- Primary Key structure for table cb_role_privileges
-- ----------------------------
ALTER TABLE [dbo].[cb_role_privileges] ADD CONSTRAINT [cb_role_privileges_PK] PRIMARY KEY CLUSTERED ([id])
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)  
ON [PRIMARY]
GO

