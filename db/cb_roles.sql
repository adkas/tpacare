/*
 Navicat Premium Data Transfer

 Source Server         : SQL Server
 Source Server Type    : SQL Server
 Source Server Version : 14001000
 Source Host           : localhost\SQLEXPRESS:1433
 Source Catalog        : tpacare
 Source Schema         : dbo

 Target Server Type    : SQL Server
 Target Server Version : 14001000
 File Encoding         : 65001

 Date: 10/05/2020 19:49:19
*/


-- ----------------------------
-- Table structure for cb_roles
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID(N'[dbo].[cb_roles]') AND type IN ('U'))
	DROP TABLE [dbo].[cb_roles]
GO

CREATE TABLE [dbo].[cb_roles] (
  [id] int  IDENTITY(1,1) NOT NULL,
  [name] nvarchar(255) COLLATE Thai_CI_AI  NOT NULL
)
GO

ALTER TABLE [dbo].[cb_roles] SET (LOCK_ESCALATION = TABLE)
GO


-- ----------------------------
-- Records of cb_roles
-- ----------------------------
SET IDENTITY_INSERT [dbo].[cb_roles] ON
GO

INSERT INTO [dbo].[cb_roles] ([id], [name]) VALUES (N'1', N'Super Admin')
GO

INSERT INTO [dbo].[cb_roles] ([id], [name]) VALUES (N'2', N'Admin Claim')
GO

INSERT INTO [dbo].[cb_roles] ([id], [name]) VALUES (N'3', N'Admin ThaiRe')
GO

SET IDENTITY_INSERT [dbo].[cb_roles] OFF
GO


-- ----------------------------
-- Primary Key structure for table cb_roles
-- ----------------------------
ALTER TABLE [dbo].[cb_roles] ADD CONSTRAINT [cb_roles_PK] PRIMARY KEY CLUSTERED ([id])
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)  
ON [PRIMARY]
GO

