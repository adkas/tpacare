

-- ----------------------------
-- Table structure for tpa_symptom
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID(N'[dbo].[tpa_symptom]') AND type IN ('U'))
	DROP TABLE [dbo].[tpa_symptom]
GO

CREATE TABLE [dbo].[tpa_symptom] (
  [id] int  IDENTITY(1,1) NOT NULL,
  [name_th] nvarchar(150) COLLATE Thai_CI_AI  NOT NULL,
  [name_en] nvarchar(150) COLLATE Thai_CI_AI  NOT NULL,
  [status] int DEFAULT 1 NOT NULL,
);
GO

-- ----------------------------
-- Primary Key structure for table tpa_promotions
-- ----------------------------
ALTER TABLE [dbo].[tpa_symptom] ADD CONSTRAINT [PK__tpa_symptom] PRIMARY KEY CLUSTERED ([id])
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
GO