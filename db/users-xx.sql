/*
 Navicat Premium Data Transfer

 Source Server         : TPA Care
 Source Server Type    : SQL Server
 Source Server Version : 12002000
 Source Host           : tpacare-v2-db.database.windows.net:1433
 Source Catalog        : tpacare-v2-dev
 Source Schema         : dbo

 Target Server Type    : SQL Server
 Target Server Version : 12002000
 File Encoding         : 65001

 Date: 10/05/2020 23:46:16
*/


-- ----------------------------
-- Table structure for users
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID(N'[dbo].[users]') AND type IN ('U'))
	DROP TABLE [dbo].[users]
GO

CREATE TABLE [dbo].[users] (
  [id] int  IDENTITY(1,1) NOT NULL,
  [name] nvarchar(250) COLLATE Thai_CI_AI  NOT NULL,
  [surname] nvarchar(250) COLLATE Thai_CI_AI  NOT NULL,
  [email] varchar(150) COLLATE Thai_CI_AI  NULL,
  [email_verified_at] datetime  NULL,
  [mobile] varchar(150) COLLATE Thai_CI_AI  NOT NULL,
  [channel] tinyint DEFAULT ((1)) NOT NULL,
  [username] varchar(150) COLLATE Thai_CI_AI  NULL,
  [password] varchar(255) COLLATE Thai_CI_AI  NOT NULL,
  [pin] varchar(32) COLLATE Thai_CI_AI  NULL,
  [access_token] varchar(250) COLLATE Thai_CI_AI  NULL,
  [remember_token] varchar(250) COLLATE Thai_CI_AI  NULL,
  [photo] varchar(150) COLLATE Thai_CI_AI  NULL,
  [cb_roles_id] int DEFAULT ((0)) NOT NULL,
  [status] int DEFAULT ((0)) NOT NULL,
  [ip_address] varchar(30) COLLATE Thai_CI_AI  NULL,
  [user_agent] varchar(250) COLLATE Thai_CI_AI NULL,
  [created_at] datetime  NULL,
  [updated_at] datetime  NULL,
  [login_at] datetime  NULL
)
GO


-- ----------------------------
-- Records of users
-- ----------------------------
SET IDENTITY_INSERT [dbo].[users] ON
GO

INSERT INTO [dbo].[users] ([id], [name], [surname], [email], [email_verified_at], [mobile], [channel], [username], [password], [pin], [access_token], [remember_token], [photo], [cb_roles_id], [status], [created_at], [updated_at], [login_at]) VALUES (N'1', N'Sakda Srijumpa', N'', N'sakda@gmail.com', NULL, N'', N'1', N'sakda@gmail.com', N'$2y$10$rKf8aBT9jdWlF5KyqVYZXeHZu1igIdR9Ki0RY1C6jAx8dgW1E7Are', NULL, N'tpacare2020', NULL, NULL, N'1', N'1', N'2020-05-10 11:49:37.000', NULL, NULL)
GO

INSERT INTO [dbo].[users] ([id], [name], [surname], [email], [email_verified_at], [mobile], [channel], [username], [password], [pin], [access_token], [remember_token], [photo], [cb_roles_id], [status], [created_at], [updated_at], [login_at]) VALUES (N'2', N'Sakda', N'Srijumpa', N'sakda', NULL, N'0891114455', N'2', N'sakda', N'$2y$10$zN3GgfXH64bozdKelL.QRez3A3Fb3zhBcYvHBX58OpwP/JachYx6C', N'e10adc3949ba59abbe56e057f20f883e', N'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ1aWQiOjIsInVkaWQiOjIsImlhdCI6MTU4OTEyMTY3MCwiZXhwIjoxNTg5MjA4MDcwfQ.Zux8Jd74efCtJ_Wx_mm6I2wXEcphO9HcKNR36FwclFI', N'', NULL, N'0', N'1', N'2020-05-10 21:33:25.000', N'2020-05-10 21:39:59.000', N'2020-05-10 21:41:10.000')
GO

SET IDENTITY_INSERT [dbo].[users] OFF
GO


-- ----------------------------
-- Triggers structure for table users
-- ----------------------------
CREATE TRIGGER [dbo].[trg_users_before_insert]
ON [dbo].[users]
WITH EXECUTE AS CALLER
FOR INSERT
AS
BEGIN
      UPDATE users
      SET email = inserted.username
      FROM inserted
      WHERE users.id = inserted.id AND users.email IS NULL;

      UPDATE users
      SET username = inserted.email
      FROM inserted
      WHERE users.id = inserted.id AND users.username IS NULL;
    END
GO


-- ----------------------------
-- Primary Key structure for table users
-- ----------------------------
ALTER TABLE [dbo].[users] ADD CONSTRAINT [users_PK] PRIMARY KEY CLUSTERED ([id])
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
GO

